
PhpMumbleAdmin - NG
===================

### Setup environment for dev
    # Enter into the directory
    cd docker/

    # Run the containers with docker-compose, it will build DockerFiles
    docker-compose up -d

    # It also require a database to work.
    # Feel free to run the database service in the docker-compose.extra.yml
    # or use any other server.
    docker-compose -f docker-compose.extra.yml up -d

### Enter into the docker containers
    docker exec -it http-pma bash
    docker exec -it nodejs-pma sh
    docker exec -it murmur sh

### Setup HTTP container
    su pma
    cd phpMumbleAdmin
    composer install

    # Setup the database
    ./bin/console doctrine:database:create
    ./bin/console doctrine:schema:create

    # Setup the database for tests
    ./bin/console --env=test doctrine:database:create
    ./bin/console --env=test doctrine:schema:create

### Setup NODE.JS container
    # install node modules
    npm install

    # Setup assets
    gulp build (or gulp rebuild)

### Run test suites (with codeception)
    # http-pma container

    ./vendor/bin/codecept run unit
    ./vendor/bin/codecept run functional

    # End-to-end suite require Selenium, uncomment the docker-compose service if needed
    ./vendor/bin/codecept run acceptance

### Enable gulp watcher tasks on assets (scss / js / vuejs / images)
    # nodejs-pma container

    gulp

### Create a SuperAdmin

    SuperAdmin is the primary user role.
    With this, you can controle everything with PhpMumbleAdmin.
    First, create a new SuperAdmin with the console command

    > bin/console app:super-admin:create LoginName password

    Then, you can authenticate with your new user

### Menus - extra options

    # Add default parameters to the translator

    $rootMenu->setExtra('translation_params', []);
    $rootMenu->setExtra('translation_domain', 'side-nav');

    # Add custom parameters for a sub menu to the translator

    $subMenu->setExtra('translation_params', []);
    $subMenu->setExtra('translation_domain', 'side-nav-custom');

    # Add an icon

    $subMenu->setExtra('icon', 'fas fa-tachometer-alt');
