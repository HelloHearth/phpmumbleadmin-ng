<?php

/**
 * Custom bootstrap parameters
 */

if ('prod' !== $_SERVER['APP_ENV']) {
    require_once __DIR__.'/ini_set.php';
}

/**
 * Set session cookie path
 *
 * I didn't find a nice way to do it with framework config files, because there
 * is no way to get the $_SERVER['BASE'] parameter.
 */
session_set_cookie_params(0, $_SERVER['BASE']);

require_once __DIR__.'/../../src/Infrastructure/Murmur/.bootstrap.php';
