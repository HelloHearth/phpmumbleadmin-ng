
/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

let gulp = require('gulp');
let gutil = require('gulp-util');
let del = require('del');
let browserSync = require('browser-sync').create();
let preservetime = require('gulp-preservetime');
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let uglifyjs = require('uglify-es');
let composer = require('gulp-uglify/composer');
let jsMinifyier = composer(uglifyjs, console);
let rename = require("gulp-rename");
let concat = require('gulp-concat');

let SRC = './assets';
let DEST = './public/assets';

/*
* TASKS
*/

gulp.task('clean', function () {
    return del([DEST]);
});

// vendor
gulp.task('vendor', function() {
    gulp.src([SRC+'/vendor/*/*.min.*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/*/css/*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/*/{img,images}/*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/*/locales/*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/*/fonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/vue/*'])
        .pipe(gulp.dest(DEST+'/vendor/vue'))
        .pipe(preservetime());
    return gulp.src([SRC+'/vendor/*/webfonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
});

// css
function compileScss() {
    return gulp.src(SRC+'/scss/**/*.scss')
        .pipe(sass.sync({outputStyle:'compressed'}).on('error', sass.logError))
        .pipe(cleanCSS({inline: 'none'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST+'/css'))
        .pipe(browserSync.stream());
}

gulp.task('css', gulp.series(compileScss));

// js
function compileJs() {
    return gulp.src(SRC+'/js/*.js')
        .pipe(jsMinifyier().on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }))
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest(DEST+'/js/'));
}

gulp.task('js', gulp.series(compileJs));

// vue
function compileVue() {
    return gulp.src([SRC+'/vue/App/**/*.vue.js'])
        .pipe(jsMinifyier().on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }))
        .pipe(gulp.dest(DEST+'/vue/'));
}

function compileVueMixin() {
    return gulp.src([SRC+'/vue/Mixin/*.vue.js'])
        .pipe(jsMinifyier().on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }))
        .pipe(concat('mixin.vue.min.js'))
        .pipe(gulp.dest(DEST+'/vue/'));
}

gulp.task('vue', gulp.parallel(compileVue, compileVueMixin));

// img
function copyImages() {
    return gulp.src(SRC+'/img/**/*')
        .pipe(gulp.dest(DEST+'/img/'))
        .pipe(preservetime());
}

gulp.task('img', gulp.series(copyImages));

function watchFiles() {
    gulp.watch(SRC+'/scss/**/*.scss', gulp.series('css'));
    gulp.watch(SRC+'/js/*.js', gulp.series('js'));
    gulp.watch([SRC+'/vue/**/*.vue.js'], gulp.series('vue'));
}

gulp.task('default', gulp.parallel(watchFiles));
gulp.task('build',  gulp.series('vendor', 'css', 'js', 'img', 'vue'));
gulp.task('rebuild', gulp.series('clean', 'build'));
