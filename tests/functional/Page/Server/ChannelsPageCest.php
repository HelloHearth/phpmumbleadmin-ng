<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Page\Server;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelsPageCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
        $I->haveServerNameOnTheTestServer('test servername for channels page');
    }

    public function it_display_the_channels_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeTheServerName('test servername for channels page');
        $I->seeTheConnectionToMumbleUrl();
    }
}
