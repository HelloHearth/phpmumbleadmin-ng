<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Page\Dashboard;

use App\Tests\FunctionalTester;
use App\Tests\Page\CommonPage;
use App\Tests\Page\DashboardPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForceServerToStopCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
        $I->haveOneMumbleClient();
        $I->amLoggedAsSuperAdmin();
    }

    public function it_cancel_the_action_with_the_submit_button(FunctionalTester $I): void
    {
        // Given
        $I->amOnPage(DashboardPage::LOCATION);
        $I->seeElement(DashboardPage::TABLE_ID.' tbody tr i.fa-power-off.online');
        $I->see('1 / 100', DashboardPage::TABLE_ID.' tbody tr td');

        // When
        $I->click('//table//a[contains(@href,"'.DashboardPage::STOP_SERVER_CMD.'1")]');

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeCurrentUrlMatches('#'.CommonPage::LOCATION_CONFIRM_SERVER_TO_STOP.'1$#');

        // When
        $I->click('cancel', 'form[name="force_server_to_stop"] .card-footer');

        // Then
        $I->seeCurrentUrlMatches('#'.DashboardPage::LOCATION.'$#');
        $I->dontSeeSuccessFlashMessage('server_stopped_by_force');
        $I->seeElement(DashboardPage::TABLE_ID.' tbody tr i.fa-power-off.online');
        $I->see('1 / 100', DashboardPage::TABLE_ID.' tbody tr td');
    }

    public function it_stop_a_server_with_a_mumble_user_connected_from_the_dashboard(FunctionalTester $I): void
    {
        // Given
        $I->amOnPage(DashboardPage::LOCATION);
        $I->seeElement(DashboardPage::TABLE_ID.' tbody tr i.fa-power-off.online');
        $I->see('1 / 100', DashboardPage::TABLE_ID.' tbody tr td');

        // When
        $I->click('//table//a[contains(@href,"'.DashboardPage::STOP_SERVER_CMD.'1")]');

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeCurrentUrlMatches('#'.CommonPage::LOCATION_CONFIRM_SERVER_TO_STOP.'1$#');

        // Given
        $I->checkOption('force_server_to_stop[kickUsers]');
        $I->fillField('force_server_to_stop[reason]', 'Reason text to see in logs');
        $I->fillField('force_server_to_stop[message]', 'Message text to send to mumble users');
        $I->click('force_server_to_stop[comfirm_to_stop_server]');

        // Then
        $I->seeCurrentUrlMatches('#'.DashboardPage::LOCATION.'$#');
        $I->seeSuccessFlashMessage('server_stopped_by_force');
        $I->seeElement(DashboardPage::TABLE_ID.' tbody tr i.fa-power-off.offline');
    }
}
