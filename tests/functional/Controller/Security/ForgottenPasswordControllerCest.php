<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Security;

use App\Infrastructure\Symfony\Form\ForgottenPasswordType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ForgottenPasswordPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordControllerCest
{
    private const FORM_TYPE = ForgottenPasswordType::BLOCK_PREFIX;

    public function it_send_a_get_request(FunctionalTester $I): void
    {
        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIs(200);
    }

    public function it_send_a_get_request_when_logged(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIs(403);
    }

    public function it_send_a_post_request_without_any_data(FunctionalTester $I): void
    {
        // When
        $I->sendPost(Page::LOCATION);

        // Then
        $I->seeResponseCodeIs(200);
        $I->dontSeeEmailIsSent();
    }

    public function it_send_a_post_request_without_csrf_token(FunctionalTester $I): void
    {
        // Given
        $I->addAdminUserInRepository(['username' => 'testAdmin', 'email' => 'test@test.tld']);

        // When
        $I->sendPost(Page::LOCATION, [
            self::FORM_TYPE => ['username' => 'testAdmin']
        ]);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeInSource('The CSRF token is invalid. Please try to resubmit the form');
        $I->dontSeeEmailIsSent();
    }

    public function it_send_a_post_request_with_valid_data(FunctionalTester $I): void
    {
        // given I have an admin with an associated email on the repository
        $I->addAdminUserInRepository(['username' => 'testAdmin', 'email' => 'test@test.tld']);

        // When I submit the form
        $I->sendPostWithCsrf(Page::LOCATION, self::FORM_TYPE, [
            'username' => 'testAdmin'
        ]);

        // Then I see the forgotten password email has been sent
        $I->seeResponseCodeIs(200);
        $I->see('email_has_been_sent');
        $I->seeEmailIsSent();
    }
}
