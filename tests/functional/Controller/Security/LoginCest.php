<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Security;

use App\Infrastructure\Symfony\Form\LoginType;
use App\Tests\FunctionalTester;
use App\Tests\Page\AdminsPage;
use App\Tests\Page\DashboardPage;
use App\Tests\Page\LoginPage;
use App\Tests\Page\ServerChannelsPage;
use App\Tests\Page\ServerPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LoginCest
{
    public function it_send_get_request_to_the_login_page_when_logged(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(LoginPage::LOCATION);

        // Then
        $I->seeResponseCodeIs(403);
    }

    public function it_send_a_post_request_without_csrf_token(FunctionalTester $I): void
    {
        // Given
        $I->addAdminUserInRepository(['username' => 'testAdmin']);

        // When
        $I->sendPost(LoginPage::LOCATION, [
            LoginType::BLOCK_PREFIX => ['username' => 'testAdmin', 'password' => 'test'],
        ]);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeInSource('Invalid CSRF token');
    }

    public function it_get_invalid_credentials_message_on_user_not_found_error(FunctionalTester $I): void
    {
        // Given
        $I->amOnPage(LoginPage::LOCATION);

        // When I log in with an invalid user
        $I->logIn('invalid username', 'invalid password');

        // Then
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
        $I->see('Invalid credentials.');
    }

    public function it_get_invalid_credentials_message_on_password_error(FunctionalTester $I): void
    {
        // Given
        $I->amOnPage(LoginPage::LOCATION);

        // When I log in with an invalid password
        $I->logIn(LoginPage::SUPER_ADMIN_USERNAME, 'invalid password');

        // Then
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
        $I->see('Invalid credentials.');
    }

    public function it_refill_the_last_username_and_focus_the_password_field_on_login_error(FunctionalTester $I): void
    {
        // Given I'm on the login page, I see I have the focus on the username field
        $I->amOnPage(LoginPage::LOCATION);
        $I->seeElement(LoginPage::USERNAME_FIELD.'[autofocus="autofocus"]');
        $I->dontSeeElement(LoginPage::PASSWORD_FIELD.'[autofocus="autofocus"]');

        // When I log in with an invalid user
        $I->logIn('invalid username', 'invalid password');

        // Then I see I have the focus on the password field
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
        $I->dontSeeElement(LoginPage::USERNAME_FIELD.'[autofocus="autofocus"]');
        $I->seeElement(LoginPage::PASSWORD_FIELD.'[autofocus="autofocus"]');
    }

    public function it_checks_remember_me_feature_integration(FunctionalTester $I): void
    {
        // Given I log in as SuperAdmin
        $I->amOnPage(LoginPage::LOCATION);
        $I->checkOption(LoginPage::REMEMBER_ME_FIELD);
        $I->logIn(LoginPage::SUPER_ADMIN_USERNAME, LoginPage::SUPER_ADMIN_PASSWORD);
        $I->seeAmLoggedAsSuperAdmin();

        // When I delete the session cookie and reload the Dashboard page
        $I->seeCookie('MOCKSESSID');
        $I->resetCookie('MOCKSESSID');
        $I->amOnPage(DashboardPage::LOCATION);

        // Then I see I'm still authenticated
        $I->seeAmLoggedAsSuperAdmin();
    }

    public function it_get_invalid_credentials_message_on_mumble_user_not_found_error(FunctionalTester $I): void
    {
        // Given
        $I->resetAllServers();
        $I->amOnPage(LoginPage::LOCATION);

        // When I log in with an invalid user
        $I->logIn('invalid mumble username', 'invalid password', ServerPage::TEST_SERVER_ID);

        // Then
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
        $I->see('Invalid credentials.');
    }

    public function it_get_invalid_credentials_message_on_mumble_user_password_error(FunctionalTester $I): void
    {
        // Given
        $I->resetAllServers();
        $I->haveOneMumbleRegistrationOnTheTestServer(['test_mumbleUser', '', '', '', 'test_password']);
        $I->amOnPage(LoginPage::LOCATION);

        // When I log in with an invalid password
        $I->logIn('test_mumbleUser', 'invalid password', ServerPage::TEST_SERVER_ID);

        // Then
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
        $I->see('Invalid credentials.');
    }

    public function it_log_in_with_mumble_user(FunctionalTester $I): void
    {
        // Given I have a mumble user
        $I->resetAllServers();
        $I->haveOneMumbleRegistrationOnTheTestServer(['test_mumbleUser', '', '', '', 'test_password']);
        $I->amOnPage(LoginPage::LOCATION);

        // When I log in with the mumble user
        $I->logIn('test_mumbleUser', 'test_password', ServerPage::TEST_SERVER_ID);

        // Then I see I'm logged as the mumble user, and I'm the viewer page
        $I->seeCurrentUrlMatches('#'.ServerChannelsPage::LOCATION.'$#');
        $I->seeAmLoggedAsMumbleUser('test_mumbleUser');
        $I->seeTheServerMenuForMumbleUser();
        $I->seeTheConnectionToMumbleUrl();
    }

    public function it_checks_remember_me_feature_integration_for_mumble_user(FunctionalTester $I): void
    {
        // Given I'm logged as mumble user
        $I->resetAllServers();
        $I->haveOneMumbleRegistrationOnTheTestServer(['test_mumbleUser', '', '', '', 'test_password']);
        $I->amOnPage(LoginPage::LOCATION);
        $I->checkOption(LoginPage::REMEMBER_ME_FIELD);
        $I->logIn('test_mumbleUser', 'test_password', ServerPage::TEST_SERVER_ID);
        $I->seeAmLoggedAsMumbleUser('test_mumbleUser');

        // When I delete the session cookie and reload the server page
        $I->seeCookie('MOCKSESSID');
        $I->resetCookie('MOCKSESSID');
        $I->amOnPage(ServerChannelsPage::LOCATION);

        // Then I see I'm still authenticated
        $I->seeCurrentUrlMatches('#'.ServerPage::LOCATION.'$#');
        $I->seeAmLoggedAsMumbleUser('test_mumbleUser');
    }

    public function it_redirect_users_to_its_default_page_if_the_target_path_is_on_unauthorized_route(FunctionalTester $I): void
    {
        // Given I get a referer to administration page
        $I->resetAllServers();
        $I->haveOneMumbleRegistrationOnTheTestServer(['test_mumbleUser', '', '', '', 'test_password']);
        $I->amOnPage(AdminsPage::LOCATION);

        // When I log in with a mumble user
        $I->logIn('test_mumbleUser', 'test_password', ServerPage::TEST_SERVER_ID);

        // Then I see I'm on the server page
        $I->seeCurrentUrlMatches('#'.ServerPage::LOCATION.'$#');
        $I->seeAmLoggedAsMumbleUser('test_mumbleUser');
    }
}
