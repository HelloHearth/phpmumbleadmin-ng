<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Registrations;

use App\Domain\Action\Server\Registrations\Registration\AccessToRegistrationIsNotAllowed;
use App\Domain\Action\Server\Registrations\Registration\RegistrationDoNotExist;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerRegistrationPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditRegistrationControllerCest
{
    public function it_send_a_request_to_the_edit_registration_page(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION_EDIT.'/0');

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement(Page::CONTAINER. ' .form-container');
        $I->dontSee('error', Page::CONTAINER);
    }

    public function it_send_a_request_to_the_registration_page_when_I_am_logged_as_superuser_ru(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperUserRu();

        // When
        $I->amOnPage(Page::LOCATION_EDIT.'/0');

        // Then
        $I->seeResponseCodeIs(403);
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'/0'.'$#');
        $I->dontSeeElement(Page::CONTAINER. ' .form-container');
        $I->seeTheCenteredErrorMessage(AccessToRegistrationIsNotAllowed::KEY);
    }

    public function it_send_a_request_to_the_registration_page_for_unexistant_registration(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION_EDIT.'/666');

        // Then
        $I->seeResponseCodeIs(404);
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'/666$#');
        $I->dontSeeElement(Page::CONTAINER. ' .form-container');
        $I->seeTheCenteredErrorMessage(RegistrationDoNotExist::KEY);
    }

    // Checking that the controller transform errors events into form errors
    public function it_submit_the_edit_registration_form_with_invalid_data(FunctionalTester $I): void
    {
        // Given
        $prx = $I->getTheTestServer();
        $prx->setConf('allowhtml', 'false');
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION_EDIT.'/0');
        $I->submitForm(
            'form[name="registration"]', [
                'registration' => [
                    'username' => 'invalid space characters',
                    'description' => 'invalid <strong>HTML</strong> element for server without HTML enabled'
                ]
            ]
        );

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement(Page::CONTAINER. ' .form-container');
        $I->seeFormError('invalid_character_for_username');
        $I->seeFormError('server_dont_allow_html_tags');
    }
}
