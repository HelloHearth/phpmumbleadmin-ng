<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Registrations;

use App\Domain\Action\Server\Registrations\Registration\AccessToRegistrationIsNotAllowed;
use App\Domain\Action\Server\Registrations\Registration\RegistrationDoNotExist;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerRegistrationPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationControllerCest
{
    public function it_send_a_request_to_the_registration_page(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION.'/0');

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement(Page::CONTAINER);
    }

    public function it_send_a_request_to_the_registration_page_when_logged_as_superuser_ru(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperUserRu();

        // When
        $I->amOnPage(Page::LOCATION.'/0');

        // Then
        $I->seeResponseCodeIs(403);
        $I->seeTheCenteredErrorMessage(AccessToRegistrationIsNotAllowed::KEY);
    }

    public function it_send_a_request_to_the_registration_page_for_unexistant_registration(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION.'/666');

        // Then
        $I->seeResponseCodeIs(404);
        $I->seeTheCenteredErrorMessage(RegistrationDoNotExist::KEY);
    }
}
