<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Action\Server\Channels\EditChannel\ChannelEdited;
use App\Infrastructure\Symfony\Form\EditChannelType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditChannelControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/edit_channel';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_post_request_to_the_controller(FunctionalTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        $I->sendPost(self::CONTROLLER_URL);

        $I->seeResponseCodeIsClientError();
    }

    public function it_send_an_ajax_post_request_with_invalid_form(FunctionalTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        $I->sendAjaxPostRequest(self::CONTROLLER_URL, ['form' => []]);

        $I->seeResponseCodeIsClientError();
    }

    public function it_send_an_ajax_post_request_with_valid_form(FunctionalTester $I): void
    {
        $I->amLoggedAsSuperAdmin();

        $I->sendPostWithCsrf(self::CONTROLLER_URL, EditChannelType::BLOCK_PREFIX, [
            'parentId' => 0,
            'name' => 'test channel name',
            'description' => 'test channel description',
            'position' => 12,
            'password' => 'test channel password',
        ]);

        $I->seeResponseCodeIsSuccessful();
        $I->seeResponseEquals(ChannelEdited::KEY);
    }

    public function it_send_an_ajax_post_request_with_invalid_channel_id(FunctionalTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        $I->sendAjaxPostRequest(self::CONTROLLER_URL, [
            'form' => [
                'channelId' => 999999999999999999999999999999,
                'name' => 'test channel name',
                'description' => 'test channel description',
                'position' => 'test channel position',
                'password' => 'test channel password'
            ]
        ]);

        $I->seeResponseCodeIsClientError();
    }
}
