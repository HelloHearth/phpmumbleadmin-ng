<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Action\Server\Channels\CreateChannel\ParentChannelIsInvalid;
use App\Infrastructure\Symfony\Form\CreateChannelType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateChannelControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/create_channel';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_request_to_create_a_channel(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send form data with valid values
        $I->sendPostWithCsrf(self::CONTROLLER_URL, CreateChannelType::BLOCK_PREFIX, [
            'name' => 'NewChannel',
            'parentId' => 0
        ]);

        // Then
        $I->seeResponseCodeIs(200);
    }

    public function it_send_a_request_to_create_a_channel_without_form_parameters(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send empty form data
        $I->sendPost(self::CONTROLLER_URL, [
            CreateChannelType::BLOCK_PREFIX => []
        ]);

        // Then I see I get the CSRF error message
        $I->seeResponseCodeIsFormError();
        $I->seeInSource('The CSRF token is invalid. Please try to resubmit the form.');
    }

    public function it_send_a_request_with_invalid_form_parameter(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send a invalid parent id value
        $I->sendPostWithCsrf(self::CONTROLLER_URL, CreateChannelType::BLOCK_PREFIX, [
            'name' => 'NewChannel',
            'parentId' => 87987987989
        ]);

        // Then I see I get a form error with the event message
        $I->seeResponseCodeIsFormError();
        $I->seeInSource(ParentChannelIsInvalid::KEY);
    }
}
