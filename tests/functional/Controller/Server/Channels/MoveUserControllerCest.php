<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Action\Server\Channels\MoveUser\UserAlreadyInChannel;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MoveUserControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/move_user';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
        $I->haveOneMumbleClient();
    }

    public function it_send_a_post_xml_request_with_missing_form_fields(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxPostRequest(self::CONTROLLER_URL);

        // Then
        $I->seeResponseCodeIsFormError();
        $I->seeResponseIsJson();
    }

    public function it_send_a_post_xml_request_to_move_the_user_in_its_channel(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxPostRequest(self::CONTROLLER_URL, [
            'form' => [
                'userSession' => $I->getUserSession(),
                'toChannelId' => 0,
            ],
        ]);

        // Then
        $I->seeResponseCodeIs(403);
        $I->seeResponseContainsJson([UserAlreadyInChannel::KEY]);
    }
}
