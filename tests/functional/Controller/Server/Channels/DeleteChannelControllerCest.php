<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Action\Server\Channels\DeleteChannel\DeleteRootChannelIsForbidden;
use App\Infrastructure\Symfony\Form\DeleteChannelType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteChannelControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/delete_channel';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_request_to_delete_a_channel_without_form_parameters(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send empty form data
        $I->sendPost(self::CONTROLLER_URL, [
            DeleteChannelType::BLOCK_PREFIX => [],
        ]);

        // Then I see I get the CSRF error message
        $I->seeResponseCodeIsFormError();
        $I->seeInSource('The CSRF token is invalid. Please try to resubmit the form.');
    }

    public function it_send_a_request_to_delete_root_channel(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendPostWithCsrf(self::CONTROLLER_URL, DeleteChannelType::BLOCK_PREFIX, [
            'channelId' => 0,
        ]);

        // Then
        $I->seeResponseCodeIs(403);
        $I->seeInSource(DeleteRootChannelIsForbidden::KEY);
    }

    public function it_send_a_request_to_delete_a_channel_with_form_parameters(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $server = $I->getTheTestServer();
        $channelId = $server->addChannel('delete_the_channel', 0);

        // When
        $I->sendPostWithCsrf(self::CONTROLLER_URL, DeleteChannelType::BLOCK_PREFIX, [
            'channelId' => $channelId,
        ]);

        // Then
        $I->seeResponseCodeIs(200);
    }
}
