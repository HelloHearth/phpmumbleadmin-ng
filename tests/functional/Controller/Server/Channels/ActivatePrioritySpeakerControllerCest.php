<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Murmur\Middleware\ServerActionError;
use App\Infrastructure\Symfony\Form\ActivatePrioritySpeakerType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActivatePrioritySpeakerControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/activate_priority_speaker';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_post_request_without_form_fields(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxPostRequest(self::CONTROLLER_URL);

        // Then
        $I->seeResponseCodeIsFormError();
    }

    public function it_send_a_post_request_for_not_connected_user(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendPostWithCsrf(self::CONTROLLER_URL, ActivatePrioritySpeakerType::BLOCK_PREFIX, [
            'userSession' => -1,
        ]);

        // Then
        $I->seeResponseCodeIs(400);
        $I->seeResponseContainsJson([ServerActionError::KEY]);
    }
}
