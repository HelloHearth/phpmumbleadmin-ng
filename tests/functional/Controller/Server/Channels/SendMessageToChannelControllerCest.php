<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Action\Server\Channels\SendMessageToChannel\MessageSentToChannel;
use App\Infrastructure\Symfony\Form\SendMessageToChannelType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToChannelControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/send_message_to_channel';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_request_without_form_fields(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send empty form data
        $I->sendPOST(self::CONTROLLER_URL, [
            SendMessageToChannelType::BLOCK_PREFIX => [],
        ]);

        // Then I see I get the CSRF error message
        $I->seeResponseCodeIsFormError();
        $I->seeInSource('The CSRF token is invalid. Please try to resubmit the form.');
    }

    public function it_send_a_post_request_with_valid_data(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send post for a not connected user
        $I->sendPostWithCsrf(self::CONTROLLER_URL, SendMessageToChannelType::BLOCK_PREFIX, [
            'channelId' => 0,
            'toSubChannels' => true,
            'message' => 'Test message sent with phpMumbleAdmin'
        ]);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeInSource(MessageSentToChannel::KEY);
    }
}
