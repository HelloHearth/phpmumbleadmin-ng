<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Logs;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerLogsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogsQueryControllerCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_test_server_logs_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement('#MumbleLogsContainer');
    }

    public function it_send_a_xml_request_to_the_latest_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxGetRequest(Page::LOCATION.'/latest/1');

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeResponseIsJson();
    }
}
