<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Settings;

use App\Domain\Service\InMemorySystemCheckerService;
use App\Domain\Service\SystemCheckerServiceInterface;
use App\Tests\FunctionalTester;
use App\Tests\Page\CertificatePage;
use App\Tests\Page\ServerSettingsPage as Page;

/**
 * Test the 4 methods to change the certificate of a server
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UpdateCertificateControllerCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->resetAllServers();

        $I->replaceSymfonyService(SystemCheckerServiceInterface::class,
            (new InMemorySystemCheckerService())->with_ssl_extension()->with_upload_file_allowed()
        );
    }

    public function _after(FunctionalTester $I): void
    {
        $I->restoreSymfonyService(SystemCheckerServiceInterface::class);
    }

    public function it_submit_a_new_certificate_with_pem_upload_file_type(FunctionalTester $I): void
    {
        // Given
        $I->grabService(SystemCheckerServiceInterface::class)->with_ssl_extension()->with_upload_file_allowed();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->dontSeeCertificateKeyId();

        // When
        $I->attachFile(Page::FORM_CERTIFICATE_FIELD.'[pem]"]', CertificatePage::VALID_PEM_CODECEPTION_PATH);
        $I->click('Submit', 'form[name="app_settings_page"]');

        // Then
        $I->seeSuccessFlashMessage('certificate_modified_success_restart_needed');
        $I->seeCertificateInformation();

        $I->restoreSymfonyService(SystemCheckerServiceInterface::class);
    }

    public function it_submit_a_new_certificate_with_pem_textarea_type(FunctionalTester $I): void
    {
        // Given
        $I->grabService(SystemCheckerServiceInterface::class)->with_ssl_extension()->without_upload_file_allowed();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->dontSeeCertificateKeyId();

        // When
        $I->fillField(Page::FORM_CERTIFICATE_FIELD.'[pem]"]', CertificatePage::PEM);
        $I->click('Submit', 'form[name="app_settings_page"]');

        // Then
        $I->seeSuccessFlashMessage('certificate_modified_success_restart_needed');
    }

    public function it_submit_a_new_certificate_with_two_upload_files_type(FunctionalTester $I): void
    {
        // Given
        $I->grabService(SystemCheckerServiceInterface::class)->without_ssl_extension()->with_upload_file_allowed();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);

        // When
        $I->attachFile(Page::FORM_CERTIFICATE_FIELD.'[_certificate]"]', CertificatePage::VALID_2F_CERTIFICATE_CODECEPTION_PATH);
        $I->attachFile(Page::FORM_CERTIFICATE_FIELD.'[_key]"]', CertificatePage::VALID_2F_KEY_CODECEPTION_PATH);
        $I->click('Submit', 'form[name="app_settings_page"]');

        // Then
        $I->seeSuccessFlashMessage('certificate_modified_success_restart_needed');
    }

    public function it_submit_a_new_certificate_with_two_textarea_type(FunctionalTester $I): void
    {
        // Given
        $I->grabService(SystemCheckerServiceInterface::class)->without_ssl_extension()->without_upload_file_allowed();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);

        // When
        $I->fillField(Page::FORM_CERTIFICATE_FIELD.'[_certificate]"]', CertificatePage::CERTIFICATE);
        $I->fillField(Page::FORM_CERTIFICATE_FIELD.'[_key]"]', CertificatePage::PRIVATE_KEY);
        $I->click('Submit', 'form[name="app_settings_page"]');

        // Then
        $I->seeSuccessFlashMessage('certificate_modified_success_restart_needed');
    }
}
