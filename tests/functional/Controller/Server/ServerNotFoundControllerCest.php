<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerPage;
use Codeception\Example;

/**
 * Each page of a server part must display a "server not found" message when
 * querying an invalid server id
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerNotFoundControllerCest
{
    private function pagesProvider(): array
    {
        return [
            ['subpage' => '/'],
            ['subpage' => '/settings'],
            ['subpage' => '/registrations'],
            ['subpage' => '/registration/42'],
            ['subpage' => '/registration/edit/42'],
            ['subpage' => '/bans'],
            ['subpage' => '/logs']
        ];
    }

    /** @dataProvider pagesProvider */
    public function it_test_server_not_found_with_server_id(FunctionalTester $I, Example $data): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(ServerPage::PATH.'/666666666'.$data['subpage']);

        // Then
        $I->seeResponseCodeIs(404);
        $I->seeTheCenteredErrorMessage('server_not_found');
        $I->dontSeeTheServerMenu();
    }
}
