<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Administration\Admins;

use App\Domain\Service\SecurityServiceInterface;
use App\Tests\FunctionalTester;
use App\Tests\Page\AdminsPage;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditAdminControllerCest
{
    public function it_request_the_page_and_submit_the_form(FunctionalTester $I): void
    {
        // Given I have an admin and I'm loggued as SuperAdmin
        $admin = $I->addAdminUserInRepository([
            'username' => 'TestEditAdmin',
            'email' => 'test@test.tld',
            'role' => [SecurityServiceInterface::ROLE_ADMIN]],
        );
        $I->amLoggedAsSuperAdmin();

        // When I'm on the page of admins, and click to edit this admin
        $I->amOnPage(AdminsPage::LOCATION);
        $I->click(['link' => $admin->getUserIdentifier()], AdminsPage::TABLE_ELEMENT);

        // Then I see the page with the form to edit the admin
        $I->seeCurrentUrlMatches('#'.AdminsPage::LOCATION_EDIT.'/'.$admin->getId().'$#');
        $I->seeResponseCodeIs(200);
        $I->seeInFormFields(AdminsPage::EDIT_FORM, [
            AdminsPage::EDIT_FORM_PREFIX.'[login]' => 'TestEditAdmin',
            AdminsPage::EDIT_FORM_PREFIX.'[email]' => 'test@test.tld',
            AdminsPage::EDIT_FORM_PREFIX.'[role]' => SecurityServiceInterface::ROLE_ADMIN,
            AdminsPage::EDIT_FORM_PREFIX.'[password][first]' => '',
            AdminsPage::EDIT_FORM_PREFIX.'[password][second]' => '',
        ]);

        // When I submit new data
        $I->submitForm(AdminsPage::EDIT_FORM, [
            AdminsPage::EDIT_FORM_PREFIX.'[login]' => 'edited_username',
            AdminsPage::EDIT_FORM_PREFIX.'[email]' => 'edited@test.tld',
            AdminsPage::EDIT_FORM_PREFIX.'[role]' => SecurityServiceInterface::ROLE_ROOT_ADMIN,
            AdminsPage::EDIT_FORM_PREFIX.'[password][first]' => 'new password',
            AdminsPage::EDIT_FORM_PREFIX.'[password][second]' => 'new password',
        ]);

        // Then I see the form display edited data
        $I->seeCurrentUrlMatches('#'.AdminsPage::LOCATION_EDIT.'/'.$admin->getId().'$#');
        $I->dontSeeFormErrors();
        $I->seeResponseCodeIs(200);
        $I->seeSuccessFlashMessage('admin_edited');
        $I->seeInFormFields(AdminsPage::EDIT_FORM, [
            AdminsPage::EDIT_FORM_PREFIX.'[login]' => 'edited_username',
            AdminsPage::EDIT_FORM_PREFIX.'[email]' => 'edited@test.tld',
            AdminsPage::EDIT_FORM_PREFIX.'[role]' => SecurityServiceInterface::ROLE_ROOT_ADMIN,
        ]);

        // When I'm on the login page
        $I->logout();
        $I->amOnPage(LoginPage::LOCATION);

        // Then I can log in with the admin, and it's new password
        $I->logIn('edited_username', 'new password');
        $I->seeAmLoggedAs('edited_username');
    }

    public function it_move_back_to_admins(FunctionalTester $I): void
    {
        // Given I'm logged as SuperAdmin
        $I->amLoggedAsSuperAdmin();
        $admin = $I->addRootAdminUserInRepository();

        // When I'm on page to edit admin, and click on the back button
        $I->amOnPage(AdminsPage::LOCATION_EDIT.'/'.$admin->getId());
        $I->click(['link' => 'back'], AdminsPage::CONTAINER);

        // Then I see I'm on the admins page
        $I->seeCurrentUrlMatches('#'.AdminsPage::LOCATION.'$#');
        $I->seeResponseCodeIs(200);
    }

    public function it_cant_request_the_page_if_connected_user_is_not_granted(FunctionalTester $I): void
    {
        // Given I'm logged as RootAdmin
        $I->amLoggedAsRootAdmin();
        $admin = $I->addRootAdminUserInRepository();

        // When I'm trying to edit a RootAdmin
        $I->amOnPage(AdminsPage::LOCATION_EDIT.'/'.$admin->getId());

        // Then I see I get page not found error
        $I->seeCurrentUrlMatches('#'.AdminsPage::LOCATION_EDIT.'/'.$admin->getId().'$#');
        $I->seeResponseCodeIs(404);
    }
}
