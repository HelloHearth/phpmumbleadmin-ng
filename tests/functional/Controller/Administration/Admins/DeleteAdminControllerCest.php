<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Administration\Admins;

use App\Tests\FunctionalTester;
use App\Tests\Page\AdminsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteAdminControllerCest
{
    public function it_delete_an_admin(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        // When
        $I->amOnPage(Page::LOCATION_DELETE.'/3');

        // Then
        $I->seeResponseIsRedirectionTo(Page::LOCATION);
    }

    public function it_get_404_error_when_trying_to_delete_a_unexistant_admin(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION_DELETE.'/42');

        // Then
        $I->seeResponseCodeIs(404);
    }

    public function it_get_403_error_when_trying_to_delete_an_admin_without_permission(FunctionalTester $I): void
    {
        // Given I'm logged as RootAdmin
        $I->amLoggedAsRootAdmin();

        // When I try to delete a RootAdmin
        $admin = $I->addRootAdminUserInRepository();
        $I->amOnPage(Page::LOCATION_DELETE.'/'.$admin->getId());

        // Then I get a 403 page
        $I->seeResponseCodeIs(403);
    }
}
