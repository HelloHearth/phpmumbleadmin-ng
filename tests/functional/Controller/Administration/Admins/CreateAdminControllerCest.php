<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Administration\Admins;

use App\Domain\Action\Administration\Admins\CreateAdmin\AdminCreated;
use App\Domain\Service\SecurityServiceInterface;
use App\Tests\FunctionalTester;
use App\Tests\Page\AdminsPage as Page;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateAdminControllerCest
{
    public function it_get_error_when_get_the_create_admin_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION_CREATE);

        // Then
        $I->seeResponseCodeIsClientError();
    }

    public function it_send_post_request_with_empty_data(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendPost(Page::LOCATION_CREATE);

        // Then
        $I->seeResponseCodeIsFormError();
    }

    public function it_send_post_request_with_valid_data_and_log_in_with_the_new_admin(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendPostWithCsrf(Page::LOCATION_CREATE, Page::CREATE_FORM_PREFIX, [
            'login' => 'test login',
            'role' => SecurityServiceInterface::ROLE_ADMIN,
            'password' => [
                'first' => 'test password',
                'second' => 'test password'
            ]
        ]);

        // Then
        $I->seeResponseCodeIs(200);
        $I->see(AdminCreated::KEY);

        // Given
        $I->logout();
        $I->amOnPage(LoginPage::LOCATION);

        // When
        $I->logIn('test login', 'test password');

        // Then
        $I->seeAmLoggedAs('test login');
    }
}
