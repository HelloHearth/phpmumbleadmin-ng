<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Administration\Admins;

use App\Tests\FunctionalTester;
use App\Tests\Page\AdminsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminsControllerCest
{
    public function it_query_the_admins_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeResponseCodeIs(200);
        $I->seeElement(Page::TABLE_ELEMENT);
    }
}
