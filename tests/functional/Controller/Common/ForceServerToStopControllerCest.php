<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Common;

use App\Controller\Common\StopServerController;
use App\Tests\FunctionalTester;
use App\Tests\Page\CommonPage;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForceServerToStopControllerCest
{
    public function _before(FunctionalTester $I): void
    {
        $session = $I->grabService(SessionInterface::class);
        $session->set(StopServerController::REFERER_CONFIRM_KEY, '/anywhere');

        $I->stopFollowingRedirects();
    }

    public function it_send_request_to_the_confirmation_page_to_force_to_stop(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(CommonPage::LOCATION_CONFIRM_SERVER_TO_STOP.'99999999999');

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->see('server_is_not_empty_title');
    }

    public function it_send_request_to_command_controller_without_parameters(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendPOST(CommonPage::LOCATION_FORCE_SERVER_TO_STOP_CMD);

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeResponseContains('Redirecting to /anywhere');
    }
}
