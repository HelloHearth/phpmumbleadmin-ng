<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Common;

use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerNavControllerCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();
    }

    public function it_send_a_request_to_refresh_server_list(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->haveHttpHeader('referer', '/old-route');

        // When
        $I->amOnPage('/cmd/servers_nav/refresh_server_list');

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink('/old-route');
    }

    protected function selectServerProvider(): array
    {
        return [
            'no query' =>
                ['referer' => '/old-route', 'query' => '', 'expected' => '/old-route'],
            'Request from any page' =>
                ['referer' => '/old-route', 'query' => '?select_server_id=42', 'expected' => '/server/42'],
            'Request with non numerical server id' =>
                ['referer' => '/old-route', 'query' => '?select_server_id=non-numerical', 'expected' => '/old-route'],
            'Request from server registrations page' =>
                ['referer' => '/server/1/registrations', 'query' => '?select_server_id=42', 'expected' => '/server/42/registrations']
        ];
    }

    /** @dataProvider selectServerProvider */
    public function it_send_a_request_to_select_a_server(FunctionalTester $I, Example $data): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->haveHttpHeader('referer', $data['referer']);

        // When
        $I->amOnPage('/cmd/servers_nav/select_server'.$data['query']);

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink($data['expected']);
    }
}
