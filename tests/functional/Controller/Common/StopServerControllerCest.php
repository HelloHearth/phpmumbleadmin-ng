<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Common;

use App\Tests\FunctionalTester;
use App\Tests\Page\CommonPage;
use App\Tests\Page\DashboardPage;
use App\Tests\Page\ServerPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class StopServerControllerCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->stopFollowingRedirects();
    }

    public function it_send_a_request_to_stop_a_server(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->haveHttpHeader('referer', '/anywhere');

        // When
        // The serverId is high to avoid to stop one
        $I->amOnPage(DashboardPage::STOP_SERVER_CMD.'99999999999');

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink('/anywhere');
    }

    public function it_send_a_request_to_stop_a_server_with_a_mumble_user_connected(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->haveOneMumbleClient();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(DashboardPage::STOP_SERVER_CMD.ServerPage::TEST_SERVER_ID);

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink(CommonPage::LOCATION_CONFIRM_SERVER_TO_STOP.ServerPage::TEST_SERVER_ID);
    }
}
