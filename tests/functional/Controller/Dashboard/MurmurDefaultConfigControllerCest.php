<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Dashboard;

use App\Domain\Murmur\Connection\ConnectionHandlerInterface;
use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\InMemoryMeta;
use App\Domain\Murmur\Model\MetaInterface;
use App\Tests\FunctionalTester;
use App\Tests\Page\CertificatePage;
use App\Tests\Page\MurmurDefaultConfigPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MurmurDefaultConfigControllerCest
{
    public function it_send_a_request_to_get_the_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeInTable('allowhtml true', Page::TABLE, 1, 1);
        $I->seeInTable('bandwidth 72000', Page::TABLE, 2, 1);
        $I->seeInTable('bonjour false', Page::TABLE, 3, 1);
        $I->seeInTable('certrequired false', Page::TABLE, 4, 1);
        $I->seeInTable('channelname [ \-=\w\#\[\]\{\}\(\)\@\|]+', Page::TABLE, 5, 1);
        $I->seeInTable('channelnestinglimit 10', Page::TABLE, 6, 1);
        $I->seeInTable('defaultchannel 0', Page::TABLE, 7, 1);
        $I->seeInTable('host ::', Page::TABLE, 8, 1);
        $I->seeInTable('obfuscate false', Page::TABLE, 9, 1);
        $I->seeInTable('opusthreshold 100', Page::TABLE, 10, 1);
        $I->seeInTable('password', Page::TABLE, 11, 1);
        $I->seeInTable('port 64738', Page::TABLE, 12, 1);
        $I->seeInTable('registerhostname', Page::TABLE, 13, 1);
        $I->seeInTable('registerlocation', Page::TABLE, 14, 1);
        $I->seeInTable('registername', Page::TABLE, 15, 1);
        $I->seeInTable('registerpassword', Page::TABLE, 16, 1);
        $I->seeInTable('registerurl', Page::TABLE, 17, 1);
        $I->seeInTable('rememberchannel true', Page::TABLE, 18, 1);
        $I->seeInTable('sslCiphers EECDH+AESGCM:AES256-SHA:AES128-SHA', Page::TABLE, 19, 1);
        $I->seeInTable('suggestpositional', Page::TABLE, 20, 1);
        $I->seeInTable('suggestpushtotalk', Page::TABLE, 21, 1);
        $I->seeInTable('suggestversion', Page::TABLE, 22, 1);
        $I->seeInTable('textmessagelength 5000', Page::TABLE, 23, 1);
        $I->seeInTable('timeout 30', Page::TABLE, 24, 1);
        $I->seeInTable('username [-=\w\[\]\{\}\(\)\@\|\.]+', Page::TABLE, 25, 1);
        $I->seeInTable('users 100', Page::TABLE, 26, 1);
        $I->seeInTable('welcometext <br />Welcome to this server running <b>Murmur</b>.<br />Enjoy your stay!<br />', Page::TABLE, 27, 1);
        $I->dontSee('key', Page::TABLE);
        $I->dontSee('certificate', Page::TABLE);
        $I->see('no_certificate', Page::CONTAINER);
        $I->dontSee('key & certificate', Page::CONTAINER);
    }

    public function it_get_the_page_when_there_is_no_murmur_connection(FunctionalTester $I): void
    {
        // Mock the ConnectionHandler to produce a connection error with murmur
        $I->replaceSymfonyService(ConnectionHandlerInterface::class,
            new class implements ConnectionHandlerInterface {
                public function connect(): ?MetaInterface { throw new ConnectionException(); }
            }
        );

        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->see('no_connection_to_murmur_found');
    }

    /**
     * Mumble don't expose SSL secrets over Ice/D-Bus anymore
     * https://github.com/mumble-voip/mumble/commit/028812012451c55f334383fa92bf2534787b43d1
     */
    public function it_get_the_page_when_key_parameter_is_not_exposed(FunctionalTester $I): void
    {
        // Mock the ConnectionHandler to return a meta without private key, and empty certificate
        $connectionHandler = new class implements ConnectionHandlerInterface {
            public function connect(): ?MetaInterface {
                return (new InMemoryMeta())->with_default_conf(['certificate' => '']);
            }
        };
        $I->replaceSymfonyService(ConnectionHandlerInterface::class, $connectionHandler);

        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->dontSee('key', Page::TABLE);
        $I->dontSee('certificate', Page::TABLE);
        $I->see('no_certificate', Page::CONTAINER);
        $I->dontSee('key & certificate', Page::CONTAINER);
    }

    public function it_get_the_page_when_certificate_is_valid(FunctionalTester $I): void
    {
        // Mock the ConnectionHandler to return a meta without private key, and empty certificate
        $connectionHandler = new class implements ConnectionHandlerInterface {
            public function connect(): ?MetaInterface {
                return (new InMemoryMeta())->with_default_conf(['certificate' => CertificatePage::CERTIFICATE]);
            }
        };
        $I->replaceSymfonyService(ConnectionHandlerInterface::class, $connectionHandler);

        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->dontSee('key', Page::TABLE);
        $I->dontSee('certificate', Page::TABLE);
        $I->seeCertificateInformation(Page::CONTAINER);
        $I->see('key & certificate', Page::CONTAINER);
    }

    public function _after(FunctionalTester $I): void
    {
        $I->restoreSymfonyService(ConnectionHandlerInterface::class);
    }
}
