<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Dashboard;

use App\Tests\FunctionalTester;
use App\Tests\Page\DashboardPage as Dashboard;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class WebAccessControllerCest
{
    private const ALLOW_ACCESS_CONTROLLER_URL = Dashboard::LOCATION.'cmd/web_access_allow/';
    private const DISALLOW_ACCESS_CONTROLLER_URL = Dashboard::LOCATION.'cmd/web_access_remove/';

    public function it_send_a_request_to_allow_web_access(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        // When
        // The serverId is high to avoid to change it's parameter
        $I->amOnPage(self::ALLOW_ACCESS_CONTROLLER_URL.'99999999999');

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink(Dashboard::LOCATION);
    }

    public function it_send_a_request_to_disallow_web_access(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        // When
        // The serverId is high to avoid to change it's parameter
        $I->amOnPage(self::DISALLOW_ACCESS_CONTROLLER_URL.'99999999999');

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink(Dashboard::LOCATION);
    }
}
