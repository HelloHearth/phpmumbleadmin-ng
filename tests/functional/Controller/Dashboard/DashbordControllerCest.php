<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Dashboard;

use App\Tests\FunctionalTester;
use App\Tests\Page\DashboardPage as Dashboard;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DashbordControllerCest
{
    public function it_send_a_request_to_get_the_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Dashboard::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement(Dashboard::TABLE_ID);
    }
}
