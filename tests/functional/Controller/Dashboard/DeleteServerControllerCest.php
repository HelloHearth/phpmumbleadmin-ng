<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Dashboard;

use App\Tests\FunctionalTester;
use App\Tests\Page\DashboardPage as Dashboard;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteServerControllerCest
{
    public function it_send_a_request_to_delete_a_server(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        // When
        // The serverId is high to avoid to delete one
        $I->amOnPage(Dashboard::LOCATION.'cmd/delete_server/99999999999');

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink(Dashboard::LOCATION);
    }
}
