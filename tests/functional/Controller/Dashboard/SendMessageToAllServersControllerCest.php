<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Dashboard;

use App\Domain\Action\Dashboard\SendMessageToAllServers\MessageSent;
use App\Infrastructure\Symfony\Form\SendMessageToAllServersType;
use App\Tests\FunctionalTester;
use App\Tests\Page\DashboardPage as Dashboard;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToAllServersControllerCest
{
    private const CONTROLLER_URL = Dashboard::LOCATION.'cmd/send_message_to_all_servers';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_request_without_form_fields(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send empty form data
        $I->sendPOST(self::CONTROLLER_URL, [
            SendMessageToAllServersType::BLOCK_PREFIX => [],
        ]);

        // Then I see I get the CSRF error message
        $I->seeResponseCodeIsFormError();
        $I->seeInSource('The CSRF token is invalid. Please try to resubmit the form.');
    }

    public function it_send_a_post_request_with_valid_data(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send post for a not connected user
        $I->sendPostWithCsrf(self::CONTROLLER_URL, SendMessageToAllServersType::BLOCK_PREFIX, [
            'message' =>  'This is a test message sent with PhpMumbleAdmin',
        ]);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeInSource(MessageSent::KEY);
    }
}
