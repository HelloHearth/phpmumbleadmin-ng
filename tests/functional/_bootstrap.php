<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

use App\Tests\TestSetupHelper;

// include Murmur/.bootstrap in the global scope for tests
require_once __DIR__.'/../../src/Infrastructure/Murmur/.bootstrap.php';

TestSetupHelper::setTimezoneForTests();
// On suite startup, load the test db.dump to avoid errors when running after the acceptance suite
TestSetupHelper::dumpInitialDatabase();
