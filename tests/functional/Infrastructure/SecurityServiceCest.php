<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure;

use App\Domain\Service\SecurityServiceInterface;
use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SecurityServiceCest
{
    private SecurityServiceInterface $service;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(SecurityServiceInterface::class);
    }

    public function it_test_is_granted_method(FunctionalTester $I): void
    {
        $I->amLoggedAsAnonymous();
        $I->assertFalse($this->service->isGranted(SecurityServiceInterface::ROLE_SUPER_USER));

        $I->amLoggedAsSuperAdmin();
        $I->assertTrue($this->service->isGranted(SecurityServiceInterface::ROLE_SUPER_USER));
    }

    protected function isGrantedRootAdminProvider(): array
    {
        return [
            ['role' => 'amLoggedAsAnonymous', 'expected' => false],
            ['role' => 'amLoggedAsMumbleUser', 'expected' => false],
            ['role' => 'amLoggedAsSuperUserRu', 'expected' => false],
            ['role' => 'amLoggedAsSuperUser', 'expected' => false],
            ['role' => 'amLoggedAsAdmin', 'expected' => false],
            ['role' => 'amLoggedAsRootAdmin', 'expected' => true],
            ['role' => 'amLoggedAsSuperAdmin', 'expected' => true]
        ];
    }

    /** @dataProvider isGrantedRootAdminProvider */
    public function it_test_is_granted_root_admin_method(FunctionalTester $I, Example $data): void
    {
        $amLoggedAs = $data['role'];
        $I->$amLoggedAs();

        $result = $this->service->isGrantedRootAdmin();

        $I->assertSame($data['expected'], $result);
    }

    protected function isGrantedAdminProvider(): array
    {
        return [
            ['role' => 'amLoggedAsAnonymous', 'expected' => false],
            ['role' => 'amLoggedAsMumbleUser', 'expected' => false],
            ['role' => 'amLoggedAsSuperUserRu', 'expected' => false],
            ['role' => 'amLoggedAsSuperUser', 'expected' => false],
            ['role' => 'amLoggedAsAdmin', 'expected' => true],
            ['role' => 'amLoggedAsRootAdmin', 'expected' => true],
            ['role' => 'amLoggedAsSuperAdmin', 'expected' => true]
        ];
    }

    /** @dataProvider isGrantedAdminProvider */
    public function it_test_is_granted_admin_method(FunctionalTester $I, Example $data): void
    {
        $amLoggedAs = $data['role'];
        $I->$amLoggedAs();

        $result = $this->service->isGrantedAdmin();

        $I->assertSame($data['expected'], $result);
    }

    protected function isGrantedSuperuserProvider(): array
    {
        return [
            ['role' => 'amLoggedAsAnonymous', 'expected' => false],
            ['role' => 'amLoggedAsMumbleUser', 'expected' => false],
            ['role' => 'amLoggedAsSuperUserRu', 'expected' => false],
            ['role' => 'amLoggedAsSuperUser', 'expected' => true],
            ['role' => 'amLoggedAsAdmin', 'expected' => true],
            ['role' => 'amLoggedAsRootAdmin', 'expected' => true],
            ['role' => 'amLoggedAsSuperAdmin', 'expected' => true]
        ];
    }

    /** @dataProvider isGrantedSuperuserProvider */
    public function it_test_is_granted_superuser_method(FunctionalTester $I, Example $data): void
    {
        $amLoggedAs = $data['role'];
        $I->$amLoggedAs();

        $result = $this->service->isGrantedSuperUser();

        $I->assertSame($data['expected'], $result);
    }

    protected function isGrantedSuperuserRuProvider(): array
    {
        return [
            ['role' => 'amLoggedAsAnonymous', 'expected' => false],
            ['role' => 'amLoggedAsMumbleUser', 'expected' => false],
            ['role' => 'amLoggedAsSuperUserRu', 'expected' => true],
            ['role' => 'amLoggedAsSuperUser', 'expected' => true],
            ['role' => 'amLoggedAsAdmin', 'expected' => true],
            ['role' => 'amLoggedAsRootAdmin', 'expected' => true],
            ['role' => 'amLoggedAsSuperAdmin', 'expected' => true]
        ];
    }

    /** @dataProvider isGrantedSuperuserRuProvider */
    public function it_test_is_granted_superuser_ru_method(FunctionalTester $I, Example $data): void
    {
        $amLoggedAs = $data['role'];
        $I->$amLoggedAs();

        $result = $this->service->isGrantedSuperUserRu();

        $I->assertSame($data['expected'], $result);
    }

    protected function isGrantedMumbleUserProvider(): array
    {
        return [
            ['role' => 'amLoggedAsAnonymous', 'expected' => false],
            ['role' => 'amLoggedAsMumbleUser', 'expected' => true],
            ['role' => 'amLoggedAsSuperUserRu', 'expected' => true],
            ['role' => 'amLoggedAsSuperUser', 'expected' => true],
            ['role' => 'amLoggedAsAdmin', 'expected' => true],
            ['role' => 'amLoggedAsRootAdmin', 'expected' => true],
            ['role' => 'amLoggedAsSuperAdmin', 'expected' => true]
        ];
    }

    /** @dataProvider isGrantedMumbleUserProvider */
    public function it_test_is_granted_mumble_user_method(FunctionalTester $I, Example $data): void
    {
        $amLoggedAs = $data['role'];
        $I->$amLoggedAs();

        $result = $this->service->isGrantedMumbleUser();

        $I->assertSame($data['expected'], $result);
    }

    protected function isRoleOrLessSuperuserRuProvider(): array
    {
        return [
            ['role' => 'amLoggedAsAnonymous', 'expected' => true],
            ['role' => 'amLoggedAsMumbleUser', 'expected' => true],
            ['role' => 'amLoggedAsSuperUserRu', 'expected' => true],
            ['role' => 'amLoggedAsSuperUser', 'expected' => false],
            ['role' => 'amLoggedAsAdmin', 'expected' => false],
            ['role' => 'amLoggedAsRootAdmin', 'expected' => false],
            ['role' => 'amLoggedAsSuperAdmin', 'expected' => false]
        ];
    }

    /** @dataProvider isRoleOrLessSuperuserRuProvider */
    public function it_test_is_role_or_less_superuser_ru_method(FunctionalTester $I, Example $data): void
    {
        $amLoggedAs = $data['role'];
        $I->$amLoggedAs();

        $result = $this->service->isRoleSuperUserRuOrLess();

        $I->assertSame($data['expected'], $result);
    }
}
