<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure;

use App\Domain\Service\SecurityServiceInterface as Security;
use App\Entity\AdminEntity;
use App\Infrastructure\Service\Role\RoleCheckerService;
use App\Infrastructure\Symfony\Security\MumbleUser;
use App\Infrastructure\Symfony\Security\UserInterface;
use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RoleCheckerServiceCest
{
    private RoleCheckerService $service;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(RoleCheckerService::class);
    }

    private function createUser(string $class): UserInterface
    {
        return ('Admin' === $class) ? new AdminEntity : new MumbleUser();
    }

    protected function rolesProvider(): array
    {
        return [
            ['user' => 'Admin', 'roles' => [Security::ROLE_SUPER_ADMIN], 'isGranted' => Security::ROLE_SUPER_ADMIN, 'expected' => true],

            ['user' => 'Admin', 'roles' => [Security::ROLE_ROOT_ADMIN], 'isGranted' => Security::ROLE_SUPER_ADMIN, 'expected' => false],
            ['user' => 'Admin', 'roles' => [Security::ROLE_ROOT_ADMIN], 'isGranted' => Security::ROLE_ROOT_ADMIN, 'expected' => true],

            ['user' => 'Admin', 'roles' => [Security::ROLE_ADMIN], 'isGranted' => Security::ROLE_ROOT_ADMIN, 'expected' => false],
            ['user' => 'Admin', 'roles' => [Security::ROLE_ADMIN], 'isGranted' => Security::ROLE_ADMIN, 'expected' => true],

            ['user' => 'MumbleUser', 'roles' => [Security::ROLE_SUPER_USER], 'isGranted' => Security::ROLE_ADMIN, 'expected' => false],
            ['user' => 'MumbleUser', 'roles' => [Security::ROLE_SUPER_USER], 'isGranted' => Security::ROLE_SUPER_USER, 'expected' => true],

            ['user' => 'MumbleUser', 'roles' => [Security::ROLE_SUPER_USER_RU], 'isGranted' => Security::ROLE_SUPER_USER, 'expected' => false],
            ['user' => 'MumbleUser', 'roles' => [Security::ROLE_SUPER_USER_RU], 'isGranted' => Security::ROLE_SUPER_USER_RU, 'expected' => true],

            ['user' => 'MumbleUser', 'roles' => [Security::ROLE_MUMBLE_USER], 'isGranted' => Security::ROLE_SUPER_USER_RU, 'expected' => false],
            ['user' => 'MumbleUser', 'roles' => [Security::ROLE_MUMBLE_USER], 'isGranted' => Security::ROLE_MUMBLE_USER, 'expected' => true]
        ];
    }

    /** @dataProvider rolesProvider */
    public function it_grant_a_user_with_one_role(FunctionalTester $I, Example $data): void
    {
        // Given we created a user with one role
        $user = $this->createUser($data['user']);
        $user->setRoles($data['roles']);

        // When we call the isGranted method
        $result = $this->service->isGranted($user, $data['isGranted']);

        // Then we expect the right boolean to grant the user
        $I->assertSame($data['expected'], $result);
    }

    /** @dataProvider rolesProvider */
    public function it_grant_a_user_with_two_roles_when_we_are_logged_as_super_admin(FunctionalTester $I, Example $data): void
    {
        // Given we created a user with two role, and we are logged as SuperAdmin
        $roles = $data['roles'];
        $roles[] = Security::ROLE_MUMBLE_USER;
        $user = $this->createUser($data['user']);
        $user->setRoles($roles);
        $I->amLoggedAsSuperAdmin();

        // When we call the isGranted method
        $result = $this->service->isGranted($user, $data['isGranted']);

        // Then we expect the right boolean to grant the user
        $I->assertSame($data['expected'], $result);
    }
}
