<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Security\Service;

use App\Entity\ForgottenPassword;
use App\Infrastructure\Symfony\Security\Service\ForgottenPasswordService;
use App\Tests\FunctionalTester;
use Codeception\Example;
use Codeception\Stub;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordServiceCest
{
    private ForgottenPasswordService $service;
    private RouterInterface $router;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(ForgottenPasswordService::class);
        $this->router = $I->grabService(RouterInterface::class);
    }

    public function it_send_forgotten_password_email_to_admin(FunctionalTester $I): void
    {
        // Given I have an admin
        $admin = $I->addAdminUserInRepository(['username' => 'testAdmin', 'email' => 'test@test.tld']);

        // When I handle the service
        $this->service->handle('testAdmin');

        // Then

        // I see the forgotten password has been persisted in database
        $forgottenPassword = $I->grabEntityFromRepository(ForgottenPassword::class, ['admin' => $admin]);

        $I->assertInstanceOf(ForgottenPassword::class, $forgottenPassword);
        // I see the forgotten password belong to the admin
        $I->assertSame($admin, $forgottenPassword->getAdmin());
        // I see the validity of the forgotten password is greater than the current time
        $I->assertGreaterThan((new \DateTime('+30seconds'))->getTimestamp(), $forgottenPassword->getValidUntil()->getTimestamp());
        // I see the hash is long
        $I->assertGreaterThan(32, \strlen($forgottenPassword->getHash()));

        // I see the forgotten password email has been sent
        $I->seeEmailIsSent();

        $I->assertEmailAddressContains('To', $admin->getEmail());
        $I->assertEmailAddressContains('From', $I->grabParameter('app.from_email_automated_sent'));
        $I->assertEmailHeaderSame('subject', 'email_title');
        $I->assertEmailHtmlBodyContains('email_title');
        $I->assertEmailHtmlBodyContains('email_body');
        $I->assertEmailHtmlBodyContains('email_button');

        // I see the email contains the link to reset forgotten password
        $href = $this->router->generate('app_forgotten_password_reset', ['hash' => $forgottenPassword->getHash()], UrlGeneratorInterface::ABSOLUTE_URL);
        $href = 'href="'.$href.'"';
        $I->assertEmailHtmlBodyContains($href);
    }

    private function adminsProvider(): array
    {
        return [
            // Unknown username
            ['username' => 'testAdmin', 'email' => 'test@test.tld', 'submitUsername' => 'unknown_user'],
            // Email is null
            ['username' => 'testAdmin', 'email' => null, 'submitUsername' => 'testAdmin'],
            // Email is empty
            ['username' => 'testAdmin', 'email' => '', 'submitUsername' => 'testAdmin'],
            // Email is invalid
            ['username' => 'testAdmin', 'email' => 'invalid_email_string', 'submitUsername' => 'testAdmin'],
        ];
    }

    /** @dataProvider adminsProvider */
    public function it_test_service_with_invalid_data(FunctionalTester $I, Example $data): void
    {
        // Given I have an admin
        $I->addAdminUserInRepository(['username' => $data['username'], 'email' => $data['email']]);

        // When I handle the service
        $this->service->handle($data['submitUsername']);

        // Then I don't see ForgottenPassword has been created, and no email sent
        $I->assertEmpty($I->grabEntitiesFromRepository(ForgottenPassword::class));
        $I->dontSeeEmailIsSent();
    }

    public function it_request_another_forgotten_password(FunctionalTester $I): void
    {
        // Given I have an admin, with a ForgottenPassword in database
        $admin = $I->addAdminUserInRepository(['username' => 'testAdmin', 'email' => 'test@test.tld']);
        $oldForgottenPassword = clone $I->addForgottenPasswordInRepository(['admin' => $admin, 'valid_until' => '+30seconds']);

        // When I handle the service
        $this->service->handle('testAdmin');

        // Then I see the ForgottenPassword has been updated in database, and a new email has been sent

        /** @var ForgottenPassword[] $forgottenPasswords */
        $forgottenPasswords = $I->grabEntitiesFromRepository(ForgottenPassword::class);
        $I->assertCount(1, $forgottenPasswords);
        // A new hash has been persisted
        $I->assertNotSame($oldForgottenPassword->getHash(), $forgottenPasswords[0]->getHash());
        // Valid until is greater than the old one
        $I->assertGreaterThan($oldForgottenPassword->getValidUntil()->getTimestamp(), $forgottenPasswords[0]->getValidUntil()->getTimestamp());

        $I->seeEmailIsSent();
    }

    public function it_test_hash_exists_method_with_existent_hash(FunctionalTester $I): void
    {
        // Given I have an admin, with a ForgottenPassword in database
        $admin = $I->addAdminUserInRepository();
        $I->addForgottenPasswordInRepository(['admin' => $admin, 'hash' => 'dummy_hash']);

        // When I run the hashExists() with
        $result = $this->service->hashExists('dummy_hash');

        // Then I see method return positive response
        $I->assertTrue($result);
    }

    public function it_test_hash_exists_method_with_non_existent_hash(FunctionalTester $I): void
    {
        // Given I have an admin, with a ForgottenPassword in database
        $admin = $I->addAdminUserInRepository();
        $I->addForgottenPasswordInRepository(['admin' => $admin, 'hash' => 'dummy_hash']);

        // When I run the hashExists()
        $result = $this->service->hashExists('non_existent_hash');

        // Then I see method return negative response
        $I->assertFalse($result);
    }
}
