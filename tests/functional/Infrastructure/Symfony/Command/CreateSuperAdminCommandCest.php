<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Command;

use App\Domain\Service\SecurityServiceInterface;
use App\Entity\AdminEntity;
use App\Tests\FunctionalTester;
use App\Tests\Page\LoginPage;
use Symfony\Component\Console\Command\Command;

class CreateSuperAdminCommandCest
{
    private const COMMAND = 'app:super-admin:create';

    public function it_create_a_super_admin(FunctionalTester $I): void
    {
        // Given I have no admin called TestSA
        $I->dontSeeInRepository(AdminEntity::class, ['username' => 'TestSA']);

        // When I execute the command
        $output = $I->runSymfonyConsoleCommand(self::COMMAND, ['username' => 'TestSA', 'password' => 'test', '--email' => 'test@test.tld'], [], Command::SUCCESS);

        // Then I see the success message
        $I->assertEquals('SuperAdmin TestSA created'.PHP_EOL, $output);

        // And I can log in with the new SuperAdmin
        $I->amOnPage(LoginPage::LOCATION);
        $I->logIn('TestSA', 'test');
        $I->seeAmLoggedAs('TestSA');
    }

    public function it_create_a_super_admin_with_identical_login(FunctionalTester $I): void
    {
        // Given I have an admin called TestSA
        $I->haveInRepository(AdminEntity::class, ['username' => 'TestSA', 'password' => 'test', 'roles' => [SecurityServiceInterface::ROLE_SUPER_ADMIN]]);

        // When I execute the command with identical username
        $output = $I->runSymfonyConsoleCommand(self::COMMAND, ['username' => 'TestSA', 'password' => 'test'], [], Command::FAILURE);

        // Then I see the error message
        $I->assertEquals('validator_login_exists'.PHP_EOL, $output);
    }

    public function it_create_a_super_admin_with_invalid_email(FunctionalTester $I): void
    {
        // Given

        // When I execute the command with invalid email
        $output = $I->runSymfonyConsoleCommand(self::COMMAND, ['username' => 'TestSA', 'password' => 'test', '--email' => 'invalid_email'], [], Command::FAILURE);

        // Then I see the error message
        $I->assertStringContainsString('This value is not a valid email address', $output);
    }
}
