<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Command\Dev;

use App\Entity\AdminEntity;
use App\Tests\FunctionalTester;
use Symfony\Component\Console\Command\Command;

class LoadFixturesCommandCest
{
    public function it_run_command_app_load_fixtures(FunctionalTester $I): void
    {
        // Given I have many admins on repository
        $I->addAdminUserInRepository(['username' => 'testAdmin 4']);
        $I->addRootAdminUserInRepository(['username' => 'testAdmin 5']);
        $I->addRootAdminUserInRepository(['username' => 'testAdmin 6']);
        $admins = $I->grabEntitiesFromRepository(AdminEntity::class);
        $I->assertCount(6, $admins);

        // When I execute the command
        $output = $I->runSymfonyConsoleCommand('app:load-fixtures', [], [], Command::SUCCESS);

        // Then I see the command truncated only table of doctrine entities
        $I->assertStringContainsString('Fast truncate workaround', $output);
        $I->assertStringContainsString('Truncate table admin', $output);
        $I->assertStringContainsString('Truncate table forgotten_password', $output);
        $I->assertStringNotContainsString('Truncate table doctrine_migration_versions', $output);

        // I see the command has loaded fixtures
        $I->assertStringContainsString('loading App\DataFixtures\ORM\LoadAdmins', $output);

        // I see in repository the command has reset the auto-increment variable
        /** @var AdminEntity[] $admins */
        $admins = $I->grabEntitiesFromRepository(AdminEntity::class);
        $I->assertCount(3, $admins);
        $I->assertSame(1, $admins[0]->getId());
        $I->assertSame(2, $admins[1]->getId());
        $I->assertSame(3, $admins[2]->getId());
    }
}
