<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Controller;

use App\Controller\BaseController;
use App\Domain\Bus\BusResponse;
use App\Tests\Functional\Infrastructure\Service\FormData\TestFieldFormErrorEvent;
use App\Tests\Functional\Infrastructure\Service\FormData\TestFieldFormErrorType;
use App\Tests\FunctionalTester;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BaseControllerCest
{
    private BaseController $controller;
    private FormFactoryInterface $formFactory;

    public function _before(FunctionalTester $I)
    {
        $this->controller = new class extends BaseController {};
        $this->formFactory = $I->grabService(FormFactoryInterface::class);
    }

    public function it_handle_bus_response_without_fields_parameters(FunctionalTester $I): void
    {
        // Given I have a form and a bus response with a status error
        $form = $this->formFactory->create(TestFieldFormErrorType::class);
        $response = new BusResponse([new TestFieldFormErrorEvent()]);

        // When I call the handle method
        $this->controller->handleBusResponseEventsWithForm($form, $response);

        // Then I see the root form has error with the event message
        $I->assertCount(1, $form->getErrors());
        $I->assertCount(0, $form->get('testField')->getErrors());
        $I->assertSame(TestFieldFormErrorEvent::KEY, $form->getErrors()[0]->getMessage());
    }

    public function it_handle_bus_response_with_fields_parameters(FunctionalTester $I): void
    {
        // Given I have a form and a bus response with a status error
        $form = $this->formFactory->create(TestFieldFormErrorType::class);
        $response = new BusResponse([new TestFieldFormErrorEvent]);

        // When I call the handle method with fieldParameters
        $this->controller->handleBusResponseEventsWithForm($form, $response, [TestFieldFormErrorEvent::class => 'testField']);

        // Then I see the form field has error with the event message
        $I->assertCount(0, $form->getErrors());
        $I->assertCount(1, $form->get('testField')->getErrors());
        $I->assertSame(TestFieldFormErrorEvent::KEY, $form->get('testField')->getErrors()[0]->getMessage());
    }
}
