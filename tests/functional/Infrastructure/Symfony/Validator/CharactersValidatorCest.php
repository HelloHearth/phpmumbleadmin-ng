<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Validator;

use App\Infrastructure\Symfony\Validator\Constraints\Characters;
use App\Tests\FunctionalTester;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CharactersValidatorCest
{
    private ValidatorInterface $validator;

    public function _before(FunctionalTester $I): void
    {
        $this->validator = $I->grabService(ValidatorInterface::class);
    }

    public function it_doesnt_produce_violation_when_constructor_is_empty(FunctionalTester $I)
    {
        $violations = $this->validator->validate('invalid space characters', new Characters(['pattern' => null]));

        $I->assertSame(0, $violations->count());
    }

    public function it_doesnt_produce_violation_when_pattern_is_null(FunctionalTester $I)
    {
        $violations = $this->validator->validate('invalid space characters', new Characters(['pattern' => null]));

        $I->assertSame(0, $violations->count());
    }

    public function it_produce_violation_when_pattern_is_empty(FunctionalTester $I)
    {
        $violations = $this->validator->validate('valid_underscore_character', new Characters(['pattern' => '']));

        $I->assertSame(1, $violations->count());
    }

    public function it_produce_violation_when_submitted_value_doesnt_match_the_pattern(FunctionalTester $I)
    {
        $violations = $this->validator->validate('invalid space character', new Characters(['pattern' => '[\w]+']));

        $I->assertSame(1, $violations->count());
    }

    public function it_doesnt_produce_violation_when_submitted_value_match_the_pattern(FunctionalTester $I)
    {
        $violations = $this->validator->validate('valid_underscore_character', new Characters(['pattern' => '[\w]+']));

        $I->assertSame(0, $violations->count());
    }
}
