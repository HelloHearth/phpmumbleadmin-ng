<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Validator;

use App\Tests\FunctionalTester;
use App\Infrastructure\Symfony\Validator\Constraints\UniqueLogin;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UniqueLoginValidatorCest
{
    private ValidatorInterface $validator;

    public function _before(FunctionalTester $I): void
    {
        $this->validator = $I->grabService(ValidatorInterface::class);
    }

    public function it_validate_with_success_an_unexistant_login(FunctionalTester $I)
    {
        $violations = $this->validator->validate('missing_username', new UniqueLogin());

        $I->assertSame(0, $violations->count());
    }

    public function it_validate_with_success_an_existant_login(FunctionalTester $I)
    {
        $I->addAdminUserInRepository(['username' => 'valid_username']);

        $violations = $this->validator->validate('valid_username', new UniqueLogin());

        $I->assertSame(1, $violations->count());
    }
}
