<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Validator;

use App\Tests\FunctionalTester;
use App\Infrastructure\Symfony\Validator\Constraints\Port;
use Codeception\Example;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PortValidatorCest
{
    private ValidatorInterface $validator;

    public function _before(FunctionalTester $I): void
    {
        $this->validator = $I->grabService(ValidatorInterface::class);
    }

    protected function validPortProvider(): array
    {
        return [[0], [64738], [65535]];
    }

    /** @dataProvider validPortProvider */
    public function it_validate_with_success_a_valid_port(FunctionalTester $I, Example $ports)
    {
        $violations = $this->validator->validate($ports[0], new Port());

        $I->assertSame(0, $violations->count());
    }

    protected function invalidPortProvider(): array
    {
        return [[-1], [65536]];
    }

    /** @dataProvider invalidPortProvider */
    public function it_does_not_validate_an_invalid_port(FunctionalTester $I, Example $ports)
    {
        $expected = $I->getNotTranslatedText('validator_port_error_message', ['{{ integer }}' => (string) $ports[0]]);

        // When I validate an invalid port
        $violations = $this->validator->validate($ports[0], new Port());
        $result = $violations[0]->getMessage();

        // Then I see I get one validation error message, with the submitted port included
        $I->assertSame($expected, $result);
        $I->assertSame(1, $violations->count());
        $I->assertInstanceOf(Port::class, $violations[0]->getConstraint());
    }
}
