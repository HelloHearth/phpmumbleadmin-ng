<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Form;

use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Symfony\Form\CreateAdminType;
use App\Tests\FunctionalTester;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateAdminTypeCest
{
    private FormFactoryInterface $factory;

    public function _before(FunctionalTester $I): void
    {
        $this->factory = $I->grabService(FormFactoryInterface::class);
    }

    public function it_test_the_choices_of_the_role_field_as_superadmin(FunctionalTester $I)
    {
        // Given I'm logged as SuperAdmin
        $I->amLoggedAsSuperAdmin();

        // When I create the form view
        $form = $this->factory->create(CreateAdminType::class);
        $view = $form->createView();

        // Then I expect to find all admin roles
        $I->assertCount(3, $view->offsetGet('role')->vars['choices']);
        $I->assertSame(SecurityServiceInterface::ROLE_SUPER_ADMIN, $view->offsetGet('role')->vars['choices'][0]->value);
        $I->assertSame(SecurityServiceInterface::ROLE_ROOT_ADMIN, $view->offsetGet('role')->vars['choices'][1]->value);
        $I->assertSame(SecurityServiceInterface::ROLE_ADMIN, $view->offsetGet('role')->vars['choices'][2]->value);
    }

    public function it_test_the_choices_of_the_role_field_as_rootadmin(FunctionalTester $I)
    {
        // Given I'm logged as RootAdmin
        $I->amLoggedAsRootAdmin();

        // When I create the form view
        $form = $this->factory->create(CreateAdminType::class);
        $view = $form->createView();

        // Then I expect to find only available admin roles
        $I->assertCount(1, $view->offsetGet('role')->vars['choices']);
        $I->assertSame(SecurityServiceInterface::ROLE_ADMIN, $view->offsetGet('role')->vars['choices'][0]->value);
    }
}
