<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Form;

use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Symfony\Form\EditAdminData;
use App\Infrastructure\Symfony\Form\EditAdminType;
use App\Tests\FunctionalTester;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditAdminTypeCest
{
    private FormFactoryInterface $factory;

    public function _before(FunctionalTester $I): void
    {
        $this->factory = $I->grabService(FormFactoryInterface::class);
    }

    public function it_submit_with_success_if_the_login_didnt_change(FunctionalTester $I)
    {
        // Given I'm logged as SuperAdmin, and I have an admin in repository
        $I->amLoggedAsSuperAdmin();
        $admin = $I->addRootAdminUserInRepository();
        $form = $this->factory->create(EditAdminType::class, EditAdminData::fromAdmin($admin), ['csrf_protection' => false]);

        // When I submit the form
        $form->submit([
            'login' => $admin->getUserIdentifier(),
            'role' => SecurityServiceInterface::ROLE_ADMIN,
        ]);

        // Then I see the form is valid
        $I->assertTrue($form->isValid());
    }

    public function it_get_error_if_user_identifier_has_changed_and_its_already_in_database(FunctionalTester $I)
    {
        // Given I'm logged as SuperAdmin, and I have two admins
        $I->amLoggedAsSuperAdmin();
        $I->addAdminUserInRepository(['username' => 'username_in_database']);
        $admin = $I->addRootAdminUserInRepository();
        $form = $this->factory->create(EditAdminType::class, EditAdminData::fromAdmin($admin), ['csrf_protection' => false]);

        // When I submit the form with user identifier already in database
        $form->submit([
            'login' => 'username_in_database',
            'role' => SecurityServiceInterface::ROLE_ADMIN,
        ]);

        // Then I see the form is not valid
        $I->assertFalse($form->isValid());
    }
}
