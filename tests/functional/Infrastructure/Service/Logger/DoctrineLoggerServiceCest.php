<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Service\Logger;

use App\Domain\Service\LoggerServiceInterface;
use App\Entity\AdminLogEntity;
use App\Infrastructure\Service\Logger\DoctrineLoggerService;
use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * Test that the LoggerService add messages to Monolog/Logger correctly
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DoctrineLoggerServiceCest
{
    private DoctrineLoggerService $service;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(DoctrineLoggerService::class);
    }

    public function it_test_log_method(FunctionalTester $I): void
    {
        // Given I have no log at all
        $I->dontSeeInRepository(AdminLogEntity::class);

        // When I use the log method to add a message with the warning level
        $this->service->log(LoggerServiceInterface::FACILITY_USER, LoggerServiceInterface::LEVEL_WARNING, 'server_started', ['%id%' => 42]);

        // Then I see a log has been added to the repository
        $I->seeInRepository(AdminLogEntity::class, [
            'facility' => LoggerServiceInterface::FACILITY_USER,
            'level' => LoggerServiceInterface::LEVEL_WARNING,
            'message' => 'server_started',
            'context' => '{"%id%":42}',
        ]);
    }

    public function it_test_log_method_with_empty_context(FunctionalTester $I): void
    {
        // Given I have no log at all
        $I->dontSeeInRepository(AdminLogEntity::class);

        // When I use the log method to add a message without context
        $this->service->log(LoggerServiceInterface::FACILITY_USER, LoggerServiceInterface::LEVEL_INFORMATIONAL, 'server_started');

        // Then I see a log has been added to the repository
        $I->seeInRepository(AdminLogEntity::class, [
            'facility' => LoggerServiceInterface::FACILITY_USER,
            'level' => LoggerServiceInterface::LEVEL_INFORMATIONAL,
            'message' => 'server_started',
            'context' => '[]',
        ]);
    }

    private function levelProvider(): array
    {
        return [
            ['level' => 'emergency',    'expected' => LoggerServiceInterface::LEVEL_EMERGENCY],
            ['level' => 'alert',        'expected' => LoggerServiceInterface::LEVEL_ALERT],
            ['level' => 'critical',     'expected' => LoggerServiceInterface::LEVEL_CRITICAL],
            ['level' => 'error',        'expected' => LoggerServiceInterface::LEVEL_ERROR],
            ['level' => 'warning',      'expected' => LoggerServiceInterface::LEVEL_WARNING],
            ['level' => 'notice',       'expected' => LoggerServiceInterface::LEVEL_NOTICE],
            ['level' => 'info',         'expected' => LoggerServiceInterface::LEVEL_INFORMATIONAL],
            ['level' => 'debug',        'expected' => LoggerServiceInterface::LEVEL_DEBUG],
        ];
    }

    /** @dataProvider levelProvider */
    public function it_test_methods(FunctionalTester $I, Example $data): void
    {
        // Given I have no log at all
        $I->dontSeeInRepository(AdminLogEntity::class);

        // When I use the level method to add a message with the service
        $level = $data['level'];
        $this->service->$level(LoggerServiceInterface::FACILITY_USER, 'server_stopped', ['%id%' => 42]);

        // Then I see a log has been added to the repository
        $I->seeInRepository(AdminLogEntity::class, [
            'facility' => LoggerServiceInterface::FACILITY_USER,
            'level' => $data['expected'],
            'message' => 'server_stopped',
            'context' => '{"%id%":42}',
        ]);
    }
}
