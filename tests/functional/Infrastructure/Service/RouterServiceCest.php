<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Service;

use App\Infrastructure\Service\RouterService;
use App\Tests\FunctionalTester;
use Codeception\Example;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RouterServiceCest
{
    private RouterService $service;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(RouterService::class);
    }

    private function routeNameProvider(): array
    {
        return [
            ['expected' => 'server_channels',       'referer' => ''],
            ['expected' => 'server_channels',       'referer' => '/'],
            ['expected' => 'server_channels',       'referer' => '/not_found_route'],
            ['expected' => 'server_channels',       'referer' => '/administrations'],
            ['expected' => 'server_channels',       'referer' => '/server/42'],
            ['expected' => 'server_settings',       'referer' => '/server/42/settings'],
            ['expected' => 'server_registrations',  'referer' => '/server/42/registrations'],
            ['expected' => 'server_registrations',  'referer' => '/server/42/registration/42'],
            ['expected' => 'server_registrations',  'referer' => '/server/42/registration/edit/42'],
            ['expected' => 'server_bans',           'referer' => '/server/42/bans'],
// Uncomment bellow tests when the CRUD for bans is ready
//            ['expected' => 'server_bans',         'referer' => '/server/42/ban/42'],
//            ['expected' => 'server_bans',         'referer' => '/server/42/ban/edit/42'],
            ['expected' => 'server_logs',           'referer' => '/server/42/logs']
        ];
    }

    /** @dataProvider routeNameProvider */
    public function it_test_get_route_name_method(FunctionalTester $I, Example $data): void
    {
        // Given I have a referer in a request
        $request = new Request();
        $request->headers->set('referer', $request->getUriForPath('').$data['referer']);

        // When I call getNameOnServersListSelection() method
        $result = $this->service->getNameOnServersListSelection($request);

        // Then I see I get the expected route name
        $I->assertSame($data['expected'], $result);
    }
}
