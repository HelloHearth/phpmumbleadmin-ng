<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Service\Role;

use App\Domain\Service\SecurityServiceInterface as Security;
use App\Entity\AdminEntity;
use App\Infrastructure\Service\Role\RoleBitService;
use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RoleBitServiceCest
{
    private RoleBitService $service;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(RoleBitService::class);
    }

    protected function rolesProvider(): array
    {
        return [
            ['roles' => [Security::ROLE_SUPER_ADMIN], 'expected' => RoleBitService::SUPER_ADMIN],
            // Reverse roles must return the best role name
            ['roles' => [Security::ROLE_MUMBLE_USER, Security::ROLE_SUPER_ADMIN], 'expected' => RoleBitService::SUPER_ADMIN],
            // Multiple roles, before and after the best role, must return the best role
            ['roles' => [Security::ROLE_SUPER_USER, Security::ROLE_SUPER_ADMIN, Security::ROLE_MUMBLE_USER, 'ROLE_NOT_CONFIGURED'], 'expected' => RoleBitService::SUPER_ADMIN],
            // Invalid/empty roles
            ['roles' => [], 'expected' => RoleBitService::ANONYMOUS],
            ['roles' => [''], 'expected' => RoleBitService::ANONYMOUS],
            ['roles' => ['ROLE_NOT_CONFIGURED'], 'expected' => RoleBitService::ANONYMOUS],
            ['roles' => ['invalid string'], 'expected' => RoleBitService::ANONYMOUS]
        ];
    }

    /** @dataProvider rolesProvider */
    public function it_find_the_right_role_bit(FunctionalTester $I, Example $data): void
    {
        // Given we have an user with roles
        $user = new AdminEntity();
        $user->setRoles($data['roles']);

        // When we call the findUserBit method
        $result = $this->service->findUserBit($user);

        // Then we expect the right role bit
        $I->assertSame($data['expected'], $result);
    }

    protected function stringRolesProvider(): array
    {
        return [
            ['role' => Security::ROLE_SUPER_ADMIN, 'expected' => RoleBitService::SUPER_ADMIN],
            ['role' => Security::ROLE_ROOT_ADMIN, 'expected' => RoleBitService::ROOT_ADMIN],
            ['role' => Security::ROLE_SUPER_USER_RU, 'expected' => RoleBitService::SUPER_USER_RU],
            ['role' => 'ROLE_NOT_CONFIGURED', 'expected' => RoleBitService::ANONYMOUS],
            ['role' => 'invalid string', 'expected' => RoleBitService::ANONYMOUS],
            ['role' => '', 'expected' => RoleBitService::ANONYMOUS],
            ['role' => '', 'expected' => RoleBitService::ANONYMOUS],
        ];
    }

    /** @dataProvider stringRolesProvider */
    public function it_find_the_right_role_bit_from_a_string(FunctionalTester $I, Example $data): void
    {
        // Given we have a string role to convert into a bit
        $role = $data['role'];

        // When we call the getFromString method
        $result = $this->service->getFromString($role);

        // Then we expect the right role bit
        $I->assertSame($data['expected'], $result);
    }
}
