<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Service;

use App\Domain\Service\LocaleServiceInterface;
use App\Tests\FunctionalTester;

/**
 * Test that the service return the parameter set on Symfony
 * kernel.default_locale when no request have been done
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LocaleServiceCest
{
    private LocaleServiceInterface $service;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(LocaleServiceInterface::class);
    }

    public function it_get_the_locale_fr_from_the_kernel_default_locale(FunctionalTester $I): void
    {
        // When I call getLocale() method without request
        $result = $this->service->getLocale();

        // Then I see I get the locale fr
        $I->assertSame('fr', $result);
    }
}
