<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Murmur\Model;

use App\Domain\Murmur\Exception\Server as ServerException;
use App\Infrastructure\Murmur\Model\Server;
use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerCest
{
    private Server $server;

    public function _before(FunctionalTester $I): void
    {
        $this->server = $I->getTheTestServer();
    }

    public function it_get_the_server_id(FunctionalTester $I): void
    {
        // Given
        $I->resetAllServers();

        // When
        $result = $this->server->getSid();

        // Then
        $I->assertSame(1, $result);
    }

    public function it_kick_all_online_users(FunctionalTester $I): void
    {
        // Given
        $I->haveOneMumbleClient();
        $I->assertNotEmpty($this->server->getUsers());

        // When
        $this->server->kickAllUsers();
        usleep(50000);

        // Then
        $I->assertEmpty($this->server->getUsers());
    }

    public function it_validate_user_characters(FunctionalTester $I): void
    {
        // Given
        $I->setUsernameCharactersOnTestServer('[-=\w\[\]\{\}\(\)\@\|\.]+');

        // When
        $result = $this->server->validateUserChars('valid_characters');
        $result2 = $this->server->validateUserChars('invalid space character');

        // Then
        $I->assertTrue($result);
        $I->assertFalse($result2);
    }

    public function it_validate_channel_characters(FunctionalTester $I): void
    {
        // Given
        $I->setChannelNameCharactersOnTestServer('[-=\w\[\]\{\}\(\)\@\|\.]+');

        // When
        $result = $this->server->validateChannelChars('valid_characters');
        $result2 = $this->server->validateChannelChars('invalid space character');

        // Then
        $I->assertTrue($result);
        $I->assertFalse($result2);
    }

    public function it_check_if_the_server_allow_html_and_strip_tags(FunctionalTester $I): void
    {
        // Given
        $I->enableAllowHtmlParameterOnTestServer();

        // When
        $result = $this->server->checkIsAllowHtmlOrStripTags('<b>message with HTML<b>', $stripped);

        // Then
        $I->assertSame('<b>message with HTML<b>', $result);
        $I->assertFalse($stripped);
    }

    public function it_check_if_the_server_disallow_html_and_strip_tags(FunctionalTester $I): void
    {
        // Given
        $I->disableAllowHtmlParameterOnTestServer();

        // When
        $result = $this->server->checkIsAllowHtmlOrStripTags('<b>message with HTML<b>', $stripped);

        // Then
        $I->assertSame('message with HTML', $result);
        $I->assertTrue($stripped);
    }

    public function parametersProvider(): array
    {
        return [
            ['key' => 'port', 'custom' => '', 'expected' => '64738'],
            ['key' => 'port', 'custom' => '64999', 'expected' => '64999'],
            ['key' => 'unknown_default_key', 'custom' => '', 'expected' => ''],
            ['key' => 'unknown_default_key', 'custom' => 'custom key is returned', 'expected' => 'custom key is returned'],
            ['key' => 'allowhtml', 'custom' => '', 'expected' => 'true'],
            ['key' => 'allowhtml', 'custom' => 'true', 'expected' => 'true'],
            ['key' => 'allowhtml', 'custom' => 'false', 'expected' => 'false']
        ];
    }

    /** @dataProvider parametersProvider */
    public function it_getting_parameters_values_of_the_server(FunctionalTester $I, Example $data): void
    {
        // Given
        $this->server->setConf($data['key'], $data['custom']);

        // When
        $result = $this->server->getParameter($data['key']);

        // Then
        $I->assertSame($data['expected'], $result);
    }

    private function methodsProvider(): array
    {
        $invalidChannel = ServerException\InvalidChannelException::class;
        $invalidSession = ServerException\InvalidSessionException::class;
        $invalidUser = ServerException\InvalidUserException::class;

        return [
            ['method' => 'addChannel', 'expected' => $invalidChannel, 'args' => ['channelName', 99999]],
            ['method' => 'getCertificateList', 'expected' => $invalidSession],
            ['method' => 'getChannelState', 'expected' => $invalidChannel],
            ['method' => 'getRegistration', 'expected' => $invalidUser],
            ['method' => 'getState', 'expected' => $invalidSession],
            ['method' => 'getTexture', 'expected' => $invalidUser],
            ['method' => 'hasPermission', 'expected' => $invalidSession, 'args' => [99999, 0, 42]],
            ['method' => 'hasPermission', 'expected' => $invalidChannel, 'args' => ['validSession', 99999, 42]],
            ['method' => 'removeChannel', 'expected' => $invalidChannel],
            ['method' => 'sendMessage', 'expected' => $invalidSession, 'args' => [99999, 'text message']],
            ['method' => 'sendMessageChannel', 'expected' => $invalidChannel, 'args' => [99999, false, 'text message']],
            ['method' => 'setACL', 'expected' => $invalidChannel, 'args' => [99999, [], [], false]],
            ['method' => 'setTexture', 'expected' => $invalidUser, 'args' => [99999, []]],
            ['method' => 'unregisterUser', 'expected' => $invalidUser],
            ['method' => 'updateRegistration', 'expected' => $invalidUser, 'args' => [99999, []]],
        ];
    }

    /**
     * The Server class have to return a ServerException instead of a
     * Murmur_Exception or a Exception from the \Murmur\ namespace.
     *
     * @dataProvider methodsProvider
     */
    public function it_throws_server_exception_on_murmur_exception(FunctionalTester $I, Example $data): void
    {
        $method = $data['method'];
        $args = $data['args'] ?? [99999];

        if ($data['method'] === 'hasPermission') {

            $I->haveOneMumbleClient();
            $users = $this->server->getUsers();
            $session = \current($users)->session;

            foreach ($args as &$arg) {
                if ($arg === 'validSession') {
                    $arg = $session;
                }
            }
        }

        $I->expectThrowable($data['expected'], function() use ($method, $args) {
            \call_user_func_array([$this->server, $method], $args);
        });
    }

    private function methodsWith32BitIntMaxNumberProvider(): array
    {
        $invalidChannel = ServerException\InvalidChannelException::class;
        $invalidSession = ServerException\InvalidSessionException::class;
        $invalidUser = ServerException\InvalidUserException::class;
        $invalidArgument = \InvalidArgumentException::class;

        return [
            ['method' => 'addChannel', 'expected' => $invalidChannel, 'args' => ['channelName', 2147483648]],
            ['method' => 'getCertificateList', 'expected' => $invalidSession],
            ['method' => 'getChannelState', 'expected' => $invalidChannel],
            ['method' => 'getRegistration', 'expected' => $invalidUser],
            ['method' => 'getState', 'expected' => $invalidSession],
            ['method' => 'getTexture', 'expected' => $invalidUser],
            ['method' => 'hasPermission', 'expected' => $invalidSession, 'args' => [2147483648, 0, 42]],
            ['method' => 'hasPermission', 'expected' => $invalidChannel, 'args' => [42, 2147483648, 42]],
            ['method' => 'hasPermission', 'expected' => $invalidArgument, 'args' => [42, 0, 2147483648]],
            ['method' => 'removeChannel', 'expected' => $invalidChannel],
            ['method' => 'sendMessage', 'expected' => $invalidSession, 'args' => [2147483648, 'text message']],
            ['method' => 'sendMessageChannel', 'expected' => $invalidChannel, 'args' => [2147483648, false, 'text message']],
            ['method' => 'setACL', 'expected' => $invalidChannel, 'args' => [2147483648, [], [], false]],
            ['method' => 'setTexture', 'expected' => $invalidUser, 'args' => [2147483648, []]],
            ['method' => 'unregisterUser', 'expected' => $invalidUser],
            ['method' => 'updateRegistration', 'expected' => $invalidUser, 'args' => [2147483648, []]],
        ];
    }

    /**
     * The Murmur_Server proxy throws \InvalidArgumentException on integer
     * argument superior to 2147483647. The Server class must catch this
     * Exception and throw a ServerException.
     *
     * @dataProvider methodsWith32BitIntMaxNumberProvider
     */
    public function it_throws_server_exception_on_invalid_argument_with_32_bit_int_max_number(FunctionalTester $I, Example $data): void
    {
        $method = $data['method'];
        $args = $data['args'] ?? [2147483648];

        $I->expectThrowable($data['expected'], function() use ($method, $args) {
            \call_user_func_array([$this->server, $method], $args);
        });
    }
}
