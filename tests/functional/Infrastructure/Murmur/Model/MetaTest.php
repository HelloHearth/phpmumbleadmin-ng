<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Murmur\Model;

use App\Domain\Murmur\Model\ServerInterface;
use App\Infrastructure\Murmur\Model\Meta;
use App\Tests\FunctionalTester;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MetaTest extends Unit
{
    protected FunctionalTester $tester;

    /**
     * First get the Meta interface
     */
    public function test_get_Murmur_server_interface(): Meta
    {
        $this->tester->resetAllServers();
        return $this->tester->getMurmurMetaInterface();
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_all_servers_method(Meta $Meta): void
    {
        $this->assertCount(1, $Meta->getAllServers());
        $this->assertContainsOnlyInstancesOf(ServerInterface::class, $Meta->getAllServers());
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_booted_servers_method(Meta $Meta): void
    {
        $this->assertCount(1, $Meta->getBootedServers());
        $this->assertContainsOnlyInstancesOf(ServerInterface::class, $Meta->getBootedServers());
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_new_server_methods(Meta $Meta): void
    {
        $prx = $Meta->newServer();
        $this->assertInstanceOf(ServerInterface::class, $prx);
        $this->assertSame(2, $prx->getSid());
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_server_method(Meta $Meta): void
    {
        $this->assertInstanceOf(ServerInterface::class, $Meta->getServer(1));
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_server_method_with_unknown_server(Meta $Meta): void
    {
        $this->assertNull($Meta->getServer(999999));
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_uptime_method(Meta $Meta): void
    {
        $this->assertIsInt($Meta->getUptime());
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_default_configuration_method(Meta $Meta): void
    {
        $this->assertIsArray($Meta->getDefaultConf());
        $this->assertNotEmpty($Meta->getDefaultConf());
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_version_method(Meta $Meta): void
    {
        $Meta->getVersion($major, $minor, $patch, $text);
        $this->assertIsInt($major);
        $this->assertIsInt($minor);
        $this->assertIsInt($patch);
        $this->assertIsString($text);
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_slice_checksums_method(Meta $Meta): void
    {
        $this->assertIsArray($Meta->getSliceChecksums());
        $this->assertNotEmpty($Meta->getSliceChecksums());
    }

    /**
     * @depends test_get_Murmur_server_interface
     */
    public function test_get_murmur_version_method(Meta $Meta): void
    {
        $this->assertIsArray($Meta->getMurmurVersion());
        $this->assertNotEmpty($Meta->getMurmurVersion());
    }
}
