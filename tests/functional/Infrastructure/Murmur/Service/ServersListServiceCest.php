<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Murmur\Service;

use App\Domain\Murmur\Model\ServersListCache;
use App\Infrastructure\Murmur\Connection\ConnectionHandler;
use App\Infrastructure\Murmur\Service\ServersListService;
use App\Tests\FunctionalTester;
use Psr\Cache\CacheItemPoolInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServersListServiceCest
{
    private FunctionalTester $tester;
    private ServersListService $service;
    private CacheItemPoolInterface $cachePool;

    public function _before(FunctionalTester $I): void
    {
        $this->tester = $I;
        $this->service = $I->grabService(ServersListService::class);
        $this->cachePool = $I->grabService(CacheItemPoolInterface::class);
    }

    private function createServiceWithInvalidConnection(): ServersListService
    {
        return new ServersListService(
            new ConnectionHandler('', '', '', ''),
            $this->tester->grabService(CacheItemPoolInterface::class)
        );
    }

    private function deleteTheServersListCache(): void
    {
        $this->cachePool->deleteItem(ServersListService::CACHE_KEY);
    }

    private function forceTheExpirationOfTheCache(): void
    {
        $item = $this->cachePool->getItem(ServersListService::CACHE_KEY);
        /** @var ServersListCache $serverList */
        $serverList = $item->get();
        $serverList->setExpiresAt(-100);
        $item->set($serverList);
        $this->cachePool->save($item);
    }

    public function it_get_the_servers_list_and_refresh_the_server_list(FunctionalTester $I): void
    {
        // Given I have one Murmur server, and I have no cache
        $I->resetAllServers();
        $this->deleteTheServersListCache();

        // When I call getList() method
        $result = $this->service->getList();

        // Then I see I get a list with one server
        $I->assertSame([['id' => 1, 'name' => '']], $result);

        // When I add a new server, and I call again getList() method
        $I->addNewMurmurServer('test_server 2');
        $result = $this->service->getList();

        // Then I see I get the cached list with one server
        $I->assertSame([['id' => 1, 'name' => '']], $result);

        // When I invalidate the cache, and I call again getList() method
        $this->service->invalidateCache();
        $result = $this->service->getList();

        // Then I see I have a fresh new list with 2 servers
        $I->assertSame([['id' => 1, 'name' => ''], ['id' => 2, 'name' => 'test_server&nbsp;2']], $result);
    }

    public function it_get_empty_servers_list_on_murmur_connection_error(FunctionalTester $I): void
    {
        // Given I have no cache
        $this->deleteTheServersListCache();

        // When I call getList() method with an invalid connection to Murmur
        $service = $this->createServiceWithInvalidConnection();
        $result = $service->getList();

        // Then I see I get an empty array
        $I->assertSame([], $result);
    }

    public function it_get_the_cached_list_on_murmur_connection_error(FunctionalTester $I): void
    {
        // Given I have a fresh cached list of servers, and I force the expiration of the cache
        $this->deleteTheServersListCache();
        $I->resetAllServers();
        $I->addNewMurmurServer('test_server_3');
        $this->service->getList();
        $this->forceTheExpirationOfTheCache();

        // When I call getList() method with an invalid connection to Murmur
        $service = $this->createServiceWithInvalidConnection();
        $result = $service->getList();
        $cacheUptime = $service->getCacheUptime();

        // Then I see I get the cached server list, and the cache uptime has been renewed
        $I->assertSame([['id' => 1, 'name' => ''], ['id' => 2, 'name' => 'test_server_3']], $result);
        $I->assertContains($cacheUptime, ['00h00m00s', '00h00m01s', '00h00m02s', '00h00m03s', '00h00m04s']);
    }

    public function it_get_the_cache_uptime(FunctionalTester $I): void
    {
        // Given I have no cache
        $this->deleteTheServersListCache();

        // When I get the cache uptime
        $cacheUptime = $this->service->getCacheUptime();

        // Then I see I get nothing
        $I->assertSame('', $cacheUptime);

        // When I call getList() method, and I get the cache uptime
        $this->service->getList();
        $cacheUptime = $this->service->getCacheUptime();

        // Then I see I get the uptime
        $I->assertContains($cacheUptime, ['00h00m00s', '00h00m01s', '00h00m02s', '00h00m03s', '00h00m04s']);
    }
}
