<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Murmur\Connection;

use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\MetaInterface;
use App\Infrastructure\Murmur\Connection\ConnectionHandler;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ConnectionHandlerTest extends Unit
{
    private string $host;
    private string $port;
    private string $timeout;
    private string $secret;

    public function _before(): void
    {
        $this->host = $_ENV['MURMUR_HOST'];
        $this->port = $_ENV['MURMUR_PORT'];
        $this->timeout = $_ENV['MURMUR_TIMEOUT'];
        $this->secret = $_ENV['MURMUR_SECRET'];
    }

    public function invalidHostParametersProvider(): array
    {
        return [
            'Empty host parameter' => ['', 'empty_host_parameter'],
            'Invalid DNS (ipv4)' => ['invalidSub.invalidDomain.invalidTld', 'DNSException'],
            'Invalid ipv4' => ['395.753.456.159', 'DNSException'],
            'Connection refused (ipv4)' => ['127.127.127.127', 'ConnectionRefusedException'],
            'Invalid ipv6' => ['GGGG::GGGG', 'DNSException'],
            // Require ipv6 actived, or ipv6 tests will failed with socket exception
            'Connection refused (ipv6)' => ['0:0:0:0:0:0:0:1', 'ConnectionRefusedException'],
            'Wrong compressed ipv6' => ['::1', 'ConnectionRefusedException'],
            'Wrong surounded ipv6' => ['[::1]', 'DNSException']
        ];
    }

    /**
     * @dataProvider invalidHostParametersProvider
     */
    public function test_invalid_host($host, $eMessage): void
    {
        $this->expectException(ConnectionException::class);
        $this->expectExceptionMessage($eMessage);

        $RPC = new ConnectionHandler(
            $host,
            $this->port,
            $this->timeout,
            $this->secret
        );
        $RPC->connect();
    }

    public function invalidPortParametersProvider(): array
    {
        return [
            'Closed port' => [0, 'ConnectionRefusedException'],
            'Negative string port' => ['-6502', 'EndpointParseException: no argument provided for -p option in endpoint'],
            'Negative integer port' => [-6502, 'EndpointParseException: no argument provided for -p option in endpoint'],
            'Out of range' => [66666, 'out of range in endpoint'],
        ];
    }

    /**
     * @dataProvider invalidPortParametersProvider
     */
    public function test_invalid_port($port, $eMessage): void
    {
        $this->expectException(ConnectionException::class);
        $this->expectExceptionMessage($eMessage);

        $RPC = new ConnectionHandler(
            $this->host,
            $port,
            $this->timeout,
            $this->secret
        );
        $RPC->connect();
    }

    public function invalidTimeoutProvider(): array
    {
        return [
            'Get a timeout with unallocated IP' => [25, 'ConnectTimeoutException'],
            'Get a timeout with unallocated IP and string number' => ['25', 'ConnectTimeoutException'],
            'Get a timeout with negative number' => [-25, 'ConnectTimeoutException'],
            'Get a timeout with negative string number' => [-25, 'ConnectTimeoutException'],
            'Zero' => [0, 'EndpointParseException: invalid timeout value `0\' in endpoint']
        ];
    }

    /**
     * @dataProvider invalidTimeoutProvider
     */
    public function test_timeout($timeout, $eMessage): void
    {
        $this->expectException(ConnectionException::class);
        $this->expectExceptionMessage($eMessage);

        $RPC = new ConnectionHandler(
            '172.0.0.0',
            $this->port,
            $timeout,
            $this->secret
        );
        $RPC->connect();
    }

    public function invalidSecretProvider(): array
    {
        return [
            'Empty secret password' => [''],
            'Wrong secret password' => ['AZERDSFsdfdsfDSFSDFsfssfddsfdfsfsdfsdffsf']
        ];
    }

    /**
     * @dataProvider invalidSecretProvider
     */
    public function test_invalid_secret_password($secret): void
    {
        $this->expectException(ConnectionException::class);
        $this->expectExceptionMessage('InvalidSecretException');

        $RPC = new ConnectionHandler(
            $this->host,
            $this->port,
            $this->timeout,
            $secret
        );
        $RPC->connect();
    }

    public function validSecretProvider(): array
    {
        return [
            'Read secret password' => ['read'],
            'Write secret password' => ['write']
        ];
    }

    /**
     * @dataProvider validSecretProvider
     */
    public function test_valid_password_secret_return_Meta_class($secret): void
    {
        $RPC = new ConnectionHandler(
            $this->host,
            $this->port,
            $this->timeout,
            $secret
        );
        $Murmur = $RPC->connect();

        $this->assertInstanceOf(MetaInterface::class, $Murmur);
    }

    public function test_connect_method_return_the_shared_instance_of_meta(): void
    {
        $RPC = new ConnectionHandler($this->host, $this->port, $this->timeout, $this->secret);

        $meta1 = $RPC->connect();
        $meta2 = $RPC->connect();

        $this->assertSame($meta1, $meta2);
    }
}
