<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Domain\Server\Channels\CreateChannel;

use App\Domain\Model\HttpCode;
use App\Domain\Murmur\Model\Mock\ChannelMock;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Action\Server\Channels\CreateChannel\CreateChannelCommand;
use App\Domain\Action\Server\Channels\CreateChannel\CreateChannelHandler;
use App\Domain\Action\Server\Channels\CreateChannel\ParentChannelIsTemporary;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateChannelHandlerTest extends Unit
{
    /**
     * I can't find a way to create a temporary channel... Maybe it's impossible
     * with Murmur_Server proxy interface.
     * Anyway, mock the parent channel
     */
    public function test_it_get_negative_event_when_parent_channel_is_temporaty(): void
    {
        // Given
        $handler = new CreateChannelHandler();
        $command = new CreateChannelCommand(-1, 'NewChannel', -1);
        $command->prx = $this->makeEmpty(ServerInterface::class);
        $channel = new ChannelMock();
        $channel->temporary = true;
        $command->prx->method('getChannelState')->willReturn($channel);

        // When I call the handle method
        $response = $handler->handle($command);

        // Then I see I get negative event
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ParentChannelIsTemporary::class));
    }
}
