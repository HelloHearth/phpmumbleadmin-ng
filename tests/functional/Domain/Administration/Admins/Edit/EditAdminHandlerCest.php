<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Domain\Administration\Admins\Edit;

use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Exception\NoPermissionToModifyUserException;
use App\Domain\Action\Administration\Admins\EditAdmin\EditAdminCommand;
use App\Domain\Action\Administration\Admins\EditAdmin\EditAdminHandler;
use App\Tests\FunctionalTester;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditAdminHandlerCest
{
    private EditAdminHandler $handler;

    public function _before(FunctionalTester $I): void
    {
        $this->handler = $I->grabService(EditAdminHandler::class);
    }

    public function it_edit_admin(FunctionalTester $I): void
    {
        // Given I have an admin, and I'm logged with a role which can edit the admin
        $admin = $I->addAdminUserInRepository();
        $I->amLoggedAsSuperAdmin();
        $oldPassword = $admin->getPassword();

        // When I handle the command
        $command = new EditAdminCommand($admin, 'new_username', 'new_email@email.tld', SecurityServiceInterface::ROLE_ROOT_ADMIN, 'new Password');
        $this->handler->handle($command);

        // Then I see the admin has changed
        $I->refreshEntities([$admin]);
        $I->assertSame('new_username', $admin->getUserIdentifier());
        $I->assertSame('new_email@email.tld', $admin->getEmail());
        $I->assertSame([SecurityServiceInterface::ROLE_ROOT_ADMIN], $admin->getRoles());
        $I->assertNotSame($oldPassword, $admin->getPassword());
    }

    public function it_edit_admin_without_password(FunctionalTester $I): void
    {
        // Given I have an admin, and I'm logged with a role which can edit the admin
        $admin = $I->addAdminUserInRepository();
        $I->amLoggedAsSuperAdmin();
        $oldPassword = $admin->getPassword();

        // When I handle the command
        $command = new EditAdminCommand($admin, 'new_username', 'new_email@email.tld', SecurityServiceInterface::ROLE_ROOT_ADMIN, null);
        $this->handler->handle($command);

        // Then I see the admin has changed, excepted the password
        $I->refreshEntities([$admin]);
        $I->assertSame('new_username', $admin->getUserIdentifier());
        $I->assertSame('new_email@email.tld', $admin->getEmail());
        $I->assertSame([SecurityServiceInterface::ROLE_ROOT_ADMIN], $admin->getRoles());
        $I->assertSame($oldPassword, $admin->getPassword());
    }

    public function it_edit_admin_without_password_and_without_email(FunctionalTester $I): void
    {
        // Given I have an admin, and I'm logged with a role which can edit the admin
        $admin = $I->addAdminUserInRepository();
        $I->amLoggedAsSuperAdmin();

        // When I handle the command
        $command = new EditAdminCommand($admin, 'new_username', null, SecurityServiceInterface::ROLE_ROOT_ADMIN, null);
        $this->handler->handle($command);

        // Then I see the admin has changed, excepted the password, and the email
        $I->refreshEntities([$admin]);
        $I->assertSame('new_username', $admin->getUserIdentifier());
        $I->assertEmpty($admin->getEmail());
        $I->assertSame([SecurityServiceInterface::ROLE_ROOT_ADMIN], $admin->getRoles());
    }

    public function it_remove_admin_email(FunctionalTester $I): void
    {
        // Given I have an admin, and I'm logged with a role which can edit the admin
        $admin = $I->addAdminUserInRepository(['email' => 'email@email.tld']);
        $I->amLoggedAsSuperAdmin();

        // When I handle the command
        $command = new EditAdminCommand($admin, 'new_username', null, SecurityServiceInterface::ROLE_ADMIN, null);
        $this->handler->handle($command);

        // Then I see the email as been removed
        $I->refreshEntities([$admin]);
        $I->assertEmpty($admin->getEmail());
    }

    public function it_cant_edit_an_admin_if_connected_user_is_not_granted_to(FunctionalTester $I): void
    {
        // Given I have an admin, and I'm logged with a role which can't edit the admin
        $admin = $I->addRootAdminUserInRepository();
        $I->amLoggedAsRootAdmin();

        // When I handle the command
        $I->expectThrowable(NoPermissionToModifyUserException::class, function () use ($admin) {

            $command = new EditAdminCommand($admin, 'new_username', null, SecurityServiceInterface::ROLE_ROOT_ADMIN, null);
            $this->handler->handle($command);

        });

        // Then I see the admin did not change
        $I->refreshEntities([$admin]);
        $I->assertEquals('TestRootAdmin', $admin->getUserIdentifier());
    }

    public function it_cant_edit_an_admin_if_connected_user_is_not_granted_to_even_when_changing_the_role(FunctionalTester $I): void
    {
        // Given I have an admin, and I'm logged with a role which can't edit the admin
        $admin = $I->addRootAdminUserInRepository();
        $I->amLoggedAsRootAdmin();

        // When I handle the command, and trying to change the role
        $I->expectThrowable(NoPermissionToModifyUserException::class, function () use ($admin) {

            $command = new EditAdminCommand($admin, 'new_username', null, SecurityServiceInterface::ROLE_ADMIN, null);
            $this->handler->handle($command);

        });

        // Then I see the admin did not change
        $I->refreshEntities([$admin]);
        $I->assertEquals('TestRootAdmin', $admin->getUserIdentifier());
        $I->assertSame([SecurityServiceInterface::ROLE_ROOT_ADMIN], $admin->getRoles());
    }

    public function it_cant_edit_an_admin_role_if_connected_user_is_not_granted_to_update_the_role(FunctionalTester $I): void
    {
        // Given I have an admin, and I'm logged with a role which can edit the admin
        $admin = $I->addAdminUserInRepository();
        $I->amLoggedAsRootAdmin();

        // When I handle the command to update with unauthorized role
        $I->expectThrowable(NoPermissionToModifyUserException::class, function () use ($admin) {

            $command = new EditAdminCommand($admin, 'new_username', null, SecurityServiceInterface::ROLE_ROOT_ADMIN, null);
            $this->handler->handle($command);

        });

        // Then I see the admin did not change
        $I->refreshEntities([$admin]);
        $I->assertEquals('TestAdmin', $admin->getUserIdentifier());
        $I->assertSame([SecurityServiceInterface::ROLE_ADMIN], $admin->getRoles());
    }
}
