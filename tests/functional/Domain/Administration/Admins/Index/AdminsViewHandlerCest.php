<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Domain\Administration\Admins\Index;

use App\Domain\Service\Role\RoleNameServiceInterface;
use App\Domain\Action\Administration\Admins\Index\AdminsViewHandler;
use App\Tests\FunctionalTester;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminsViewHandlerCest
{
    private AdminsViewHandler $handler;

    public function _before(FunctionalTester $I): void
    {
        $this->handler = $I->grabService(AdminsViewHandler::class);
    }

    public function the_handler_return_only_admin_users(FunctionalTester $I): void
    {
        // Given I'm logged as SuperAdmin
        $I->amLoggedAsSuperAdmin();

        // When I call the createView method
        $view = $this->handler->createView();

        // Then I get only users with admin role
        $I->assertCount(3, $view->getAdmins());
        $I->assertSame(RoleNameServiceInterface::SUPER_ADMIN, $view->getAdmins()[0]->roleName);
        $I->assertSame(RoleNameServiceInterface::ROOT_ADMIN, $view->getAdmins()[1]->roleName);
        $I->assertSame(RoleNameServiceInterface::ADMIN, $view->getAdmins()[2]->roleName);
    }

    public function the_handler_return_only_permitted_to_modify_admins(FunctionalTester $I): void
    {
        // Given I'm logged as SuperAdmin
        $I->amLoggedAsRootAdmin();

        // When I call the createView method
        $view = $this->handler->createView();

        // Then I get only users with admin role
        $I->assertCount(1, $view->getAdmins());
        $I->assertSame(RoleNameServiceInterface::ADMIN, $view->getAdmins()[0]->roleName);
    }
}
