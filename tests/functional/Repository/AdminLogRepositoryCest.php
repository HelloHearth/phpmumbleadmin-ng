<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Repository;

use App\Repository\AdminLogRepository;
use App\Tests\FunctionalTester;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminLogRepositoryCest
{
    private AdminLogRepository $repository;

    public function _before(FunctionalTester $I): void
    {
        $this->repository = $I->grabService(AdminLogRepository::class);
    }

    public function it_get_latest_log_with_null_value(FunctionalTester $I): void
    {
        // Given I have 4 adminLog in the repository
        $log1 = $I->addAdminLogInRepository(['message' => 'message log1', 'timestamp' => 42424242]);
        $log2 = $I->addAdminLogInRepository(['message' => 'message log2', 'timestamp' => 84848484]);
        $log3 = $I->addAdminLogInRepository(['message' => 'message log3', 'timestamp' => 84848490]);
        $log4 = $I->addAdminLogInRepository(['message' => 'message log4', 'timestamp' => 84848495]);

        // When I call the getLatest() method with null value for $lastLogCount
        $result = $this->repository->getLatest(null);

        // Then I see I got all logs, ordered by ascendant
        $I->assertSame([$log4, $log3, $log2, $log1], $result);
    }

    public function it_get_latest_log_with_value_of_0(FunctionalTester $I): void
    {
        // Given I have 4 adminLog in the repository
        $log1 = $I->addAdminLogInRepository(['message' => 'message log1', 'timestamp' => 42424242]);
        $log2 = $I->addAdminLogInRepository(['message' => 'message log2', 'timestamp' => 84848484]);
        $log3 = $I->addAdminLogInRepository(['message' => 'message log3', 'timestamp' => 84848490]);
        $log4 = $I->addAdminLogInRepository(['message' => 'message log4', 'timestamp' => 84848495]);

        // When I call the getLatest() method with value of 0 for $lastLogCount
        $result = $this->repository->getLatest(0);

        // Then I see I got all logs, ordered by ascendant
        $I->assertSame([$log4, $log3, $log2, $log1], $result);
    }


    public function it_get_latest_log_with_value_of_1(FunctionalTester $I): void
    {
        // Given I have 4 adminLog in the repository
        $I->addAdminLogInRepository(['message' => 'message log1', 'timestamp' => 42424242]);
        $log2 = $I->addAdminLogInRepository(['message' => 'message log2', 'timestamp' => 84848484]);
        $log3 = $I->addAdminLogInRepository(['message' => 'message log3', 'timestamp' => 84848490]);
        $log4 = $I->addAdminLogInRepository(['message' => 'message log4', 'timestamp' => 84848495]);

        // When I call the getLatest() method with value of 1 for $lastLogCount
        $result = $this->repository->getLatest(1);

        // Then I see I got the last 3 logs, ordered by ascendant
        $I->assertSame([$log4, $log3, $log2], $result);
    }

    public function it_get_latest_log_with_value_of_2(FunctionalTester $I): void
    {
        // Given I have 4 adminLog in the repository
        $I->addAdminLogInRepository(['message' => 'message log1', 'timestamp' => 42424242]);
        $I->addAdminLogInRepository(['message' => 'message log2', 'timestamp' => 84848484]);
        $log3 = $I->addAdminLogInRepository(['message' => 'message log3', 'timestamp' => 84848490]);
        $log4 = $I->addAdminLogInRepository(['message' => 'message log4', 'timestamp' => 84848495]);

        // When I call the getLatest() method with value of 2 for $lastLogCount
        $result = $this->repository->getLatest(2);

        // Then I see I got the last 2 logs, ordered by ascendant
        $I->assertSame([$log4, $log3], $result);
    }

    public function it_get_latest_log_with_value_of_3(FunctionalTester $I): void
    {
        // Given I have 4 adminLog in the repository
        $I->addAdminLogInRepository(['message' => 'message log1', 'timestamp' => 42424242]);
        $I->addAdminLogInRepository(['message' => 'message log2', 'timestamp' => 84848484]);
        $I->addAdminLogInRepository(['message' => 'message log3', 'timestamp' => 84848490]);
        $log4 = $I->addAdminLogInRepository(['message' => 'message log4', 'timestamp' => 84848495]);

        // When I call the getLatest() method with value of 3 for $lastLogCount
        $result = $this->repository->getLatest(3);

        // Then I see I got the last log only
        $I->assertSame([$log4], $result);
    }

    public function it_get_latest_log_with_value_of_4(FunctionalTester $I): void
    {
        // Given I have 4 adminLog in the repository
        $I->addAdminLogInRepository(['message' => 'message log1', 'timestamp' => 42424242]);
        $I->addAdminLogInRepository(['message' => 'message log2', 'timestamp' => 84848484]);
        $I->addAdminLogInRepository(['message' => 'message log3', 'timestamp' => 84848490]);
        $I->addAdminLogInRepository(['message' => 'message log4', 'timestamp' => 84848495]);

        // When I call the getLatest() method with value of 4 for $lastLogCount
        $result = $this->repository->getLatest(4);

        // Then I see I got no log
        $I->assertEmpty($result);
    }

    public function it_get_latest_logs_with_superior_value(FunctionalTester $I): void
    {
        // Given I have 4 adminLog in the repository
        $I->addAdminLogInRepository(['message' => 'message log1', 'timestamp' => 42424242]);
        $I->addAdminLogInRepository(['message' => 'message log2', 'timestamp' => 84848484]);
        $I->addAdminLogInRepository(['message' => 'message log3', 'timestamp' => 84848490]);
        $I->addAdminLogInRepository(['message' => 'message log4', 'timestamp' => 84848495]);

        // When I call the getLatest() method with superior value than logs for $lastLogCount
        $result = $this->repository->getLatest(5);

        // Then I see I got no log
        $I->assertEmpty($result);
    }
}
