<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Login;

use App\Tests\AcceptanceTester;
use App\Tests\Helper\Prepare;
use App\Tests\Page\DashboardPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LoginCest
{
    use Prepare;

    /** @prepare DbPopulate */
    public function it_login_the_superadmin(AcceptanceTester $I): void
    {
        // Given
        $I->amNotLogged();

        // When
        $I->amLoggedAsSuperAdmin();

        // Then
        $I->seeCurrentUrlMatches('#'.DashboardPage::LOCATION.'$#');
        $I->seeAmLoggedAsSuperAdmin();
    }
}
