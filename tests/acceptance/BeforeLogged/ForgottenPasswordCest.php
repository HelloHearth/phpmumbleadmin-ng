<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\BeforeLogged;

use App\Tests\AcceptanceTester;
use App\Tests\Helper\Prepare;
use App\Tests\Page\ForgottenPasswordPage as Page;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordCest
{
    use Prepare;

    /** @prepare WebDriverDontClearCookies */
    public function it_get_the_forgotten_password_page(AcceptanceTester $I): void
    {
        // Given I'm on the login page
        $I->amOnPage(LoginPage::LOCATION);

        // When I click the link of the forgotten password
        $I->click('forgot_password');

        // Then I see I've been directed to the forgotten password page
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeElement(Page::FORM_ELEMENT);
        $I->see('retrieve_your_password');
    }

    /** @depends it_get_the_forgotten_password_page */
    public function it_submit_a_username_with_the_form(AcceptanceTester $I): void
    {
        // Given I fill the login fields of the forgotten password page
        $I->fillField('form input[name="'.Page::FORM_BLOCK_PREFIX.'[username]"]', 'SA');

        // When I submit the form
        $I->click('submit', Page::FORM_ELEMENT);

        // Then I see a message announcing the processing of forgotten password
        $I->see('email_has_been_sent');
        $I->dontSeeElement(Page::FORM_ELEMENT);
    }

    public function it_submit_a_reset_password_with_the_form(AcceptanceTester $I): void
    {
        // Given I have a ForgottenPassword set
        $I->haveInDatabase('forgotten_password', [
            'admin_id' => 1,
            'hash' => 'dummy_hash',
            'created_at' => (new \DateTimeImmutable('-1 minute'))->format('Y-m-d H:i:s'),
            'valid_until' => (new \DateTimeImmutable('+1 hour'))->format('Y-m-d H:i:s'),
        ]);

        // When I'm on the reset page, I fill the new password form, and I submit
        $I->amOnPage(Page::LOCATION_RESET.'/dummy_hash');
        $I->fillField('form input[name="'.Page::FORM_RESET_BLOCK_PREFIX.'[password][first]"]', 'newPassword');
        $I->fillField('form input[name="'.Page::FORM_RESET_BLOCK_PREFIX.'[password][second]"]', 'newPassword');
        $I->click('submit', Page::FORM_RESET_ELEMENT);

        // Then I see a message announcing the password has been reset
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
        $I->seeSuccessFlashMessage('password_has_been_reset');
    }
}
