<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\BeforeLogged;

use App\Tests\AcceptanceTester;
use App\Tests\Page\Cookie;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChangeLanguageCest
{
    public function it_see_the_default_language_is_in_the_html(AcceptanceTester $I): void
    {
        // Given I don't have any option cookie
        $I->resetCookie(Cookie::OPTIONS);

        // When I'm on the login page
        $I->amOnPage(LoginPage::LOCATION);

        // Then I see the default language have been set (fr)
        $I->seeTheLocaleIsSelectedInTheMenu('fr');
        $I->seeTheLocaleIsSelectedInDOM('fr');
    }

    /** @depends it_see_the_default_language_is_in_the_html */
    public function it_change_locale_parameter_on_the_menu(AcceptanceTester $I): void
    {
        // When I select the English language in the menu
        $I->selectTheLocaleOnTheMenu('en');

        // Then I see the English language have been set
        $I->seeTheLocaleIsSelectedInTheMenu('en');
        $I->seeTheLocaleIsSelectedInDOM('en');
        $I->seeInTheOptionsCookieTheLanguageIs('en');
    }

    /** @depends it_change_locale_parameter_on_the_menu */
    public function it_change_back_the_locale_parameter_on_the_menu(AcceptanceTester $I): void
    {
        // When I select the French language
        $I->selectTheLocaleOnTheMenu('fr');

        // Then I see the French language have been set
        $I->seeTheLocaleIsSelectedInTheMenu('fr');
        $I->seeTheLocaleIsSelectedInDOM('fr');
        $I->seeInTheOptionsCookieTheLanguageIs('fr');
    }
}
