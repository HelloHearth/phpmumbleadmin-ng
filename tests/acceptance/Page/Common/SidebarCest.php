<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Common;

use App\Tests\AcceptanceTester;
use App\Tests\Page\AdminsPage;
use App\Tests\Page\DashboardPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SidebarCest
{
    private const MENU = '#layoutSidenav_nav .sb-sidenav-menu';
    private const NAV_LINK = self::MENU.' .nav-link';

    public function it_toggle_the_sidebar_administration_dropdown(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(DashboardPage::LOCATION);
        $I->see('dashboard', self::NAV_LINK.'.active');
        $I->see('administration', self::NAV_LINK);
        $I->dontSee('administration', self::NAV_LINK.'.active');
        $I->dontSee('admins', self::NAV_LINK);

        // When
        $I->click('administration', self::MENU);

        // Then
        $I->waitForElementVisible(self::MENU.' #collapseAdministration');
        \usleep(500_000); // Wait for bootstrap animation ending

        // When
        $I->click('administration', self::MENU);

        // Then
        $I->waitForElementNotVisible(self::MENU.' #collapseAdministration');
    }

    public function it_check_that_the_menu_activate_current_route(AcceptanceTester $I): void
    {
        // Given I'm logged as SuperAdmin
        $I->amLoggedAsSuperAdmin();

        // When I'm on page admins
        $I->amOnPage(AdminsPage::LOCATION);

        // Then I see the administration tab of the menu is open and active, and the admins element is also active
        $I->see('administration', self::NAV_LINK.'.active');
        $I->see('admins', self::NAV_LINK.'.active');
        // The dashboard link is no more active
        $I->see('dashboard', self::NAV_LINK);
        $I->dontSee('dashboard', self::NAV_LINK.'.active');
    }
}
