<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Common;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ModalCancelButtonCest
{
    public function it_test_the_modal_close_button_block(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(DashboardPage::LOCATION);

        // When
        $I->openInTheUserMenu('a[data-bs-target="#logoutModal"]');
        $I->waitForModalFadeEffect('#logoutModal');
        $I->click('cancel', '#logoutModal .modal-footer');

        // Then
        $I->waitForElementNotVisible('#logoutModal');
        $I->dontSeeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
    }
}
