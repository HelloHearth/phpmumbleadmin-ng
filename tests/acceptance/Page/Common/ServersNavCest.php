<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Common;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage;
use App\Tests\Page\ServerPage;
use App\Tests\Page\ServerSettingsPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServersNavCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->deleteTheSfCachePool();
        $I->resetAllServers();
        $meta = $I->getMurmurMetaInterface();
        $prx = $meta->newServer();
        $prx->setConf('registername', 'test server   option');
        $meta->newServer();
    }

    private function deleteTheFirstServer(AcceptanceTester $I): void
    {
        $prx = $I->getTheTestServer();
        $prx->stop();
        $prx->delete();
    }

    public function it_navigate_with_the_servers_list_buttons(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(DashboardPage::LOCATION);
        $I->seeCurrentUrlMatches('#'.DashboardPage::LOCATION.'$#');
        $I->see('00h00m0', 'form#select-server select');
        $I->see('1 -', 'form#select-server select option:nth-child(2)');
        $I->see('2 - test server   option', 'form#select-server select option:nth-child(3)');
        $I->see('3 -', 'form#select-server select option:nth-child(4)');

        // When
        $I->selectOption('form#select-server select', '1 -');

        // Then
        $I->seeCurrentUrlMatches('#'.ServerPage::PATH.'/1$#');

        // When
        $I->amOnPage(ServerSettingsPage::LOCATION);
        $I->selectOption('form#select-server select', '2 - test server   option');

        // Then
        $I->seeCurrentUrlMatches('#'.ServerPage::PATH.'/2'.ServerSettingsPage::PATH.'$#');

        // Given
        $this->deleteTheFirstServer($I);

        // When (I'm using the refresh server list cache button)
        $I->click('#servers-nav a:nth-child(1)');

        // Then
        $I->dontSee('1 -', 'form#select-server select option');
        $I->see('2 - test server   option', 'form#select-server select option:nth-child(2)');
        $I->see('3 -', 'form#select-server select option:nth-child(3)');
    }
}
