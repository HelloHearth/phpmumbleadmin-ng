<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Administration\AdminLogs;

use App\Tests\AcceptanceTester;
use App\Tests\Page\AdminLogsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminLogsCest
{
    // Given I have 3 logs on database, and I'm logged as SuperAdmin
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
        $I->haveAdminLogInDatabase(['message' => 'message log1', 'timestamp' => 42424242]);
        $I->haveAdminLogInDatabase(['message' => 'message log2', 'timestamp' => 84848484]);
        $I->haveAdminLogInDatabase(['message' => 'message log3', 'context' => ['%id%' => '42'], 'timestamp' => 84848490]);
    }

    public function it_get_logs_page(AcceptanceTester $I): void
    {
        // Given...

        // When I get the admin logs page
        $I->amOnPage(Page::LOCATION);

        // Then I see in the menu the page is selected, and the container
        $I->seeOnSideMenuIsSelected(['administration', 'logs']);
        $I->seeElement(Page::CONTAINER);
        //
        // Then I see 2 different days, for 3 logs
        $I->seeDayElementsAreUnique(Page::CONTAINER);
        $I->seeNumberOfDayOnLogsElements(2, Page::CONTAINER);
        $I->seeNumberOfLogElements(3, Page::CONTAINER);
        //
        $I->seeDay('9 septembre 1972', 1, Page::CONTAINER);
        $I->seeLog('02:01:30', 'message log3 {"%id%":"42"}', 2, Page::CONTAINER);
        $I->seeLog('02:01:24', 'message log2', 3, Page::CONTAINER);
        //
        $I->seeDay('7 mai 1971', 4, Page::CONTAINER);
        $I->seeLog('01:30:42', 'message log1', 5, Page::CONTAINER);
    }

    public function it_wait_for_new_log(AcceptanceTester $I): void
    {
        // Given I have a new log in database
        $I->haveAdminLogInDatabase(['message' => 'new log message', 'timestamp' => 84848499]);

        // When I wait for it apparition
        $I->waitForNewLog('new log message', Page::CONTAINER);

        // Then I see 2 different days, for 4 logs, updated with javascript:
        $I->seeNumberOfDayOnLogsElements(2, Page::CONTAINER);
        $I->seeNumberOfLogElements(4, Page::CONTAINER);
        //
        $I->seeDay('9 septembre 1972', 1, Page::CONTAINER);
        $I->seeNewLog('02:01:39', 'new log message', 2, Page::CONTAINER);
        $I->seeLog('02:01:30', 'message log3 {"%id%":"42"}', 3, Page::CONTAINER);
        $I->seeLog('02:01:24', 'message log2', 4, Page::CONTAINER);
        //
        $I->seeDay('7 mai 1971', 5, Page::CONTAINER);
        $I->seeLog('01:30:42', 'message log1', 6, Page::CONTAINER);
    }
}
