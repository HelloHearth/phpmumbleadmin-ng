<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Administration\Admins;

use App\Tests\AcceptanceTester;
use App\Tests\Helper\Prepare;
use App\Tests\Page\AdminsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminsCest
{
    use Prepare;

    /** @prepare DbPopulate */
    public function it_display_the_admin_user_table(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);

        // I see the Datatables plugin is enabled
        $I->seeDatatablesPluginIsEnabled(Page::TABLE_ELEMENT);
        $I->seeNumberOfLineInTable(Page::TABLE_ELEMENT, 3);

        // I see cells in the login column
        $I->seeInTableHeader('login', Page::TABLE_ELEMENT, Page::TABLE_CELL_LOGIN);
        $I->seeInTable('SA', Page::TABLE_ELEMENT, 1, Page::TABLE_CELL_LOGIN);
        $I->seeInTable('rootadmin', Page::TABLE_ELEMENT, 2, Page::TABLE_CELL_LOGIN);
        $I->seeInTable('admin', Page::TABLE_ELEMENT, 3, Page::TABLE_CELL_LOGIN);

        // I see cells in the role column
        $I->seeInTableHeader('role', Page::TABLE_ELEMENT, Page::TABLE_CELL_ROLE);
        $I->seeInTable('SuperAdmin', Page::TABLE_ELEMENT, 1, Page::TABLE_CELL_ROLE);
        $I->seeInTable('RootAdmin', Page::TABLE_ELEMENT, 2, Page::TABLE_CELL_ROLE);
        $I->seeInTable('Admin', Page::TABLE_ELEMENT, 3, Page::TABLE_CELL_ROLE);

        // I see cells in the id column
        $I->seeInTable('1', Page::TABLE_ELEMENT, 1, Page::TABLE_CELL_ID);
        $I->seeInTable('2', Page::TABLE_ELEMENT, 2, Page::TABLE_CELL_ID);
        $I->seeInTable('3', Page::TABLE_ELEMENT, 3, Page::TABLE_CELL_ID);
    }
}
