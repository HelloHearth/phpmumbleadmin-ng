<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Administration\Admins;

use App\Domain\Action\Administration\Admins\CreateAdmin\AdminCreated;
use App\Domain\Service\Role\RoleNameServiceInterface;
use App\Tests\AcceptanceTester;
use App\Tests\Helper\Prepare;
use App\Tests\Page\AdminsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateAdminCest
{
    use Prepare;

    /** @prepare DbPopulate */
    public function it_create_an_admin(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);

        // When I open the modal, fill the fields with existing login, and click to submit
        $I->click(Page::CONTAINER.' button[title="create_admin"]');
        $I->waitForModalFadeEffect(Page::MODAL_CREATE);
        $I->seeFieldIsRequiredAndFillField(Page::CREATE_FORM_PREFIX.'[login]', 'admin');
        $I->seeFieldIsRequiredAndFillField(Page::CREATE_FORM_PREFIX.'[password][first]', 'test password field');
        $I->seeFieldIsRequiredAndFillField(Page::CREATE_FORM_PREFIX.'[password][second]', 'bad repeated password');
        $I->click(Page::MODAL_CREATE.' form [type="submit"]');

        // Then I see the form has errors
        $I->waitForTheLoadingAnimation();
        $I->waitForFormError('validator_login_exists');
        $I->seeFormError('validator_password_not_identical');
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeNumberOfLineInTable(Page::TABLE_ELEMENT, 3);
        $I->seeElement(Page::MODAL_CREATE);

        // When I retry with valid data
        $I->seeFieldIsRequiredAndFillField(Page::CREATE_FORM_PREFIX.'[login]', 'test login field');
        $I->seeFieldIsRequiredAndFillField(Page::CREATE_FORM_PREFIX.'[email]', 'test_email@field.tld');
        $I->seeFieldIsRequiredAndFillField(Page::CREATE_FORM_PREFIX.'[password][first]', 'test password field');
        $I->seeFieldIsRequiredAndFillField(Page::CREATE_FORM_PREFIX.'[password][second]', 'test password field');
        $I->selectOption(Page::CREATE_FORM_PREFIX.'[role]', RoleNameServiceInterface::ROOT_ADMIN);
        $I->click(Page::MODAL_CREATE.' form [type="submit"]');

        // Then I see the new admin
        $I->waitForSuccessFlashMessage(AdminCreated::KEY);
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeNumberOfLineInTable(Page::TABLE_ELEMENT, 4);
        $I->seeInTable('test login field', Page::TABLE_ELEMENT, 4, Page::TABLE_CELL_LOGIN);
        $I->seeInTable(RoleNameServiceInterface::ROOT_ADMIN, Page::TABLE_ELEMENT, 4, Page::TABLE_CELL_ROLE);
        $I->seeInTable('4', Page::TABLE_ELEMENT, 4, Page::TABLE_CELL_ID);
    }
}
