<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Administration\Admins;

use App\Tests\AcceptanceTester;
use App\Tests\Helper\Prepare;
use App\Tests\Page\AdminsPage as Page;
use App\Tests\Page\ModalPage as Modal;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteAdminCest
{
    use Prepare;

    /** @prepare DbPopulate */
    public function it_delete_an_admin(AcceptanceTester $I): void
    {
        // Given I'm on the admins index page, I see the admin user
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeInTable('Admin', Page::TABLE_ELEMENT, 3, Page::TABLE_CELL_ROLE);

        // When I open the delete modal, and click the confirmation button
        $I->click(Page::CONTAINER.' table tbody tr:nth-child(3) td '.Modal::DELETE_TRIGGER);
        $I->confirmToDeleteOnTheModal();

        // Then I dont see anymore the admin user in the admins table
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeNumberOfLineInTable(Page::TABLE_ELEMENT, 2);
        $I->seeInTable('SA', Page::TABLE_ELEMENT, 1, Page::TABLE_CELL_LOGIN);
        $I->seeInTable('rootadmin', Page::TABLE_ELEMENT, 2, Page::TABLE_CELL_LOGIN);
        $I->seeSuccessFlashMessage('admin_deleted');
    }
}
