<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeactivatePrioritySpeackerCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $I->haveOneMumbleClient();

        $prx = $I->getTheTestServer();

        foreach ($prx->getUsers() as $user) {
            if (Page::NODEJS_CLIENT1_NAME !== $user->name) {
                continue;
            }
            $user->prioritySpeaker = true;
            $prx->setState($user);
        }
    }

    public function it_deactivate_priority_speaker_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeElement(Page::VIEWER_CHANNELS.' .user img[alt="status_user_with_priority_speaker"]');

        // When
        $I->selectMumbleClientInTheViewer();
        $I->openTheActionMenuInTheViewer();
        $I->click(Page::VIEWER_MENU.' a[data-bs-target="#deactivatePrioritySpeaker"]');

        // Then
        $I->waitForElementNotVisible(Page::VIEWER_CHANNELS.' .user img[alt="status_user_with_priority_speaker"]');
    }
}
