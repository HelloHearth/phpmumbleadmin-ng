<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MoveUserCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();
        $prx->addChannel('channel', 0);
    }

    public function it_move_users_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->haveOneMumbleClient();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->see(Page::NODEJS_CLIENT1_NAME, Page::VIEWER_CHANNELS.' div:nth-child(2)');

        // When
        $I->dragAndDrop(Page::VIEWER_CHANNELS.' .user #u-1', Page::VIEWER_CHANNELS.' .channel #c-1');

        // Then
        $I->waitForElement(Page::VIEWER_CHANNELS.' div:nth-child(3) #u-1');
        $I->dontSee(Page::JS_CONFIRM_MODAL);
    }
}
