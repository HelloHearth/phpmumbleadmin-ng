<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Domain\Action\Server\Channels\DeleteChannel\DeleteRootChannelIsForbidden;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteChannelCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
    }

    public function it_initialize_the_server(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();

        // I have a channel below the root channel
        $prx->addChannel('channel_to_delete', 0);
    }

    /** @depends it_initialize_the_server */
    public function it_get_the_channels_page(AcceptanceTester $I): void
    {
        // When I'm on the page of server channels
        $I->amOnPage(Page::LOCATION);

        // Then I see the created channel, and the selected channel is Root
        $I->seeTheChannelInTheViewer('channel_to_delete');
        $I->seeTheChannelInTheViewerIsSelected('root');
    }

    /** @depends it_get_the_channels_page */
    public function it_delete_the_root_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // When I select delete channel modal, and confirm to delete the root channel
        $I->clickTheActionButtonInTheViewer('#deleteChannelModal');
        $I->clickTheSubmitButtonOfTheModalForm('#deleteChannelModal');

        // Then I see I get an alert message on the modal
        $I->waitForTheLoadingAnimationToEnd();
        $I->waitForText(DeleteRootChannelIsForbidden::KEY, 10, '#deleteChannelModal');
    }

    /** @depends it_delete_the_root_channel_with_the_viewer */
    public function it_verify_the_modal_has_reset_the_alert_message(AcceptanceTester $I): void
    {
        // When I close the modal, select the sub channel, and reopen the modal
        $I->click('cancel', '#deleteChannelModal');
        $I->selectChannelInTheViewer('channel_to_delete');
        $I->seeTheChannelInTheViewerIsSelected('channel_to_delete');
        $I->clickTheActionButtonInTheViewer('#deleteChannelModal');

        // Then I don't see the alert message anymore
        $I->dontSee(DeleteRootChannelIsForbidden::KEY, '#deleteChannelModal');
    }

    /** @depends it_verify_the_modal_has_reset_the_alert_message */
    public function it_delete_a_sub_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // When I confirm to delete the sub channel
        $I->clickTheSubmitButtonOfTheModalForm('#deleteChannelModal');

        // Then I don't see the channel on the viewer anymore
        $I->waitForModalClosureWithFadeEffect('#deleteChannelModal');
        $I->dontSeeTheChannelInTheViewer('channel_to_delete');
        $I->waitForSuccessJsFlashMessage('channel_deleted');
    }
}
