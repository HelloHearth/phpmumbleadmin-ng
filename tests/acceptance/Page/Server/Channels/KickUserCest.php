<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class KickUserCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_kick_one_user_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->haveOneMumbleClient();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeTheMumbleUserInTheViewer(Page::NODEJS_CLIENT1_NAME);

        // When
        $I->selectMumbleClientInTheViewer();
        $I->clickTheActionButtonInTheViewer('#kickUserModal');
        $I->clickTheSubmitButtonOfTheModalForm('#kickUserModal');

        // Then
        $I->waitForSuccessJsFlashMessage('user_has_been_kicked_off_the_server {"%user%":"'.Page::NODEJS_CLIENT1_NAME.'"}');
        $I->waitForElementNotVisible(Page::VIEWER_CHANNELS.' .user');
        $I->dontSeeTheMumbleUserInTheViewer(Page::NODEJS_CLIENT1_NAME);
    }
}
