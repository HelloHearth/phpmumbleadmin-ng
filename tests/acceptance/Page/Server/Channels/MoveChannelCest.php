<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MoveChannelCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();

        $prx = $I->getTheTestServer();
        $prx->addChannel('channel_to_move', 0);
        $prx->addChannel('channel_to_move_to', 0);
    }

    public function it_move_a_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) #c-1');

        // When
        $I->dragAndDrop(Page::VIEWER_CHANNELS.' #c-1 .name', Page::VIEWER_CHANNELS.' #c-2 .name');
        $I->waitForElementVisible(Page::JS_CONFIRM_MODAL);
        $I->see('move channel 1 to 2', Page::JS_CONFIRM_MODAL);
        $I->click(Page::JS_CONFIRM_MODAL.' button.jconfirm-submit-action');

        // Then
        $I->waitForElement(Page::VIEWER_CHANNELS.' div:nth-child(3) #c-1');
        $I->dontSee(Page::JS_CONFIRM_MODAL);
    }
}
