<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Domain\Action\Server\Channels\NestingLimitReached;
use App\Infrastructure\Symfony\Form\CreateChannelType;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateChannelCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
    }

    public function it_initialize_the_server(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();

        // I have a channel below the root channel
        $prx->addChannel('sub_root_channel', 0);
        // I set the condition for reaching channel nesting limit Exception
        $prx->setConf('channelnestinglimit', '3');
    }

    /** @depends it_initialize_the_server */
    public function it_create_a_channel_with_the_viewer_and_with_invalid_character(AcceptanceTester $I): void
    {
        // Given I'm on the channel page of the server, and I see the sub_root_channel is a sub-channel of root
        $I->amOnPage(Page::LOCATION);
        $I->seeNumberOfElements(Page::VIEWER_CHANNELS.' .channel:nth-child(2) .branchArea img', 1);

        // When I select the sub_root_channel, and I create a channel with invalid characters
        $I->selectChannelInTheViewer('sub_root_channel');
        $I->clickTheActionButtonInTheViewer('#createChannelModal');
        $I->fillField('#createChannelModal form input[name="'.CreateChannelType::BLOCK_PREFIX.'[name]"]', 'invalid°character');
        $I->clickTheSubmitButtonOfTheModalForm('#createChannelModal');

        // Then I see I get a form error
        $I->waitForTheLoadingAnimationToEnd();
        $I->waitForText(ChannelNameIsInvalid::KEY, 10, '#createChannelModal form .modal-body .form-error-message');
    }

    /** @depends it_create_a_channel_with_the_viewer_and_with_invalid_character */
    public function it_create_a_sub_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // Given I don't see the channel on the viewer
        $I->dontSeeTheChannelInTheViewer('Test_create_sùbchannél_with_valid_chàractèrs');

        // When I create a channel with valid characters
        $I->fillField('#createChannelModal form input[name="'.CreateChannelType::BLOCK_PREFIX.'[name]"]', 'Test_create_sùbchannél_with_valid_chàractèrs');
        $I->clickTheSubmitButtonOfTheModalForm('#createChannelModal');

        // Then I see the created channel is a sub-channel of sub_root_channel
        $I->waitForElementNotVisible('#createChannelModal');
        $I->waitForText('Test_create_sùbchannél_with_valid_chàractèrs', 10, Page::VIEWER_CHANNELS);
        $I->seeNumberOfElements(Page::VIEWER_CHANNELS.' .channel:nth-child(3) .branchArea img', 2);
        $I->waitForSuccessJsFlashMessage('channel_created');
    }

    /** @depends it_create_a_sub_channel_with_the_viewer */
    public function it_see_the_form_has_been_reseted(AcceptanceTester $I): void
    {
        // When I select the created channel, I can submit the form again
        $I->selectChannelInTheViewer('Test_create_sùbchannél_with_valid_chàractèrs');
        $I->clickTheActionButtonInTheViewer('#createChannelModal');

        // Then I don't see the form error anymore
        $I->dontSee(ChannelNameIsInvalid::KEY, '#createChannelModal');
    }

    /** @depends it_see_the_form_has_been_reseted */
    public function it_create_a_second_sub_channels_with_the_viewer_to_check_jquery_and_vue_js_are_working_as_intented(AcceptanceTester $I): void
    {
        // When I submit the form again for a new sub sub channel
        $I->fillField('#createChannelModal form input[name="'.CreateChannelType::BLOCK_PREFIX.'[name]"]', 'second-channel');
        $I->clickTheSubmitButtonOfTheModalForm('#createChannelModal');

        // Then I see the created channel is a sub-sub-channel of root
        $I->waitForElementNotVisible('#createChannelModal');
        $I->waitForText('second-channel', 10, Page::VIEWER_CHANNELS);
        $I->seeNumberOfElements(Page::VIEWER_CHANNELS.' .channel:nth-child(4) .branchArea img', 3);
        $I->waitForSuccessJsFlashMessage('channel_created');
    }

    /** @depends it_create_a_second_sub_channels_with_the_viewer_to_check_jquery_and_vue_js_are_working_as_intented */
    public function it_create_a_3rd_sub_channel_with_channel_nesting_limit_of_3(AcceptanceTester $I): void
    {
        // When I submit for a 3rd sub channel, with a channelnestinglimit of 3
        $I->selectChannelInTheViewer('second-channel');
        $I->clickTheActionButtonInTheViewer('#createChannelModal');
        $I->fillField('#createChannelModal form input[name="'.CreateChannelType::BLOCK_PREFIX.'[name]"]', 'third-channel');
        $I->clickTheSubmitButtonOfTheModalForm('#createChannelModal');

        // Then I see the created channel is a sub-sub-channel of root
        $I->waitForTheLoadingAnimationToEnd();
        $I->waitForText(NestingLimitReached::KEY, 10, '#createChannelModal form .modal-body .form-error-message');
    }

    /** @depends it_create_a_3rd_sub_channel_with_channel_nesting_limit_of_3 */
    public function it_see_the_form_has_been_reseted_after_closing_the_modal(AcceptanceTester $I): void
    {
        // Given I see the error message and the field is not empty
        $I->see(NestingLimitReached::KEY, '#createChannelModal');
        $I->seeInField('#createChannelModal form input[name="'.CreateChannelType::BLOCK_PREFIX.'[name]"]', 'third-channel');

        // When I close, and reopen the form
        $I->click('cancel', '#createChannelModal');
        $I->waitForModalClosureWithFadeEffect('#createChannelModal');
        $I->clickTheActionButtonInTheViewer('#createChannelModal');

        // Then I see the form has been rested
        $I->dontSee(NestingLimitReached::KEY, '#createChannelModal');
        $I->dontSeeInField('#createChannelModal form input[name="'.CreateChannelType::BLOCK_PREFIX.'[name]"]', 'third-channel');
    }
}
