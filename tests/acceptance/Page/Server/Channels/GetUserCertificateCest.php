<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class GetUserCertificateCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $I->haveOneMumbleClient(Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME);
    }

    public function it_get_user_certificate_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);

        // When I select the Mumble user with a certificate in the viewer, and click on the hash link
        $I->selectMumbleClientInTheViewer(Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME);
        $I->click('#MumbleViewerPanel .clickable');

        // Then I see the modal with the certificate in it
        $I->waitForElementVisible('#userCertificateModal');
        $I->waitForText('Internet Widgits Pty Ltd', 3, '#userCertificateModal .certificate');
        $I->see('82:75:A4:5E:F3:23:C5:A9:50:45:03:D7:E5:73:1C:F0:7A:B3:00:71', '#userCertificateModal .certificate');
    }
}
