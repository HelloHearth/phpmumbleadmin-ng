<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Domain\Helper\IpHelper;
use App\Infrastructure\Symfony\Form\BanUserType;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BanUserCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
    }

    public function it_initialize_the_server(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $I->haveOneMumbleClient(Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME);

        $prx = $I->getTheTestServer();

        $I->assertCount(0, $prx->getBans());
    }

    /** @depends it_initialize_the_server */
    public function it_get_the_channels_page(AcceptanceTester $I): void
    {
        // Given I'm on channels page
        $I->amOnPage(Page::LOCATION);

        // Then I see the client
        $I->seeTheMumbleUserInTheViewer(Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME);
    }

    /** @depends it_get_the_channels_page */
    public function it_open_the_ban_user_modal(AcceptanceTester $I): void
    {
        $I->selectMumbleClientInTheViewer(Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME);
        $I->clickTheActionButtonInTheViewer('#banUserModal');
    }

    /** @depends it_open_the_ban_user_modal */
    public function it_check_the_permanent_checkbox_javascript(AcceptanceTester $I): void
    {
        // Given I see the permanent checkbox is checked, and the pick date element is disabled
        $I->seeCheckboxIsChecked('#banUserModal [name="'.BanUserType::BLOCK_PREFIX.'[endDate][permanent]"]');
        $I->seeElement('#banUserModal [name="'.BanUserType::BLOCK_PREFIX.'[endDate][date]"][disabled="disabled"]');

        // When I uncheck the permanent checkbox
        $I->uncheckOption('#banUserModal [name="'.BanUserType::BLOCK_PREFIX.'[endDate][permanent]"]');

        // Then I see the pick date element is now enabled
        $I->dontSeeElement('#banUserModal [name="'.BanUserType::BLOCK_PREFIX.'[endDate][date]"][disabled="disabled"]');
        $I->seeElement('#banUserModal [name="'.BanUserType::BLOCK_PREFIX.'[endDate][date]"]');
    }

    /** @depends it_check_the_permanent_checkbox_javascript */
    public function it_ban_an_unregistred_user_with_the_viewer(AcceptanceTester $I): void
    {
        // Given I fill the inputs
        $I->fillField('#banUserModal [name="'.BanUserType::BLOCK_PREFIX.'[reason]"]', 'test reason');
        $I->fillField('#banUserModal [name="'.BanUserType::BLOCK_PREFIX.'[endDate][date]"]', '07-20-002036-1515');

        // When I submit the ban user form
        $I->clickTheSubmitButtonOfTheModalForm('#banUserModal');

        // Then I don't see anymore the user on the viewer
        $I->waitForElementNotVisible(Page::VIEWER_CHANNELS.' .user');
        $I->waitForSuccessJsFlashMessage('ban_created');
    }

    /** @depends it_ban_an_unregistred_user_with_the_viewer */
    public function it_see_the_ban_has_been_created(AcceptanceTester $I): void
    {
        $prx = $I->getTheTestServer();
        $bans = $prx->getBans();
        $banAddress = IpHelper::decimalTostring($bans[0]->address)['ip'];
        $expectedDuration = (new \DateTimeImmutable('2036-07-20T1515'))->getTimestamp() - \time();

        $I->assertCount(1, $bans);
        $I->assertMatchesRegularExpression('/^172.18.2.[2-4]$/', $banAddress);
        $I->assertSame(128, $bans[0]->bits);
        $I->assertSame(Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME, $bans[0]->name);
        $I->assertSame('test reason', $bans[0]->reason);
        $I->assertSame('e6a41cd09e719aa72ee531e92ad097babaeee9bb', $bans[0]->hash);
        $I->assertEqualsWithDelta(\time(), $bans[0]->start, 3);
        $I->assertEqualsWithDelta($expectedDuration, $bans[0]->duration, 3);
    }
}
