<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Domain\Action\Server\Channels\ModifyUserName\UserNameModified;
use App\Infrastructure\Symfony\Form\ModifyUserNameType;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ModifyUserNameCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_modify_user_name_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->haveOneMumbleClient();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeTheMumbleUserInTheViewer(Page::NODEJS_CLIENT1_NAME);

        // When
        $I->selectMumbleClientInTheViewer();
        $I->clickTheActionButtonInTheViewer('#modifyUserNameModal');

        // Then
        $I->waitForText('modify_user_name_info', 3, '#modifyUserNameModal');

        // When
        $I->fillField('#modifyUserNameModal [name="'.ModifyUserNameType::BLOCK_PREFIX.'[newName]"]', 'test new name');
        $I->clickTheSubmitButtonOfTheModalForm('#modifyUserNameModal');

        // Then
        $I->waitForText('test new name', 3, Page::VIEWER_CHANNELS.' .user');
        $I->waitForSuccessJsFlashMessage(UserNameModified::KEY);
    }
}
