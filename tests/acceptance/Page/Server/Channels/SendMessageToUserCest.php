<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Domain\Action\Server\Channels\SendMessageToUser\MessageSentToUser;
use App\Infrastructure\Symfony\Form\SendMessageToUserType;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToUserCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_send_a_message_to_user_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->haveOneMumbleClient();
        $I->amOnPage(Page::LOCATION);
        $I->seeTheMumbleUserInTheViewer(Page::NODEJS_CLIENT1_NAME);

        // When
        $I->selectMumbleClientInTheViewer();
        $I->clickTheActionButtonInTheViewer('#sendMessageToUserModal');

        // Then
        $I->waitForElementHasFocus('#sendMessageToUserModal [name="'.SendMessageToUserType::BLOCK_PREFIX.'[message]"]');

        // When
        $I->fillField('#sendMessageToUserModal [name="'.SendMessageToUserType::BLOCK_PREFIX.'[message]"]', 'test message sent with phpMumbleAdmin');
        $I->clickTheSubmitButtonOfTheModalForm('#sendMessageToUserModal');

        // Then
        $I->waitForElementNotVisible('#sendMessageToUserModal');
        $I->waitForSuccessJsFlashMessage(MessageSentToUser::KEY);
    }
}
