<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Bans;

use App\Domain\Helper\IpHelper;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ModalPage;
use App\Tests\Page\ServerBansPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteBanCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $I->haveMurmurBansOnTheTestServer([
            new BanMock(IpHelper::stringToDecimalIPv4('127.0.0.1'), 128, 'test username', 'test hash', 'test reason', 1531673100)
        ]);
    }

    public function it_delete_a_ban_in_the_bans_page(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->see('127.0.0.1', Page::TABLE_ELEMENT);

        // When
        $I->click(Page::TABLE_ELEMENT.' tr:nth-child(1) td '.ModalPage::DELETE_TRIGGER);
        $I->confirmToDeleteOnTheModal();

        // Then
        $I->seeInCurrentUrl(Page::LOCATION);
        $I->dontSee('127.0.0.1', Page::TABLE_ELEMENT);
        $I->seeSuccessFlashMessage('ban_deleted');
    }
}
