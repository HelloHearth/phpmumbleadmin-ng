<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Registrations;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ModalPage;
use App\Tests\Page\ServerRegistrationsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteRegistrationCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();
        $prx->registerUser(['test_user']);
    }

    public function it_delete_a_registration_in_the_registrations_page(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->see('test_user', Page::CONTAINER.' table');
        // I expect to not see the delete button for the SuperUser
        $I->dontSeeElement(Page::CONTAINER.' table tbody tr:nth-child(1) td '.ModalPage::DELETE_TRIGGER);
        $I->seeElement(Page::CONTAINER.' table tbody tr:nth-child(2) td '.ModalPage::DELETE_TRIGGER);

        // When
        $I->click(Page::CONTAINER.' table tbody tr:nth-child(2) td '.ModalPage::DELETE_TRIGGER);
        $I->confirmToDeleteOnTheModal();

        // Then
        $I->seeInCurrentUrl(Page::LOCATION);
        $I->dontSee('test_user', Page::CONTAINER.' table');
        $I->seeSuccessFlashMessage('registration_deleted');
    }
}
