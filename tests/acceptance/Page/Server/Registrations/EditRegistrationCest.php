<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Registrations;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerRegistrationPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditRegistrationCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
    }

    public function it_initialize_the_server(AcceptanceTester $I): void
    {
        $I->resetAllServers();

        $I->haveServerNameOnTheTestServer('test servername for editRegistration page');
        $I->haveOneMumbleRegistrationOnTheTestServer(['test_username_field', 'test_email_field', 'test_description_field', 4 => 'password_field']);

        // Checking that the password is "password_field"
        // verifyPassword() method return the user id on successful authentication
        $prx = $I->getTheTestServer();
        $result = $prx->verifyPassword('test_username_field', 'password_field');
        $I->assertSame(1, $result);
    }

    /** @depends it_initialize_the_server */
    public function it_get_the_registration_edit_page(AcceptanceTester $I): void
    {
        // Given I'm on the registration page
        $I->amOnPage(Page::LOCATION.'/1');

        // When I click the link to edit the registration
        $I->click('#info-panel [title="edit"]');

        // Then I see the edit page
        $I->seeTheServerName('test servername for editRegistration page');
        $I->seeTheServerMenuIsActiveFor('registrations');
        $I->see('registrations_total', '#info-panel');
        $I->seeTheConnectionToMumbleUrl();
        $I->seeElement(Page::CONTAINER);
        $I->seeInField('registration[username]', 'test_username_field');
        $I->seeInField('registration[email]', 'test_email_field');
        $I->seeInField('registration[description]', 'test_description_field');
    }

    /** @depends it_get_the_registration_edit_page */
    public function it_edit_the_registration_with_the_form(AcceptanceTester $I): void
    {
        // When I modify registration entries on the form, and submit it
        $I->fillField('registration[username]', 'modified_username_field');
        $I->fillField('registration[email]', 'modified_email_field@test.tld');
        $I->fillField('registration[plainPassword][first]', 'modified_password');
        $I->fillField('registration[plainPassword][second]', 'modified_password');
        $I->fillField('registration[description]', 'modified_description_field');
        $I->click('submit', Page::CONTAINER.' form[name="registration"]');

        // Then I see the registration has been edited
        $I->seeSuccessFlashMessage('registration_edited');
        $I->seeInField('registration[username]', 'modified_username_field');
        $I->seeInField('registration[email]', 'modified_email_field@test.tld');
        $I->seeInField('registration[description]', 'modified_description_field');
    }

    /** @depends it_edit_the_registration_with_the_form */
    public function it_check_that_the_password_has_been_updated(AcceptanceTester $I): void
    {
        $prx = $I->getTheTestServer();
        $result = $prx->verifyPassword('modified_username_field', 'modified_password');
        $I->assertSame(1, $result);
    }
}
