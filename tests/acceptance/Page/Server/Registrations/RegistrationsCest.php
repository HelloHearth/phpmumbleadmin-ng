<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Registrations;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerRegistrationsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->haveTheTestServerOnline();
        $I->haveServerNameOnTheTestServer('test servername for registrations page');
    }

    public function it_display_the_index_page(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeTheServerName('test servername for registrations page');
        $I->seeTheServerMenuIsActiveFor('registrations');
        $I->seeElement(Page::CONTAINER);
        // I check that the datatables plugin have been initialized with success
        $I->seeElement(Page::CONTAINER.' #table-registrations_wrapper.dataTables_wrapper');
        $I->seeTheConnectionToMumbleUrl();
    }
}
