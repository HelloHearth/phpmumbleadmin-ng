<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Dashboard;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage as Page;
use App\Tests\Page\ModalPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteServerCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_delete_a_server_on_the_dashboard(AcceptanceTester $I): void
    {
        // Given I'm on the dashboard page, and I see I have one server
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeNumberOfElements(Page::TABLE_ID.' tbody tr[data-server]', 1);

        // When I open the action menu, click to delete the server, and confirm the action
        $I->openTheActionMenuOnTheDashboard(1);
        $I->click(Page::TABLE_ID.' tbody tr:nth-child(1) td.icon .dropdown-menu '.ModalPage::DELETE_TRIGGER);
        $I->confirmToDeleteOnTheModal();

        // Then I see no more server on the table
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeSuccessFlashMessage('server_deleted');
        $I->seeNumberOfElements(Page::TABLE_ID.' tbody tr[data-server]', 0);
    }
}
