<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Dashboard;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class StopServerCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_stop_an_empty_server(AcceptanceTester $I): void
    {
        // Given I'm on the dashboard page, and I see I have one started server
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(1) i.fa-power-off.online');

        // When I open the action menu, and click to stop the server
        $I->openTheActionMenuOnTheDashboard(1);
        $I->seeActionOnTheActionMenuOfTheDashboard('stop_server', 'i.fa-power-off.offline', 1);
        $I->click(Page::TABLE_ID.' tbody tr:nth-child(1) td.icon .dropdown-menu i.fa-power-off.offline');

        // Then I see the server is stopped
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'#');
        $I->seeSuccessFlashMessage('server_stopped');
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(1) i.fa-power-off.offline');
    }
}
