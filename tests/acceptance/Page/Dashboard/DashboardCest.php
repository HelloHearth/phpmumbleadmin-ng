<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Dashboard;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage as Page;
use App\Tests\Page\MurmurDefaultConfigPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DashboardCest
{
    private function setupServers(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $server1 = $I->getTheTestServer();
        $server2 = $I->addNewMurmurServer();
        $server3 = $I->addNewMurmurServer();
        $I->haveOneMumbleClient();

        $server1->setConf('registername', 'Test server 1');
        $server1->setConf('PMA_permitConnection', 'true');
        $server2->setConf('host', 'FC00::151');
        $server3->start();
        $server3->setConf('host', '127.0.0.1');

        // Add more offline server for pagination
        for ($i = 1; $i <= 9; ++$i) {
            $I->addNewMurmurServer();
        }
    }

    public function it_get_the_dashboard(AcceptanceTester $I): void
    {
        $this->setupServers($I);

        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
    }

    /** @depends it_get_the_dashboard */
    public function it_see_link_for_the_murmur_default_parameters_page(AcceptanceTester $I): void
    {
        $I->seeLink('info_murmur_version {"version":"1.2.18"}', MurmurDefaultConfigPage::LOCATION);
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_js_popup_hover_the_murmur_version(AcceptanceTester $I): void
    {
        $I->dontSeeTooltip('title_murmur_default_config');
        $I->moveMouseOver(Page::INFO_PANEL.' a');
        $I->waitForTooltipAnimation();
        $I->seeTooltip('title_murmur_default_config');
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_js_popup_hover_the_murmur_uptime(AcceptanceTester $I): void
    {
        $I->dontSeeTooltip('uptime_started_at');
        $I->moveMouseOver(Page::INFO_PANEL.' time.uptime');
        $I->waitForTooltipAnimation();
        $I->seeTooltip('uptime_started_at');
    }

    /** @depends it_get_the_dashboard */
    public function it_see_total_of_servers_info(AcceptanceTester $I): void
    {
        // I see 12 servers on info panel, and 2 online on
        $I->see('info_servers {"%count%":12,"%booted%":2}', Page::INFO_PANEL);
    }

    /** @depends it_get_the_dashboard */
    public function it_see_total_of_users_info(AcceptanceTester $I): void
    {
        // I see 1 online user on info panel
        $I->see('info_connected_users {"%count%":1}', Page::INFO_PANEL);
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_js_popup_hover_the_add_server_button(AcceptanceTester $I): void
    {
        $I->dontSeeTooltip('create_server_title');
        $I->moveMouseOver(Page::CONTAINER.' [data-bs-target="'.Page::MODAL_CREATE_SERVER.'"]');
        $I->waitForTooltipAnimation();
        $I->seeTooltip('create_server_title');
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_js_popup_hover_the_send_message_to_all_servers_button(AcceptanceTester $I): void
    {
        $I->dontSeeTooltip('send_message_to_servers');
        $I->moveMouseOver(Page::CONTAINER.' [data-bs-target="'.Page::MODAL_SEND_MESSAGE.'"]');
        $I->waitForTooltipAnimation();
        $I->seeTooltip('send_message_to_servers');
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_pagination(AcceptanceTester $I): void
    {
        // I see navigate to minor page are disabled
        $I->seeElement(Page::CONTAINER.' .pagination .disabled .fa-angle-double-left');
        $I->seeElement(Page::CONTAINER.' .pagination .disabled .fa-angle-left');
        // I see current page are active
        $I->see('1', Page::CONTAINER.' .pagination .active');
        // I see page 2 is a link
        $I->see('2', Page::CONTAINER.' .pagination a');
        // I see navigate to upper page are enabled
        $I->seeElement(Page::CONTAINER.' .pagination a .fa-angle-right');
        $I->seeElement(Page::CONTAINER.' .pagination a .fa-angle-double-right');
        // I see the pagination have 2 pages
        $I->see('total_of_pages {"%count%":2}', Page::CONTAINER.' .pagination .disabled');
    }

    /** @depends it_get_the_dashboard */
    public function it_see_10_servers_on_the_table(AcceptanceTester $I): void
    {
        $I->seeNumberOfLineInTable(Page::TABLE_ID, 10);
    }

    /** @depends it_get_the_dashboard */
    public function it_see_sortable_elements_on_the_table(AcceptanceTester $I): void
    {
        $I->see('s', Page::TABLE_ID.' thead tr th:nth-child(1) a');
        $I->seeElement(Page::TABLE_ID.' thead tr th:nth-child(1) a .fa-sort');
        $I->see('id', Page::TABLE_ID.' thead tr th:nth-child(2) a');
        $I->seeElement(Page::TABLE_ID.' thead tr th:nth-child(2) a .fa-sort');
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_1st_server_on_the_table(AcceptanceTester $I): void
    {
        // I see the status button is started
        $I->seeElementInTable('i.fa-power-off.online', Page::TABLE_ID, 1, 1);
        // I see the id of the server is 1
        $I->seeInTable('1', Page::TABLE_ID, 1, 2);
        // I see the serverName of the server
        $I->seeInTable('Test server 1', Page::TABLE_ID, 1, 3);
        // I see the port of the server
        $I->seeInTable('[::]:64738', Page::TABLE_ID, 1, 3);
        // I see the uptime of the server because the server is online
        $I->seeElementInTable('time.uptime', Page::TABLE_ID, 1, 3);
        // I see the connect icon because the server is online
        $I->seeElementInTable('a img', Page::TABLE_ID, 1, 4);
        // I see online users
        $I->seeInTable('1 / 100', Page::TABLE_ID, 1, 5);
        // I see web access is enabled
        $I->seeElementInTable('i.fa-globe.online', Page::TABLE_ID, 1, 6);
        // I see the menu action icon
        $I->seeElementInTable('i.fa-ellipsis-h', Page::TABLE_ID, 1, 7);
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_2nd_server_on_the_table(AcceptanceTester $I): void
    {
        // I see the status button is started
        $I->seeElementInTable('i.fa-power-off.offline', Page::TABLE_ID, 2, 1);
        // I see the id of the server is 2
        $I->seeInTable('2', Page::TABLE_ID, 2, 2);
        // I see the host and the port of the server
        $I->seeInTable('[FC00::151]:64739', Page::TABLE_ID, 2, 3);
        // I don't see the uptime of the server because the server is offline
        $I->dontSeeElementInTable('time.uptime', Page::TABLE_ID, 2, 3);
        // I don't see the connect icon because the server is offline
        $I->dontSeeElementInTable('a img', Page::TABLE_ID, 2, 4);
        // I don't see online users field because the server is offline
        $I->dontSeeInTable('0 / 100', Page::TABLE_ID, 2, 5);
        // I see web access is disabled
        $I->seeElementInTable('i.fa-globe.offline', Page::TABLE_ID, 2, 6);
        // I see the menu action icon
        $I->seeElementInTable('i.fa-ellipsis-h', Page::TABLE_ID, 2, 7);
    }

    /** @depends it_get_the_dashboard */
    public function it_see_the_3rd_server_on_the_table(AcceptanceTester $I): void
    {
        // I see the status button is started
        $I->seeElementInTable('i.fa-power-off.online', Page::TABLE_ID, 3, 1);
        // I see the id of the server is 3
        $I->seeInTable('3', Page::TABLE_ID, 3, 2);
        // I see the host and the port of the server
        $I->seeInTable('127.0.0.1:64740', Page::TABLE_ID, 3, 3);
        // I see the uptime of the server because the server is online
        $I->seeElementInTable('time.uptime', Page::TABLE_ID, 3, 3);
        // I see the connect icon because the server is online
        $I->seeElementInTable('a img', Page::TABLE_ID, 3, 4);
        // I see online users field because the server is online
        $I->seeInTable('0 / 100', Page::TABLE_ID, 3, 5);
        // I see web access is disabled
        $I->seeElementInTable('i.fa-globe.offline', Page::TABLE_ID, 3, 6);
        // I see the menu action icon
        $I->seeElementInTable('i.fa-ellipsis-h', Page::TABLE_ID, 3, 7);
    }

    /** @depends it_get_the_dashboard */
    public function it_paginate_to_page_2(AcceptanceTester $I): void
    {
        // given
        $I->amLoggedAsSuperAdmin();

        // When I click to paginate to the last page
        $I->click(Page::CONTAINER.' .pagination a .fa-angle-double-right');

        // Then I see the table has only 2 servers
        $I->seeNumberOfLineInTable(Page::TABLE_ID, 2);
        $I->seeInTable('11', Page::TABLE_ID, 1, 2);
        $I->seeInTable('12', Page::TABLE_ID, 2, 2);
    }

    /** @depends it_paginate_to_page_2 */
    public function it_see_the_pagination_on_page_2(AcceptanceTester $I): void
    {
        // I see navigate to minor page is enabled
        $I->seeElement(Page::CONTAINER.' .pagination a .fa-angle-double-left');
        $I->seeElement(Page::CONTAINER.' .pagination a .fa-angle-left');
        // I see page 1 is a link
        $I->see('1', Page::CONTAINER.' .pagination a');
        // I see current page are active
        $I->see('2', Page::CONTAINER.' .pagination .active');
        // I see navigate to upper page are disabled
        $I->seeElement(Page::CONTAINER.' .pagination .disabled .fa-angle-right');
        $I->seeElement(Page::CONTAINER.' .pagination .disabled .fa-angle-double-right');
        // I see the pagination have 2 pages
        $I->see('total_of_pages {"%count%":2}', Page::CONTAINER.' .pagination .disabled');
    }

    /** @depends it_paginate_to_page_2 */
    public function it_paginate_to_first_page(AcceptanceTester $I): void
    {
        // given
        $I->amLoggedAsSuperAdmin();

        // When I click to paginate to the previous page
        $I->click(Page::CONTAINER.' .pagination a .fa-angle-left');

        // Then I see I'm back to the first page
        $I->see('1', Page::CONTAINER.' .pagination .active');
        $I->seeNumberOfLineInTable(Page::TABLE_ID, 10);
    }

    /** @depends it_get_the_dashboard */
    public function it_sort_servers_by_status(AcceptanceTester $I): void
    {
        // given
        $I->amLoggedAsSuperAdmin();

        // When I click to sort on server status
        $I->click('s', Page::TABLE_ID.' thead tr th:nth-child(1)');

        // Then I see servers are sorted by online first
        $I->seeInTable('1', Page::TABLE_ID, 1, 2);
        $I->seeInTable('3', Page::TABLE_ID, 2, 2);
        $I->seeInTable('2', Page::TABLE_ID, 3, 2);
        $I->seeInTable('4', Page::TABLE_ID, 4, 2);
        $I->seeInTable('5', Page::TABLE_ID, 5, 2);
        $I->seeInTable('6', Page::TABLE_ID, 6, 2);
        $I->seeInTable('7', Page::TABLE_ID, 7, 2);
        $I->seeInTable('8', Page::TABLE_ID, 8, 2);
        $I->seeInTable('9', Page::TABLE_ID, 9, 2);
        $I->seeInTable('10', Page::TABLE_ID, 10, 2);
    }

    /** @depends it_sort_servers_by_status */
    public function it_sort_servers_to_invert_status(AcceptanceTester $I): void
    {
        // given
        $I->amLoggedAsSuperAdmin();

        // When I click to inverse sort on server status
        $I->click('s', Page::TABLE_ID.' thead tr th:nth-child(1)');

        // Then I don't see online servers on the list
        $I->seeInTable('12', Page::TABLE_ID, 1, 2);
        $I->seeInTable('11', Page::TABLE_ID, 2, 2);
        $I->seeInTable('10', Page::TABLE_ID, 3, 2);
        $I->seeInTable('9', Page::TABLE_ID, 4, 2);
        $I->seeInTable('8', Page::TABLE_ID, 5, 2);
        $I->seeInTable('7', Page::TABLE_ID, 6, 2);
        $I->seeInTable('6', Page::TABLE_ID, 7, 2);
        $I->seeInTable('5', Page::TABLE_ID, 8, 2);
        $I->seeInTable('4', Page::TABLE_ID, 9, 2);
        $I->seeInTable('2', Page::TABLE_ID, 10, 2);
    }

    /** @depends it_sort_servers_to_invert_status */
    public function it_sort_servers_by_id(AcceptanceTester $I): void
    {
        // given
        $I->amLoggedAsSuperAdmin();

        // When I click to sort on server id
        $I->click('id', Page::TABLE_ID.' thead tr th:nth-child(2)');

        // Then I see the list of servers is sorted by id
        $I->seeInTable('1', Page::TABLE_ID, 1, 2);
        $I->seeInTable('2', Page::TABLE_ID, 2, 2);
        $I->seeInTable('3', Page::TABLE_ID, 3, 2);
        $I->seeInTable('4', Page::TABLE_ID, 4, 2);
        $I->seeInTable('5', Page::TABLE_ID, 5, 2);
        $I->seeInTable('6', Page::TABLE_ID, 6, 2);
        $I->seeInTable('7', Page::TABLE_ID, 7, 2);
        $I->seeInTable('8', Page::TABLE_ID, 8, 2);
        $I->seeInTable('9', Page::TABLE_ID, 9, 2);
        $I->seeInTable('10', Page::TABLE_ID, 10, 2);
    }

    /** @depends it_sort_servers_by_id */
    public function it_sort_servers_to_invert_id(AcceptanceTester $I): void
    {
        // given
        $I->amLoggedAsSuperAdmin();

        // When I click to inverse sort on server id
        $I->click('id', Page::TABLE_ID.' thead tr th:nth-child(2)');

        // Then I see the list of servers id is inverted
        $I->seeInTable('12', Page::TABLE_ID, 1, 2);
        $I->seeInTable('11', Page::TABLE_ID, 2, 2);
        $I->seeInTable('10', Page::TABLE_ID, 3, 2);
        $I->seeInTable('9', Page::TABLE_ID, 4, 2);
        $I->seeInTable('8', Page::TABLE_ID, 5, 2);
        $I->seeInTable('7', Page::TABLE_ID, 6, 2);
        $I->seeInTable('6', Page::TABLE_ID, 7, 2);
        $I->seeInTable('5', Page::TABLE_ID, 8, 2);
        $I->seeInTable('4', Page::TABLE_ID, 9, 2);
        $I->seeInTable('3', Page::TABLE_ID, 10, 2);
    }
}
