<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Dashboard;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class StartServerCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();
        $prx->stop();
    }

    public function it_start_a_server(AcceptanceTester $I): void
    {
        // Given I'm on the dashboard page, and I see I have one stopped server
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(1) i.fa-power-off.offline');

        // When I open the action menu, and click to start the server
        $I->openTheActionMenuOnTheDashboard(1);
        $I->seeActionOnTheActionMenuOfTheDashboard('start_server', 'i.fa-power-off.online', 1);
        $I->click(Page::TABLE_ID.' tbody tr:nth-child(1) td.icon .dropdown-menu i.fa-power-off.online');

        // Then I see the server is started
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'#');
        $I->seeSuccessFlashMessage('server_started');
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(1) i.fa-power-off.online');
    }

    public function it_fail_to_start_a_bad_configured_server(AcceptanceTester $I): void
    {
        // I set the server with an invalid IP address
        $prx = $I->getTheTestServer();
        $prx->setConf('host', '42:0:0:0:0:0:0:42');

        // Given I'm on the dashboard page, and I see I have one stopped server
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(1) i.fa-power-off.offline');

        // When I open the action menu, and click to start the server
        $I->openTheActionMenuOnTheDashboard(1);
        $I->seeActionOnTheActionMenuOfTheDashboard('start_server', 'i.fa-power-off.online', 1);
        $I->click(Page::TABLE_ID.' tbody tr:nth-child(1) td.icon .dropdown-menu i.fa-power-off.online');

        // Then I see an error message, and the server is still stopped
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'#');
        $I->seeErrorFlashMessage('ServerFailureException');
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(1) i.fa-power-off.offline');
    }
}
