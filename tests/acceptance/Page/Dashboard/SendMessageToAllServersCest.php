<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Dashboard;

use App\Infrastructure\Symfony\Form\SendMessageToAllServersType;
use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToAllServersCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_send_a_message_to_all_servers_on_the_dashboard(AcceptanceTester $I): void
    {
        // Given
        $I->haveOneMumbleClient();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);

        // When
        $I->click('[data-bs-target="'.Page::MODAL_SEND_MESSAGE.'"]');

        // Then
        $I->waitForElementVisible(Page::MODAL_SEND_MESSAGE);
        $I->waitForElementHasFocus(Page::MODAL_SEND_MESSAGE.' form [name="'.SendMessageToAllServersType::BLOCK_PREFIX.'[message]"]');

        // When
        $I->fillField(Page::MODAL_SEND_MESSAGE.' form [name="'.SendMessageToAllServersType::BLOCK_PREFIX.'[message]"]', 'Test message to all servers');
        $I->click(Page::MODAL_SEND_MESSAGE.' form button[type="submit"]');

        // Then
        $I->waitForElementNotVisible(Page::MODAL_SEND_MESSAGE);
        $I->waitForSuccessJsFlashMessage('message_sent');
    }
}
