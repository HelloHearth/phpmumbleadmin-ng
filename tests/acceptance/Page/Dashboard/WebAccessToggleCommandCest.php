<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Dashboard;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class WebAccessToggleCommandCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_toggle_web_access_status_on_the_dashboard(AcceptanceTester $I): void
    {
        // Given I'm on the dashboard page, and I see I have one server without web access
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(6) i.fa-globe.offline');

        // When I open the action menu, click to enable web access for the server
        $I->openTheActionMenuOnTheDashboard(1);
        $I->seeActionOnTheActionMenuOfTheDashboard('enable_web_access', 'i.fa-globe.online', 1);
        $I->click(Page::TABLE_ID.' tbody tr:nth-child(1) td.icon .dropdown-menu i.fa-globe.online');

        // Then I see the server has web access enabled
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(6) i.fa-globe.online');

        // When I open the action menu, click to disable web access for the server
        $I->openTheActionMenuOnTheDashboard(1);
        $I->seeActionOnTheActionMenuOfTheDashboard('disable_web_access', 'i.fa-globe.offline', 1);
        $I->click(Page::TABLE_ID.' tbody tr:nth-child(1) td.icon .dropdown-menu i.fa-globe.offline');

        // Then I see the server has web access disabled
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeElement(Page::TABLE_ID.' tbody tr:nth-child(1) td:nth-child(6) i.fa-globe.offline');
    }
}
