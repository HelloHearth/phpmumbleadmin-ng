<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Shutdown;

use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogoutCest
{
    public function it_logout_the_superadmin_with_the_modal(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(DashboardPage::LOCATION);

        // When
        $I->openInTheUserMenu('a[data-bs-target="#logoutModal"]');
        $I->waitForElementVisible('#logoutModal');
        $I->click('logout', '#logoutModal .modal-footer');

        // Then
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');
        $I->deleteSessionSnapshotForSuperAdmin();
    }
}
