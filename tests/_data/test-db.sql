-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le :  lun. 12 avr. 2021 à 16:22
-- Version du serveur :  10.3.15-MariaDB-1:10.3.15+maria~bionic
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `phpMumbleAdmin_test`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `forgotten_password`;
DROP TABLE IF EXISTS `admin`;
DROP TABLE IF EXISTS `admin_log`;
DROP TABLE IF EXISTS `doctrine_migration_versions`;

CREATE TABLE `admin` (
                        `id` int(11) NOT NULL,
                        `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
                        `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id`, `username`, `roles`, `password`) VALUES
(1, 'SA', '[\"ROLE_SUPER_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$zPzFPd+aJ6ZyNDr97GHp5Q$rlY5d6/MYASGpNXi6ZgAv9Bm8LudSdSbWOcpNTPB2m8'),
(2, 'rootadmin', '[\"ROLE_ROOT_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$3VspZUt0n+K1guDuze9x3A$qMPbVnx2ErwRLy2+KjCOn2LmHs/0uJJdqfOSdNQA7Fo'),
(3, 'admin', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$D3Qcog5i1QuqDSTLfHLW+Q$JgyxfhJofxWWeVII75+B6/ZNwZVogMaY1Z5SVNzCDQQ');

--
-- Structure de la table `admin_log`
--

CREATE TABLE `admin_log` (
                             `id` int(11) NOT NULL,
                             `timestamp` int(11) NOT NULL,
                             `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `context` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
                             `level` int(11) NOT NULL,
                             `facility` int(11) NOT NULL,
                             `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------


--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
   `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
   `executed_at` datetime DEFAULT NULL,
   `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `forgotten_password`
--

CREATE TABLE `forgotten_password` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `valid_until` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `UNIQ_880E0D76F85E0677` (`username`),
    ADD UNIQUE KEY `UNIQ_880E0D76E7927C74` (`email`);

--
-- Index pour la table `admin_log`
--
ALTER TABLE `admin_log`
    ADD PRIMARY KEY (`id`);

--
-- Index pour la table `forgotten_password`
--
ALTER TABLE `forgotten_password`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2EDC8D24642B8210` (`admin_id`),
  ADD UNIQUE KEY `UNIQ_2EDC8D24D1B862B8` (`hash`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- AUTO_INCREMENT pour la table `admin_login`
--
ALTER TABLE `admin_log`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- AUTO_INCREMENT pour la table `forgotten_password`
--
ALTER TABLE `forgotten_password`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Contraintes pour la table `forgotten_password`
--
ALTER TABLE `forgotten_password`
    ADD CONSTRAINT `FK_2EDC8D24642B8210` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
