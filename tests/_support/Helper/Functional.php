<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Helper;

use Codeception\Exception\ModuleException;
use Codeception\Module\Symfony;
use Codeception\TestInterface;

/**
 * Here you can define custom actions
 * All public methods declared in helper class will be available in $I
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Functional extends \Codeception\Module
{
    /**
     * Workaround:
     *
     * Don't print the full response from the REST module in the body artefact
     * of codeception, it can output a very long text...
     *
     * See: https://github.com/Codeception/Codeception/issues/5826
     */
    public function _failed(TestInterface $test, $fail): void
    {
        $reports = $test->getMetadata()->getReports();
        if (! isset($reports['body'])) {
            return;
        }

        $maxLen = 200;

        if (\strlen($reports['body']) < $maxLen) {
            return;
        }

        $truncate = substr($reports['body'], 0, $maxLen);

        $test->getMetadata()->addReport('body', $truncate);
        $test->getMetadata()->addReport('Body Trucated', '[maxLength = '.$maxLen.' characters]');
    }

    /**
     * @throws ModuleException
     */
    public function replaceSymfonyService(string $serviceClassName, $service): void
    {
        /** @var Symfony $symfony */
        $symfony = $this->getModule('Symfony');

        $symfony->kernel->getContainer()->set($serviceClassName, $service);
        $symfony->persistService($serviceClassName);
    }

    /**
     * @throws ModuleException
     */
    public function restoreSymfonyService(string $serviceClassName): void
    {
        /** @var Symfony $symfony */
        $symfony = $this->getModule('Symfony');

        $symfony->unpersistService($serviceClassName);
    }
}