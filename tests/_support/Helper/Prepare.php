<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Helper;

use Codeception\Module\Db;
use Codeception\Module\WebDriver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait Prepare
{
    protected function WebDriverDontClearCookies(WebDriver $webdriver)
    {
        $webdriver->_reconfigure(['clear_cookies' => false]);
    }

    protected function DbPopulate(Db $db)
    {
        $db->_reconfigure(['populate' => true]);
    }
}
