<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Helper;

use Codeception\Module\WebDriver;

/**
 * Here you can define custom actions.
 * All public methods declared in helper class will be available in $I.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Acceptance extends \Codeception\Module
{
    // Wait for the focus of an element
    public function waitForElementHasFocus(string $element): void
    {
        /** @var WebDriver $webdriver */
        $webdriver = $this->getModule('WebDriver');

       $webdriver->waitForJS('return $(\''.$element.'\').is(\':focus\')', 3);
    }
}
