<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerSettingsPage extends ServerPage
{
    public const PATH = '/settings';
    public const LOCATION = parent::LOCATION.self::PATH;

    public const CONTAINER = '#MumbleSettingsContainer';
    public const TABS = self::CONTAINER.' #MumbleSettingsTabs';
    public const FORM = self::CONTAINER.' form';
    public const OUTPUT_ELEMENT = self::FORM.' .html-output .inner';

    public const FORM_CERTIFICATE_FIELD = self::FORM.' [name="app_settings_page[new_certificate]';
}
