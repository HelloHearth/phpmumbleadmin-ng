<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

use App\Infrastructure\Symfony\Form\LoginType;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LoginPage extends Page
{
    public const LOCATION = '/login';

    public const USERNAME_FIELD = 'form input[name="'.LoginType::BLOCK_PREFIX.'[username]"]';
    public const PASSWORD_FIELD = 'form input[name="'.LoginType::BLOCK_PREFIX.'[password]"]';
    public const SERVER_ID_FIELD = 'form input[name="'.LoginType::BLOCK_PREFIX.'[server]"]';
    public const REMEMBER_ME_FIELD = 'form input[name="'.LoginType::BLOCK_PREFIX.'[remember_me]"]';
    public const SUBMIT_BUTTON = 'form button[type=submit]';

    public const SUPER_ADMIN_USERNAME = 'SA';
    public const SUPER_ADMIN_PASSWORD = 'test';
}
