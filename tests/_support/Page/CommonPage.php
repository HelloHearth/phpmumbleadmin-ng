<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CommonPage extends Page
{
    public const LOCATION_CONFIRM_SERVER_TO_STOP = '/confirm_server_to_stop/';
    public const LOCATION_FORCE_SERVER_TO_STOP_CMD = '/cmd/forceServerToStop';
}
