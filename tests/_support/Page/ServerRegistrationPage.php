<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerRegistrationPage extends ServerPage
{
    public const PATH = '/registration';
    public const LOCATION = parent::LOCATION.self::PATH;
    public const LOCATION_EDIT = self::LOCATION.'/edit';

    public const CONTAINER = '#MumbleRegistrationContainer';
}
