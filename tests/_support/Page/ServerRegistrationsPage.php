<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerRegistrationsPage extends ServerPage
{
    public const PATH = '/registrations';
    public const LOCATION = parent::LOCATION.self::PATH;

    public const CONTAINER = '#MumbleRegistrationsContainer';
}
