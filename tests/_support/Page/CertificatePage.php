<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CertificatePage extends Page
{
    public const VALID_PEM_CODECEPTION_PATH = 'certificate/valid_certificate.pem';
    public const VALID_PEM_PATH = __DIR__.'/../../_data/'.self::VALID_PEM_CODECEPTION_PATH;

    public const INVALID_PEM_CODECEPTION_PATH = 'certificate/invalid_certificate.pem';
    public const INVALID_PEM_PATH = __DIR__.'/../../_data/'.self::INVALID_PEM_CODECEPTION_PATH;

    // 2 files certificate
    public const VALID_2F_CERTIFICATE_CODECEPTION_PATH = 'certificate-2-files/valid_certificate.pem';
    public const VALID_2F_KEY_CODECEPTION_PATH = 'certificate-2-files/valid_key.pem';
    public const VALID_2F_CERTIFICATE_PATH = __DIR__.'/../../_data/'.self::VALID_2F_CERTIFICATE_CODECEPTION_PATH;
    public const VALID_2F_KEY_PATH = __DIR__.'/../../_data/'.self::VALID_2F_KEY_CODECEPTION_PATH;

    public const PRIVATE_KEY = '-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA1JKl6KgqdX942QlkiPd9V0LoGwGEeMxSKDv3Xr/myuf4lyOt
bsa9Eib69Ivqd9GvOOZNnr3YN9Vaf/77cYpUhtzCuzFLIrRHDBL8rcLS+bMRhJCZ
D29I2En0DFYX2LgEmJqNubLB5SltIGA7m2qbD28H1pqo8Tb6RBUQojtMP5PXa5AY
8WIbsTsyKOE3BZhtYFWFRZYylc9gWkXV9rSE/Vg0wYj6d4sebhSMbVroQlCksyKw
rr4jI7h696PX0+nIXTVI12O+LnMkh7L/SRJDoANy7ujL8JKS0wyIAjj726RKl84M
iePVK9HgpDWBP8weJtoicXEsZMAJR5SGGYnTuwIDAQABAoIBADyMJLuXIrji/R38
wekNbWMsiIYgsYXi1MfnBLe1O1yyJCmQnPa/nt0Lw1FihP9p91nip1s+wdZta+Cn
VSFrAFaFjOXazYbuCIqU4enwYz2CVswGj8JItVv9/IiDATnWeGCrLREmrHnEifO9
1VyYSeBkhLwIw3Qpb7FyupErCLXWtm+hGfERSRwwdcMXPdbO+t62jLGWqYmx7ple
Nm4UMbgJ0rXkL8SouJn9xFTIz8s9Q0mUsfxzdBDhc8vAcBhyq+ErxB5hbNDttouU
DzSiydxe0YeLKGVpi0iQlcTn29fK+8mWYDVgCb3z1zmM7QmNvOfbuCxj5naMvVFQ
9TyMo1ECgYEA8/84zzc3pn1X9IqmGaYGUqZe/e2GXDcmPdpIYv1v/GhBevH9Uelp
mhOmmYy73Jm1gBMTorhWuMTlxTOM8POzYh5nM6l0wGJUKfjIHXZczuvW9Ht/chW0
9Zb0qaQvPUb0dnYWIt/Atq41bhVeZ6JwCfLa1svkSnYcCN6SW3yI6zkCgYEA3wev
rk29Ua1DR+Hh2dvA+8An707o40UQV7TRsLzczaSiEF0r/nLHRmkuRSBuRT5Au550
WjXoXI5HwAVyhwerxfw26KzkBHii6j+PTX8Gm9dsXJJ6Zfvq1rVzWbRmhfzcbrVj
kv7KYz1BwQ1BEvhzkJTPT0QD2pbJJBtpH7em0pMCgYBVoyxzPjQgETo/sDIgKm9p
rs8EXMudrhWT+/sQE1esxYLF/g0lJMFVvLPqjMSa5rAXZZqsr/pwG1tx3WgIYeCB
/Z3HOiCqLVjQ6mqCPLrSPWBSqs3OYuQClpsEt4WFAlRnB4c5ckMXWwmRu5pI0XB1
bXF1llZn9BBe5I+F4K3FsQKBgQCaT5cd4mu6t0Z11bOOKJ29kZYP3WkE1iQYiuMK
haHAJsh6yfxGcOOvs9ZvvaCOCCSBq4CVafb7EvZRxXMYOhZ/RaI4GmcpMM6HhLKB
zAGmkcZHxpEpPsOdCq5kwoys5+ccMiRS8DbXsuprx9/eYEiVIGDPVx/TfcBtWwnB
CDBEQQKBgBonIWG3MZhYEwi+2pNgQUM4kCbRtimoEIHfAvK26QFnhAZiGygai7MI
sdNiDBAxFgM5tBYi/iYESobAje9sCyKmLgk3FMEobW7ATR17wU8ygvgx0pdq7hyU
3CWiyCeVijMwZghhwPD55+sofJfrkxViro7o9bwU9rZGsBUf7Ex4
-----END RSA PRIVATE KEY-----';
    public const CERTIFICATE = '-----BEGIN CERTIFICATE-----
MIIDSzCCAjOgAwIBAgIBATANBgkqhkiG9w0BAQUFADAuMSwwKgYDVQQDDCNNdXJt
dXIgQXV0b2dlbmVyYXRlZCBDZXJ0aWZpY2F0ZSB2MjAeFw0xODExMTIyMjM3MTFa
Fw0zODExMDcyMjM3MTFaMC4xLDAqBgNVBAMMI011cm11ciBBdXRvZ2VuZXJhdGVk
IENlcnRpZmljYXRlIHYyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
1JKl6KgqdX942QlkiPd9V0LoGwGEeMxSKDv3Xr/myuf4lyOtbsa9Eib69Ivqd9Gv
OOZNnr3YN9Vaf/77cYpUhtzCuzFLIrRHDBL8rcLS+bMRhJCZD29I2En0DFYX2LgE
mJqNubLB5SltIGA7m2qbD28H1pqo8Tb6RBUQojtMP5PXa5AY8WIbsTsyKOE3BZht
YFWFRZYylc9gWkXV9rSE/Vg0wYj6d4sebhSMbVroQlCksyKwrr4jI7h696PX0+nI
XTVI12O+LnMkh7L/SRJDoANy7ujL8JKS0wyIAjj726RKl84MiePVK9HgpDWBP8we
JtoicXEsZMAJR5SGGYnTuwIDAQABo3QwcjAMBgNVHRMBAf8EAjAAMB0GA1UdJQQW
MBQGCCsGAQUFBwMBBggrBgEFBQcDAjAdBgNVHQ4EFgQUhsY7hyeSzvDgXlU8b8ke
/Oz6LxAwJAYJYIZIAYb4QgENBBcWFUdlbmVyYXRlZCBmcm9tIG11cm11cjANBgkq
hkiG9w0BAQUFAAOCAQEAnauy9sWUy8XchceTdRYhi22EPh9b8ihr3CfxsczvLK5B
v7q6BOyGluiNzB9jZbnZhUS8HtEab3vdAqrxmq2goPA69NodgKAHjuZiZI2siTrD
GTOkMeXGO+zz8tv+AOenICMKbJR8urlmSyE1rDAv44VcVifiDtI5JufsjzbJD2Tc
mem8eFq7/5O1DI6KFzuIJVkDJXiphl8yxhLvS3uFUlpd+Cxm3nG3WB4QPZW8exv1
A5ESYn5cS/lgtxvHAYh9JA1WCsS3vVr9XNCO1ZzuR+eX8gMOvelseQQzTu0ELeGj
lCGzKhPQYtM+X6308QWhiENHIICmIvgu/LHfdWywIA==
-----END CERTIFICATE-----';
    // This certificate isn't generated with the PRIVATE_KEY of this class
    public const INVALID_CERTIFICATE = '-----BEGIN CERTIFICATE-----
MIIDSzCCAjOgAwIBAgIBATANBgkqhkiG9w0BAQUFADAuMSwwKgYDVQQDDCNNdXJt
dXIgQXV0b2dlbmVyYXRlZCBDZXJ0aWZpY2F0ZSB2MjAeFw0xOTA1MjUxMzIyNDha
Fw0zOTA1MjAxMzIyNDhaMC4xLDAqBgNVBAMMI011cm11ciBBdXRvZ2VuZXJhdGVk
IENlcnRpZmljYXRlIHYyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
uG0/jHtVkZCQhxIijcGUHITUTAM/PFOWG65/F6SfJcCrEaHIiI9KyNbGvWBB2aOb
NJyVm4qk2lXl/Be/3GPJe2Rfg4M9fkOawSbxJXs+Oc+2y240ALsexae/g/tyVThP
Bca7xdrdCv4SJdMU7t+zm1Ceq7c1/mLhHFJpkiUkK9qAvVvuAaOjjHNKcCFMszR3
+RZCdoLvR3omo7Y+p9I7yzXfGcUZhgbfO9w3qtlDG+nbEaUMj520I2ohgAjTdmmy
AnV940Fwauw2P/A9XsYXQuUy3RvmyUYCcko8Psk0TY91GSBUsd7UlHC/twhbw0VZ
xXFOrh4jh12fCfnJUQmJ+wIDAQABo3QwcjAMBgNVHRMBAf8EAjAAMB0GA1UdJQQW
MBQGCCsGAQUFBwMBBggrBgEFBQcDAjAdBgNVHQ4EFgQUdYvzAApoZNKI/ixzXIo0
FGGvjRUwJAYJYIZIAYb4QgENBBcWFUdlbmVyYXRlZCBmcm9tIG11cm11cjANBgkq
hkiG9w0BAQUFAAOCAQEAaMzQqmBiX4856p2o71cWzKxApnL1/YLLS7TQDqApOvRt
Hmy2cmg7JYw43LDPGEgL+pxw7WLzG7H14KEaemAdHRxJJSKNCtyzsL7DR0hlTfQE
tttCiEbm7SrWZ53pnXmhsqeisSFgjQFpN3nuthASMprAQSDYWs+z/m93Kln7Zd98
UVd6aG3V4Ebh2rZAQsYjAX/g2Hz9dJtw7uhmqs6V4oiCSrcWoDM7Kc3BioXUwL1H
oyvQEz3KNMgydGRQ2hhYjwmawU95EjhsGBdRnQF8bmf0UFruZVY16+ZDg6AlIOYC
WCIM8EoAjEeYlXe0xN2B9tUVghABkB7liT+AwyNeNA==
-----END CERTIFICATE-----';

    public const PEM = self::PRIVATE_KEY.PHP_EOL.self::CERTIFICATE.PHP_EOL;

    public static function output(): array
    {
        return [
            'subject' => [
                'commonName' => 'Murmur Autogenerated Certificate v2'
            ],
            'issuer' => [
                'commonName' => 'Murmur Autogenerated Certificate v2'
            ],
            'extensions' => [
                'basicConstraints' => 'CA:FALSE',
                'extendedKeyUsage' => 'TLS Web Server Authentication, TLS Web Client Authentication',
                'nsComment' => 'Generated from murmur',
                'subjectKeyIdentifier' => '86:C6:3B:87:27:92:CE:F0:E0:5E:55:3C:6F:C9:1E:FC:EC:FA:2F:10'
            ],
            'others' => [
                'hash' => '5967aca4',
                'name' => '/CN=Murmur Autogenerated Certificate v2',
                'serialNumber' => '1',
                'serialNumberHex' => '01',
                'signatureTypeLN' => 'sha1WithRSAEncryption',
                'signatureTypeNID' => 65,
                'signatureTypeSN' => 'RSA-SHA1',
                'Valid from' => new \DateTime('2018-11-12T23:37:11.000000+0100'),
                'Valid to' => new \DateTime('2038-11-07T23:37:11.000000+0100'),
                'version' => 2
            ]
        ];
    }
}
