<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

use App\Infrastructure\Symfony\Form\ForgottenPasswordResetType;
use App\Infrastructure\Symfony\Form\ForgottenPasswordType;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordPage extends Page
{
    public const LOCATION = '/forgotten-password';
    public const LOCATION_RESET = self::LOCATION.'/reset';

    public const FORM_BLOCK_PREFIX = ForgottenPasswordType::BLOCK_PREFIX;
    public const FORM_ELEMENT = 'form[name="'.self::FORM_BLOCK_PREFIX.'"]';

    public const FORM_RESET_BLOCK_PREFIX = ForgottenPasswordResetType::BLOCK_PREFIX;
    public const FORM_RESET_ELEMENT = 'form[name="'.self::FORM_RESET_BLOCK_PREFIX.'"]';
}
