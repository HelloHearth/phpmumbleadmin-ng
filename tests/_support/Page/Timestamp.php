<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Timestamp
{
    // 12 July 1998 - 20:50:00 /UTC
    public const UTC_WC_1998 = 900276600;
    // 15 July 2018 - 16:50:00 /UTC
    public const UTC_WC_2018 = 1531673400;
}
