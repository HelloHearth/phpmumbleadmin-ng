<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerPage extends Page
{
    public const PATH = '/server';
    public const TEST_SERVER_ID = 1;

    public const LOCATION = self::PATH.'/'.self::TEST_SERVER_ID;

    public const CONTENT_ELEMENT = 'main#content';
    public const INFO_PANEL = self::CONTENT_ELEMENT.' #info-panel';
}
