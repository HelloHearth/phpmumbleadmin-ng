<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DashboardPage extends Page
{
    public const LOCATION = '/';

    public const CONTAINER = '#PageDashboardContainer';
    public const INFO_PANEL = self::CONTAINER.' #info-panel';
    public const TABLE_ID = 'table#dashboard-table';

    public const CREATE_SERVER_CMD = '/cmd/create_server';
    public const START_SERVER_CMD = '/cmd/startServer/';
    public const STOP_SERVER_CMD = '/cmd/stopServer/';

    public const MODAL_CREATE_SERVER = '#createServerModal';
    public const MODAL_SEND_MESSAGE = '#sendMessageModal';
}
