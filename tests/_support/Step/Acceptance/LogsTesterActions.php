<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait LogsTesterActions
{
    public function seeNumberOfDayOnLogsElements(int $expected, string $selector = null): void
    {
        $this->seeNumberOfElements($selector.' .day', $expected);
    }

    public function seeNumberOfLogElements(int $expected, string $selector = null): void
    {
        $this->seeNumberOfElements($selector.' .log time', $expected);
        $this->seeNumberOfElements($selector.' .log .text', $expected);
    }

    public function seeDay(string $date, int $line, string $selector = null): void
    {
        $this->see($date, $selector.' div:nth-child('.$line.').day');
    }

    public function seeLog(string $time = null, string $text, int $line, string $selector = null): void
    {
        if (\is_string($time)) {
            $this->see($time, $selector.' div:nth-child('.$line.').log time');
        }
        $this->see($text, $selector.' div:nth-child('.$line.').log .text');
    }

    public function seeNewLog(string $time = null, string $text, int $line, string $selector = null): void
    {
        if (\is_string($time)) {
            $this->see($time, $selector.' div:nth-child('.$line.').log.new-log time');
        }
        $this->see($text, $selector.' div:nth-child('.$line.').log.new-log .text');
    }

    public function waitForNewLog(string $text, string $selector = null): void
    {
        $this->waitForText($text, 10, $selector);
    }

    // Day elements have to be unique
    public function seeDayElementsAreUnique(string $selector = null): void
    {
        // When I grab all day elements
        $dayElements = $this->grabMultiple($selector.' .day');

        // One day element must appear
        if (\count($dayElements) < 2) {
            $this->seeNumberOfDayOnLogsElements(1);
            return;
        }

        // Then if more than 1 element is found, I remove duplicate texts
        $isUnique  = \array_unique($dayElements);

        // I see that array_unique function didn't remove any text,
        // it's means that all day elements are unique
        $this->assertSame($isUnique, $dayElements);
    }
}
