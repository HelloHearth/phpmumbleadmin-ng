<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait SfToolbarTesterActions
{
    /*
     * Close the SfToolbar which can be hover a clickable element
     */
    public function closeSfToolbar(): void
    {
        $sfToolbar = '.sf-toolbar .hide-button';
        $this->waitForElementClickable($sfToolbar, 1);
        $this->click($sfToolbar);
    }
}
