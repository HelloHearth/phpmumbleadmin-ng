<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait JsEditorTesterActions
{
    public function fillQuillEditor(string $value): void
    {
        $this->executeJS('document.getElementsByClassName("ql-editor")[0].innerHTML = "<p>'.$value.'</p>";');
    }
}
