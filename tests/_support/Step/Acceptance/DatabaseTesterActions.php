<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

/**
 * Tester actions for the Viewer using the Webdriver module
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait DatabaseTesterActions
{
    public function haveAdminLogInDatabase(array $data = []): int
    {
        $adminLog = $this->createAdminLog($data);
        return $this->haveInDatabase('admin_log', [
            'address' => $adminLog->getAddress(),
            'context' => \json_encode($adminLog->getContext()),
            'facility' => $adminLog->getFacility(),
            'level' => $adminLog->getLevel(),
            'message' => $adminLog->getMessage(),
            'timestamp' => $adminLog->getTimestamp(),
        ]);
    }
}
