<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait DropdownTesterActions
{
    public function openInTheUserMenu(string $menu): void
    {
        $this->click('#user-menu');
        $this->waitForElementVisible('#user-menu');
        $this->click('#user-menu '.$menu);
    }
}
