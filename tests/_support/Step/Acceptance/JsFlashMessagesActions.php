<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

use App\Tests\Page\FlashMessagePage;

/**
 * Action on the murmur server for tests
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait JsFlashMessagesActions
{
    public function waitForJsFlashMessage(string $message, string $type): void
    {
        $this->waitForText($message, 3, FlashMessagePage::JS.$type);
    }

    public function waitForSuccessJsFlashMessage(string $message): void
    {
        $this->waitForJsFlashMessage($message, FlashMessagePage::SUCCESS);
    }
}
