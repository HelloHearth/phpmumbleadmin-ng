<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

use App\Tests\Page\ServerChannelsPage as Page;

/**
 * Tester actions for the Viewer using the Webdriver module
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ViewerTesterActions
{
    public function openTheActionMenuInTheViewer(): void
    {
        $this->click(Page::VIEWER_MENU.' [data-bs-toggle="dropdown"]');
        $this->waitForElementVisible(Page::VIEWER_MENU.' .dropdown-menu');
    }

    public function clickTheActionButtonInTheViewer(string $action): void
    {
        $this->openTheActionMenuInTheViewer();
        $this->click(Page::VIEWER_MENU.' .dropdown-item[data-bs-target="'.$action.'"]');
        $this->waitForModalFadeEffect($action);
    }
}
