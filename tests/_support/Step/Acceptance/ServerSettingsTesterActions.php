<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

use App\Tests\Page\CertificatePage;
use App\Tests\Page\ServerSettingsPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ServerSettingsTesterActions
{
    private function fieldElement(string $parameter): string
    {
        return ServerSettingsPage::FORM.' [name="app_settings_page['.$parameter.']"]';
    }

    public function selectTabOfServerSettings(string $tab): void
    {
        $this->click($tab, ServerSettingsPage::TABS);
        $this->waitForServerSettingsTabVisible($tab);
    }

    public function waitForServerSettingsTabVisible(string $tab): void
    {
        $this->waitForElementVisible(ServerSettingsPage::CONTAINER.' #tab-'.$tab);
    }

    public function fillServerSettingsField(string $parameter, string $newValue = null): void
    {
        if (\is_null($newValue)) {
            $newValue = 'test '.$parameter.' field';
        }

        $this->dontSee($newValue, ServerSettingsPage::OUTPUT_ELEMENT.'.field-'.$parameter);
        $this->fillField($this->fieldElement($parameter), $newValue);
    }

    public function fillServerSettingsFieldWithNumber(string $parameter, int $newValue = 42): void
    {
        $this->dontSee($newValue, ServerSettingsPage::OUTPUT_ELEMENT.'.field-'.$parameter);
        $this->fillField($this->fieldElement($parameter), $newValue);
    }

    public function attachCertificateFileOnServerSettings(bool $validCertificate): void
    {
        $pathName = $validCertificate ?
            CertificatePage::VALID_PEM_CODECEPTION_PATH :
            CertificatePage::INVALID_PEM_CODECEPTION_PATH;

        $this->attachFile(ServerSettingsPage::FORM.' [name="app_settings_page[new_certificate][pem]"]', $pathName);
    }

    public function selectServerSettingsOption(string $parameter, string $newValue): void
    {
        $this->dontSee($newValue, ServerSettingsPage::OUTPUT_ELEMENT.'.field-'.$parameter);
        $this->selectOption($this->fieldElement($parameter), $newValue);
    }

    public function seeTheContentOfTheServerSettingTab(string $tabName): void
    {
        $this->seeElement(ServerSettingsPage::CONTAINER.' #tab-'.$tabName);
    }

    public function dontSeeTheContentOfTheServerSettingTab(string $tabName): void
    {
        $this->dontSeeElement(ServerSettingsPage::CONTAINER.' #tab-'.$tabName);
    }
}
