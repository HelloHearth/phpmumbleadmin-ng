<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Functional;

use App\Entity\AdminEntity;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait LoggedTesterActions
{
    /**
     * Simulate authentication by creating the Authentication Token and
     * populate both cookie and session.
     */
    private function authenticateUser(UserInterface $user, string $firewallContext = 'main'): void
    {
        $token = new UsernamePasswordToken($user, $user->getPassword(), $firewallContext, $user->getRoles());

        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $this->grabService('security.token_storage');
        $tokenStorage->setToken($token);

        /** @var SessionInterface $session */
        $session = $this->grabService('session');
        $session->set('_security_'.$firewallContext, \serialize($token));
        $session->save();

        $this->setCookie($session->getName(), $session->getId());
    }

    public function amLoggedAsSuperAdmin(): void
    {
        $superAdminUser = $this->grabEntityFromRepository(AdminEntity::class, ['username' => 'SA']);
        $this->authenticateUser($superAdminUser);
    }

    public function amLoggedAsRootAdmin(): void
    {
        $superAdminUser = $this->grabEntityFromRepository(AdminEntity::class, ['username' => 'rootadmin']);
        $this->authenticateUser($superAdminUser);
    }

    public function amLoggedAsAdmin(): void
    {
        $superAdminUser = $this->grabEntityFromRepository(AdminEntity::class, ['username' => 'admin']);
        $this->authenticateUser($superAdminUser);
    }

    public function amLoggedAsSuperUser(): void
    {
        $superUser = $this->createSuperUser();
        $this->haveOneMumbleRegistrationOnTheTestServer([$superUser->getUserIdentifier(), '', '', '', 'test']);
        $this->authenticateUser($superUser);
    }

    public function amLoggedAsSuperUserRu(): void
    {
        $superUserRu = $this->createSuperUser_Ru();
        $this->haveOneMumbleRegistrationOnTheTestServer([$superUserRu->getUserIdentifier(), '', '', '', 'test']);
        $this->authenticateUser($superUserRu);
    }

    public function amLoggedAsMumbleUser(): void
    {
        $mumbleUser = $this->createMumbleUser();
        $this->haveOneMumbleRegistrationOnTheTestServer([$mumbleUser->getUserIdentifier(), '', '', '', 'test']);
        $this->authenticateUser($mumbleUser);
    }

    public function amLoggedAsAnonymous(): void
    {
        $this->logout();
    }
}
