<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Domain\Service\LoggerServiceInterface;
use App\Domain\Service\SecurityServiceInterface;
use App\Entity;
use App\Infrastructure\Symfony\Security\MumbleUser;

/**
 * Trait to create easily entities of the App.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait CreateEntities
{
    public function createSuperAdmin(array $data = []): Entity\AdminEntity
    {
        $default = [
            'username' => 'TestSuperAdmin',
            'role' => [SecurityServiceInterface::ROLE_SUPER_ADMIN],
        ];
        $data = \array_merge($default, $data);

        return $this->createAdmin($data);
    }

    public function createRootAdmin(array $data = []): Entity\AdminEntity
    {
        $default = [
            'username' => 'TestRootAdmin',
            'role' => [SecurityServiceInterface::ROLE_ROOT_ADMIN],
        ];
        $data = \array_merge($default, $data);

        return $this->createAdmin($data);
    }

    public function createAdmin(array $data = []): Entity\AdminEntity
    {
        // password = test
        $default = [
            'username' => 'TestAdmin',
            'email' => null,
            'password' => '$2y$12$DlEjbQ71jHaF5pHpInQOve2kurkfdZTRfcgi3.xzq9fXn9HwlNKCq',
            'role' => [SecurityServiceInterface::ROLE_ADMIN],
        ];
        $data = \array_merge($default, $data);

        $user = new Entity\AdminEntity();
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);
        $user->setPassword($data['password']);
        $user->setRoles($data['role']);

        return $user;
    }

    public function createSuperUser(array $data = []): MumbleUser
    {
        $default = [
            'username' => 'TestSuperUser',
            'registrationId' => 0,
            'role' => [SecurityServiceInterface::ROLE_SUPER_USER],
        ];
        $data = \array_merge($default, $data);

        return $this->createMumbleUser($data);
    }

    public function createSuperUser_Ru(array $data = []): MumbleUser
    {
        $default = [
            'username' => 'TestSuperUserRu',
            'role' => [SecurityServiceInterface::ROLE_SUPER_USER_RU],
        ];
        $data = \array_merge($default, $data);

        return $this->createMumbleUser($data);
    }

    public function createMumbleUser(array $data = []): MumbleUser
    {
        $default = [
            'username' => 'TestMumbleUser',
            'email' => null,
            'serverId' => 1,
            'registrationId' => 1,
            'role' => ['ROLE_MUMBLE_USER'],
        ];
        $data = \array_merge($default, $data);

        $user = new MumbleUser();
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);
        $user->setServerId($data['serverId']);
        $user->setRegistrationId($data['registrationId']);
        $user->setRoles($data['role']);

        return $user;
    }

    public function createForgottenPassword(array $data = []): Entity\ForgottenPassword
    {
        $default = [
            'admin' => null,
            'hash' => null,
            'created_at' => 'now',
            'valid_until' => '+1hour',
        ];
        $data = \array_merge($default, $data);

        $forgottenPassword = new Entity\ForgottenPassword();

        $forgottenPassword->setAdmin($data['admin']);
        $forgottenPassword->setCreatedAt(new \DateTimeImmutable($data['created_at']));
        $forgottenPassword->setValidUntil(new \DateTimeImmutable($data['valid_until']));
        if (! \is_null($data['hash'])) {
            $forgottenPassword->setHash($data['hash']);
        } else {
            $forgottenPassword->generateHash();
        }

        return $forgottenPassword;
    }

    public function createAdminLog(array $data = []): Entity\AdminLogEntity
    {
        $default = [
            'timestamp' => \time(),
            'message' => '',
            'facility' => LoggerServiceInterface::FACILITY_USER,
            'level' => LoggerServiceInterface::LEVEL_INFORMATIONAL,
            'address' => '192.168.42.42',
            'context' => [],
        ];
        $data = \array_merge($default, $data);

        $adminLog = new Entity\AdminLogEntity();

        $adminLog->setTimestamp($data['timestamp']);
        $adminLog->setMessage($data['message']);
        $adminLog->setFacility($data['facility']);
        $adminLog->setLevel($data['level']);
        $adminLog->setAddress($data['address']);
        $adminLog->setContext($data['context']);

        return $adminLog;
    }
}
