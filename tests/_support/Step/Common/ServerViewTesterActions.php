<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Tests\Page\ServerPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ServerViewTesterActions
{
    public function seeTheServerName(string $serverName): void
    {
        $this->see((string) ServerPage::TEST_SERVER_ID, '#servers-nav #servers-nav-title .id');
        $this->see($serverName, '#servers-nav #servers-nav-title .name');
    }
}
