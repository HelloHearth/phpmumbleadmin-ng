<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Domain\Murmur\Factory\MurmurObjectFactory;
use App\Domain\Murmur\Model\MetaInterface;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Domain\Murmur\Model\ServerInterface;
use App\Infrastructure\Murmur\Connection\ConnectionHandler;
use App\Tests\Page\ServerChannelsPage;
use App\Tests\Page\ServerPage;

/**
 * Action on the murmur server for tests
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait MurmurActions
{
    private MetaInterface $murmurMeta;

    private ServerInterface $serverProxy;

    public function getMurmurMetaInterface(): MetaInterface
    {
        if (!isset($this->murmurMeta)) {
            $RPC = new ConnectionHandler(
                $_ENV['MURMUR_HOST'],
                $_ENV['MURMUR_PORT'],
                $_ENV['MURMUR_TIMEOUT'],
                $_ENV['MURMUR_SECRET'],
            );
            $this->murmurMeta = $RPC->connect();
        }
        return $this->murmurMeta;
    }

    private function getServerProxyInterface(int $serverId): ServerInterface
    {
        if (! isset($this->serverProxy)) {
            $meta = $this->getMurmurMetaInterface();
            $this->serverProxy = $meta->getServer($serverId);
        }
        return $this->serverProxy;
    }

    public function getTheTestServer(): ServerInterface
    {
        $meta = $this->getMurmurMetaInterface();

        foreach ($meta->getAllServers() as $prx) {
            if ($prx->getSid() === ServerPage::TEST_SERVER_ID) {
                return $prx;
            }
        }

        // On fail, reset all servers
        $this->resetAllServers();

        return $meta->getServer(ServerPage::TEST_SERVER_ID);
    }

    public function resetAllServers(bool $testServerOnline = true): void
    {
        $meta = $this->getMurmurMetaInterface();

        foreach ($meta->getAllServers() as $prx) {
            if ($prx->isRunning()) {
                $prx->stop();
            }
            $prx->delete();
        }

        $prx = $meta->newServer();

        if ($testServerOnline) {
            $prx->start();
        }
    }

    public function haveTheTestServerOnline(): void
    {
        $prx = $this->getServerProxyInterface(ServerPage::TEST_SERVER_ID);

        if (! $prx->isRunning()) {
            $prx->start();
        }
    }

    public function haveTheTestServerOffline(): void
    {
        $prx = $this->getServerProxyInterface(ServerPage::TEST_SERVER_ID);

        if ($prx->isRunning()) {
            $prx->stop();
        }
    }

    public function addNewMurmurServer(?string $name = null): ServerInterface
    {
        $meta = $this->getMurmurMetaInterface();
        $server = $meta->newServer();
        if (\is_string($name)) {
            $server->setConf('registername', $name);
        }

        return $server;
    }

    public function stopTheTestServer(): void
    {
        $server = $this->getTheTestServer();
        $server->stop();
    }

    public function haveOneMumbleRegistrationOnTheTestServer(array $user): void
    {
        $prx = $this->getServerProxyInterface(ServerPage::TEST_SERVER_ID);

        $registrations = $prx->getRegisteredUsers($user[0]);

        // Create the registration if not already registered
        if (! \in_array($user[0], $registrations)) {
            $prx->registerUser($user);
        }
    }

    public function haveTextureForRegistrationOnTheTestServer(int $registrationId): void
    {
        $prx = $this->getServerProxyInterface(ServerPage::TEST_SERVER_ID);
        $prx->setTexture($registrationId, [0, 1, 2 ,3]);
    }

    public function haveServerSettingOnTheTestServer(string $key, string $value): void
    {
        $prx = $this->getServerProxyInterface(ServerPage::TEST_SERVER_ID);
        $prx->setConf($key, $value);
    }

    public function haveServerNameOnTheTestServer(string $value): void
    {
        $this->haveServerSettingOnTheTestServer('registername', $value);
    }

    /**
     * @param BanMock[] $bans
     */
    public function haveMurmurBansOnTheTestServer(array $bans): void
    {
        $murmurBans = [];

        foreach ($bans as $ban) {
            $murmurBan = MurmurObjectFactory::createBan();
            $murmurBan->address = $ban->address;
            $murmurBan->bits = $ban->bits;
            $murmurBan->name = $ban->name;
            $murmurBan->hash = $ban->hash;
            $murmurBan->reason = $ban->reason;
            $murmurBan->start = $ban->start;
            $murmurBan->duration = $ban->duration;

            $murmurBans[] = $murmurBan;
        }

        $prx = $this->getServerProxyInterface(ServerPage::TEST_SERVER_ID);
        $prx->setBans($murmurBans);
    }

    public function setUsernameCharactersOnTestServer(string $setting): void
    {
        $server = $this->getTheTestServer();
        $server->setConf('username', $setting);
    }

    public function setChannelNameCharactersOnTestServer(string $setting): void
    {
        $server = $this->getTheTestServer();
        $server->setConf('channelname', $setting);
    }

    public function enableAllowHtmlParameterOnTestServer(): void
    {
        $server = $this->getTheTestServer();
        $server->setConf('allowhtml', 'true');
    }

    public function disableAllowHtmlParameterOnTestServer(): void
    {
        $server = $this->getTheTestServer();
        $server->setConf('allowhtml', 'false');
    }

    // Get the session id of connected user
    public function getUserSession(string $userName = ServerChannelsPage::NODEJS_CLIENT1_NAME): int
    {
        $server = $this->getTheTestServer();
        $users = $server->getUsers();

        foreach ($users as $user) {
            if ($user->name === $userName) {
                return $user->session;
            }
        }

        $this->fail('NodeJs Mumble user is not connected');
        return -1;
    }
}
