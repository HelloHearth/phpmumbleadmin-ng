<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Domain\Service\SecurityServiceInterface;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait LoginTesterActions
{
    public function logIn(string $username, string $password, ?int $serverId = null): void
    {
        $this->fillField(LoginPage::USERNAME_FIELD, $username);
        $this->fillField(LoginPage::PASSWORD_FIELD, $password);
        if (\is_int($serverId)) {
            $this->fillField(LoginPage::SERVER_ID_FIELD, $serverId);
        }
        $this->click(LoginPage::SUBMIT_BUTTON);
    }

    public function seeAmLoggedAsSuperAdmin(): void
    {
        $this->see(LoginPage::SUPER_ADMIN_USERNAME, '.sb-sidenav-footer .name');
        $this->see(SecurityServiceInterface::ROLE_SUPER_ADMIN, '.sb-sidenav-footer .role');
    }

    public function seeAmLoggedAsMumbleUser(string $username = 'mumble user'): void
    {
        $this->see($username, '.sb-sidenav-footer .name');
        $this->see(SecurityServiceInterface::ROLE_MUMBLE_USER, '.sb-sidenav-footer .role');
    }

    public function seeAmLoggedAs(string $username): void
    {
        $this->see($username, '.sb-sidenav-footer .name');
    }
}
