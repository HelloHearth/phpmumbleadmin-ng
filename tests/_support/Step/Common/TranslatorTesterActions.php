<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Infrastructure\Service\NoTranslator;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait TranslatorTesterActions
{
    public function getNotTranslatedText(string $message, array $arguments): string
    {
        return NoTranslator::noTransWithParameters($message, $arguments);
    }

    public function seetNotTranslatedText(string $text, array $arguments, string $selector = null): void
    {
        $text = $this->getNotTranslatedText($text, $arguments);
        $this->see($text, $selector);
    }
}
