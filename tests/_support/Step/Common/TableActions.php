<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

/**
 * Action on the murmur server for tests
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait TableActions
{
    public function seeInTableHeader(string $text, string $selector, int $cell): void
    {
        $this->see($text, $selector." thead tr th:nth-child($cell)");
    }

    public function seeInTable(string $text, $selector, int $line, int $cell): void
    {
        $this->see($text, $selector." tbody tr:nth-child($line) td:nth-child($cell)");
    }

    public function dontSeeInTable(string $text, $selector, int $line, int $cell): void
    {
        $this->dontSee($text, $selector." tbody tr:nth-child($line) td:nth-child($cell)");
    }

    public function seeElementInTable(string $element, string $selector, int $line, int $cell): void
    {
        $this->seeElement($selector." tbody tr:nth-child($line) td:nth-child($cell) $element");
    }

    public function dontSeeElementInTable($element, $selector, int $line, int $cell): void
    {
        $this->dontSeeElement($selector." tbody tr:nth-child($line) td:nth-child($cell) $element");
    }

    public function seeCertificateIconInTable(string $selector, int $line, int $cell): void
    {
        $this->seeElementInTable('i.fa-id-card-alt', $selector, $line, $cell);
    }

    public function dontSeeCertificateIconInTable(string $selector, int $line, int $cell): void
    {
        $this->dontSeeElementInTable('i.fa-id-card-alt', $selector, $line, $cell);
    }

    public function seeNumberOfLineInTable(string $selector, $expected): void
    {
        $this->seeNumberOfElements($selector.' tbody tr', $expected);
    }

    public function seeDatatablesPluginIsEnabled(string $selector): void
    {
        $this->seeElement($selector.'_wrapper.dataTables_wrapper');
    }

    public function seeCellIsEmptyInTable(string $selector, int $line, int $cell): void
    {
        $this->dontSeeElement($selector."tbody tr:nth-child($line) td:nth-child($cell)");
    }
}
