<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Tests\Page\FlashMessagePage;

/**
 * Action on the murmur server for tests
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait FlashMessagesActions
{
    public function seeFlashMessage(string $message, string $type): void
    {
        $this->see($message, $type);
    }

    public function dontSeeFlashMessage(string $message, string $type): void
    {
        $this->dontSee($message, $type);
    }

    public function seeSuccessFlashMessage(string $message): void
    {
        $this->seeFlashMessage($message, FlashMessagePage::SUCCESS);
    }

    public function dontSeeSuccessFlashMessage(string $message): void
    {
        $this->dontSeeFlashMessage($message, FlashMessagePage::SUCCESS);
    }

    public function seeErrorFlashMessage(string $message): void
    {
        $this->seeFlashMessage($message, FlashMessagePage::ERROR);
    }

    public function dontSeeErrorFlashMessage(string $message): void
    {
        $this->dontSeeFlashMessage($message, FlashMessagePage::ERROR);
    }

    // Wait flash message when reloading a page
    public function waitForFlashMessage(string $message, string $type): void
    {
        $this->waitForText($message, 3, $type);
    }

    public function waitForSuccessFlashMessage(string $message): void
    {
        $this->waitForFlashMessage($message, FlashMessagePage::SUCCESS);
    }
}
