<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Tests\Page\Cookie;

/**
 * Action on the murmur server for tests
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait OptionsCookieTesterActions
{
    public function seeInTheOptionsCookieTheLanguageIs(string $language): void
    {
        $cookie = $this->grabCookie(Cookie::OPTIONS);
        $options = \json_decode(\urldecode($cookie));
        $this->assertSame([$language], $options);
    }
}
