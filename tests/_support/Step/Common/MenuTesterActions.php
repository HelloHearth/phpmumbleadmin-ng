<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Tests\Page\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait MenuTesterActions
{
    public function seeOnSideMenuIsSelected(array $menus): void
    {
        foreach ($menus as $menu) {
            $this->see($menu, '.sb-sidenav-menu .nav-link.active');
        }
    }

    public function seeTheServerMenu(): void
    {
        $this->see('channels settings registrations bans logs', Page::CONTENT_NAV.' ul ul');
    }

    public function dontSeeTheServerMenu(): void
    {
        $this->dontSeeElement(Page::CONTENT_NAV.' ul');
    }

    public function seeTheServerMenuForMumbleUser(): void
    {
        $this->seeElement(Page::CONTENT_NAV.' ul');
        $this->dontSee('channels settings registrations bans logs', Page::CONTENT_NAV.' ul');
    }

    public function seeTheConnectionToMumbleUrl(): void
    {
        $this->seeElement(Page::CONTENT_NAV.' a img[alt="connecting_to_the_server"]');
    }

    public function seeTheServerMenuIsActiveFor(string $menu): void
    {
        $this->see($menu, Page::CONTENT_NAV.' ul .menu_level_1 .nav-link.active');
    }
}
