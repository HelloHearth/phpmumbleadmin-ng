<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form;

use App\Domain\Murmur\Model\ServerSetting;
use App\Domain\Service\InMemorySystemCheckerService;
use App\Domain\Service\TranslatorServiceInterface;
use App\Infrastructure\Symfony\Form\DataTransformer\UploadedFileTransformer;
use App\Infrastructure\Symfony\Form\ServerUpdateSettingsType;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\CertificateType;
use Symfony\Component\Form\PreloadedExtension;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerUpdateSettingsTypeTest extends BaseTypeTest
{
    protected function getExtensions(): array
    {
        $parent = parent::getExtensions();

        $translator = (new class implements TranslatorServiceInterface {
            public function trans(string $id, array $parameters = [], string $domain = null, string $locale = null): string
            {
                return '';
            }
        });

        $systemChecker = (new InMemorySystemCheckerService())
            ->with_ssl_extension()
            ->with_upload_file_allowed()
        ;

        $type = new ServerUpdateSettingsType($translator);
        $certificateType = new CertificateType($systemChecker, new UploadedFileTransformer());

        return \array_merge(
            $parent,
            [new PreloadedExtension([$type], [])],
            [new PreloadedExtension([$certificateType], [])],
        );
    }

    public function test_form_with_empty_data(): void
    {
        // Given I create a form
        $form = $this->factory->create(ServerUpdateSettingsType::class);

        // When I submit with empty data
        $form->submit([]);

        // Then
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
    }

    public function test_port_constraint(): void
    {
        // Given I create a form
        $form = $this->factory->create(ServerUpdateSettingsType::class, null, [
            'settings' => [
                'host' => new ServerSetting('host'),
                'port' => new ServerSetting('port'),
            ],
        ]);


        // When I submit with empty data
        $form->submit(['port' => 666666]);

        // Then
        $this->assertTrue($form->isSynchronized());
        $this->assertFalse($form->isValid());
        $this->assertCount(1, $form->get('port')->getErrors());
        $this->assertSame('validator_port_error_message', $form->get('port')->getErrors()[0]->getMessage());
    }

    public function test_form_return_data_only_for_custom_value(): void
    {
        // String fields
        $registerName = new ServerSetting('registername');
        $registerName->setCustomValue('must_to_be_displayed');

        $password = new ServerSetting('password');
        $password->setDefaultValue('must_be_null');

        $registerHostname = new ServerSetting('registerhostname');
        $registerHostname->setCustomValue('must_be_null');
        $registerHostname->setCustomValue('must_to_be_displayed');

        // Integer fields

        // Given I create a form with ServerSetting[]
        $form = $this->factory->create(ServerUpdateSettingsType::class, null, [
            'settings' => [
                'registername' => $registerName,
                'password' => $password,
                'registerhostname' => $registerHostname,
            ],
        ]);

        // When I create the FormView
        $form->createView();

        // Then I see custom data on the FormView
        $this->assertSame('must_to_be_displayed', $form->get('registername')->getData());
        $this->assertNull($form->get('password')->getData());
        $this->assertSame('must_to_be_displayed', $form->get('registerhostname')->getData());
    }

    public function test_registername_field_dont_validate_characters_when_no_pattern_is_set(): void
    {
        // Given I create a form without "registername_pattern" option
        $form = $this->factory->create(ServerUpdateSettingsType::class);

        // When I submit a registername field
        $form->submit(['registername' => 'invalid characters']);

        // Then I see no errors
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
    }

    public function test_registername_field_validate_characters_when_pattern_is_set(): void
    {
        // Given I create a form with "registername_pattern" option
        $form = $this->factory->create(ServerUpdateSettingsType::class, null, [
            'registername_pattern' => '[\w]+',
        ]);

        // When I submit a registername field
        $form->submit(['registername' => 'invalid characters']);

        // Then I see the form has error on the registername field
        $this->assertTrue($form->isSynchronized());
        $this->assertFalse($form->isValid());
        $this->assertCount(1, $form->get('registername')->getErrors());
        $this->assertSame('validator_characters_error_message', $form->get('registername')->getErrors()[0]->getMessage());
    }
}
