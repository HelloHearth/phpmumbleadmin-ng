<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\BanUserType;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Routing\RouterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BanUserTypeTest extends BaseTypeTest
{
    protected function getExtensions(): array
    {
        $parent = parent::getExtensions();

        $type = new BanUserType($this->createMock(RouterInterface::class));

        return \array_merge(
            $parent,
            [new PreloadedExtension([$type], [])],
        );
    }

    public function validDataProvider(): array
    {
        return [
            'All fields' => [[
                'userSession' => 42,
                'ip' => '127.0.0.1',
                'name' => 'any login',
                'hash' => '',
                'reason' => '',
                'endDate' => [
                    'permanent' => true,
                    'date' => '',
                ],
            ]],
            'Required fields' => [[
                'userSession' => 42,
                'ip' => '192.168.42.42',
                'name' => 'any login',
            ]],
        ];
    }

    /** @dataProvider validDataProvider  */
    public function test_form_with_valid_data($data): void
    {
        // Given I create a form
        $form = $this->factory->create(BanUserType::class);

        // When I submit with empty data
        $form->submit($data);

        // Then
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
    }

    public function invalidDataProvider(): array
    {
        return [
            'Invalid userSession' => [
                'userSession',
                [
                    'userSession' => 'Invalid userSession',
                    'ip' => '127.0.0.1',
                    'name' => 'any login',
                ],
            ],
            'Empty ip' => [
                'ip',
                [
                    'userSession' => 42,
                    'ip' => '',
                    'name' => 'any login',
                ],
            ],
            'ip field not submitted' => [
                'ip',
                [
                    'userSession' => 42,
                    'name' => 'any login',
                ],
            ],
            'Empty name' => [
                'name',
                [
                    'userSession' => 42,
                    'ip' => '127.0.0.1',
                    'name' => '',
                ],
            ],
            'name field not submitted' => [
                'name',
                [
                    'userSession' => 42,
                    'ip' => '127.0.0.1',
                ],
            ],
            'Invalid date' => [
                'date',
                [
                    'userSession' => 42,
                    'ip' => '127.0.0.1',
                    'name' => 'any login',
                    'endDate' => [
                        'date' => 'in valid date',
                    ],
                ],
            ],
        ];
    }

    /** @dataProvider invalidDataProvider  */
    public function test_form_with_invalid_data(string $field, array $data): void
    {
        // Given I create a form
        $form = $this->factory->create(BanUserType::class);

        // When I submit with empty data
        $form->submit($data);

        // Then
        $this->assertTrue($form->isSynchronized());
        $this->assertFalse($form->isValid());
        $this->assertsame($field, $form->getErrors(true)[0]->getOrigin()->getName());
    }
}
