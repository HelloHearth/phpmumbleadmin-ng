<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form\CertificateType;

use App\Domain\Service\InMemorySystemCheckerService;
use App\Infrastructure\Symfony\Form\DataTransformer\UploadedFileTransformer;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\CertificateType;
use App\Tests\Unit\Infrastructure\Symfony\Form\BaseTypeTest;
use Symfony\Component\Form\PreloadedExtension;

/**
 * Test CertificateTypeTest without SSL extension and upload file allowed
 *
 * Memo: it's impossible to unit test data of a file upload form type, because
 * of the upload system itself.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CertificateType_file_upload_only_Test extends BaseTypeTest
{
    protected function getExtensions(): array
    {
        $parent = parent::getExtensions();

        $systemChecker = (new InMemorySystemCheckerService())
            ->without_ssl_extension()
            ->with_upload_file_allowed()
        ;

        $certificateType = new CertificateType($systemChecker, new UploadedFileTransformer());

        return \array_merge(
            $parent,
            [new PreloadedExtension([$certificateType], [])],
        );
    }

    public function test_it_add_two_file_type(): void
    {
        // Given I create a form and add a CertificateType field
        $form = $this->factory->create(CertificateType::class);

        // When I create the FormView
        $form = $form->createView();

        // Then I see two FileType has been added
        $this->assertSame(2, $form->count());
        $this->assertSame('file', $form->children['_certificate']->vars['type']);
        $this->assertSame('file', $form->children['_key']->vars['type']);
        $this->assertContains('file', $form->children['_certificate']->vars['block_prefixes']);
        $this->assertContains('file', $form->children['_key']->vars['block_prefixes']);
    }

    // Memo: it's impossible to unit test data of a file upload form type, because
    // of the upload system itself.
    public function test_submit_form_with_invalid_certificate_file(): void
    {
        // Given I create a form and add a CertificateType field
        $form = $this->factory->create();
        $form->add('add_certificate', CertificateType::class);

        // When I submit the form with invalid certificate string
        $form->submit([]);

        // Then I see the form is valid
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
    }
}
