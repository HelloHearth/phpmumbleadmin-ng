<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form\CertificateType;

use App\Domain\Service\InMemorySystemCheckerService;
use App\Infrastructure\Symfony\Form\DataTransformer\UploadedFileTransformer;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\CertificateType;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\PemTextareaCertificateType;
use App\Tests\Page\CertificatePage;
use App\Tests\Unit\Infrastructure\Symfony\Form\BaseTypeTest;
use Symfony\Component\Form\PreloadedExtension;

/**
 * Test CertificateTypeTest with SSL extension and without upload file allowed
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CertificateType_ssl_only_Test extends BaseTypeTest
{
    protected function getExtensions(): array
    {
        $parent = parent::getExtensions();

        $systemChecker = (new InMemorySystemCheckerService())
            ->with_ssl_extension()
            ->without_upload_file_allowed()
        ;

        $certificateType = new CertificateType($systemChecker, new UploadedFileTransformer());

        return \array_merge(
            $parent,
            [new PreloadedExtension([$certificateType], [])],
        );
    }

    public function test_it_add_pem_textarea_certificate_type(): void
    {
        // Given I create a form and add a CertificateType field
        $form = $this->factory->create(CertificateType::class);

        // When I create the FormView
        $form = $form->createView();

        // Then I see PemTextareaCertificateType has been added
        $this->assertSame(1, $form->count());
        $this->assertContains(PemTextareaCertificateType::BLOCK_PREFIX, $form->children['pem']->vars['block_prefixes']);
    }

    public function test_it_submit_the_form_with_invalid_certificate(): void
    {
        // Given I create a form and add a CertificateType field
        $form = $this->factory->create();
        $form->add('certificate', CertificateType::class);
        $field = $form->get('certificate')->get('pem');

        // When I submit the form with invalid certificate string
        $form->submit([
            'certificate' => [
                'pem' => 'invalid certificate'
            ],
        ]);

        // Then I see the form has errors
        $this->assertTrue($form->isSynchronized());
        $this->assertFalse($form->isValid());
        $this->assertSame('invalid certificate', $field->getData());
        $this->assertCount(1, $field->getErrors());
        $this->assertSame('validator_certificate_error_message', $field->getErrors()[0]->getMessage());
    }

    public function test_it_submit_the_form_with_valid_certificate(): void
    {
        // Given I create a form and add a CertificateType field
        $form = $this->factory->create();
        $form->add('new_certificate', CertificateType::class);

        // When I submit the form with valid certificate string
        $form->submit([
            'new_certificate' => [
                'pem' => CertificatePage::PEM,
            ],
        ]);

        // Then I see the form is valid
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
        $this->assertSame(CertificatePage::PEM, $form->get('new_certificate')->get('pem')->getData());
    }

    public function test_it_create_key_and_certificate_fields_on_root_form(): void
    {
        // Given I create a form and add a CertificateType field
        $form = $this->factory->create();
        $form->add('new_certificate', CertificateType::class);

        // When I submit the form with valid certificate string
        $form->submit([
            'new_certificate' => [
                'pem' => CertificatePage::PEM,
            ],
        ]);

        // Then I see key and certificate fields with valid PEM has been created
        $this->assertSame(CertificatePage::PRIVATE_KEY.PHP_EOL, $form->getRoot()->get('key')->getData());
        $this->assertSame(CertificatePage::CERTIFICATE.PHP_EOL, $form->getRoot()->get('certificate')->getData());
    }
}
