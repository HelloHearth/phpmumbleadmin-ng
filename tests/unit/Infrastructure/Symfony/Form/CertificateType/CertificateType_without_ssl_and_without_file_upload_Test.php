<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form\CertificateType;

use App\Domain\Service\InMemorySystemCheckerService;
use App\Infrastructure\Symfony\Form\DataTransformer\UploadedFileTransformer;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\CertificateType;
use App\Tests\Unit\Infrastructure\Symfony\Form\BaseTypeTest;
use Symfony\Component\Form\PreloadedExtension;

/**
 * Test CertificateTypeTest with SSL extension and upload file allowed
 *
 * Memo: it's impossible to unit test data of a file upload form type, because
 * of the upload system itself.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CertificateType_without_ssl_and_without_file_upload_Test extends BaseTypeTest
{
    protected function getExtensions(): array
    {
        $parent = parent::getExtensions();

        $systemChecker = (new InMemorySystemCheckerService())
            ->without_ssl_extension()
            ->without_upload_file_allowed()
        ;

        $certificateType = new CertificateType($systemChecker, new UploadedFileTransformer());

        return \array_merge(
            $parent,
            [new PreloadedExtension([$certificateType], [])],
        );
    }

    public function test_it_add_two_textarea_type(): void
    {
        // Given I create a form with CertificateType field
        $form = $this->factory->create(CertificateType::class);

        // When I create the FormView
        $form = $form->createView();

        // Then I see two TextareaType has been added
        $this->assertSame(2, $form->count());
        $this->assertContains('textarea', $form->children['_certificate']->vars['block_prefixes']);
        $this->assertContains('textarea', $form->children['_key']->vars['block_prefixes']);
    }

    public function test_it_submit_the_form(): void
    {
        // Given I create a form and add a CertificateType field
        $form = $this->factory->create();
        $form->add('add_certificate', CertificateType::class);

        // When I submit the form with invalid certificate string
        $form->submit([
            'add_certificate' => [
                '_certificate' => 'invalid certificate',
                '_key' => 'invalid key',
            ],
        ]);

        // Then I see the form is valid, it's impossible to validate certificate
        // without SSL extension...
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
        $this->assertSame('invalid certificate', $form->get('certificate')->getData());
        $this->assertSame('invalid key', $form->get('key')->getData());
    }
}
