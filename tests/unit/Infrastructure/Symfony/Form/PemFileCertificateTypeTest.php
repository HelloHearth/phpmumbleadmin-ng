<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\Type\ServerSettings\PemFileCertificateType;

/**
 * Memo: it's impossible to unit test data of a file upload form type, because
 * of the upload system itself.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PemFileCertificateTypeTest extends BaseTypeTest
{
    public function test_submit_form_with_invalid_certificate_file(): void
    {
        // Given I create a form and add a PemFileCertificateType field
        $form = $this->factory->create();
        $form->add('pem', PemFileCertificateType::class);

        // When I submit the form
        $form->submit(['pem' => null]);

        // Then I see the form is valid
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
    }
}
