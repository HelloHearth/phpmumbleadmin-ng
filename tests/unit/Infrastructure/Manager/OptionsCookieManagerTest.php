<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Manager;

use App\Domain\Model\OptionsCookie;
use App\Infrastructure\Manager\OptionsCookieManager;
use App\Infrastructure\Service\RequestService;
use Codeception\Test\Unit;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class OptionsCookieManagerTest extends Unit
{
    private RequestStack $requestStack;
    private RequestService $requestService;
    private ParameterBag $parameterBag;

    public function _before(): void
    {
        $this->requestStack = new RequestStack();
        $this->requestService = new RequestService($this->requestStack);
        $this->parameterBag = new ParameterBag();
    }

    private function init(): void
    {
        $this->parameterBag->set('app.options_cookie_name', 'optionCookieName');
        $request = new Request();
        $request->cookies->set('optionCookieName', '["test_locale"]');
        $this->requestStack->push($request);
    }

    public function test_get_cookie_method_without_options_cookie(): void
    {
        $optionsCookieManager = new OptionsCookieManager($this->requestService, $this->parameterBag);

        $cookie = $optionsCookieManager->getCookie();

        $this->assertEquals(new OptionsCookie(), $cookie);
    }

    public function test_constructor_with_options_cookie_and_without_request(): void
    {
        $this->parameterBag->set('app.options_cookie_name', 'optionCookieName');
        $optionsCookieManager = new OptionsCookieManager($this->requestService, $this->parameterBag);

        $cookie = $optionsCookieManager->getCookie();

        $this->assertEquals(new OptionsCookie(), $cookie);
    }

    public function test_constructor_initialize_the_options_cookie(): void
    {
        $this->init();
        $optionsCookieManager = new OptionsCookieManager($this->requestService, $this->parameterBag);

        $cookie = $optionsCookieManager->getCookie();

        $this->assertEquals('test_locale', $cookie->getLocale());
    }
}
