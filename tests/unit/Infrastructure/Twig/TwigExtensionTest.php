<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Twig;

use App\Domain\Service\LocaleServiceInterface;
use App\Infrastructure\Menu\LocalesMenuBuilder;
use App\Infrastructure\Twig\AppExtension;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TwigExtensionTest extends Unit
{
    public function htmlForRowWithoutJavascriptProvider(): array
    {
        return [
            [null, ''],
            ['', ''],
            ['Hello <script>alert("ho ho ho");</script>dear', 'Hello dear'],
        ];
    }

    /**
     * @dataProvider htmlForRowWithoutJavascriptProvider
     */
    public function test_raw_without_javascript_method($html, $expected): void
    {
        $twigExtension = new AppExtension($this->makeEmpty(LocaleServiceInterface::class), $this->makeEmpty(LocalesMenuBuilder::class));

        $result = $twigExtension->RawWithoutJavascript($html);

        $this->assertSame($expected, $result);
    }
}
