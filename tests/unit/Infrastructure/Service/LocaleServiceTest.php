<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Service;

use App\Infrastructure\Service\LocaleService;
use App\Infrastructure\Service\RequestService;
use Codeception\Test\Unit;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LocaleServiceTest extends Unit
{
    public function test_get_locale_method_with_full_configuration(): void
    {
        // Given I have a request and a parameter default_local sets
        $request = new Request();
        $request->setLocale('locale_from_request');

        $parameterBag = new ParameterBag();
        $parameterBag->set('kernel.default_locale', 'locale_from_parameter_bag');

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $requestService = new RequestService($requestStack);

        $service = new LocaleService($requestService, $parameterBag);

        // When I call the getLocale() method
        $result = $service->getLocale();

        // Then I see I get the locale from the request
        $this->assertSame('locale_from_request', $result);
    }

    public function test_get_locale_method_without_request(): void
    {
        // Given I have an empty RequestStack, but I have the default parameter set
        $parameterBag = new ParameterBag();
        $parameterBag->set('kernel.default_locale', 'locale_from_parameter_bag');
        $service = new LocaleService(new RequestService(new RequestStack()), $parameterBag);

        // When I call the getLocale() method
        $result = $service->getLocale();

        // Then I see I get the locale from the request
        $this->assertSame('locale_from_parameter_bag', $result);
    }

    public function test_get_locale_method_without_request_or_parameter(): void
    {
        // Given I have an empty RequestStack, and I don't have the default parameter set
        $service = new LocaleService(new RequestService(new RequestStack()), new ParameterBag());

        // When I call the getLocale() method
        $result = $service->getLocale();

        // Then I see I get the english locale
        $this->assertSame('en', $result);
    }
}
