<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Service;

use App\Infrastructure\Service\RequestService;
use Codeception\Test\Unit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RequestServiceTest extends Unit
{
    public function test_is_http_request(): void
    {
        // Given
        $request = new Request();
        $requestStack = new RequestStack();
        $requestStack->push($request);
        $service = new RequestService($requestStack);

        // When
        $result = $service->isHttpRequest();

        // Then
        $this->assertTrue($result);
    }

    public function test_is_http_request_with_xml_http_header(): void
    {
        // Given
        $request = new Request();
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        $requestStack = new RequestStack();
        $requestStack->push($request);
        $service = new RequestService($requestStack);

        // When
        $result = $service->isHttpRequest();

        // Then
        $this->assertFalse($result);
    }

    public function test_is_http_request_with_empty_requestStack(): void
    {
        // Given
        $requestStack = new RequestStack();
        $service = new RequestService($requestStack);

        // When
        $result = $service->isHttpRequest();

        // Then
        $this->assertFalse($result);
    }

    public function test_is_xml_http_request(): void
    {
        // Given
        $request = new Request();
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        $requestStack = new RequestStack();
        $requestStack->push($request);
        $service = new RequestService($requestStack);

        // When
        $result = $service->isXmlHttpRequest();

        // Then
        $this->assertTrue($result);
    }

    public function test_is_xml_http_request_without_xml_http_header(): void
    {
        // Given
        $request = new Request();
        $requestStack = new RequestStack();
        $requestStack->push($request);
        $service = new RequestService($requestStack);

        // When
        $result = $service->isXmlHttpRequest();

        // Then
        $this->assertFalse($result);
    }

    public function test_is_xml_http_request_with_empty_requestStack(): void
    {
        // Given
        $requestStack = new RequestStack();
        $service = new RequestService($requestStack);

        // When
        $result = $service->isXmlHttpRequest();

        // Then
        $this->assertFalse($result);
    }
}
