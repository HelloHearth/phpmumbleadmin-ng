<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Service;

use App\Infrastructure\Service\ClockService;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ClockServiceTest extends Unit
{
    public function timezoneProvider(): array
    {
        return [
            'UTC'               => ['UTC', '+0000'],
            'Europe/Paris'      => ['Europe/Paris', '+0200'],
            'America/Curacao'   => ['America/Curacao', '-0400'],
            'Etc/GMT+0'         => ['Etc/GMT+0', '+0000'],
            'Etc/GMT-6'         => ['Etc/GMT-6', '+0600'],
            'Etc/GMT+9'         => ['Etc/GMT+9', '-0900'],
        ];
    }

    /** @dataProvider timezoneProvider */
    public function test_get_timezone_diff_method(string $timezone, string $expected): void
    {
        // Given
        \date_default_timezone_set($timezone);
        $service = new ClockService();

        // When
        $result = $service->getTimezoneDiff();

        // Then
        $this->assertSame($expected, $result);
    }
}
