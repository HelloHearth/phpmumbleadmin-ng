<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Service;

use App\Domain\Service\LocaleServiceInterface;
use App\Infrastructure\Service\I18DateService;
use App\Tests\Page\Timestamp;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class I18DateServiceTest extends Unit
{
    public function timestampProvider(): array
    {
        return [
            [Timestamp::UTC_WC_1998, '12 juillet 1998', '22:50:00'],
            [Timestamp::UTC_WC_2018, '15 juillet 2018', '18:50:00']
        ];
    }

    /** @dataProvider timestampProvider */
    public function test_service_return_internationalized_date(int $timestamp, string $expectedDate): void
    {
        // Given
        date_default_timezone_set('Europe/Paris');
        $service = new I18DateService($this->makeEmpty(LocaleServiceInterface::class, ['getLocale' => 'fr']));

        // When I call the getLocale() method
        $resultDate = $service->logDate($timestamp);

        // Then I see I get the locale from the request
        $this->assertSame($expectedDate, $resultDate);
    }

    /** @dataProvider timestampProvider */
    public function test_service_return_time(int $timestamp, string $expectedDate, string $expectedTime): void
    {
        // Given
        date_default_timezone_set('Europe/Paris');
        $service = new I18DateService($this->makeEmpty(LocaleServiceInterface::class, ['getLocale' => 'fr']));

        // When I call the getLocale() method
        $resultTime = $service->logTime($timestamp);

        // Then I see I get the locale from the request
        $this->assertSame($expectedTime, $resultTime);
    }
}
