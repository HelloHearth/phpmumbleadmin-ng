<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Bus;

use App\Domain\Bus\AbstractEvent;
use App\Domain\Bus\BusResponse;
use Codeception\Test\Unit;

class AbstractTestEvent extends AbstractEvent
{
    public function getKey(): string
    {
        return '';
    }
}

class TestEvent extends AbstractTestEvent {}
class TestEvent2 extends AbstractTestEvent {}
class TestEvent3 extends AbstractTestEvent {}

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BusResponseTest extends Unit
{
    public function test_constructor(): void
    {
        $busResponse = new BusResponse();

        $this->assertInstanceOf(BusResponse::class, $busResponse);
    }

    public function test_constructor_with_invalid_events_throw_exception(): void
    {
        $this->expectException(\TypeError::class);

        new BusResponse(['invalid arg']);
    }

    public function test_has_events_method(): void
    {
        $busResponse = new BusResponse([new TestEvent(), new TestEvent2(), new TestEvent3()]);

        $this->assertTrue($busResponse->hasEvents());
    }

    public function test_has_events_method_without_events(): void
    {
        $busResponse = new BusResponse();

        $this->assertFalse($busResponse->hasEvents());
    }

    public function test_has_event_method(): void
    {
        $busResponse = new BusResponse([new TestEvent(), new TestEvent2(), new TestEvent3()]);

        $this->assertTrue($busResponse->hasEvent(TestEvent::class));
        $this->assertTrue($busResponse->hasEvent(TestEvent2::class));
        $this->assertTrue($busResponse->hasEvent(TestEvent3::class));
        $this->assertFalse($busResponse->hasEvent('test other event class'));
    }

    public function test_has_event_method_without_event(): void
    {
        $busResponse = new BusResponse();

        $this->assertFalse($busResponse->hasEvent(TestEvent2::class));
    }

    public function test_get_events_method(): void
    {
        $busResponse = new BusResponse([new TestEvent(), new TestEvent2(), new TestEvent3()]);

        $this->assertCount(3, $busResponse->getEvents());
        $this->assertEquals([new TestEvent(), new TestEvent2(), new TestEvent3()], $busResponse->getEvents());
    }

    public function test_get_events_method_without_events(): void
    {
        $busResponse = new BusResponse();

        $this->assertCount(0, $busResponse->getEvents());
        $this->assertEquals([], $busResponse->getEvents());
    }

    public function test_get_event_method(): void
    {
        $busResponse = new BusResponse([new TestEvent(), new TestEvent2(), new TestEvent3()]);

        $this->assertInstanceOf(TestEvent2::class, $busResponse->getEvent(TestEvent2::class));
        $this->assertNull($busResponse->getEvent('test other event class'));
    }

    public function test_get_event_method_without_event(): void
    {
        $busResponse = new BusResponse();

        $this->assertNull($busResponse->getEvent(TestEvent2::class));
    }

    public function test_get_data_method(): void
    {
        $busResponse = new BusResponse();

        $this->assertNull($busResponse->getData());
    }

    public function test_get_set_data_methods(): void
    {
        $busResponse = new BusResponse();

        $busResponse->setData(['test get set data']);

        $this->assertSame(['test get set data'], $busResponse->getData());
    }
}
