<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Mapping;

use App\Domain\Model\AdminLog;
use App\Domain\Service\LoggerServiceInterface;
use App\Entity\AdminLogEntity;
use App\Infrastructure\Mapping\AdminLogMapping;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminLogMappingTest extends Unit
{
    protected UnitTester $tester;

    public function test_to_entity_method(): void
    {
        // Given
        $mapper = new AdminLogMapping();

        $adminLog = new AdminLog();
        $adminLog->setAddress('test model address');
        $adminLog->setContext(['test model context']);
        $adminLog->setFacility(42);
        $adminLog->setLevel(84);
        $adminLog->setMessage('test model message');
        $adminLog->setTimestamp(42424242);

        // When
        $result = $mapper->toEntity($adminLog);

        // Then
        $this->assertSame('test model address', $result->getAddress());
        $this->assertSame(['test model context'], $result->getContext());
        $this->assertSame(42, $result->getFacility());
        $this->assertSame(84, $result->getLevel());
        $this->assertSame('test model message', $result->getMessage());
        $this->assertSame(42424242, $result->getTimestamp());
    }

    public function test_from_entity_method(): void
    {
        // Given
        $mapper = new AdminLogMapping();

        $entity = new AdminLogEntity();
        $entity->setAddress('test entity address');
        $entity->setContext(['test entity context']);
        $entity->setFacility(42);
        $entity->setLevel(84);
        $entity->setMessage('test entity message');
        $entity->setTimestamp(42424242);

        // When
        $result = $mapper->fromEntity($entity);

        // Then
        $this->assertSame('test entity address', $result->getAddress());
        $this->assertSame(['test entity context'], $result->getContext());
        $this->assertSame(42, $result->getFacility());
        $this->assertSame(84, $result->getLevel());
        $this->assertSame('test entity message', $result->getMessage());
        $this->assertSame(42424242, $result->getTimestamp());
    }

    public function test_from_entity_to_array_method(): void
    {
        // Given
        $mapper = new AdminLogMapping();

        $entity = new AdminLogEntity();
        $entity->setAddress('test entity address');
        $entity->setContext(['test entity context']);
        $entity->setFacility(42);
        $entity->setLevel(84);
        $entity->setMessage('test entity message');
        $entity->setTimestamp(42424242);

        // When
        $result = $mapper->fromEntityToArray($entity);

        // Then
        $this->assertSame('test entity address', $result['address']);
        $this->assertSame(['test entity context'], $result['context']);
        $this->assertSame(42, $result['facility']);
        $this->assertSame(84, $result['level']);
        $this->assertSame('test entity message', $result['message']);
        $this->assertSame(42424242, $result['timestamp']);
    }

    public function test_from_entities_method(): void
    {
        // Given
        $mapper = new AdminLogMapping();

        $entity1 = $this->tester->createAdminLog(['timestamp' => 42424242, 'message' => 'test entity1 message']);
        $entity2 = $this->tester->createAdminLog(['timestamp' => 84848484, 'message' => 'test entity2 message']);

        // When
        $result = $mapper->fromEntities([$entity1, $entity2]);

        // Then
        $this->assertSame('192.168.42.42', $result[0]->getAddress());
        $this->assertSame([], $result[0]->getContext());
        $this->assertSame(LoggerServiceInterface::FACILITY_USER, $result[0]->getFacility());
        $this->assertSame(LoggerServiceInterface::LEVEL_INFORMATIONAL, $result[0]->getLevel());
        $this->assertSame('test entity1 message', $result[0]->getMessage());
        $this->assertSame(42424242, $result[0]->getTimestamp());

        $this->assertSame('192.168.42.42', $result[1]->getAddress());
        $this->assertSame([], $result[1]->getContext());
        $this->assertSame(LoggerServiceInterface::FACILITY_USER, $result[1]->getFacility());
        $this->assertSame(LoggerServiceInterface::LEVEL_INFORMATIONAL, $result[1]->getLevel());
        $this->assertSame('test entity2 message', $result[1]->getMessage());
        $this->assertSame(84848484, $result[1]->getTimestamp());
    }

    public function test_from_entities_to_array_method(): void
    {
        // Given
        $mapper = new AdminLogMapping();

        $entity1 = $this->tester->createAdminLog(['timestamp' => 42424242, 'message' => 'test entity1 message']);
        $entity2 = $this->tester->createAdminLog(['timestamp' => 84848484, 'message' => 'test entity2 message']);

        // When
        $result = $mapper->fromEntitiesToArray([$entity1, $entity2]);

        // Then
        $this->assertSame('192.168.42.42', $result[0]['address']);
        $this->assertSame([], $result[0]['context']);
        $this->assertSame(LoggerServiceInterface::FACILITY_USER, $result[0]['facility']);
        $this->assertSame(LoggerServiceInterface::LEVEL_INFORMATIONAL, $result[0]['level']);
        $this->assertSame('test entity1 message', $result[0]['message']);
        $this->assertSame(42424242, $result[0]['timestamp']);

        $this->assertSame('192.168.42.42', $result[1]['address']);
        $this->assertSame([], $result[1]['context']);
        $this->assertSame(LoggerServiceInterface::FACILITY_USER, $result[1]['facility']);
        $this->assertSame(LoggerServiceInterface::LEVEL_INFORMATIONAL, $result[1]['level']);
        $this->assertSame('test entity2 message', $result[1]['message']);
        $this->assertSame(84848484, $result[1]['timestamp']);
    }
}
