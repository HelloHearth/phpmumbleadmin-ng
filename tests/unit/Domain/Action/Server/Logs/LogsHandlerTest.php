<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Logs;

use App\Domain\Action\Server\Logs\LogsHandler;
use App\Domain\Action\Server\Logs\LogsQuery;
use App\Domain\Action\Server\Logs\LogsRequested;
use App\Domain\Action\Server\Logs\LogsViewModel;
use App\Domain\Action\Server\Logs\TotalLogsInfoPanel;
use App\Domain\Model\Log;
use App\Domain\Murmur\Exception\Server\InvalidSecretException;
use App\Domain\Murmur\Helper\LogsHelper;
use App\Domain\Murmur\Model\Mock\LogEntryMock;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Service\ClockInterface;
use App\Domain\Service\LocaleServiceInterface;
use App\Infrastructure\Service\I18DateService;
use App\Tests\Page\Timestamp;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogsHandlerTest extends Unit
{
    private LogsViewModel $viewModel;
    private LogsHandler $handler;

    public function _before(): void
    {
        $this->viewModel = new LogsViewModel(42);
        $this->handler = new LogsHandler(
            new I18DateService($this->makeEmpty(LocaleServiceInterface::class, ['getLocale' => 'fr'])),
            $this->makeEmpty(ClockInterface::class, ['getTimezoneDiff' => '+0200']),
        );
    }

    private function mockServerWithLogs(array $logs): MockObject
    {
        $mockServer = $this->createMock(ServerInterface::class);
        $mockServer->method('getLogLen')->willReturn(\count($logs));
        $mockServer->method('getLog')->willReturn($logs);

        return $mockServer;
    }

    public function test_listen_to_method(): void
    {
        $result = $this->handler->listenTo();

        $this->assertSame(LogsQuery::class, $result);
    }

    public function test_handler_with_no_logs_on_server(): void
    {
        // Given
        $query = new LogsQuery($this->viewModel, $this->viewModel->getServerId(), -1);
        $query->prx = $this->makeEmpty(ServerInterface::class);

        // When
        $response = $this->handler->handle($query);

        // Then
        $logsBag = \json_decode($this->viewModel->getLogsBag());
        $infoPanels = $this->viewModel->getInfoPanel();

        $this->assertCount(1, $infoPanels);
        $this->assertInstanceOf(TotalLogsInfoPanel::class, $infoPanels[0]);
        /** @var TotalLogsInfoPanel[] $infoPanels */
        $this->assertSame(0, $infoPanels[0]->getArguments()['%total%']);
        $this->assertSame(0, $this->viewModel->getTotalOfLogs());
        $this->assertSame(42, $logsBag->serverId);
        $this->assertCount(0, $logsBag->logs);
    }

    public function test_total_methods_with_logs_on_server(): void
    {
        // Given
        $query = new LogsQuery($this->viewModel, $this->viewModel->getServerId(), -1);
        $query->prx = $this->mockServerWithLogs([
                new LogEntryMock(123, 'text'),
                new LogEntryMock(123123, 'text for second log entry'),
                new LogEntryMock(123123123, 'text for third log entry')
        ]);

        // When
        $response = $this->handler->handle($query);

        // Then
        $logsBag = \json_decode($this->viewModel->getLogsBag());
        $infoPanels = $this->viewModel->getInfoPanel();

        $this->assertCount(1, $infoPanels);
        $this->assertInstanceOf(TotalLogsInfoPanel::class, $infoPanels[0]);
        /** @var TotalLogsInfoPanel[] $infoPanels */
        $this->assertSame(3, $infoPanels[0]->getArguments()['%total%']);
        $this->assertSame(3, $this->viewModel->getTotalOfLogs());
        $this->assertCount(3, $logsBag->logs);
    }

    public function test_log_date_and_time_parameters_when_the_day_has_changed_or_not(): void
    {
        // Given I have logEntries with modified timestamp (like Murmur::getLog do)
        date_default_timezone_set('Europe/Paris');
        $query = new LogsQuery($this->viewModel, $this->viewModel->getServerId(), -1);
        $diff = LogsHelper::timestampDiffWorkaround('+0200');
        $logs = [
            new LogEntryMock(Timestamp::UTC_WC_1998 -$diff,    'text'),
            new LogEntryMock(Timestamp::UTC_WC_1998 -$diff,    'text'),
            new LogEntryMock(Timestamp::UTC_WC_1998 -$diff +1, 'text'),
            new LogEntryMock(Timestamp::UTC_WC_2018 -$diff,    'text'),
            new LogEntryMock(Timestamp::UTC_WC_2018 -$diff +1, 'text')
        ];
        $query->prx = $this->mockServerWithLogs($logs);

        // When I handle the command
        $this->handler->handle($query);

        // Then I see date and time are correct
        $logsBag = \json_decode($this->viewModel->getLogsBag());
        /** @var Log[] $logs */
        $logs = $logsBag->logs;
        $this->assertSame('12 juillet 1998', $logs[0]->date);
        $this->assertSame('22:50:00', $logs[0]->time);

        $this->assertEmpty($logs[1]->date);
        $this->assertSame('22:50:00', $logs[1]->time);

        $this->assertEmpty($logs[2]->date);
        $this->assertSame('22:50:01', $logs[2]->time);

        $this->assertSame('15 juillet 2018', $logs[3]->date);
        $this->assertSame('18:50:00', $logs[3]->time);

        $this->assertEmpty($logs[4]->date);
        $this->assertSame('18:50:01', $logs[4]->time);
    }

    public function test_handler_dont_crash_on_invalid_secret_exception_with_get_log_len_method(): void
    {
        // Given I have a getLogLen() which will throw a InvalidSecretException
        $query = new LogsQuery($this->viewModel, $this->viewModel->getServerId(), -1);
        $query->prx = $this->createMock(ServerInterface::class);
        $query->prx->method('getLogLen')->willThrowException(new InvalidSecretException());

        // When I handle the method
        $response = $this->handler->handle($query);

        // Then I see the command ended with success
        $this->assertTrue($response->hasEvent(LogsRequested::class));
        $this->assertCount(1, $response->getEvents());
    }
}
