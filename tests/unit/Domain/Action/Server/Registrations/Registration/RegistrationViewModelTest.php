<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\Registration;

use App\Domain\Action\Server\Registrations\Registration;
use App\Domain\Action\Server\Registrations\Registration\RegistrationViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationViewModelTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new RegistrationViewModel(42);

        $result = $viewModel->getServerId();

        $this->assertSame(42, $result);
    }

    public function test_getter_and_setter_for_registration_field(): void
    {
        $viewModel = new RegistrationViewModel(42);
        $this->assertNull($viewModel->getRegistration());

        $viewModel->setRegistration($registration = new Registration(42, []));

        $this->assertSame($registration, $viewModel->getRegistration());
    }

    public function test_getter_and_setter_for_avatar_field(): void
    {
        $viewModel = new RegistrationViewModel(42);
        $this->assertNull($viewModel->getAvatar());

        $viewModel->setAvatar('data:image/png;base64, avatar blob');

        $this->assertSame('data:image/png;base64, avatar blob', $viewModel->getAvatar());
    }
}
