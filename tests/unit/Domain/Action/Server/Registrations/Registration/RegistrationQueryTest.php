<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\Registration;

use App\Domain\Action\Server\Registrations\Registration\RegistrationQuery;
use App\Domain\Action\Server\Registrations\Registration\RegistrationViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationQueryTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new RegistrationViewModel(42);
        $query = new RegistrationQuery(42, 96, $viewModel);

        $this->assertSame(42, $query->serverId);
        $this->assertSame(96, $query->registrationId);
        $this->assertSame($viewModel, $query->viewModel);
    }
}
