<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\Index;

use App\Domain\Action\Server\Registrations\Index\RegistrationsQuery;
use App\Domain\Action\Server\Registrations\Index\RegistrationsViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsQueryTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new RegistrationsViewModel(42);
        $query = new RegistrationsQuery(42, $viewModel);

        $this->assertSame(42, $query->serverId);
        $this->assertSame($viewModel, $query->viewModel);
    }
}
