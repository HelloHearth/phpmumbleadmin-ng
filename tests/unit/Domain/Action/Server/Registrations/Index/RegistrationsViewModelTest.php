<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\Index;

use App\Domain\Action\Server\Registrations\Index\RegistrationsViewModel;
use App\Domain\Action\Server\Registrations\Registration;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsViewModelTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new RegistrationsViewModel(42);

        $result = $viewModel->getServerId();

        $this->assertSame(42, $result);
    }

    public function test_registrations_getter_and_setter(): void
    {
        $viewModel = new RegistrationsViewModel(42);

        $result = $viewModel->getRegistrations();
        $this->assertSame([], $result);

        $viewModel->setRegistrations([$registration = new Registration(1, ['test_registrations_getter'])]);
        $result = $viewModel->getRegistrations();

        $this->assertSame([$registration], $result);
    }

    public function test_registrations_setter_accept_only_registration_object(): void
    {
        $viewModel = new RegistrationsViewModel(42);
        $this->expectException(\InvalidArgumentException::class);

        $viewModel->setRegistrations(['test_registrations_setter']);
    }
}
