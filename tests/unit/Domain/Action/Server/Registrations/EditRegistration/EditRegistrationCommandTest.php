<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Action\Server\Registrations\EditRegistration\EditRegistrationCommand;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditRegistrationCommandTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new EditRegistrationCommand(42, 96, ['original_data'], 'username field', 'email field', 'description field', 'plainPassword field');

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->registrationId);
        $this->assertSame(['original_data'], $command->originalData);
        $this->assertSame('username field', $command->username);
        $this->assertSame('email field', $command->email);
        $this->assertSame('description field', $command->description);
        $this->assertSame('plainPassword field', $command->plainPassword);
    }
}
