<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Action\Server\Registrations\EditRegistration\ServerDontAllowHtmlTags;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerDontAllowHtmlTagsTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new ServerDontAllowHtmlTags(24, 42, 'description with html');

        $this->assertSame(24, $command->serverId);
        $this->assertSame(42, $command->registrationId);
        $this->assertSame('description with html', $command->description);
    }

    public function test_get_key_method(): void
    {
        $event = new ServerDontAllowHtmlTags(24, 42, 'description with html');

        $this->assertSame(ServerDontAllowHtmlTags::KEY, $event->getKey());
    }
}
