<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations;

use App\Domain\Action\Server\Registrations\TotalRegistrationsInfoPanel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TotalRegistrationsInfoPanelTest extends Unit
{
    public function test_constructor(): void
    {
        $infoPanel = new TotalRegistrationsInfoPanel(42);

        $result = $infoPanel->getArguments();

        $this->assertSame(['%total%' => 42], $result);
    }

    public function test_get_test_method(): void
    {
        $infoPanel = new TotalRegistrationsInfoPanel(42);

        $result = $infoPanel->getText();

        $this->assertSame('registrations_total', $result);
    }
}
