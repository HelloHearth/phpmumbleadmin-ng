<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\Channels;

use App\Domain\Action\Server\Channels\Channels\ChannelsHandler;
use App\Domain\Action\Server\Channels\Channels\ChannelsPageRequested;
use App\Domain\Action\Server\Channels\Channels\ChannelsQuery;
use App\Domain\Action\Server\Channels\Channels\ChannelsViewModel;
use App\Domain\Action\Server\ServerIsNotBooted;
use App\Domain\Murmur\Middleware\ConnectionError;
use App\Domain\Murmur\Model\MetaInterface;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Service\SecurityServiceInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelsHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new ChannelsHandler($this->makeEmpty(SecurityServiceInterface::class));

        $this->assertSame(ChannelsQuery::class, $handler->listenTo());
    }

    private function getEmptyTree(): object
    {
        $tree = new \stdClass();
        $tree->users = [];
        $tree->children = [];
        $tree->c = new \stdClass();
        $tree->c->id = 0;
        $tree->c->description = '';
        $tree->c->temporary = false;
        $tree->c->links = [];

        return $tree;
    }

    private function mockServer(bool $isRunning = true): ServerInterface
    {
        return $this->makeEmpty(ServerInterface::class, [
            'getSid' => 42,
            'isRunning' => $isRunning,
            'getTree' => $this->getEmptyTree(),
            'getChannels' => [],
            'getACL' => function (int $serverId, &$aclList, &$null, &$null2) {
                $aclList = [];
            }
        ]);
    }

    public function test_request_viewer_return_success_event(): void
    {
        $model = new ChannelsViewModel(42);
        $query = new ChannelsQuery($model, 42);
        $query->Murmur = $this->createMock(MetaInterface::class);
        $query->prx = $this->mockServer();

        $handler = new ChannelsHandler($this->makeEmpty(SecurityServiceInterface::class));
        $response = $handler->handle($query);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelsPageRequested::class));
        $this->assertJson($model->getJsonViewer());
        $this->assertTrue($model->isRunning());
        $this->assertTrue($model->requireIsRunning());
        $this->assertTrue($model->getHasConnection());
    }

    public function test_request_viewer_when_server_is_not_running_return_success_event(): void
    {
        $model = new ChannelsViewModel(42);
        $query = new ChannelsQuery($model, 42);
        $query->prx = $this->mockServer(false);

        $handler = new ChannelsHandler($this->makeEmpty(SecurityServiceInterface::class));
        $response = $handler->handle($query);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ServerIsNotBooted::class));
        $this->assertNull($model->getJsonViewer());
        $this->assertFalse($model->isRunning());
        $this->assertTrue($model->requireIsRunning());
        $this->assertTrue($model->getHasConnection());
    }

    private function mockThrowIceMemoryLimit(): object
    {
        return $this->makeEmpty(ServerInterface::class, [
            'getSid' => 42,
            'isRunning' => true,
            'getTree' => function() { throw new \Ice_MemoryLimitException(); },
            'getChannels' => [],
            'getACL' => function (int $serverId, &$aclList, &$null, &$null2) {
                $aclList = [];
            }
        ]);
    }

    public function test_request_viewer_when_ice_memory_limit_return_error_event(): void
    {
        $model = new ChannelsViewModel(42);
        $query = new ChannelsQuery($model, 42);
        $query->prx = $this->mockThrowIceMemoryLimit();

        $handler = new ChannelsHandler($this->makeEmpty(SecurityServiceInterface::class));
        $response = $handler->handle($query);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ConnectionError::class));
        $this->assertNull($model->getJsonViewer());
    }
}
