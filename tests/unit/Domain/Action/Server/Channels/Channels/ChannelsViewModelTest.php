<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\Channels;

use App\Domain\Action\Server\Channels\Channels\ChannelsViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelsViewModelTest extends Unit
{
    public function test_empty_constructor(): void
    {
        new ChannelsViewModel(12);
    }

    public function test_get_set_mumble_tree_method(): void
    {
        $viewModel = new ChannelsViewModel(12);

        $this->assertNull($viewModel->getJsonViewer());

        $viewModel->setJsonViewer('{test mumble tree field}');
        $this->assertSame('{test mumble tree field}', $viewModel->getJsonViewer());
    }
}
