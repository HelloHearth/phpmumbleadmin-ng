<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\Channels;

use App\Domain\Action\Server\Channels\Channels\ChannelsQuery;
use App\Domain\Action\Server\Channels\Channels\ChannelsViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelQueryTest extends Unit
{
    public function test_constructor(): void
    {
        $model = new ChannelsViewModel(42);
        $query = new ChannelsQuery($model, 42);

        $this->assertSame($model, $query->viewModel);
        $this->assertSame(42, $query->serverId);
    }
}
