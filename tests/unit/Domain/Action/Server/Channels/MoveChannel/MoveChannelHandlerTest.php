<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\MoveChannel;

use App\Domain\Action\Server\Channels\MoveChannel\ChannelMoved;
use App\Domain\Action\Server\Channels\MoveChannel\MoveChannelCommand;
use App\Domain\Action\Server\Channels\MoveChannel\MoveChannelHandler;
use App\Domain\Action\Server\Channels\MoveChannel\MoveTheRootChannelNotAllowed;
use App\Domain\Action\Server\Channels\MoveChannel\MoveToChildrenChannelNotAllowed;
use App\Domain\Action\Server\Channels\MoveChannel\MoveToItselfNotAllowed;
use App\Domain\Action\Server\Channels\MoveChannel\MoveToParentChannelNotAllowed;
use App\Domain\Murmur\Exception\Server\InvalidChannelException;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;
use stdClass;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MoveChannelHandlerTest extends Unit
{
    private function createMockedServer(bool $mockSetChannelState = false): object
    {
        $params = [
            'getChannelState' => function () {
                $channelState = new stdClass();
                $channelState->parent = 42;
                return $channelState;
            }
        ];
        if ($mockSetChannelState) {
            $params['setChannelState'] = function () {
                throw new InvalidChannelException();
            };
        }

        return $this->makeEmpty(ServerInterface::class, $params);
    }

    public function test_listen_to_method(): void
    {
        // Given
        $handler = new MoveChannelHandler();

        // When
        $result = $handler->listenTo();

        // Then
        $this->assertSame(MoveChannelCommand::class, $result);
    }

    public function test_moving_the_root_channel(): void
    {
        // Given
        $command = new MoveChannelCommand(-1);
        $command->channelId = 0;
        $command->toChannelId = -1;
        $command->prx = $this->createMockedServer();
        $handler = new MoveChannelHandler();

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(MoveTheRootChannelNotAllowed::class));
    }

    public function test_moving_channel_to_its_parent(): void
    {
        // Given
        $command = new MoveChannelCommand(-1);
        $command->channelId = -1;
        $command->toChannelId = 42;
        $command->prx = $this->createMockedServer();
        $handler = new MoveChannelHandler();

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(MoveToParentChannelNotAllowed::class));
    }

    public function test_moving_channel_to_itself(): void
    {
        // Given
        $command = new MoveChannelCommand(-1);
        $command->channelId = 42;
        $command->toChannelId = 42;
        $command->prx = $this->createMockedServer();
        $handler = new MoveChannelHandler();

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(MoveToItselfNotAllowed::class));
    }

    public function test_move_to_children(): void
    {
        // Given
        $command = new MoveChannelCommand(-1);
        $command->channelId = -1;
        $command->toChannelId = 99;
        $command->prx = $this->createMockedServer(true);
        $handler = new MoveChannelHandler();

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(MoveToChildrenChannelNotAllowed::class));
    }

    public function test_move_channel_with_success(): void
    {
        // Given
        $command = new MoveChannelCommand(-1);
        $command->channelId = -1;
        $command->toChannelId = 99;
        $command->prx = $this->createMockedServer();
        $handler = new MoveChannelHandler();

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelMoved::class));
    }
}
