<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\DeactivatePrioritySpeaker;

use App\Domain\Action\Server\Channels\DeactivatePrioritySpeaker\PrioritySpeakerNotActive;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PrioritySpeakerNotActiveTest extends Unit
{
    public function test_constructor(): void
    {
        $event = new PrioritySpeakerNotActive(42, 128);

        $this->assertSame(42, $event->serverId);
        $this->assertSame(128, $event->userSession);
    }

    public function test_get_key_method(): void
    {
        $event = new PrioritySpeakerNotActive(42, 128);

        $this->assertSame(PrioritySpeakerNotActive::KEY, $event->getKey());
    }
}
