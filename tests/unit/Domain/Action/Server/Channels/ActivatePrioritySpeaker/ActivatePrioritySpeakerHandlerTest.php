<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\ActivatePrioritySpeaker;

use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\ActivatePrioritySpeakerCommand;
use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\ActivatePrioritySpeakerHandler;
use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\PrioritySpeakerActivated;
use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\PrioritySpeakerAlreadyActive;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActivatePrioritySpeakerHandlerTest extends Unit
{
    private function createMockedServer(bool $priorityIsActive): object
    {
        return $this->makeEmpty(ServerInterface::class, [
            'getState' => function () use ($priorityIsActive) {
                $userState = new \stdClass();
                $userState->prioritySpeaker = $priorityIsActive;
                return $userState;
            }
        ]);
    }

    public function test_listen_to_method(): void
    {
        $handler = new ActivatePrioritySpeakerHandler();

        $this->assertSame(ActivatePrioritySpeakerCommand::class, $handler->listenTo());
    }

    public function test_activate_priority_speaker_return_success_event(): void
    {
        $command = new ActivatePrioritySpeakerCommand(-1);
        $command->userSession = -1;
        $command->prx = $this->createMockedServer(false);

        $handler = new ActivatePrioritySpeakerHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(PrioritySpeakerActivated::class));
    }

    public function test_activate_priority_speaker_when_already_active_return_error_event(): void
    {
        $command = new ActivatePrioritySpeakerCommand(-1);
        $command->userSession = -1;
        $command->prx = $this->createMockedServer(true);

        $handler = new ActivatePrioritySpeakerHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(PrioritySpeakerAlreadyActive::class));
    }
}
