<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\EditChannel;

use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Domain\Action\Server\Channels\EditChannel\CantEditChannelWhenIsTemporary;
use App\Domain\Action\Server\Channels\EditChannel\ChannelEdited;
use App\Domain\Action\Server\Channels\EditChannel\EditChannelCommand;
use App\Domain\Action\Server\Channels\EditChannel\EditChannelHandler;
use App\Domain\Action\Server\Channels\EditChannel\InvalidNumericalForPosition;
use App\Domain\Action\Server\Channels\EditChannel\ServerDontAllowHtmlTags;
use App\Domain\Action\Server\Channels\NestingLimitReached;
use App\Domain\Action\Server\Channels\RequestedChannelNotExist;
use App\Domain\Murmur\Exception\Server\InvalidChannelException;
use App\Domain\Murmur\Exception\Server\NestingLimitException;
use App\Domain\Murmur\Model\Mock\ChannelMock;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditChannelHandlerTest extends EditChannelHandlerAbstract
{
    public function test_listen_to_method(): void
    {
        $handler = new EditChannelHandler();

        $this->assertSame(EditChannelCommand::class, $handler->listenTo());
    }

    public function test_handler_method_return_success_event(): void
    {
        $command = new EditChannelCommand(1, 1, false, 'name', 'description', '42', 'password');
        $command->prx = $this->createPrxMock();
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelEdited::class));
    }

    public function test_handler_method_when_requested_channel_doesnt_exist(): void
    {
        $command = new EditChannelCommand(1, 1, false, 'name', 'description', '42', 'password');
        $command->prx = $this->createPrxMock();
        $command->prx->method('getChannelState')->willThrowException(new InvalidChannelException());
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(RequestedChannelNotExist::class));
    }

    public function test_handler_method_on_set_default_channel_when_requested_channel_is_temporary(): void
    {
        $command = new EditChannelCommand(1, 42, true, 'name', 'description', '25', 'password');
        $command->prx = $this->createPrxMock(['getChannelState' => new ChannelMock(-1, '', 0, null, '', true, 0)]);
        $command->prx->expects($this->never())->method('setConf')->with('defaultchannel', 42);
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(CantEditChannelWhenIsTemporary::class));
    }

    public function test_handler_method_on_set_default_channel_when_requested_channel_is_not_temporary(): void
    {
        $command = new EditChannelCommand(1, 42, true, 'name', 'description', '25', 'password');
        $command->prx = $this->createPrxMock(['getChannelState' => new ChannelMock(-1, '', 0, null, '', false, 0)]);
        $command->prx->expects($this->once())->method('setConf')->with('defaultchannel', 42);
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertFalse($response->hasEvent(CantEditChannelWhenIsTemporary::class));
        $this->assertTrue($response->hasEvent(ChannelEdited::class));
    }

    public function providerDefaultChannel(): array
    {
        return [
            [true],
            [false]
        ];
    }

    /** @dataProvider providerDefaultChannel */
    public function test_handler_method_dont_set_default_channel_if_command_doesnt_request_it($channelIsTemporary): void
    {
        $command = new EditChannelCommand(1, 42, false, 'name', 'description', '25', 'password');
        $command->prx = $this->createPrxMock(['getChannelState' => new ChannelMock(-1, '', 0, null, '', $channelIsTemporary, 0)]);
        $command->prx->expects($this->never())->method('setConf')->with('defaultchannel', 42);
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelEdited::class));
    }

    public function test_handler_method_on_set_name_when_the_channel_is_root(): void
    {
        $command = new EditChannelCommand(1, 0, false, 'name', 'description', '42', 'password');
        $command->prx = $this->createPrxMock(['validateChannelChars' => true]);
        $command->prx->expects($this->never())->method('validateChannelChars');
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelEdited::class));
    }

    public function test_handler_method_on_set_name_when_the_new_channel_name_is_invalid(): void
    {
        $command = new EditChannelCommand(1, 1, false, 'name', 'description', '42', 'password');
        $command->prx = $this->createPrxMock(['validateChannelChars' => false]);
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelNameIsInvalid::class));
    }

    public function test_handler_method_on_set_description_when_it_has_been_stripped(): void
    {
        $command = new EditChannelCommand(1, 1, false, 'name', 'description', '42', 'password');
        $command->prx = $this->createPrxMock(['checkIsAllowHtmlOrStripTags' => function($description, &$stripped) {
            $stripped = true;
            return 'stripped description';
        }]);
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ServerDontAllowHtmlTags::class));
    }

    public function providerChannelPosition(): array
    {
        return [
            ['invalid position', false, 'once'],
            [true, true, 'never'],
            [false, true, 'never'],
            [false, false, 'never']
        ];
    }

    public function test_handler_method_on_set_position_with_invalid_position(): void
    {
        $command = new EditChannelCommand(1, 1, false, 'name', 'description', 'invalid position', 'password');
        $command->prx = $this->createPrxMock();
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(InvalidNumericalForPosition::class));
    }

    public function providerValidChannelPosition(): array
    {
        return [
            [''], ['10'], ['0'], ['-10']
        ];
    }

    /** @dataProvider providerValidChannelPosition */
    public function test_handler_method_for_valid_position_argument($position): void
    {
        $command = new EditChannelCommand(1, 1, false, 'name', 'description', $position, 'password');
        $command->prx = $this->createPrxMock();
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelEdited::class));
    }

    public function test_handler_method_when_nesting_limit_exception_is_reached(): void
    {
        $command = new EditChannelCommand(1, 42, true, 'name', 'description', '99', 'password');
        $command->prx = $this->createPrxMock();
        $command->prx->method('setChannelState')->willThrowException(new NestingLimitException());
        $command->prx->expects($this->never())->method('setConf');
        $command->prx->expects($this->never())->method('getACL');
        $handler = new EditChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(NestingLimitReached::class));
    }
}
