<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\EditChannel;

use App\Domain\Action\Server\Channels\EditChannel\InvalidNumericalForPosition;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class InvalidNumericalForPositionTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new InvalidNumericalForPosition(42, 96, 'invalid position');

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->channelId);
        $this->assertSame('invalid position', $command->position);
    }

    public function test_get_key_method(): void
    {
        $event = new InvalidNumericalForPosition(42, 128, 'invalid position');

        $this->assertSame(InvalidNumericalForPosition::KEY, $event->getKey());
    }
}
