<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\EditChannel;

use App\Domain\Action\Server\Channels\EditChannel\EditChannelCommand;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditChannelCommandTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new EditChannelCommand(42, 96, true, 'name', 'description', 'position', 'password');

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->channelId);
        $this->assertTrue($command->default);
        $this->assertSame('name', $command->name);
        $this->assertSame('description', $command->description);
        $this->assertSame('position', $command->position);
        $this->assertSame('password', $command->password);
    }
}
