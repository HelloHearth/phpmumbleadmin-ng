<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\EditChannel;

use App\Domain\Murmur\Model\Mock\ChannelMock;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditChannelHandlerAbstract extends Unit
{
    protected function createPrxMock(array $methods = [])
    {
        $default = [
            'validateChannelChars' => true,
            'getChannelState' => new ChannelMock(),
            'getACL' => function($channelId, &$aclList, &$groupList, &$inherit) {
                $aclList = [];
                $groupList = [];
                $inherit = false;
            }
        ];
        $mockedMethods = \array_merge($default, $methods);

        $server = $this->createMock(ServerInterface::class);

        foreach ($mockedMethods as $methodName => $return) {
            if (\is_callable($return)) {
                $server->method($methodName)->willReturnCallback($return);
            } else {
                $server->method($methodName)->willReturn($return);
            }
        }

        return $server;
    }
}
