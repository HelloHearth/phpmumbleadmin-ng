<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\DeleteChannel;

use App\Domain\Action\Server\Channels\DeleteChannel;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteChannelHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new DeleteChannel\DeleteChannelHandler();

        $this->assertSame(DeleteChannel\DeleteChannelCommand::class, $handler->listenTo());
    }

    public function test_delete_root_channel_return_forbidden_event(): void
    {
        $command = new DeleteChannel\DeleteChannelCommand(-1, 0);
        $command->prx = $this->makeEmpty(ServerInterface::class);

        $handler = new DeleteChannel\DeleteChannelHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(DeleteChannel\DeleteRootChannelIsForbidden::class));
    }

    public function test_delete_channel_return_success_event(): void
    {
        $command = new DeleteChannel\DeleteChannelCommand(-1, -1);
        $command->prx = $this->makeEmpty(ServerInterface::class);

        $handler = new DeleteChannel\DeleteChannelHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(DeleteChannel\ChannelDeleted::class));
    }
}
