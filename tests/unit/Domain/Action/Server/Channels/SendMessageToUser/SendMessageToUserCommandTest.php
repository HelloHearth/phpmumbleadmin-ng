<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Action\Server\Channels\SendMessageToUser\SendMessageToUserCommand;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToUserCommandTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new SendMessageToUserCommand(42, 96, 'test message');

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->userSessionId);
        $this->assertSame('test message', $command->message);
    }
}
