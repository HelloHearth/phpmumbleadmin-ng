<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Action\Server\Channels\SendMessageToUser\EmptyMessage;
use App\Domain\Action\Server\Channels\SendMessageToUser\MessageSentToUser;
use App\Domain\Action\Server\Channels\SendMessageToUser\SendMessageToUserCommand;
use App\Domain\Action\Server\Channels\SendMessageToUser\SendMessageToUserHandler;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Service\SecurityServiceInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToUserHandlerTest extends Unit
{
    private function createMockedSecurityService(bool $bool = true): SecurityServiceInterface
    {
        return $this->makeEmpty(SecurityServiceInterface::class, ['isGrantedRootAdmin' => $bool]);
    }

    public function test_listen_to_method(): void
    {
        $handler = new SendMessageToUserHandler($this->createMockedSecurityService());

        $this->assertSame(SendMessageToUserCommand::class, $handler->listenTo());
    }
    public function test_handler_method_return_success_event(): void
    {
        $command = new SendMessageToUserCommand(1, 96, 'test message');
        $command->prx = $this->createMock(ServerInterface::class);
        $command->prx->expects($this->once())->method('sendMessage')->with(96, 'test message');
        $handler = new SendMessageToUserHandler($this->createMockedSecurityService());

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(MessageSentToUser::class));
        /** @var MessageSentToUser $event */
        $event = $response->getEvent(MessageSentToUser::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(96, $event->userSessionId);
        $this->assertFalse($event->stripped);
    }

    public function test_handler_method_when_message_is_empty_return_error_event(): void
    {
        $command = new SendMessageToUserCommand(1, 96, '');
        $command->prx = $this->createMock(ServerInterface::class);
        $command->prx->expects($this->never())->method('sendMessage');
        $handler = new SendMessageToUserHandler($this->createMockedSecurityService());

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(EmptyMessage::class));
        /** @var EmptyMessage $event */
        $event = $response->getEvent(EmptyMessage::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(96, $event->userSessionId);
    }
}
