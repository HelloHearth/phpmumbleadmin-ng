<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\LinkChannel;

use App\Domain\Action\Server\Channels\LinkChannel\CannotLinkSelf;
use App\Domain\Action\Server\Channels\LinkChannel\ChannelLinked;
use App\Domain\Action\Server\Channels\LinkChannel\LinkChannelCommand;
use App\Domain\Action\Server\Channels\LinkChannel\LinkChannelHandler;
use App\Domain\Murmur\Model\Mock\ChannelMock;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LinkChannelHandlerTest extends Unit
{
    private function mockServer(): MockObject
    {
        $mockedServer = $this->createMock(ServerInterface::class);
        $mockedServer->method('getChannelState')->willReturn(new ChannelMock(42));
        return $mockedServer;
    }

    public function test_listen_to_method(): void
    {
        $handler = new LinkChannelHandler();

        $this->assertSame(LinkChannelCommand::class, $handler->listenTo());
    }

    public function test_handler_method_return_success_event(): void
    {
        $command = new LinkChannelCommand(1);
        $command->channelId = 2;
        $command->withChannelId = 42;
        $command->prx = $this->mockServer();
        $command->prx->expects($this->once())->method('setChannelState');
        $handler = new LinkChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelLinked::class));
        /** @var ChannelLinked $event */
        $event = $response->getEvent(ChannelLinked::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(2, $event->channelId);
        $this->assertSame(42, $event->linkId);
    }

    public function test_handler_method_when_trying_to_link_self_channel(): void
    {
        $command = new LinkChannelCommand(1);
        $command->channelId = 2;
        $command->withChannelId = 2;
        $command->prx = $this->mockServer();
        $handler = new LinkChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(CannotLinkSelf::class));
        /** @var CannotLinkSelf $event */
        $event = $response->getEvent(CannotLinkSelf::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(2, $event->channelId);
        $this->assertSame(2, $event->linkId);
    }
}
