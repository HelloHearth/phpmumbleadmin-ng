<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\LinkChannel;

use App\Domain\Action\Server\Channels\LinkChannel\ChannelLinked;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelLinkedTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new ChannelLinked(42, 96, 128);

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->channelId);
        $this->assertSame(128, $command->linkId);
    }

    public function test_get_key_method(): void
    {
        $event = new ChannelLinked(42, 96, 128);

        $this->assertSame(ChannelLinked::KEY, $event->getKey());
    }
}
