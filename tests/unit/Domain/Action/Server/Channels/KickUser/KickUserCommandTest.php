<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\KickUser;

use App\Domain\Action\Server\Channels\KickUser;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class KickUserCommandTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new KickUser\KickUserCommand(42, 96, 'test reason field');

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->userSessionId);
        $this->assertSame('test reason field', $command->reason);
    }
}
