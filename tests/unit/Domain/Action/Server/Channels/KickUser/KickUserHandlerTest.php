<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\KickUser;

use App\Domain\Action\Server\Channels\KickUser;
use App\Domain\Action\Server\Channels\UserNotConnected;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class KickUserHandlerTest extends Unit
{
    private function createMockedServerForGetUsers(): object
    {
        return $this->makeEmpty(ServerInterface::class, [
            'getUsers' => function () {
                $MurmurUser = new \stdClass();
                $MurmurUser->name = 'test murmur user name';
                return [42 => $MurmurUser];
            }
        ]);
    }

    public function test_listen_to_method(): void
    {
        $handler = new KickUser\KickUserHandler();

        $this->assertSame(KickUser\KickUserCommand::class, $handler->listenTo());
    }

    public function test_kick_user_return_success_event(): void
    {
        $command = new KickUser\KickUserCommand(-1, 42, 'reason text');
        $command->prx = $this->createMockedServerForGetUsers();

        $handler = new KickUser\KickUserHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(KickUser\UserKicked::class));
    }

    public function test_kick_user_when_he_is_not_in_server_return_error_event(): void
    {
        $command = new KickUser\KickUserCommand(-1, 24, 'reason text');
        $command->prx = $this->createMockedServerForGetUsers();

        $handler = new KickUser\KickUserHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(UserNotConnected::class));
    }
}
