<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\SendMessageToChannel;

use App\Domain\Action\Server\Channels\ChannelDoNotExist;
use App\Domain\Action\Server\Channels\SendMessageToChannel;
use App\Domain\Murmur\Exception\Server\InvalidChannelException;
use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Murmur\Model\Server;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToChannelHandlerTest extends Unit
{
    private function createMockedSecurityService(bool $bool = true): SecurityServiceInterface
    {
        return $this->makeEmpty(SecurityServiceInterface::class, ['isGrantedRootAdmin' => $bool]);
    }

    public function test_listen_to_method(): void
    {
        $handler = new SendMessageToChannel\SendMessageToChannelHandler($this->makeEmpty(SecurityServiceInterface::class));

        $this->assertSame(SendMessageToChannel\SendMessageToChannelCommand::class, $handler->listenTo());
    }

    public function isGrantedRootAdminProvider(): array
    {
        return [
            'When user is granted RootAdmin' => ['isGranted' => true],
            'When user is not granted RootAdmin' => ['isGranted' => false]
        ];
    }

    /** @dataProvider isGrantedRootAdminProvider */
    public function test_send_message_return_success_event($isGranted): void
    {
        $expectedStripped = ! $isGranted;
        $command = new SendMessageToChannel\SendMessageToChannelCommand(-1, 0, '<i>test message with HTML tags</i>', true);
        $command->prx = $this->makeEmptyExcept(Server::class, 'checkIsAllowHtmlOrStripTags', ['getParameter' => 'false']);

        $handler = new SendMessageToChannel\SendMessageToChannelHandler($this->createMockedSecurityService($isGranted));
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(SendMessageToChannel\MessageSentToChannel::class));
        $this->assertSame($expectedStripped, $response->getEvent(SendMessageToChannel\MessageSentToChannel::class)->stripped);
    }

    public function test_send_message_on_invalid_channel_id(): void
    {
        // Given
        $command = new SendMessageToChannel\SendMessageToChannelCommand(-1, -1, '<i>test message with HTML tags</i>', true);
        $command->prx = $this->makeEmpty(Server::class, ['sendMessageChannel' => function() { throw new InvalidChannelException(); }]);
        $handler = new SendMessageToChannel\SendMessageToChannelHandler($this->createMockedSecurityService());

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelDoNotExist::class));
    }

    public function test_send_message_when_is_empty_return_error_event(): void
    {
        $command = new SendMessageToChannel\SendMessageToChannelCommand(-1, 0, '', true);
        $command->prx = $this->makeEmpty(Server::class);

        $handler = new SendMessageToChannel\SendMessageToChannelHandler($this->createMockedSecurityService());
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(SendMessageToChannel\EmptyMessage::class));
    }
}
