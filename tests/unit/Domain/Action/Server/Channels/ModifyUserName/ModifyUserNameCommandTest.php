<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\ModifyUserName;

use App\Domain\Action\Server\Channels\ModifyUserName\ModifyUserNameCommand;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ModifyUserNameCommandTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new ModifyUserNameCommand(42, 96, 'new name field');

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->userSessionId);
        $this->assertSame('new name field', $command->newName);
    }
}
