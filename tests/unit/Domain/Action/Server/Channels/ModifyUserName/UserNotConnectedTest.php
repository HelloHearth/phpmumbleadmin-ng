<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\ModifyUserName;

use App\Domain\Action\Server\Channels\ModifyUserName\UserNotConnected;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UserNotConnectedTest extends Unit
{
    public function test_constructor(): void
    {
        $event = new UserNotConnected(24, 42);

        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->userSessionId);
    }

    public function test_get_key_method(): void
    {
        $event = new UserNotConnected(24, 42);

        $this->assertSame(UserNotConnected::KEY, $event->getKey());
    }
}
