<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\ModifyUserName;

use App\Domain\Action\Server\Channels\ModifyUserName\CantModifyUserName;
use App\Domain\Action\Server\Channels\ModifyUserName\InvalidNameArgument;
use App\Domain\Action\Server\Channels\ModifyUserName\ModifyUserNameCommand;
use App\Domain\Action\Server\Channels\ModifyUserName\ModifyUserNameHandler;
use App\Domain\Action\Server\Channels\ModifyUserName\UserNameModified;
use App\Domain\Action\Server\Channels\ModifyUserName\UserNotConnected;
use App\Domain\Murmur\Exception\Server\InvalidSessionException;
use App\Domain\Murmur\Model\MetaInterface;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Service\SecurityServiceInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ModifyUserNameHandlerTest extends Unit
{
    private function createPrxMock(): MockObject
    {
        $userState = new \stdClass();
        $userState->name = 'old name';

        $prxMock = $this->createMock(ServerInterface::class);
        $prxMock->method('getState')->willReturn($userState);

        return $prxMock;
    }

    public function test_listen_to_method(): void
    {
        $handler = new ModifyUserNameHandler($this->makeEmpty(SecurityServiceInterface::class));

        $this->assertSame(ModifyUserNameCommand::class, $handler->listenTo());
    }

    public function test_handle_method_on_success(): void
    {
        $command = new ModifyUserNameCommand(24, 42, 'new name');
        $command->Murmur = $this->createMock(MetaInterface::class);
        $command->prx = $this->createPrxMock();
        $state = new \stdClass();
        $state->name = $command->newName;
        $command->prx->expects($this->once())->method('setState')->with($state);
        $handler = new ModifyUserNameHandler($this->makeEmpty(SecurityServiceInterface::class, ['canModifyMumbleUsernameOnChannelTree' => true]));

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $event = $response->getEvent(UserNameModified::class);
        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->userSessionId);
        $this->assertSame('old name', $event->oldName);
        $this->assertSame('new name', $event->newName);
    }

    public function test_handle_method_when_its_not_granted_to_modify_name(): void
    {
        $command = new ModifyUserNameCommand(24, 42, 'new name');
        $command->Murmur = $this->createMock(MetaInterface::class);
        $command->prx = $this->createPrxMock();
        $handler = new ModifyUserNameHandler($this->makeEmpty(SecurityServiceInterface::class, ['canModifyMumbleUsernameOnChannelTree' => false]));

        $response = $handler->handle($command);

        $this->assertTrue($response->hasEvent(CantModifyUserName::class));
        $this->assertCount(1, $response->getEvents());
    }

    public function invalidNameProvider(): array
    {
        return [
            [''], ['old name']
        ];
    }

    /**
     * @dataProvider invalidNameProvider
     */
    public function test_handle_method_when_the_new_name_argument_is_invalid($newName): void
    {
        $command = new ModifyUserNameCommand(24, 42, $newName);
        $command->Murmur = $this->createMock(MetaInterface::class);
        $command->prx = $this->createPrxMock();
        $handler = new ModifyUserNameHandler($this->makeEmpty(SecurityServiceInterface::class, ['canModifyMumbleUsernameOnChannelTree' => true]));

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $event = $response->getEvent(InvalidNameArgument::class);
        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->userSessionId);
        $this->assertSame($newName, $event->newName);
    }

    public function test_handle_method_when_session_id_is_invalid(): void
    {
        $command = new ModifyUserNameCommand(24, 42, 'new name');
        $command->Murmur = $this->createMock(MetaInterface::class);
        $command->prx = $this->makeEmpty(ServerInterface::class, ['getState' => function() { throw new InvalidSessionException(); }]);
        $handler = new ModifyUserNameHandler($this->makeEmpty(SecurityServiceInterface::class, ['canModifyMumbleUsernameOnChannelTree' => true]));

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $event = $response->getEvent(UserNotConnected::class);
        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->userSessionId);
    }
}
