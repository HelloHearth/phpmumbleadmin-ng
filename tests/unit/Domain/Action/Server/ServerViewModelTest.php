<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server;

use App\Domain\Action\Server\ServerViewModel;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerViewModelTest extends Unit
{
    public function test_empty_constructor(): void
    {
        $viewModel = new ServerViewModel(12);

        $this->assertSame(12, $viewModel->getServerId());
    }

    public function test_get_set_server_ata_method(): void
    {
        $viewModel = new ServerViewModel(12);

        $this->assertFalse($viewModel->getHasConnection());
        $this->assertNull($viewModel->getServerName());
        $this->assertFalse($viewModel->isRunning());

        $viewModel->setServerData($this->makeEmpty(ServerInterface::class, ['getServerName' => 'test server name', 'isRunning' => true]));

        $this->assertTrue($viewModel->getHasConnection());
        $this->assertSame('test server name', $viewModel->getServerName());
        $this->assertTrue($viewModel->isRunning());
    }

    public function test_get_set_require_is_running_method(): void
    {
        $viewModel = new ServerViewModel(12);

        $this->assertTrue($viewModel->requireIsRunning());

        $viewModel->setRequireIsRunning(false);
        $this->assertFalse($viewModel->requireIsRunning());
    }

    public function test_get_set_connection_uri_method(): void
    {
        $viewModel = new ServerViewModel(12);

        $this->assertNull($viewModel->getConnectionUri());

        $viewModel->setConnectionUri('test connection uri');
        $this->assertSame('test connection uri', $viewModel->getConnectionUri());
    }
}
