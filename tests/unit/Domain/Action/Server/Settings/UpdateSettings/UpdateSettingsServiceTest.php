<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Settings\UpdateSettings;

use App\Domain\Action\Server\Settings\UpdateSettings\UpdateSettingsService;
use App\Domain\Murmur\Model\InMemoryServer;
use App\Domain\Murmur\Model\ServerSetting;
use App\Domain\Murmur\Model\ServerSettingsManager;
use App\Domain\Service\FlashMessageServiceInterface;
use App\Tests\Page\CertificatePage;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UpdateSettingsServiceTest extends Unit
{
    public function test_get_settings_method_with_empty_arguments(): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->makeEmpty(ServerSettingsManager::class, ['getAll' => []]));
        $service->setServer(new InMemoryServer());

        // When
        $results = $service->getSettings();

        // Then
        $this->assertSame([], $results);
    }

    protected function mockServerSettingsManager(): ServerSettingsManager
    {
        $parameter1 = new ServerSetting('parameter_1');
        $parameter2 = new ServerSetting('parameter_2');
        $parameter3 = new ServerSetting('parameter_3');

        return $this->makeEmpty(ServerSettingsManager::class, [
            'getAll' => [$parameter1, $parameter2, $parameter3]
        ]);
    }

    public function test_get_settings_method_without_default_and_custom_config(): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer(new InMemoryServer());

        // When
        $results = $service->getSettings();

        // Then
        $this->assertContainsOnlyInstancesOf(ServerSetting::class, $results);
        $this->assertCount(3, $results);
        $this->assertSame('parameter_1', $results['parameter_1']->getKey());
        $this->assertSame('parameter_2', $results['parameter_2']->getKey());
        $this->assertSame('parameter_3', $results['parameter_3']->getKey());
        $this->assertSame(null, $results['parameter_1']->getValue());
        $this->assertSame(null, $results['parameter_1']->getDefaultValue());
        $this->assertSame(null, $results['parameter_1']->getCustomValue());
        $this->assertSame(false, $results['parameter_1']->isModified());
        $this->assertSame(null, $results['parameter_2']->getValue());
        $this->assertSame(null, $results['parameter_2']->getDefaultValue());
        $this->assertSame(null, $results['parameter_2']->getCustomValue());
        $this->assertSame(false, $results['parameter_2']->isModified());
        $this->assertSame(null, $results['parameter_3']->getValue());
        $this->assertSame(null, $results['parameter_3']->getDefaultValue());
        $this->assertSame(null, $results['parameter_3']->getCustomValue());
        $this->assertSame(false, $results['parameter_3']->isModified());
    }

    public function test_get_settings_method_without_custom_config(): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer(new InMemoryServer());
        $service->setDefaultSettingValues(['parameter_1' => 'default value for parameter 1']);

        // When
        $results = $service->getSettings();

        // Then
        $this->assertContainsOnlyInstancesOf(ServerSetting::class, $results);
        $this->assertCount(3, $results);
        $this->assertSame('parameter_1', $results['parameter_1']->getKey());
        $this->assertSame('parameter_2', $results['parameter_2']->getKey());
        $this->assertSame('parameter_3', $results['parameter_3']->getKey());
        $this->assertSame('default value for parameter 1', $results['parameter_1']->getValue());
        $this->assertSame('default value for parameter 1', $results['parameter_1']->getDefaultValue());
        $this->assertSame(null, $results['parameter_1']->getCustomValue());
        $this->assertSame(false, $results['parameter_1']->isModified());
        $this->assertSame(null, $results['parameter_2']->getValue());
        $this->assertSame(null, $results['parameter_2']->getDefaultValue());
        $this->assertSame(null, $results['parameter_2']->getCustomValue());
        $this->assertSame(false, $results['parameter_2']->isModified());
        $this->assertSame(null, $results['parameter_3']->getValue());
        $this->assertSame(null, $results['parameter_3']->getDefaultValue());
        $this->assertSame(null, $results['parameter_3']->getCustomValue());
        $this->assertSame(false, $results['parameter_3']->isModified());
    }

    public function test_get_settings_method_without_default_config(): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer((new InMemoryServer())->with_custom_conf(['parameter_2' => 'custom value for parameter 2']));

        // When
        $results = $service->getSettings();

        // Then
        $this->assertContainsOnlyInstancesOf(ServerSetting::class, $results);
        $this->assertCount(3, $results);
        $this->assertSame('parameter_1', $results['parameter_1']->getKey());
        $this->assertSame('parameter_2', $results['parameter_2']->getKey());
        $this->assertSame('parameter_3', $results['parameter_3']->getKey());
        $this->assertSame(null, $results['parameter_1']->getValue());
        $this->assertSame(null, $results['parameter_1']->getDefaultValue());
        $this->assertSame(null, $results['parameter_1']->getCustomValue());
        $this->assertSame(false, $results['parameter_1']->isModified());
        $this->assertSame('custom value for parameter 2', $results['parameter_2']->getValue());
        $this->assertSame(null, $results['parameter_2']->getDefaultValue());
        $this->assertSame('custom value for parameter 2', $results['parameter_2']->getCustomValue());
        $this->assertSame(true, $results['parameter_2']->isModified());
        $this->assertSame(null, $results['parameter_3']->getValue());
        $this->assertSame(null, $results['parameter_3']->getDefaultValue());
        $this->assertSame(null, $results['parameter_3']->getCustomValue());
        $this->assertSame(false, $results['parameter_3']->isModified());
    }

    public function test_get_settings_method_with_all_arguments(): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer((new InMemoryServer())->with_custom_conf(['parameter_3' => 'custom value for parameter 3']));
        $service->setDefaultSettingValues([
            'parameter_1' => 'default value for parameter 1',
            'parameter_2' => 'default value for parameter 2',
            'parameter_3' => 'default value for parameter 3'
        ]);

        // When
        $results = $service->getSettings();

        // Then
        $this->assertContainsOnlyInstancesOf(ServerSetting::class, $results);
        $this->assertCount(3, $results);
        $this->assertSame('parameter_1', $results['parameter_1']->getKey());
        $this->assertSame('parameter_2', $results['parameter_2']->getKey());
        $this->assertSame('parameter_3', $results['parameter_3']->getKey());
        $this->assertSame('default value for parameter 1', $results['parameter_1']->getValue());
        $this->assertSame('default value for parameter 1', $results['parameter_1']->getDefaultValue());
        $this->assertSame(null, $results['parameter_1']->getCustomValue());
        $this->assertSame(false, $results['parameter_1']->isModified());
        $this->assertSame('default value for parameter 2', $results['parameter_2']->getValue());
        $this->assertSame('default value for parameter 2', $results['parameter_2']->getDefaultValue());
        $this->assertSame(null, $results['parameter_2']->getCustomValue());
        $this->assertSame(false, $results['parameter_2']->isModified());
        $this->assertSame('custom value for parameter 3', $results['parameter_3']->getValue());
        $this->assertSame('default value for parameter 3', $results['parameter_3']->getDefaultValue());
        $this->assertSame('custom value for parameter 3', $results['parameter_3']->getCustomValue());
        $this->assertSame(true, $results['parameter_3']->isModified());
    }

    public function saveSettingsOfServerProviderWithoutNewData(): array
    {
        return [
            '#01' => [[], [], []],
            '#02' => [[], [], [null]],
            '#03' => [[], [], ['']],
            '#04' => [['parameter_1' => 'default value'], [], []],
            '#05' => [['parameter_1' => 'default value'], [], [null]],
            '#06' => [['parameter_1' => 'default value'], [], ['']],
            '#07' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], []],
            '#08' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], [null]],
            '#09' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], ['']],
            '#10' => [[], ['parameter_1' => 'custom value'], []],
            '#11' => [[], ['parameter_1' => 'custom value'], [null]],
            '#12' => [[], ['parameter_1' => 'custom value'], ['']]
        ];
    }

    /** @dataProvider saveSettingsOfServerProviderWithoutNewData */
    public function test_save_settings_of_server_method_without_new_data($defaultValue, $customValue, $newValue): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer($server = (new InMemoryServer())->with_custom_conf($customValue));
        $service->setDefaultSettingValues($defaultValue);

        // When I call the method
        $service->saveSettingsOfServer($newValue);

        // Then I see the server custom configuration didn't change
        $this->assertSame($customValue, $server->getCustomConf());
    }

    public function saveSettingsOfServerProvider(): array
    {
        return [
            '#01' => [['parameter_1' => 'default value'], [], ['parameter_1' => 'custom value'], 'custom value'],
            '#02' => [['parameter_1' => 'default value'], [], ['parameter_1' => 'default value'], ''],
            '#03' => [['parameter_1' => '42'], [], ['parameter_1' => 42], ''],
            '#04' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], ['parameter_1' => null], ''],
            '#05' => [['parameter_1' => 'default value'], ['parameter_1' => '42'], ['parameter_1' => 42], '42'],
            '#06' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], ['parameter_1' => ''], ''],
            // I expect to reset the custom value, because the new value is the same as the default value
            '#07' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], ['parameter_1' => 'default value'], ''],
            '#08' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], ['parameter_1' => 'new custom value'], 'new custom value'],
            '#09' => [['parameter_1' => 'default value'], ['parameter_1' => 'custom value'], ['parameter_1' => 'custom value'], 'custom value'],
            '#10' => [[], [], ['parameter_1' => 'custom value'], 'custom value'],
        ];
    }

    /**
     * @dataProvider saveSettingsOfServerProvider
     */
    public function test_save_settings_of_server_method($defaultValue, $customValue, $newValue, $expected): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer($server = (new InMemoryServer())->with_custom_conf($customValue));
        $service->setDefaultSettingValues($defaultValue);

        // when I call the method
        $service->saveSettingsOfServer($newValue);

        // Then I see the expected value on the server configuration
        $this->assertSame($expected, $server->getConf('parameter_1'));
    }

    public function test_save_settings_of_server_method_with_invalid_key(): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer($server = new InMemoryServer());

        // when I call the method to save unknown key
        $service->saveSettingsOfServer(['unknown_parameter' => 'custom value']);

        // Then I see the method didn't save the custom value
        $this->assertSame('', $server->getConf('unknown_parameter'));
    }

    public function test_save_settings_of_server_method_for_key_and_certificate(): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer($server = (new InMemoryServer())->is_started());

        // When
        $service->saveSettingsOfServer([
            'key' => CertificatePage::PRIVATE_KEY,
            'certificate' => CertificatePage::CERTIFICATE,
        ]);

        // Then I see the method saved custom key and certificate
        $this->assertSame(CertificatePage::PRIVATE_KEY, $server->getConf('key'));
        $this->assertSame(CertificatePage::CERTIFICATE, $server->getConf('certificate'));
    }

    public function badKeyAndCertificateProvider(): array
    {
        return [
            [['key' => CertificatePage::PRIVATE_KEY, 'certificate' => '']],
            [['key' => CertificatePage::PRIVATE_KEY]],
            [['key' => '', 'certificate' => CertificatePage::CERTIFICATE]],
            [['certificate' => CertificatePage::CERTIFICATE]],
        ];
    }

    /** @dataProvider badKeyAndCertificateProvider */
    public function test_save_settings_of_server_method_for_key_and_certificate_with_bad_data($data): void
    {
        // Given
        $service = new UpdateSettingsService($this->makeEmpty(FlashMessageServiceInterface::class), $this->mockServerSettingsManager());
        $service->setServer($server = new InMemoryServer());

        // When
        $service->saveSettingsOfServer($data);

        // Then I see the method didn't save key and certificate
        $this->assertSame('', $server->getConf('key'));
        $this->assertSame('', $server->getConf('certificate'));
    }

    private function createMockServerParametersManager2(): ServerSettingsManager
    {
        $parameter1 = new ServerSetting('host');
        $parameter2 = new ServerSetting('port');
        $parameter3 = new ServerSetting('registername');

        return $this->makeEmpty(ServerSettingsManager::class, [
            'getAll' => [$parameter1, $parameter2, $parameter3]
        ]);
    }

    public function flashMessagesProvider(): array
    {
        return [
            'host' => [['host' => 'custom.host.tld'], 'host_modified_success_restart_needed'],
            'port' => [['port' => 64738], 'port_modified_success_restart_needed'],
            'certificate' => [['key' => CertificatePage::PRIVATE_KEY, 'certificate' => CertificatePage::CERTIFICATE], 'certificate_modified_success_restart_needed'],
        ];
    }

    /** @dataProvider flashMessagesProvider */
    public function test_save_settings_of_server_method_for_flash_messages($data, $messageKey): void
    {
        $flashService = $this->createMock(FlashMessageServiceInterface::class);
        $service = new UpdateSettingsService($flashService, $this->createMockServerParametersManager2());
        $service->setServer((new InMemoryServer())->is_started());

        $flashService->expects($this->once())->method('success')->with($messageKey);

        $service->saveSettingsOfServer($data);
    }
}
