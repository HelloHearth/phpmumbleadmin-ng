<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Settings;

use App\Domain\Action\Server\Settings\SettingsQuery;
use App\Domain\Action\Server\Settings\SettingsViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SettingsQueryTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new SettingsViewModel(42);
        $query = new SettingsQuery(42, $viewModel);

        $this->assertSame(42, $query->serverId);
        $this->assertSame($viewModel, $query->viewModel);
    }
}
