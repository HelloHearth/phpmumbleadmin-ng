<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Settings\ResetCertificate;

use App\Domain\Action\Server\Settings\ResetCertificate\CertificateReset;
use App\Domain\Action\Server\Settings\ResetCertificate\ResetCertificateCommand;
use App\Domain\Action\Server\Settings\ResetCertificate\ResetCertificateHandler;
use App\Domain\Murmur\Model\InMemoryServer;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ResetCertificateHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        // Given
        $handler = new ResetCertificateHandler();

        // When
        $result = $handler->listenTo();

        // Then I see the handler listen to ResetCertificateCommand
        $this->assertSame($result, ResetCertificateCommand::class);
    }

    public function test_handler_remove_custom_certificate(): void
    {
        // Given I have a server with a custom certificate
        $server = (new InMemoryServer())->with_id(42);
        $server->setConf('key', 'RSA PRIVATE KEY');
        $server->setConf('certificate', 'CERTIFICATE');

        $command = new ResetCertificateCommand($server->getSid());
        $command->prx = $server;
        $handler = new ResetCertificateHandler();

        // When I handle the command
        $handler->handle($command);

        // Then I see the handler has reset certificate and key on the Server
        $this->assertSame('', $server->getConf('key'));
        $this->assertSame('', $server->getConf('certificate'));
    }

    public function test_handler_return_event_on_success(): void
    {
        // Given
        $server = (new InMemoryServer())->with_id(42);
        $command = new ResetCertificateCommand($server->getSid());
        $command->prx = $server;
        $handler = new ResetCertificateHandler();

        // When I handle the command
        $response = $handler->handle($command);

        // Then I see the handler returned CertificateReset event
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(CertificateReset::class));
    }
}
