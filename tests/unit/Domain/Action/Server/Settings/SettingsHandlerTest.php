<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Settings;

use App\Domain\Action\Server\Settings\SettingsHandler;
use App\Domain\Action\Server\Settings\SettingsQuery;
use App\Domain\Action\Server\Settings\UpdateSettings\UpdateSettingsService;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SettingsHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new SettingsHandler($this->makeEmpty(UpdateSettingsService::class));

        $result = $handler->listenTo();

        $this->assertSame(SettingsQuery::class, $result);
    }

    // Do not test the handler, it require too many mocks, it's a difficult work,
    // It's preferable a functional and/or acceptance test
    public function test_handler_on_success(): void
    {
        $this->assertTrue(true);
    }
}
