<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Bans\CreateBan;

use App\Domain\Action\Server\Bans\CreateBan\BanCreated;
use App\Domain\Action\Server\Bans\CreateBan\CreateBanCommand;
use App\Domain\Action\Server\Bans\CreateBan\CreateBanHandler;
use App\Domain\Murmur\Model\InMemoryServer;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Domain\Service\ClockInterface;
use App\Tests\Page\Timestamp;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateBanHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new CreateBanHandler($this->makeEmpty(ClockInterface::class));

        $this->assertSame(CreateBanCommand::class, $handler->listenTo());
    }

    public function commandProvider(): \Generator
    {
        yield 'With ipv4' => [
            (new CreateBanCommand())->server(42)->address('192.168.42.42'),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 42, 42], 128, '', '', '', Timestamp::UTC_WC_1998, 0),
        ];

        yield 'With ipv4 and mask' => [
            (new CreateBanCommand())->server(42)->address('192.168.99.99/25'),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 99, 99], 121, '', '', '', Timestamp::UTC_WC_1998, 0),
        ];

        yield 'With ipv6' => [
            (new CreateBanCommand())->server(42)->address('2001:db8::1'),
            new BanMock([32, 1, 13, 184, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 128, '', '', '', Timestamp::UTC_WC_1998, 0),
        ];

        yield 'With ipv6 and mask' => [
            (new CreateBanCommand())->server(42)->address('2001:db8::1/24'),
            new BanMock([32, 1, 13, 184, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 24, '', '', '', Timestamp::UTC_WC_1998, 0),
        ];

        yield 'With a name' => [
            (new CreateBanCommand())->server(42)->address('192.168.42.42')->name('name field'),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 42, 42], 128, 'name field', '', '', Timestamp::UTC_WC_1998, 0),
        ];

        yield 'With a hash' => [
            (new CreateBanCommand())->server(42)->address('192.168.42.42')->hash('hash field'),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 42, 42], 128, '', 'hash field', '', Timestamp::UTC_WC_1998, 0),
        ];

        yield 'With a reason' => [
            (new CreateBanCommand())->server(42)->address('192.168.42.42')->reason('reason field'),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 42, 42], 128, '', '', 'reason field', Timestamp::UTC_WC_1998, 0),
        ];

        $endDate = (new \DateTimeImmutable())->setTimestamp(Timestamp::UTC_WC_2018);
        $duration = Timestamp::UTC_WC_2018 - Timestamp::UTC_WC_1998;
        yield 'With end of date' => [
            (new CreateBanCommand())->server(42)->address('192.168.42.42')->permanent(false)->endDate($endDate),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 42, 42], 128, '', '', '', Timestamp::UTC_WC_1998, $duration),
        ];

        yield 'With end of date and permanent' => [
            (new CreateBanCommand())->server(42)->address('192.168.42.42')->permanent(true)->endDate((new \DateTimeImmutable())->setTimestamp(Timestamp::UTC_WC_2018)),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 42, 42], 128, '', '', '', Timestamp::UTC_WC_1998, 0),
        ];

        yield 'Not permanent and without end of date' => [
            (new CreateBanCommand())->server(42)->address('192.168.42.42')->permanent(false),
            new BanMock([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 192, 168, 42, 42], 128, '', '', '', Timestamp::UTC_WC_1998, 0),
        ];
    }

    /** @dataProvider commandProvider */
    public function test_handle_method(CreateBanCommand $command, BanMock $expectedBan): void
    {
        // Given
        $command->prx = new InMemoryServer();
        $handler = new CreateBanHandler($this->makeEmpty(ClockInterface::class, ['timestamp' => Timestamp::UTC_WC_1998]));

        // When I handle the command
        $response = $handler->handle($command);

        // Then I see the response return the success event, and the ban has been created
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(BanCreated::class));
        $this->assertCount(1, $command->prx->getBans());
        $this->assertEquals($expectedBan, $command->prx->getBans()[0]);
    }

    public function test_handle_method_when_the_server_already_has_bans(): void
    {
        // Given
        $command = (new CreateBanCommand())->server(42)->address('192.168.42.42')->reason('when the server already has bans');
        $oldBan = new BanMock();
        $command->prx = new InMemoryServer();
        $command->prx->setBans([$oldBan]);
        $handler = new CreateBanHandler($this->makeEmpty(ClockInterface::class, ['timestamp' => Timestamp::UTC_WC_1998]));

        // When I handle the command
        $response = $handler->handle($command);

        // Then I see the response return the success event, and the ban has been created
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(BanCreated::class));
        $this->assertCount(2, $command->prx->getBans());
        $this->assertSame($oldBan, $command->prx->getBans()[0]);
        $this->assertSame('when the server already has bans', $command->prx->getBans()[1]->reason);
    }
}
