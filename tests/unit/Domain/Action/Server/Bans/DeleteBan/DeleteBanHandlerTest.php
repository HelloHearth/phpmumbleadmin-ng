<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Bans\DeleteBan;

use App\Domain\Action\Server\Bans\DeleteBan\BanDeleted;
use App\Domain\Action\Server\Bans\DeleteBan\DeleteBanCommand;
use App\Domain\Action\Server\Bans\DeleteBan\DeleteBanHandler;
use App\Domain\Helper\IpHelper;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteBanHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new DeleteBanHandler();

        $this->assertSame(DeleteBanCommand::class, $handler->listenTo());
    }

    private function createServerMock(): MockObject
    {
        $server = $this->createMock(ServerInterface::class);
        $server->method('getBans')->willReturn([
            new BanMock(IpHelper::stringToDecimalIPv4('127.0.0.1'), 128, 'test username', 'test hash', 'test reason', 1531673100),
            new BanMock(IpHelper::stringToDecimalIPv4('192.168.0.1'), 128, 'test username2', 'test hash2', 'test reason2', 1531673101),
            new BanMock(IpHelper::stringToDecimalIPv4('172.17.0.1'), 128, 'test username3', 'test hash3', 'test reason3', 1531673102),
            new BanMock(IpHelper::stringToDecimalIPv6('::1'), 128, 'test username4', 'test hash4', 'test reason4', 1531673103)
        ]);
        $server->expects($this->once())->method('setBans')->with([
            0 => new BanMock(IpHelper::stringToDecimalIPv4('127.0.0.1'), 128, 'test username', 'test hash', 'test reason', 1531673100),
            1 => new BanMock(IpHelper::stringToDecimalIPv4('192.168.0.1'), 128, 'test username2', 'test hash2', 'test reason2', 1531673101),
            3 => new BanMock(IpHelper::stringToDecimalIPv6('::1'), 128, 'test username4', 'test hash4', 'test reason4', 1531673103)
        ]);
        return $server;
    }

    public function test_handler_method_return_success_event(): void
    {
        $command = new DeleteBanCommand(1, 2);
        $command->prx = $this->createServerMock();
        $handler = new DeleteBanHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(BanDeleted::class));
        /** @var BanDeleted $event */
        $event = $response->getEvent(BanDeleted::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(2, $event->banId);
    }
}
