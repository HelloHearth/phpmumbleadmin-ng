<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Bans\Index;

use App\Domain\Action\Server\Bans\Ban;
use App\Domain\Action\Server\Bans\Index\BansViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BansViewModelTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new BansViewModel(42);

        $result = $viewModel->getServerId();

        $this->assertSame(42, $result);
    }

    public function test_bans_field_getter_and_setter(): void
    {
        $viewModel = new BansViewModel(42);

        $result = $viewModel->getBans();
        $this->assertSame([], $result);

        $viewModel->setBans([$bans = new Ban(1, '127.0.0.1', 32, 'username', 'hash', 'reason', 666, 42)]);
        $result = $viewModel->getBans();

        $this->assertSame([$bans], $result);
    }

    public function test_bans_field_setter_accept_only_ban_object(): void
    {
        $viewModel = new BansViewModel(42);
        $this->expectException(\InvalidArgumentException::class);

        $viewModel->setBans(['not_valid_ban_model_object']);
    }
}
