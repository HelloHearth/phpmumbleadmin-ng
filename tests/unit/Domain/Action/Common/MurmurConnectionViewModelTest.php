<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Common;

use App\Domain\Action\Common\MurmurConnectionViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MurmurConnectionViewModelTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new MurmurConnectionViewModel();

        $this->assertFalse($viewModel->getHasConnection());
    }

    public function test_set_has_connection_method(): void
    {
        $viewModel = new MurmurConnectionViewModel();

        $viewModel->setHasConnection();

        $this->assertTrue($viewModel->getHasConnection());
    }
}
