<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Dashboard\Dashboard;

use App\Domain\Action\Dashboard\Dashboard\ServerData;
use App\Domain\Action\Dashboard\Dashboard\ServerDataSort;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerDataSortTest extends Unit
{
    /**
     * @return ServerData[]
     */
    private function getData(): array
    {
        $bag = [];

        $data = new ServerData();
        $data->key = 1;
        $data->isBooted = true;
        $bag[] = $data;

        $data = new ServerData();
        $data->key = 2;
        $data->isBooted = false;
        $bag[] = $data;

        $data = new ServerData();
        $data->key = 4;
        $data->isBooted = true;
        $bag[] = $data;

        $data = new ServerData();
        $data->key = 5;
        $data->isBooted = false;
        $bag[] = $data;

        $data = new ServerData();
        $data->key = 8;
        $data->isBooted = true;
        $bag[] = $data;

        $data = new ServerData();
        $data->key = 9;
        $data->isBooted = false;
        $bag[] = $data;

        // Any order of data must be ordered in the same way
        \shuffle($bag);

        return $bag;
    }

    /**
     * In ASC order, servers started should be at the top and ordered by key
     *
     * @param ServerData[] $data
     */
    private function expectDataSortedByAscOrder(array $data): void
    {
        $this->assertSame(1, $data[0]->key);
        $this->assertSame(4, $data[1]->key);
        $this->assertSame(8, $data[2]->key);
        $this->assertSame(2, $data[3]->key);
        $this->assertSame(5, $data[4]->key);
        $this->assertSame(9, $data[5]->key);
    }

    /**
     * In DESC order, servers started must be at the bottom and key order reversed
     *
     * @param ServerData[] $data
     */
    private function expectDataSortedByDescOrder(array $data): void
    {
        $this->assertSame(9, $data[0]->key);
        $this->assertSame(5, $data[1]->key);
        $this->assertSame(2, $data[2]->key);
        $this->assertSame(8, $data[3]->key);
        $this->assertSame(4, $data[4]->key);
        $this->assertSame(1, $data[5]->key);
    }

    public function test_sort_by_is_booted_method_in_desc_order(): void
    {
        // Given I create ServerDataSort with asc order
        $sort = new ServerDataSort('asc');

        // When I call ServerDataSort::sortByIsBooted method
        $data = $this->getData();
        $sort->sortByIsBooted($data);

        // Then I see the data has been sorted in DESC order
        $this->expectDataSortedByAscOrder($data);
    }

    public function test_sort_by_is_booted_method_in_asc_order(): void
    {
        // Given I create ServerDataSort with desc order
        $sort = new ServerDataSort('desc');

        // When I call ServerDataSort::sortByIsBooted method
        $data = $this->getData();
        $sort->sortByIsBooted($data);

        // Then I see the data has been sorted in ASC order
        $this->expectDataSortedByDescOrder($data);
    }

    public function test_sort_by_is_booted_method_without_order(): void
    {
        // Given I create ServerDataSort without any order
        $sort = new ServerDataSort();

        // When I call ServerDataSort::sortByIsBooted method
        $data = $this->getData();
        $sort->sortByIsBooted($data);

        // Then I see the data has been sorted in DESC order
        $this->expectDataSortedByAscOrder($data);
    }

    public function test_sort_by_is_booted_method_with_invalid_order(): void
    {
        // Given I create ServerDataSort with an invalid order
        $sort = new ServerDataSort('Invalid');

        // When I call ServerDataSort::sortByIsBooted method
        $data = $this->getData();
        $sort->sortByIsBooted($data);

        // Then I see the data has been sorted in ASC order
        $this->expectDataSortedByAscOrder($data);
    }

    public function test_sort_by_is_booted_method_with_uppercase_asc_order(): void
    {
        // Given I create ServerDataSort with asc order in uppercase
        $sort = new ServerDataSort('ASC');

        // When I call ServerDataSort::sortByIsBooted method
        $data = $this->getData();
        $sort->sortByIsBooted($data);

        // Then I see the data has been sorted in DESC order
        $this->expectDataSortedByAscOrder($data);
    }

    public function test_sort_by_is_booted_method_with_uppercase_desc_order(): void
    {
        // Given I create ServerDataSort with desc order in uppercase
        $sort = new ServerDataSort('DESC');

        // When I call ServerDataSort::sortByIsBooted method
        $data = $this->getData();
        $sort->sortByIsBooted($data);

        // Then I see the data has been sorted in ASC order
        $this->expectDataSortedByDescOrder($data);
    }
}
