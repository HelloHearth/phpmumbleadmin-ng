<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Dashboard\CreateServer;

use App\Domain\Action\Dashboard\CreateServer\CreateServerCommand;
use App\Domain\Action\Dashboard\CreateServer\CreateServerHandler;
use App\Domain\Action\Dashboard\CreateServer\ServerCreated;
use App\Domain\Action\Server\Settings\UpdateSettings\UpdateSettingsService;
use App\Domain\Murmur\Model\MetaInterface;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateServerHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new CreateServerHandler($this->createMock(UpdateSettingsService::class));

        $this->assertSame(CreateServerCommand::class, $handler->listenTo());
    }

    private function createMetaMock(): MockObject
    {
        $prx = $this->createMock(ServerInterface::class);
        $prx->method('getSid')->willReturn(25);

        $meta = $this->createMock(MetaInterface::class);
        $meta->method('newServer')->willReturn($prx);

        return $meta;
    }

    public function test_handler_method_return_success_event(): void
    {
        // Given
        $command = new CreateServerCommand(null, null, null);
        $command->Murmur = $this->createMetaMock();
        $handler = new CreateServerHandler($this->createMock(UpdateSettingsService::class));

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ServerCreated::class));
        /** @var ServerCreated $event */
        $event = $response->getEvent(ServerCreated::class);
        $this->assertSame(25, $event->serverId);
    }

    private function createMetaMock2(): MockObject
    {
        $prx = $this->createMock(ServerInterface::class);
        $prx->method('getSid')->willReturn(25);

        $meta = $this->createMock(MetaInterface::class);
        $meta->method('newServer')->willReturn($prx);
        $meta->method('getDefaultConf')->willReturn([]);

        return $meta;
    }

    public function test_handler_method_with_settings(): void
    {
        // Given
        $command = new CreateServerCommand('registername field', 'password field', 42);
        $command->Murmur = $this->createMetaMock2();
        $settingsService = $this->createMock(UpdateSettingsService::class);
        $handler = new CreateServerHandler($settingsService);
        $settingsService->expects($this->once())->method('saveSettingsOfServer')->with([
            'registername' => 'registername field',
            'password' => 'password field',
            'users' => '42'
        ]);

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertTrue($response->hasEvent(ServerCreated::class));
    }
}
