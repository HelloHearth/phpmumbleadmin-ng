<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Dashboard\CreateServer;

use App\Domain\Action\Dashboard\CreateServer\CreateServerCommand;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateServerCommandTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new CreateServerCommand('registername field', 'password field', 42);

        $this->assertSame('registername field', $command->registername);
        $this->assertSame('password field', $command->password);
        $this->assertSame(42, $command->users);
    }
}
