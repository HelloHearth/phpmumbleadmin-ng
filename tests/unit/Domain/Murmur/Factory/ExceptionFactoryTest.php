<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Factory;

use App\Domain\Murmur\Exception\Ice as IceException;
use App\Domain\Murmur\Exception\Server as ServerException;
use App\Domain\Murmur\Factory\ExceptionFactory;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ExceptionFactoryTest extends Unit
{
    public function murmurExceptionProvider(): array
    {
        return [
            '\Exception' => [new \Exception(), \Exception::class],
            '\Ice_MemoryLimitException' => [new \Ice_MemoryLimitException(), IceException\MemoryLimitException::class],
            '\Ice\MemoryLimitException' => [new \Ice\MemoryLimitException(), IceException\MemoryLimitException::class],
            '\Murmur_ServerBootedException' => [new \Murmur_ServerBootedException(), ServerException\ServerBootedException::class],
            '\Murmur\ServerBootedException' => [new \Murmur\ServerBootedException(), ServerException\ServerBootedException::class],
            '\Murmur_ServerFailureException' => [new \Murmur_ServerFailureException(), ServerException\ServerFailureException::class],
            '\Murmur\ServerFailureException' => [new \Murmur\ServerFailureException(), ServerException\ServerFailureException::class],
            '\Murmur_InvalidServerException' => [new \Murmur_InvalidServerException(), ServerException\InvalidServerException::class],
            '\Murmur\InvalidServerException' => [new \Murmur\InvalidServerException(), ServerException\InvalidServerException::class],
            '\Murmur_InvalidSecretException' => [new \Murmur_InvalidSecretException(), ServerException\InvalidSecretException::class],
            '\Murmur\InvalidSecretException' => [new \Murmur\InvalidSecretException(), ServerException\InvalidSecretException::class],
            '\Murmur_InvalidChannelException' => [new \Murmur_InvalidChannelException(), ServerException\InvalidChannelException::class],
            '\Murmur\InvalidChannelException' => [new \Murmur\InvalidChannelException(), ServerException\InvalidChannelException::class],
            '\Murmur_InvalidUserException' => [new \Murmur_InvalidUserException(), ServerException\InvalidUserException::class],
            '\Murmur\InvalidUserException' => [new \Murmur\InvalidUserException(), ServerException\InvalidUserException::class],
            '\Murmur_InvalidSessionException' => [new \Murmur_InvalidSessionException(), ServerException\InvalidSessionException::class],
            '\Murmur\InvalidSessionException' => [new \Murmur\InvalidSessionException(), ServerException\InvalidSessionException::class],
            '\Murmur_NestingLimitException' => [new \Murmur_NestingLimitException(), ServerException\NestingLimitException::class],
            '\Murmur\NestingLimitException' => [new \Murmur\NestingLimitException(), ServerException\NestingLimitException::class],
            '\Murmur_InvalidTextureException' => [new \Murmur_InvalidTextureException(), ServerException\InvalidTextureException::class],
            '\Murmur\InvalidTextureException' => [new \Murmur\InvalidTextureException(), ServerException\InvalidTextureException::class],
            '\Murmur_InvalidCallbackException' => [new \Murmur_InvalidCallbackException(), ServerException\InvalidCallbackException::class],
            '\Murmur\InvalidCallbackException' => [new \Murmur\InvalidCallbackException(), ServerException\InvalidCallbackException::class],
            '\RuntimeException: any other exception must return the submitted exception' => [new \RuntimeException(), \RuntimeException::class]
        ];
    }

    /**
     * @dataProvider murmurExceptionProvider
     */
    public function test_exception_factory_method($exception, $expectedExceptionClass): void
    {
        $result = ExceptionFactory::get($exception);
        $this->assertInstanceOf($expectedExceptionClass, $result);
    }
}
