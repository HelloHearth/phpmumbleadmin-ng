<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Model;

use App\Domain\Murmur\Middleware\ConnectionBusMiddleware;
use App\Domain\Murmur\Model\ServerSetting;
use App\Domain\Murmur\Model\ServerSettingsManager;
use App\Domain\Service\SecurityServiceInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerSettingsManagerTest extends Unit
{
    public function test_get_all_method(): void
    {
        // Given
        $_SERVER[ConnectionBusMiddleware::MURMUR_VERSION_STRING] = '1.2.18';
        $manager = new ServerSettingsManager($this->makeEmpty(SecurityServiceInterface::class, ['canEditMurmurSettingTypeAdministration' => true]));

        // When
        $results = $manager->getAll();

        // Then
        $this->assertContainsOnlyInstancesOf(ServerSetting::class, $results);
        $this->assertCount(27, $results);
    }


    public function test_get_all_method_without_rootadmin_role(): void
    {
        // Given
        $_SERVER[ConnectionBusMiddleware::MURMUR_VERSION_STRING] = '1.2.18';
        $manager = new ServerSettingsManager($this->makeEmpty(SecurityServiceInterface::class, ['canEditMurmurSettingTypeAdministration' => false]));

        // When
        $results = $manager->getAll();

        // Then
        $this->assertContainsOnlyInstancesOf(ServerSetting::class, $results);
        $this->assertCount(14, $results);
        foreach ($results as $setting) {
            $this->assertSame(ServerSetting::ROLE_ALL, $setting->getRole());
        }
    }

    public function test_get_all_method_with_murmur_version_1_2_3(): void
    {
        // Given
        $_SERVER[ConnectionBusMiddleware::MURMUR_VERSION_STRING] = '1.2.3';
        $manager = new ServerSettingsManager($this->makeEmpty(SecurityServiceInterface::class, ['canEditMurmurSettingTypeAdministration' => true]));

        // When
        $results = $manager->getAll();

        // Then
        $this->assertContainsOnlyInstancesOf(ServerSetting::class, $results);
        $this->assertCount(23, $results);
        foreach ($results as $setting) {
            $this->assertSame('1.2.3', $setting->getMurmurVersion());
        }
    }
}
