<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Model;

use App\Domain\Murmur\Model\ServerSetting;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerSettingTest extends Unit
{
    public function test_key_must_be_in_lower_case(): void
    {
        // Given
        $setting = new ServerSetting('test Key Field');

        // When
        $result = $setting->getKey();

        // Then
        $this->assertSame('test key field', $result);
    }

    public function test_is_modified_method_when_has_no_default_and_custom_values(): void
    {
        // Given
        $setting = new ServerSetting('test_setting');

        // When
        $result = $setting->isModified();

        // Then
        $this->assertFalse($result);
    }

    public function test_is_modified_method_when_has_default_value(): void
    {
        // Given
        $setting = new ServerSetting('test_setting');

        // When
        $setting->setDefaultValue('default value');
        $result = $setting->isModified();

        // Then
        $this->assertFalse($result);
    }

    public function test_is_modified_method_when_has_custom_value(): void
    {
        // Given
        $setting = new ServerSetting('test_setting');

        // When
        $setting->setCustomValue('custom value');
        $result = $setting->isModified();

        // Then
        $this->assertTrue($result);
    }

    public function test_get_value_method_when_has_no_default_and_custom_values(): void
    {
        // Given
        $setting = new ServerSetting('test_setting');

        // When
        $result = $setting->getValue();

        // Then
        $this->assertNull($result);
    }

    public function test_get_value_method_when_has_default_value(): void
    {
        // Given
        $setting = new ServerSetting('test_setting');

        // When
        $setting->setDefaultValue('default value');
        $result = $setting->getValue();

        // Then
        $this->assertSame('default value', $result);
    }

    public function test_get_value_method_when_has_custom_value(): void
    {
        // Given
        $setting = new ServerSetting('test_setting');

        // When
        $setting->setCustomValue('custom value');
        $result = $setting->getValue();

        // Then
        $this->assertSame('custom value', $result);
    }

    public function test_get_value_method_when_has_default_and_custom_values(): void
    {
        // Given
        $setting = new ServerSetting('test_setting');

        // When
        $setting->setDefaultValue('default value');
        $setting->setCustomValue('custom_value');
        $result = $setting->getValue();

        // Then
        $this->assertSame('custom_value', $result);
    }
}
