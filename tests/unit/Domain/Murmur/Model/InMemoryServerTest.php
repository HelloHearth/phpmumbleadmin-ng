<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Model;

use App\Domain\Murmur\Exception\Server\InvalidChannelException;
use App\Domain\Murmur\Exception\Server\InvalidSessionException;
use App\Domain\Murmur\Model\InMemoryServer;
use App\Domain\Murmur\Model\Mock\AclMock;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Domain\Murmur\Model\Mock\ChannelMock;
use App\Domain\Murmur\Model\Mock\GroupMock;
use App\Domain\Murmur\Model\Mock\UserMock;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class InMemoryServerTest extends Unit
{
    public function test_id_property_has_to_be_initialized_manually(): void
    {
        $this->expectExceptionMessage('Typed property '.InMemoryServer::class.'::$id must not be accessed before initialization');

        // Given
        $server = new InMemoryServer();

        // When
        $server->getSid();
    }

    public function test_with_id_method(): void
    {
        // Given
        $server = new InMemoryServer();
        $server->with_id(42);

        // When
        $result = $server->getSid();

        // Then
        $this->assertSame(42, $result);
    }

    public function test_kick_all_users(): void
    {
        // Given
        $server = (new InMemoryServer())->with_users([new UserMock(3), new UserMock(42), new UserMock(99)]);

        // When
        $server->kickAllUsers();

        // Then
        $this->assertEmpty($server->getUsers());
    }

    public function test_validate_user_characters_with_valid_characters(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['username' => '[-=\w\[\]\{\}\(\)\@\|\.]+']);

        // When
        $result = $server->validateUserChars('valid_characters');

        // Then
        $this->assertTrue($result);
    }

    public function test_validate_user_characters_with_invalid_characters(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['username' => '[-=\w\[\]\{\}\(\)\@\|\.]+']);

        // When
        $result = $server->validateUserChars('invalid space character');

        // Then
        $this->assertFalse($result);
    }

    public function test_validate_channel_characters_with_valid_characters(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['channelname' => '[-=\w\[\]\{\}\(\)\@\|\.]+']);

        // When
        $result = $server->validateChannelChars('valid_characters');
        $result2 = $server->validateChannelChars('invalid space character');

        // Then
        $this->assertTrue($result);
        $this->assertFalse($result2);
    }

    public function test_validate_channel_characters_with_invalid_characters(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['channelname' => '[-=\w\[\]\{\}\(\)\@\|\.]+']);

        // When
        $result = $server->validateChannelChars('invalid space character');

        // Then
        $this->assertFalse($result);
    }

    public function test_is_allow_html_when_is_not_true(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['allowHtml' => '']);

        // When
        $result = $server->isAllowHtml();

        // Then
        $this->assertFalse($result);
    }

    public function test_is_allow_html_when_true(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['allowhtml' => 'true']);

        // When
        $result = $server->isAllowHtml();

        // Then
        $this->assertTrue($result);
    }

    public function test_get_server_name(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['registername' => 'custom register name']);

        // When
        $result = $server->getServerName();

        // Then
        $this->assertSame('custom register name', $result);
    }

    public function test_check_is_allow_html_or_strip_tags_when_is_allowed(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['allowhtml' => 'true']);

        // When
        $result = $server->checkIsAllowHtmlOrStripTags('<b>message with HTML<b>', $stripped);

        // Then
        $this->assertSame('<b>message with HTML<b>', $result);
        $this->assertFalse($stripped);
    }

    public function test_check_is_allow_html_or_strip_tags_when_is_not_allowed(): void
    {
        // Given
        $server = (new InMemoryServer())->with_custom_conf(['allowhtml' => 'false']);

        // When
        $result = $server->checkIsAllowHtmlOrStripTags('<b>message with HTML<b>', $stripped);

        // Then
        $this->assertSame('message with HTML', $result);
        $this->assertTrue($stripped);
    }

    public function test_get_acl_when_channel_doesnt_exists(): void
    {
        $this->expectException(InvalidChannelException::class);

        // Given
        $server = new InMemoryServer();

        // When
        $server->getACL(42, $aclList, $groupList, $inherit);
    }

    public function test_set_acl_when_channel_doesnt_exists(): void
    {
        $this->expectException(InvalidChannelException::class);

        // Given
        $server = new InMemoryServer();

        // When
        $server->setACL(42, [], [], true);
    }

    public function test_set_acl_with_invalid_acl(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('invalid value for sequence element `__MOCK::Murmur::ACLList');

        // Given
        $server = new InMemoryServer();
        $server->with_channels([new ChannelMock(42)]);

        // When
        $server->setACL(42, [new AclMock(), 'invalid acl'], [], true);
    }

    public function test_set_acl_with_invalid_group(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('invalid value for sequence element `__MOCK::Murmur::GroupList');

        // Given
        $server = new InMemoryServer();
        $server->with_channels([new ChannelMock(42)]);

        // When
        $server->setACL(42, [], [new GroupMock(), 'invalid group'], true);
    }

    public function test_get_set_acl(): void
    {
        // Given
        $server = new InMemoryServer();
        $server->with_channels([new ChannelMock(42)]);
        $acls = [new AclMock(), new AclMock()];
        $groups = [new GroupMock(), new GroupMock(), new GroupMock];

        // When
        $server->setACL(42, $acls, $groups, true);
        $server->getACL(42, $aclList, $groupList, $inherit);

        // Then
        $this->assertSame($acls, $aclList);
        $this->assertSame($groups, $groupList);
        $this->assertSame(true, $inherit);
    }

    public function defaultConfProvider(): array
    {
        return [
            ['key' => 'registername', 'value' => '',          'expected' => ''],
            ['key' => 'registername', 'value' => 'Ipnoz.net', 'expected' => 'Ipnoz.net'],
            ['key' => 'allowhtml',    'value' => 'true',      'expected' => 'true'],
            ['key' => 'allowhtml',    'value' => 'false',     'expected' => 'false'],
            ['key' => 'new_feature',  'value' => '42',        'expected' => '42'],
            ['key' => 'port',         'value' => '64738',     'expected' => '64738'],
        ];
    }

    /** @dataProvider defaultConfProvider */
    public function test_get_parameter_with_default_conf(string $key, string $value, string $expected): void
    {
        // Given
        $server = new InMemoryServer();
        $server->with_id(1);
        $server->with_default_conf([$key => $value]);

        // When
        $result = $server->getParameter($key);

        // Then
        $this->assertSame($expected, $result);
    }

    public function test_get_parameter_on_port_when_server_id_is_superior_than_1(): void
    {
        // Given
        $server = new InMemoryServer();
        $server->with_id(42);
        $server->with_default_conf(['port' => '64738']);

        // When
        $result = $server->getParameter('port');

        // Then
        $this->assertSame('64779', $result);
    }

    public function test_get_parameter_when_default_conf_doesnt_exists(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $result = $server->getParameter('unknown_key');

        // Then
        $this->assertSame('', $result);
    }

    public function customConfProvider(): array
    {
        return [
            ['key' => 'port',        'value' => '64999', 'expected' => '64999'],
            ['key' => 'allowhtml',   'value' => 'true',  'expected' => 'true'],
            ['key' => 'allowhtml',   'value' => 'false', 'expected' => 'false'],
            ['key' => 'unknown_key', 'value' => 'custom key must be returned', 'expected' => 'custom key must be returned'],
        ];
    }

    /** @dataProvider customConfProvider */
    public function test_get_parameter_with_custom_conf(string $key, string $value, string $expected): void
    {
        // Given
        $server = new InMemoryServer();
        $server->with_default_conf([
            'port' => '64738',
            'allowhtml' => 'true',
        ]);
        $server->with_custom_conf([$key => $value]);

        // When
        $result = $server->getParameter($key);

        // Then
        $this->assertSame($expected, $result);
    }

    public function test_isRunning_property_has_to_be_initialized_manually(): void
    {
        $this->expectExceptionMessage('Typed property '.InMemoryServer::class.'::$isRunning must not be accessed before initialization');

        // Given
        $server = new InMemoryServer();

        // When
        $server->isRunning();
    }

    public function test_kick_user_when_submitted_user_doesnt_exists(): void
    {
        $this->expectException(InvalidSessionException::class);

        // Given
        $server = new InMemoryServer();

        // When
        $server->kickUser(42, '');
    }

    public function test_kick_user(): void
    {
        // Given
        $user3 = new UserMock(3);
        $user99 = new UserMock(99);
        $server = (new InMemoryServer())->with_users([$user3, new UserMock(42), $user99]);

        // When
        $server->kickUser(42, '');

        // Then
        $this->assertSame([0 => $user3, 2 => $user99], $server->getUsers());
    }

    public function test_start_method(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $server->start();

        // Then
        $this->assertTrue($server->isRunning());
    }

    public function test_stop_method(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $server->stop();

        // Then
        $this->assertFalse($server->isRunning());
    }

    public function test_is_started_method(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $server->is_started();

        // Then
        $this->assertTrue($server->isRunning());
    }

    public function test_is_stopped_method(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $server->is_stopped();

        // Then
        $this->assertFalse($server->isRunning());
    }

    public function test_set_conf_method(): void
    {
        // Given
        $server = new InMemoryServer();
        $server->setConf('test_key', 'test_value');

        // When
        $result = $server->getConf('test_key');

        // Then
        $this->assertSame('test_value', $result);
    }

    public function test_get_conf_method_has_to_return_empty_string_when_the_key_is_not_set(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $result = $server->getConf('unknown_key');

        // Then
        $this->assertSame('', $result);
    }

    public function test_with_custom_conf(): void
    {
        // Given
        $server = new InMemoryServer();
        $server->with_custom_conf(['test_key' => 'test_value', 'key2' => 'value2']);

        // When
        $result = $server->getCustomConf();

        // Then
        $this->assertSame(['test_key' => 'test_value', 'key2' => 'value2'], $result);

        // When
        $result = $server->getConf('key2');

        // Then
        $this->assertSame('value2', $result);
    }

    public function test_get_bans_when_list_is_empty(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $result = $server->getBans();

        // Then
        $this->assertEmpty($result);
    }

    public function test_set_bans_with_invalid_ban(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('invalid value for sequence element `__MOCK::Murmur::BanList');

        // Given
        $server = new InMemoryServer();

        // When
        $server->setBans(['invalid ban']);
    }

    public function test_get_set_bans(): void
    {
        // Given
        $server = new InMemoryServer();
        $bans = [new BanMock(), new BanMock(), new BanMock];

        // When
        $server->setBans($bans);
        $result = $server->getBans();

        // Then
        $this->assertSame($result, $bans);
    }

    public function test_with_channels_when_invalid_object_is_submitted(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        // Given
        $server = new InMemoryServer();

        // When
        $server->with_channels(['invalid channel']);
    }

    public function test_with_channels_and_get_channels_method_when_valid_object_is_submitted(): void
    {
        // Given
        $server = new InMemoryServer();
        $channels = [new ChannelMock(42), new ChannelMock(99)];

        // When
        $server->with_channels($channels);

        // Then
        $this->assertSame([42 => $channels[0], 99 => $channels[1]], $server->getChannels());
    }

    public function test_get_channels_when_channel_list_is_empty(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $result = $server->getChannels();

        // Then
        $this->assertEmpty($result);
    }

    public function test_get_channel_state_when_channel_list_is_empty(): void
    {
        $this->expectException(InvalidChannelException::class);

        // Given
        $server = new InMemoryServer();

        // When
        $server->getChannelState(42);
    }

    public function test_get_channel_state_on_unavailable_channel_id(): void
    {
        $this->expectException(InvalidChannelException::class);

        // Given
        $server = (new InMemoryServer())->with_channels([new ChannelMock(99)]);

        // When
        $server->getChannelState(42);
    }

    public function test_get_channel_state_with_valid_channel_id(): void
    {
        // Given
        $channel99 = new ChannelMock(99);
        $server = (new InMemoryServer())->with_channels([$channel99]);

        // When
        $result = $server->getChannelState(99);

        // Then
        $this->assertSame($channel99, $result);
    }

    public function test_get_users_when_list_is_empty(): void
    {
        // Given
        $server = new InMemoryServer();

        // When
        $result = $server->getUsers();

        // Then
        $this->assertEmpty($result);
    }

    public function test_get_users(): void
    {
        // Given
        $server = new InMemoryServer();
        $users = [new UserMock(), new UserMock()];

        // When
        $server->with_users($users);

        // Then
        $this->assertSame($users, $server->getUsers());
    }

    public function test_with_users_when_invalid_object_is_submitted(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        // Given
        $server = new InMemoryServer();

        // When
        $server->with_users(['invalid user']);
    }
}
