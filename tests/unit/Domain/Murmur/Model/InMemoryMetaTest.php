<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Model;

use App\Domain\Murmur\Model\InMemoryMeta;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class InMemoryMetaTest extends Unit
{
    public function test_get_default_conf(): void
    {
        // Given
        $meta = (new InMemoryMeta())->with_default_conf([
            'default_key' => 'default_value',
            'empty_value' => '',
        ]);

        // When
        $result = $meta->getDefaultConf();

        // Then
        $this->assertSame(['default_key' => 'default_value', 'empty_value' => ''], $result);
    }

    public function test_get_version(): void
    {
        // Given
        $meta = (new InMemoryMeta())->with_version('3', '7', '42', 'text version');

        // When
        $meta->getVersion($major, $minor, $patch, $text);

        // Then
        $this->assertSame('3', $major);
        $this->assertSame('7', $minor);
        $this->assertSame('42', $patch);
        $this->assertSame('text version', $text);
    }

    public function test_get_murmur_version_without_text(): void
    {
        // Given
        $meta = (new InMemoryMeta())->with_version('6', '17', '99');

        // When
        $result = $meta->getMurmurVersion();

        // Then
        $this->assertSame(['full' => '6.17.99', 'str' => '6.17.99'], $result);
    }

    public function test_get_murmur_version_with_text(): void
    {
        // Given
        $meta = (new InMemoryMeta())->with_version('6', '17', '99', 'another text version');

        // When
        $result = $meta->getMurmurVersion();

        // Then
        $this->assertSame(['full' => '6.17.99 - another text version', 'str' => '6.17.99'], $result);
    }

    public function test_get_murmur_version_with_same_text_as_version(): void
    {
        // Given
        $meta = (new InMemoryMeta())->with_version('6', '17', '99', '6.17.99');

        // When
        $result = $meta->getMurmurVersion();

        // Then
        $this->assertSame(['full' => '6.17.99', 'str' => '6.17.99'], $result);
    }
}
