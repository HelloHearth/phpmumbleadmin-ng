<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Manager;

use App\Domain\Murmur\Manager\AclManager;
use App\Domain\Murmur\Model\Mock\AclMock;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AclManagerTest extends Unit
{
    private function createMurmurAclMock(array $properties = []): AclMock
    {
        $mock = new AclMock();

        foreach ($properties as $field => $value) {
            $mock->$field = $value;
        }

        return $mock;
    }

    private function createDenyAllAcl(): AclMock
    {
        return $this->createMurmurAclMock([
            'group' => 'all',
            'userid' => -1,
            'inherited' => false,
            'applyHere' => true,
            'applySubs' => true,
            'allow' => 0,
            'deny' => 908
        ]);
    }

    private function createTokenAcl(): AclMock
    {
        return $this->createMurmurAclMock([
            'group' => '#token',
            'userid' => -1,
            'inherited' => false,
            'applyHere' => true,
            'applySubs' => true,
            'allow' => 908,
            'deny' => 0
        ]);
    }

    public function test_controller_getter_and_setter(): void
    {
        $acl1 = $this->createMurmurAclMock(['inherited' => false]);
        $acl2 = $this->createMurmurAclMock(['inherited' => false]);
        $acl3 = $this->createMurmurAclMock(['inherited' => false]);

        $manager = new AclManager([$acl1, $acl2], [$acl3], true);

        $this->assertSame([$acl1, $acl2], $manager->getAclList());
        $this->assertSame([$acl3], $manager->getGroupList());
        $this->assertTrue($manager->getInherit());
    }

    public function test_empty_controller_getter_and_setter(): void
    {
        $manager = new AclManager([], [], false);

        $this->assertSame([], $manager->getAclList());
        $this->assertSame([], $manager->getGroupList());
        $this->assertFalse($manager->getInherit());
    }

    public function test_controller_remove_inherited_acl(): void
    {
        $acl1 = $this->createMurmurAclMock(['inherited' => true]);
        $acl2 = $this->createMurmurAclMock(['inherited' => false]);
        $acl3 = $this->createMurmurAclMock(['inherited' => true]);

        $manager = new AclManager([$acl1, $acl2, $acl3], [], true);

        $this->assertSame([1 => $acl2], $manager->getAclList());
    }

    public function test_controller_remove_inherited_group(): void
    {
        $acl1 = $this->createMurmurAclMock(['inherited' => true]);
        $acl2 = $this->createMurmurAclMock(['inherited' => true]);
        $acl3 = $this->createMurmurAclMock(['inherited' => false]);

        $manager = new AclManager([], [$acl1, $acl2, $acl3], true);

        $this->assertSame([2 => $acl3], $manager->getGroupList());
    }

    public function test_is_token_method(): void
    {
        $acl1 = $this->createMurmurAclMock(['inherited' => false, 'userid' => -1, 'group' => '#token']);
        $acl2 = $this->createMurmurAclMock(['inherited' => false, 'userid' => -1, 'group' => 'token']);

        $manager = new AclManager([], [], true);

        $this->assertTrue($manager->isToken($acl1));
        $this->assertFalse($manager->isToken($acl2));
    }

    public function test_has_token_method(): void
    {
        $acl1 = $this->createMurmurAclMock(['inherited' => false, 'userid' => -1, 'group' => '#token']);
        $acl2 = $this->createMurmurAclMock(['inherited' => false, 'userid' => -1, 'group' => 'token']);

        $manager = new AclManager([$acl1], [], true);
        $manager2 = new AclManager([$acl2], [], true);

        $this->assertTrue($manager->hasToken());
        $this->assertFalse($manager2->hasToken());
    }

    public function test_is_deny_all_method(): void
    {
        $acl = $this->createDenyAllAcl();

        $manager = new AclManager([], [], true);

        $this->assertTrue($manager->isDenyAllToken($acl));
    }

    public function test_set_password_method_when_no_token_is_set(): void
    {
        $manager = new AclManager([], [], true);

        $manager->setPassword('test set password');

        $this->assertTrue($manager->hasToken());
        $this->assertCount(2, $manager->getAclList());
        $this->assertSame('#test set password', $manager->getAclList()[1]->group);
    }

    public function test_set_password_method_when_a_token_is_set_edit_token(): void
    {
        $manager = new AclManager([$this->createDenyAllAcl(), $this->createTokenAcl()], [], true);

        $manager->setPassword('test set password edit token');

        $this->assertTrue($manager->hasToken());
        $this->assertCount(2, $manager->getAclList());
        $this->assertSame('#test set password edit token', $manager->getAclList()[1]->group);
    }

    public function test_remove_password_method(): void
    {
        $acl1 = $this->createMurmurAclMock();
        $aclDenyAll = $this->createDenyAllAcl();
        $aclToken = $this->createTokenAcl();
        $acl2 = $this->createMurmurAclMock();

        $manager = new AclManager([$acl1, $aclDenyAll, $aclToken, $acl2], [], true);
        $manager->removePassword();

        $this->assertSame([0 => $acl1, 3 => $acl2], $manager->getAclList());
    }

    public function test_remove_password_method_only_if_a_token_is_set(): void
    {
        $acl1 = $this->createMurmurAclMock();
        $aclDenyAll1 = $this->createDenyAllAcl();
        $aclDenyAll2 = $this->createDenyAllAcl();
        $acl2 = $this->createMurmurAclMock();

        $manager = new AclManager([$acl1, $aclDenyAll1, $aclDenyAll2, $acl2], [], true);
        $manager->removePassword();

        $this->assertSame([$acl1, $aclDenyAll1, $aclDenyAll2, $acl2], $manager->getAclList());
    }

    public function test_remove_password_method_remove_only_the_first_deny_all_rule(): void
    {
        $acl1 = $this->createMurmurAclMock();
        $aclDenyAll1 = $this->createDenyAllAcl();
        $aclDenyAll2 = $this->createDenyAllAcl();
        $aclToken = $this->createTokenAcl();
        $acl2 = $this->createMurmurAclMock();

        $manager = new AclManager([$acl1, $aclDenyAll1, $aclDenyAll2, $aclToken, $acl2], [], true);
        $manager->removePassword();

        $this->assertSame([0 => $acl1, 2 => $aclDenyAll2, 4 => $acl2], $manager->getAclList());
    }
}
