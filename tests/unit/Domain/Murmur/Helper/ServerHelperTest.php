<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Helper;

use App\Domain\Murmur\Helper\ServerHelper;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerHelperTest extends Unit
{
    public function proxyProvider(): array
    {
        return [
            ['s/1', 1],
            ['s/42', 42],
            ['sid/12', null],
            ['/12', null]
        ];
    }

    /** @dataProvider proxyProvider */
    public function test_proxy_to_id_method($proxy, $expected): void
    {
        $result = ServerHelper::proxyToId($proxy);

        $this->assertSame($expected, $result);
    }
}
