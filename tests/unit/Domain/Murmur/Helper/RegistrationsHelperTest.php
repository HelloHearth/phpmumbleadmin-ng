<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Helper;

use App\Domain\Murmur\Helper\RegistrationsHelper;
use App\Domain\Murmur\Model\Mock\UserMock;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsHelperTest extends Unit
{
    public function isOnlineProvider(): array
    {
        return [
            [1, [new UserMock(42, 0), new UserMock(42, 1), new UserMock(42, 2)], true],
            [42, [new UserMock(42, 0), new UserMock(42, 1), new UserMock(42, 2)], false],
            [1, [new UserMock(42, 0), new UserMock(42, 2)], false],
            [1, [], false]
        ];
    }

    /**
     * @dataProvider isOnlineProvider
     */
    public function test_is_online_method($id, $users, $expected): void
    {
        $result = RegistrationsHelper::isOnline($id, $users);

        $this->assertSame($expected, $result);
    }
}
