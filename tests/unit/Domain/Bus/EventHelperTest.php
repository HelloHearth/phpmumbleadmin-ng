<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Bus;

use App\Domain\Action\Server\ServerNotFound;
use App\Domain\Action\UnknownError;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\EventHelper;
use App\Domain\Murmur\Middleware\ConnectionError;
use App\Domain\Murmur\Middleware\ServerActionError;
use App\Tests\Unit\Domain\Bus\Mock\TestEvent;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EventHelperTest extends Unit
{
    public function busResponseProvider(): array
    {
        return [
            'empty bus response expect default status' => [new BusResponse(), [], 200],
            'UnknownError' => [new BusResponse([new UnknownError(new \Exception())]), [], 500],
            'ConnectionError' => [new BusResponse([new ConnectionError('test message')]), [], 503],
            'ServerActionError' => [new BusResponse([new ServerActionError('test message')]), [], 400],
            'ServerNotFound' => [new BusResponse([new ServerNotFound(42)]), [], 404],
            'Custom event expects custom status code' => [new BusResponse([new TestEvent()]), [TestEvent::class => 666], 666],
            'Custom event not activated expects default status' => [new BusResponse([new TestEvent()]), [], 200],
            'Custom event not found expects default status' => [new BusResponse([]), [TestEvent::class => 666], 200],
        ];
    }

    /** @dataProvider busResponseProvider */
    public function test_get_status_code(BusResponse $busResponse, array $customEvents, int $expectedCode): void
    {
        // Given

        // When
        $response = EventHelper::getStatusCode($busResponse, $customEvents);

        // Then
        $this->assertSame($expectedCode, $response);
    }
}
