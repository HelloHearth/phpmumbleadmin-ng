<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Bus\Mock;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class TestEvent extends AbstractEvent
{
    public const KEY = 'TestCustomEvent';

    public function getKey(): string
    {
        return self::KEY;
    }
}
