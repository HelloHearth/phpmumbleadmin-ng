<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Service;

use App\Domain\Service\InMemorySystemCheckerService;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class InMemorySystemCheckerServiceTest extends Unit
{
    public function test_extension_ssl_is_loaded_has_to_be_initialized_manually(): void
    {
        $this->expectExceptionMessage('Typed property '.InMemorySystemCheckerService::class.'::$extension_ssl_is_loaded must not be accessed before initialization');

        // Given
        $service = new InMemorySystemCheckerService();

        // When
        $service->extension_ssl_is_loaded();
    }

    public function test_with_extension_ssl_method(): void
    {
        // Given
        $service = new InMemorySystemCheckerService();
        $service->with_ssl_extension();

        // When
        $result = $service->extension_ssl_is_loaded();

        // Then
        $this->assertTrue($result);
    }

    public function test_without_extension_ssl_method(): void
    {
        // Given
        $service = new InMemorySystemCheckerService();
        $service->without_ssl_extension();

        // When
        $result = $service->extension_ssl_is_loaded();

        // Then
        $this->assertFalse($result);
    }

    public function test_upload_file_is_allowed_has_to_be_initialized_manually(): void
    {
        $this->expectExceptionMessage('Typed property '.InMemorySystemCheckerService::class.'::$upload_file_is_allowed must not be accessed before initialization');

        // Given
        $service = new InMemorySystemCheckerService();

        // When
        $service->upload_file_is_allowed();
    }

    public function test_with_ssl_extension_method(): void
    {
        // Given
        $service = new InMemorySystemCheckerService();
        $service->with_upload_file_allowed();

        // When
        $result = $service->upload_file_is_allowed();

        // Then
        $this->assertTrue($result);
    }

    public function test_without_ssl_extension_method(): void
    {
        // Given
        $service = new InMemorySystemCheckerService();
        $service->without_upload_file_allowed();

        // When
        $result = $service->upload_file_is_allowed();

        // Then
        $this->assertFalse($result);
    }
}
