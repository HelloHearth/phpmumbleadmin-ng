<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Model;

use App\Domain\Murmur\Model\ServersListCache;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServersListCacheTest extends Unit
{
    public function test_set_expires_at_method(): void
    {
        $model = new ServersListCache([]);

        $model->setExpiresAt(3600);
        $result = $model->getExpiresAt();

        $this->assertEqualsWithDelta($result, \time()+3600, 1);

        $model->setExpiresAt(-3600);
        $result = $model->getExpiresAt();

        $this->assertEqualsWithDelta($result, \time()-3600, 1);
    }

    public function test_is_expired_method(): void
    {
        $model = new ServersListCache([]);

        $result = $model->isExpired();

        $this->assertFalse($result);

        $model->setExpiresAt(-3600);
        $result = $model->isExpired();

        $this->assertTrue($result);
    }

    public function test_is_valid_and_invalidate_method(): void
    {
        $model = new ServersListCache([]);

        $result = $model->isValid();

        $this->assertTrue($result);

        $model->invalidate();
        $result = $model->isValid();

        $this->assertFalse($result);
    }

    public function test_is_valid_method_on_expired_time(): void
    {
        $model = new ServersListCache([]);

        $model->setExpiresAt(-3600);
        $result = $model->isValid();

        $this->assertFalse($result);
    }
}
