<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Model;

use App\Domain\Model\IpAddress;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class IpAddressTest extends Unit
{
    public function test_from_string_method_with_invalid_ip_address(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid ip address');

        IpAddress::fromString('invalid ip address string/prefix');
    }

    public function invalidMaskProvider(): array
    {
        return [
            ['127.0.0.1/-1'],
            ['127.0.0.1/0'],
            ['127.0.0.1/0'],
            ['127.0.0.1/33'],
            ['2001:db8::1/-1'],
            ['2001:db8::1/0'],
            ['2001:db8::1/129'],
            ['2001:db8::1/characters'],
        ];
    }

    /** @dataProvider invalidMaskProvider */
    public function test_from_string_method_with_invalid_mask($address): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid bitmask');

        IpAddress::fromString($address);
    }

    public function ipv4Provider(): array
    {
        return [
            ['127.0.0.1', 32],
            ['127.0.0.1/25', 25],
        ];
    }

    /** @dataProvider ipv4Provider */
    public function test_from_string_method_with_ipv4($address, int $expected): void
    {
        $ipAddress = IpAddress::fromString($address);

        $this->assertSame('127.0.0.1', $ipAddress->address);
        $this->assertSame($expected, $ipAddress->bitmask);
    }

    public function ipv6Provider(): array
    {
        return [
            ['2001:db8::1', 128],
            ['2001:db8::1/32', 32],
        ];
    }

    /** @dataProvider ipv6Provider */
    public function test_from_string_method_with_ipv6($address, int $expected): void
    {
        $ipAddress = IpAddress::fromString($address);

        $this->assertSame('2001:db8::1', $ipAddress->address);
        $this->assertSame($expected, $ipAddress->bitmask);
    }

    public function test_is_ipv4_method(): void
    {
        $ipAddress = IpAddress::fromString('127.0.0.1');

        $this->assertTrue($ipAddress->isIpv4());

        $ipAddress = IpAddress::fromString('2001:db8::1');

        $this->assertFalse($ipAddress->isIpv4());
    }

    public function test_is_ipv6_method(): void
    {
        $ipAddress = IpAddress::fromString('127.0.0.1');

        $this->assertFalse($ipAddress->isIpv6());

        $ipAddress = IpAddress::fromString('2001:db8::1');

        $this->assertTrue($ipAddress->isIpv6());
    }
}
