<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain;

use App\Domain\Action\AbstractViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AbstractViewModelTest extends Unit
{
    public function test_empty_constructor(): void
    {
        $viewModel = $this->getMockForAbstractClass(AbstractViewModel::class);

        $this->assertFalse($viewModel->getHasError());
        $this->assertSame(['key' => null, 'translationDomain' => null], $viewModel->getErrorMessage());
    }

    public function test_getter_and_setter_for_error_message_field(): void
    {
        $viewModel = $this->getMockForAbstractClass(AbstractViewModel::class);

        $viewModel->setError('test error message');

        $this->assertTrue($viewModel->getHasError());
        $this->assertSame(['key' => 'test error message', 'translationDomain' => null], $viewModel->getErrorMessage());
    }

    public function test_getter_and_setter_for_error_message_field_with_translation_domain(): void
    {
        $viewModel = $this->getMockForAbstractClass(AbstractViewModel::class);

        $viewModel->setError('test error message', 'test translation domain');

        $this->assertTrue($viewModel->getHasError());
        $this->assertSame(['key' => 'test error message', 'translationDomain' => 'test translation domain'], $viewModel->getErrorMessage());
    }

    public function test_getter_and_setter_for_debug_field(): void
    {
        $viewModel = $this->getMockForAbstractClass(AbstractViewModel::class);
        $this->assertSame('', $viewModel->getDebug());

        $viewModel->setDebug('test debug field');

        $this->assertSame('test debug field', $viewModel->getDebug());
    }

    public function test_getter_and_setter_for_debug_field_is_mixed(): void
    {
        $viewModel = $this->getMockForAbstractClass(AbstractViewModel::class);

        $viewModel->setDebug(1.2);

        $this->assertSame(1.2, $viewModel->getDebug());
    }
}
