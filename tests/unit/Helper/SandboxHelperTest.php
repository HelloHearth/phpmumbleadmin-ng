<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Domain\Helper\SandboxHelper;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SandboxHelperTest extends Unit
{
    private const OUTPUT_PATH = __DIR__.'/../../_output/sandbox/';
    private string $filename = '';

    public function test_filename_method(): void
    {
        $filename = SandboxHelper::filename();
        $this->assertSame('sandbox_.html', $filename);
    }

    public function test_html_code_method(): void
    {
        $data = 'DummyDataToInject';
        $return = SandboxHelper::htmlCode($data);

        $this->assertStringContainsString('<title>SANDBOX</title>', $return);
        $this->assertStringContainsString($data, $return);
    }

    public function test_to_create_a_sandbox_file(): void
    {
        $this->filename = SandboxHelper::filename();
        $datas = 'Test to create a sandbox.html file with datas';

        SandboxHelper::createFile(self::OUTPUT_PATH, $datas);

        $this->assertFileIsWritable(self::OUTPUT_PATH.$this->filename);
    }

    public function _after(): void
    {
        if (is_file(self::OUTPUT_PATH.$this->filename)) {
            unlink(self::OUTPUT_PATH.$this->filename);
            rmdir(self::OUTPUT_PATH);
        }
    }
}
