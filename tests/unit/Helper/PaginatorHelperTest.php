<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Domain\Helper\PaginatorHelper;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PaginatorHelperTest extends Unit
{
    public function getValidPageProvider(): array
    {
        return [
            'it test when we are on first page' => [250, 1, 10, 1],
            'it test when we are on on middle page' => [250, 20, 10, 20],
            'it test when we are on on last page' => [250, 25, 10, 25],
            'it test when the current page argument is too high' => [250, 9999999, 10, 25],
            'it test when we set a invalid totalOfItems argument of 0' => [0, 9999999, 10, 1],
            'it test when we set 0 to the current page number' => [250, 0, 10, 1],
            'it test when we set 0 to the $limitPerPage argument' => [250, 99999999, 0, 250],
            'it test when we set all parameters with 0' => [0, 0, 0, 1],
            'it test when we set a negative $totalOfItems' => [-10, 9999999, 10, 1],
            'it test when we set a negative $currentPage' => [250, -9999999, 10, 1],
            'it test when we set a negative $limitPerPages' => [250, 9999999, -10, 250],
            'it test when we set a negative $totalOfItems and $limitPerPages' => [-10, 9999999, -10, 1],
            'it test when we set a negative for all arguments' => [-10, -9999999, -10, 1],
        ];
    }

    /** @dataProvider getValidPageProvider */
    public function test_get_valid_page_method($totalOfItems, $currentPage, $limitPerPages, $expected): void
    {
        $page = PaginatorHelper::getValidPage($totalOfItems, $currentPage, $limitPerPages);
        $this->assertSame($expected, $page);
    }
}
