<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Domain\Helper\HtmlHelper;
use Codeception\Test\Unit;

class HtmlHelperTest extends Unit
{
    public function spaceToEntityProvider(): array
    {
        return [
            [null, ''],
            ['', ''],
            [' ', '&nbsp;'],
            [' tree   spaces ','&nbsp;tree&nbsp;&nbsp;&nbsp;spaces&nbsp;'],
            ['two words', 'two&nbsp;words'],
            ['tree words yeah', 'tree&nbsp;words&nbsp;yeah'],
            [' tree words yeah ', '&nbsp;tree&nbsp;words&nbsp;yeah&nbsp;'],
        ];
    }

    /**
     * @dataProvider spaceToEntityProvider
     */
    public function test_space_to_entity_method($string, $expected): void
    {
        $result = HtmlHelper::spaceToEntity($string);
        $this->assertSame($expected, $result);
    }

    public function htmlForRowWithoutJavascriptProvider(): array
    {
        return [
            ['', ''],
            ['only text', 'only text'],
            ['only téxt with non ASCII chàràctèrs', 'only téxt with non ASCII chàràctèrs'],
            ['Non valid HTML [\.\<\>\/ \-=\w\#\[\]\{\}\(\)\@\|]+', 'Non valid HTML [\.\<\>\/ \-=\w\#\[\]\{\}\(\)\@\|]+'],
            ['Hello <script>alert("ho ho ho");</script>dear', 'Hello dear'],
            ['Hello <script type="text/javascript">alert("ho ho ho");</script>dear', 'Hello dear'],
            ['<strong>Hello <script type="text/javascript">alert("ho ho ho");</script>dear</strong>', '<strong>Hello dear</strong>'],
            ['<strong>Hello </strong><script type="text/javascript">alert("ho ho ho");</script>dear', '<strong>Hello </strong>dear'],
            ['<strong>Héllo </strong><script type="text/javascript">alert("ho ho ho");</script>dèàr', '<strong>Héllo </strong>dèàr'],
            ['Hello <strong>dear</strong><script type="text/javascript">alert("ho ho ho");</script>', 'Hello <strong>dear</strong>'],
            ['Hello <strong> my </strong><script type="text/javascript">alert("ho ho ho");</script> dear', 'Hello <strong> my </strong> dear'],
            ['<strong onload="javascript:alert(\'ah ah ah\')">Hello dear</strong>', '<strong>Hello dear</strong>'],
            ['<strong onload="notMalicious">Hello dear</strong>', '<strong>Hello dear</strong>'],
            ['<strong onload="with Non ASCII chars">Héllo dèàr</strong>', '<strong>Héllo dèàr</strong>'],
            ['<strong onClick="javascript:alert(\'ah ah ah\')">Hello dear</strong>', '<strong>Hello dear</strong>'],
            ['Hello<strong ONBLUR="javascript:alert(\'ah ah ah\')"> my </strong>dear', 'Hello<strong> my </strong>dear'],
            ['Hello<strong onunloaD="notMalicious"> my dear</strong>', 'Hello<strong> my dear</strong>'],
            ['<strong onUnknownEvent="notMalicious">Hello my </strong>dear', '<strong>Hello my </strong>dear']
        ];
    }

    /**
     * @dataProvider htmlForRowWithoutJavascriptProvider
     */
    public function test_raw_without_javascript_method($html, $expected): void
    {
        $result = HtmlHelper::RawWithoutJavascript($html);
        $this->assertSame($expected, $result);
    }
}
