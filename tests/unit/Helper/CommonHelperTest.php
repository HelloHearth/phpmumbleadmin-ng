<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Domain\Helper\CommonHelper;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CommonHelperTest extends Unit
{
    protected UnitTester $tester;

    public function test_decimal_array_to_chars_with_empty_array(): void
    {
        $decimalArray = [];
        $this->assertSame('', CommonHelper::decimalArrayToChars($decimalArray));
    }

    public function test_decimal_array_to_chars(): void
    {
        $decimalArray = [
            84, 104, 105, 115, 32, 105, 83, 32, 119, 72, 97, 84, 32, 101, 120,
            112, 101, 99, 116, 101, 68
        ];
        $this->assertSame('This iS wHaT expecteD', CommonHelper::decimalArrayToChars($decimalArray));
    }
}
