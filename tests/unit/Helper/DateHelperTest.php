<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Domain\Helper\DateHelper;
use App\Tests\UnitTester;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DateHelperTest extends Unit
{
    protected UnitTester $tester;

    public function test_uptime_method_with_empty_format_parameter_return_format_one(): void
    {
        $this->assertSame('1 day 01h01m01s', DateHelper::uptime(90061));
    }

    public function uptimeTimestampProvider(): array
    {
        return [
            'Zero day format parameter 1' => [3661, 1, '01h01m01s'],
            'Zero day format parameter 2' => [3661, 2, '01h01m'],
            'Zero day format parameter 3' => [3661, 3, '01h01m'],
            'One day format parameter 1' => [90061, 1, '1 day 01h01m01s'],
            'One day format parameter 2' => [90061, 2, '1 day 01h01m'],
            'One day format parameter 3' => [90061, 3, '1 day'],
            'Two days format parameter 1' => [90061* 2, 1, '2 days 02h02m02s'],
            'Two days format parameter 2' => [90061* 2, 2, '2 days 02h02m'],
            'Two days format parameter 3' => [90061* 2, 3, '2 days'],

            'Negative integer for timestamp parameter' => [-90061, 1, '1 day 01h01m01s'],
            'Invalid format parameter' => [90061, 123, '1 day 01h01m01s'],
            'Negative integer format parameter' => [90061, -123, '1 day 01h01m01s'],
        ];
    }

    /**
     * @dataProvider uptimeTimestampProvider
     */
    public function test_uptime_method($timestamp, $format, $expected): void
    {
        $this->assertSame($expected, DateHelper::uptime($timestamp, $format));
    }

    public function datetimeStringProvider(): array
    {
        return [
            'Valid datetime: 2038-01-19 03:14:07' => ['2038-01-19 03:14:07', 2147483647],
            'Valid datetime: 2038-01-19T03:14:07' => ['2038-01-19T03:14:07', 2147483647],
            'Valid datetime: 2038-01-19' => ['2038-01-19', 2147472000],
            'Valid datetime: 2018-07-15 16:48:00' => ['2018-07-15 16:48:00', 1531673280],
            'Valid datetime: 2018-07-15T16:48:00' => ['2018-07-15T16:48:00', 1531673280],
            'Valid datetime: 2018-07-15' => ['2018-07-15', 1531612800],
            'Valid datetime: 1998-07-12 22:48:00' => ['1998-07-12 22:48:00', 900283680],
            'Valid datetime: 1998-07-12T22:48:00' => ['1998-07-12T22:48:00', 900283680],
            'Valid datetime: 1998-07-12' => ['1998-07-12', 900201600],
            'Valid datetime: 1980-01-12 12:00:00' => ['1980-01-12 12:00:00', 316526400],
            'Valid datetime: 1980-01-12T12:00:00' => ['1980-01-12T12:00:00', 316526400],
            'Valid datetime: 1980-01-12' => ['1980-01-12', 316483200],
            'Valid datetime: 1970-01-01T00:00:00' => ['1970-01-01T00:00:00', 0],
            'Valid datetime: 1970-01-01 00:00:00' => ['1970-01-01 00:00:00', 0],
            'Valid datetime: 1970-01-01' => ['1970-01-01', 0],
            'Valid datetime: 1951-09-30 11:59:59' => ['1951-09-30 11:59:59', -576072001],
            'Valid datetime: 1951-09-30T11:59:59' => ['1951-09-30T11:59:59', -576072001],
            'Valid datetime: 1951-09-30' => ['1951-09-30', -576115200],
            'Valid datetime: 1951-03-23 23:59:59' => ['1951-03-23 23:59:59', -592531201],
            'Valid datetime: 1951-03-23T23:59:59' => ['1951-03-23T23:59:59', -592531201],
            'Valid datetime: 1951-03-23' => ['1951-03-23', -592617600],
            'Valid datetime: 1901-12-13 20:45:53' => ['1901-12-13 20:45:53', -2147483647],
            'Valid datetime: 1901-12-13T20:45:53' => ['1901-12-13T20:45:53', -2147483647],

            'Invalid datetime: d2012-04-07 17:25:03' => ['d2012-04-07 17:25:03', false],
            'Invalid datetime: 2012-d04-07 17:25:03' => ['2012-d04-07 17:25:03', false],
            'Invalid datetime: 2012-04-d07 17:25:03' => ['2012-04-d07 17:25:03', false],
            'Invalid datetime: 2012-04-07 d17:25:03' => ['2012-04-07 d17:25:03', false],
            'Invalid datetime: 2012-04-07 17:d25:03' => ['2012-04-07 17:d25:03', false],
            'Invalid datetime: 2012-04-07 17:25:d03' => ['2012-04-07 17:25:d03', false],
            'Invalid datetime: 92012-04-07 17:25:03' => ['92012-04-07 17:25:03', false],
            'Invalid datetime: 2012-204-07 17:25:03' => ['2012-204-07 17:25:03', false],
            'Invalid datetime: 2012-20-107 17:25:03' => ['2012-20-107 17:25:03', false],
            'Invalid datetime: 2012-20-07 171:25:03' => ['2012-20-07 171:25:03', false],
            'Invalid datetime: 2012-20-07 17:251:03' => ['2012-20-07 17:251:03', false],
            'Invalid datetime: 2012-20-07 17:25:013' => ['2012-20-07 17:25:013', false],
            'Invalid datetime: 2012-20-07 17:25:d3' => ['2012-20-07 17:25:d3', false],
            'Invalid datetime: 2012-20-07 17:2f:13' => ['2012-20-07 17:2f:13', false],
            'Invalid datetime: 2012-20-07 1P:25:13' => ['2012-20-07 P7:25:13', false],
            'Invalid datetime: 2012-20-z7 17:25:13' => ['2012-20-a7 17:25:13', false],
            'Invalid datetime: 2012-2i-07 17:25:13' => ['2012-2i-07 17:25:13', false],
            'Invalid datetime: 20r2-20-07 17:25:13' => ['20r2-20-07 17:25:13', false],
        ];
    }

    /**
     * @dataProvider datetimeStringProvider
     */
    public function test_datetime_to_timestamp_method($datetime, $expected): void
    {
        $this->assertSame($expected, DateHelper::datetimeToTimestamp($datetime));
    }
}
