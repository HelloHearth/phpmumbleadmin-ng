<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Domain\Helper\IpHelper;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class IpHelperTest extends Unit
{
    public function validIpv4Provider(): array
    {
        return [
            ['192.168.0.1'],
            ['172.17.17.18'],
            ['0.0.0.0'],
            ['255.255.255.255'],
            ['150.150.150.150']
        ];
    }

    /**
     * @dataProvider validIpv4Provider
     */
    public function test_is_ipv4_method_with_valid_ip($ipv4): void
    {
        $this->assertTrue(IpHelper::isIPv4($ipv4));
    }

    public function invalidIpv4Provider(): array
    {
        return [
            ['256.255.255.255'],
            ['255.256.255.255'],
            ['255.255.256.255'],
            ['255.255.255.256'],
            ['-15.15.15.15'],
            ['15.-15.15.15'],
            ['15.15.-15.15'],
            ['15.15.15.-15'],
            ['0000'],
            ['0:0:0:0'],
            ['0::0'],
            ['::1'],
            ['1::1'],
            ['150150150150'],
            ['plop']
        ];
    }

    /**
     * @dataProvider invalidIpv4Provider
     */
    public function test_is_ipv4_method_with_invalid_ip($ipv4): void
    {
        $this->assertFalse(IpHelper::isIPv4($ipv4));
    }

    public function validIpv6Provider(): array
    {
        return [
            ['0000:0000:0000:0000:0000:0000:0000:0000'],
            ['::'],
            ['0000:0000:0000:0000:0000:0000:0000:0001'],
            ['::1'],
            ['1000:0000:0000:0000:0000:0000:0000:0001'],
            ['1::1'],
            ['F0B4:0000:0000:ADBC:0000:0000:0000:B0FF'],
            ['F0B4::ADBC:0000:0000:0000:B0FF'],
            ['ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff'],
            ['FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF'],
            ['FFFF:FFFF:EfFe:FFFF:FFFF:aBcD:FFFF:FFFF'],
        ];
    }

    /**
     * @dataProvider validIpv6Provider
     */
    public function test_is_ipv6_method_with_valid_ip($ipv6): void
    {
        $this->assertTrue(IpHelper::isIPv6($ipv6));
    }

    public function zeroPadProvider(): array
    {
        return [
            ['plop', 8, '0000plop'],
            ['plop', 16, '000000000000plop'],
            ['plop', 4, 'plop'],
            ['plop', 2, 'plop'],
            ['plop', 0, 'plop'],
            ['p', 8, '0000000p'],
            ['p', 16, '000000000000000p'],
            ['p', 4, '000p'],
            ['p', 2, '0p'],
            ['p', 0, 'p']
        ];
    }

    /**
     * @dataProvider zeroPadProvider
     */
    public function test_zero_pad_method($string, $limit, $expected): void
    {
        $this->assertSame($expected, IpHelper::zeroPad($string, $limit));
    }

    public function validDecimalTostringProvider(): array
    {
        return [
            'decimals of ipv4' => [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 10, 20, 30, 40], ['type' => 'ipv4', 'ip' => '10.20.30.40']],
            'decimals of ipv6' => [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16], ['type' => 'ipv6', 'ip' => '102:304:506:708:90a:b0c:d0e:f10']]
        ];
    }

    /**
     * @dataProvider validDecimalTostringProvider
     */
    public function test_decimal_to_string_method_with_valid_data($decimals, $expected): void
    {
        $this->assertSame($expected, IpHelper::decimalTostring($decimals));
    }

    public function invalidDecimalTostringProvider(): array
    {
        return [
            'array of 0 entry' => [[]],
            'array of 1 entry' => [[1]],
            'array of 8 entries' => [[1, 2, 3, 4, 5, 6, 7, 8]],
            'array of 15 entries' => [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]],
            'array of 17 entries' => [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]],
            'array of 255 entries' => [range(1, 255)]
        ];
    }

    /**
     * @dataProvider invalidDecimalTostringProvider
     */
    public function test_decimal_to_string_method_with_invalid_data($decimals): void
    {
        $this->assertSame(['type' => 'invalid', 'ip' => ''], IpHelper::decimalTostring($decimals));
    }

    public function mask4To6Provider(): array
    {
        return [
            'bit of 32' => [32, 128],
            'bit of 24' => [24, 120],
            'bit of 1' => [1, 97]
        ];
    }

    /**
     * @dataProvider mask4To6Provider
     */
    public function test_mask_4_to_6_method($bit, $expected): void
    {
        $this->assertSame($expected, IpHelper::mask4To6($bit));
    }

    public function mask6To4Provider(): array
    {
        return [
            'bit of 128' => [128, 32],
            'bit of 112' => [112, 16],
            'bit of 97' => [97, 1]
        ];
    }

    /**
     * @dataProvider mask6To4Provider
     */
    public function test_mask_6_to_4_method($bit, $expected): void
    {
        $this->assertSame($expected, IpHelper::mask6To4($bit));
    }

    public function test_string_to_decimal_ipv4_method(): void
    {
        $this->assertSame([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 24, 35, 112, 250], IpHelper::stringToDecimalIPv4('24.35.112.250'));
    }

    public function stringToDecimalIPv6Provider(): array
    {
        return [
            ':::' => ['::', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]],
            ':::1' => ['::1', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]],
            '1:::1' => ['1::1', [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]],
            'fb10:::e150' => ['fb10::e150', [251, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 225, 80]],
        ];
    }

    /**
     * @dataProvider stringToDecimalIPv6Provider
     */
    public function test_string_to_decimal_ipv6_method($ipv6, $expected): void
    {
        $this->assertSame($expected, IpHelper::stringToDecimalIPv6($ipv6));
    }

    public function compressIPv6StringProvider(): array
    {
        return [
            'Already compressed ipv6' => ['bcde:3459::ACDC:0000:0000:0001', 'bcde:3459::ACDC:0000:0000:0001'],
            'Already compressed ipv6 (2)' => ['bcde:3459:0000:0000:ACDC::1', 'bcde:3459:0000:0000:ACDC::1'],
            'compress all zero' => ['1000:0200:0030:0004:5000:0060:0700:0008', '1000:200:30:4:5000:60:700:8'],
            '0000:0000:0000:0000:0000:0000:0000:0000' => ['0000:0000:0000:0000:0000:0000:0000:0000', '::'],
            '0000:0000:0000:0000:0000:0000:0000:0001' => ['0000:0000:0000:0000:0000:0000:0000:0001', '::1'],
            '0001:0000:0000:0000:0000:0000:0000:0001' => ['0001:0000:0000:0000:0000:0000:0000:0001', '1::1'],
            '1000:0000:0000:0000:0000:0000:0000:0001' => ['1000:0000:0000:0000:0000:0000:0000:0001', '1000::1'],
            'Fe80:0000:0000:0000:0000:0000:0000:0000' => ['Fe80:0000:0000:0000:0000:0000:0000:0000', 'Fe80::'],
            'F0B4:0000:ADBC:0000:ADBC:0000:0000:B0FF' => ['F0B4:0000:ADBC:0000:0000:ADBC:0000:B0FF', 'F0B4::ADBC:0:0:ADBC:0:B0FF'],
            'F0B4:0000:0000:ADBC:0000:0000:0000:B0FF' => ['F0B4:0000:0000:ADBC:0000:0000:0000:B0FF', 'F0B4::ADBC:0:0:0:B0FF'],
            '00B4:0000:0000:0000:0000:BC:0000:B0FF' => ['00B4:0000:0000:0000:0000:BC:0000:B0FF', 'B4::BC:0:B0FF']
        ];
    }

    /**
     * @dataProvider compressIPv6StringProvider
     */
    public function test_compress_ipv6_string_method($ipv6, $expected): void
    {
        $this->assertSame($expected, IpHelper::compressIPv6String($ipv6));
    }
}
