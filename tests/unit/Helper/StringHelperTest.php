<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Domain\Helper\StringHelper;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class StringHelperTest extends Unit
{
    public function cutLongStringProvider(): array
    {
        return [
            ['short', 10, '...', 'short'],
            ['very long string', 10, '...', 'very long...'],
            ['very long string', 10, '', 'very long'],
            [null, 10, '...', '']
        ];
    }

    /**
     * @dataProvider cutLongStringProvider
     */
    public function test_cut_long_string_method($string, $maxLen, $cutString, $expected): void
    {
        $result = StringHelper::cutLongString($string, $maxLen, $cutString);

        $this->assertSame($expected, $result);
    }
}
