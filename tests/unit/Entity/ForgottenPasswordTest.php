<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Entity\ForgottenPassword;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordTest extends Unit
{
    public function test_is_expired_method_when_date_is_negative(): void
    {
        $forgottenPassword = new ForgottenPassword();

        $forgottenPassword->setValidUntil(new \DateTimeImmutable('-5seconds'));

        $this->assertTrue($forgottenPassword->isExpired());
    }

    public function test_is_expired_method_when_date_is_positive(): void
    {
        $forgottenPassword = new ForgottenPassword();

        $forgottenPassword->setValidUntil(new \DateTimeImmutable('+5seconds'));

        $this->assertFalse($forgottenPassword->isExpired());
    }
}
