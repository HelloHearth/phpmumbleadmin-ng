<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Security\Voter;

use App\Domain\Murmur\Model\MetaInterface;
use App\Infrastructure\Symfony\Security\Voter\CanModifyMumbleUserNameOnChannelTreeVoter;
use Codeception\Test\Unit;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CanModifyMumbleUserNameOnChannelTreeVoterTest extends Unit
{
    private function createMetaMock(string $version): MetaInterface
    {
        return $this->makeEmpty(MetaInterface::class, ['getMurmurVersion' => ['str' => $version]]);
    }

    public function validVoterProvider(): array
    {
        return [
            [$this->createMetaMock('1.2.4'), CanModifyMumbleUserNameOnChannelTreeVoter::KEY, VoterInterface::ACCESS_GRANTED],
            [$this->createMetaMock('1.2.3'), CanModifyMumbleUserNameOnChannelTreeVoter::KEY, VoterInterface::ACCESS_DENIED]
        ];
    }

    /**
     * @dataProvider validVoterProvider
     */
    public function test_with_valid_voter($meta, $attribute, $expected): void
    {
        $voter = new CanModifyMumbleUserNameOnChannelTreeVoter();

        $result = $voter->vote($this->makeEmpty(TokenInterface::class), $meta, [$attribute]);

        $this->assertSame($expected, $result);
    }

    public function invalidVoterProvider(): array
    {
        return [
            [$this->createMetaMock('1.2.4'), 'not_supported_attribute_key'],
            ['not_supported_subject', CanModifyMumbleUserNameOnChannelTreeVoter::KEY],
            [null, CanModifyMumbleUserNameOnChannelTreeVoter::KEY]
        ];
    }

    /**
     * @dataProvider invalidVoterProvider
     */
    public function test_with_invalid_voter($meta, $attribute): void
    {
        $voter = new CanModifyMumbleUserNameOnChannelTreeVoter();

        $result = $voter->vote($this->makeEmpty(TokenInterface::class), $meta, [$attribute]);

        $this->assertSame(VoterInterface::ACCESS_ABSTAIN, $result);
    }
}
