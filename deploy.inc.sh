#!/bin/bash

# Font colors
RED1='\033[0;31m'
RED2='\033[1;31m'
GREEN1='\033[0;32m'
GREEN2='\033[1;32m'
YELLOW1='\033[0;33m'
YELLOW2='\033[1;33m'
BLUE1='\033[0;34m'
BLUE2='\033[1;34m'
CYAN1='\033[0;36m'
CYAN2='\033[1;36m'

# Background colors
BG_BLACK='\033[40m'
BG_RED='\033[41m'
BG_GREEN='\033[42m'
BG_YELLOW='\033[43m'
BG_BLUE='\033[44m'
BG_MAGENTA='\[45m'
BG_CYAN='\033[46m'
BG_WHITE='\033[47m'

CANCEL_COLORS='\033[0m'

function announce_command()
{
  echo
  echo -e $BG_BLUE'Command'$CANCEL_COLORS ":" $YELLOW2"$1"$CANCEL_COLORS
}

function announce_ssh_connection()
{
  echo
  echo -e $BG_RED"SSH connection to"$CANCEL_COLORS  ":" $YELLOW2"$1"$CANCEL_COLORS
  echo
}

function shutdown()
{
  # Return to the initial git branch
  if [ "$INITIAL_BRANCH" != "master" ]; then
    echo 'Run command: git checkout '"$INITIAL_BRANCH"
    git checkout "$INITIAL_BRANCH"
  fi

  exit "$1"
}
