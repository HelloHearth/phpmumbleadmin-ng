<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\DataFixtures\ORM;

use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Service\AdminPasswordHasherService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\AdminEntity;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LoadAdmins extends Fixture
{
    private AdminPasswordHasherService $hasherService;

    public function __construct(AdminPasswordHasherService $hasherService)
    {
        $this->hasherService = $hasherService;
    }

    public function load(ObjectManager $manager)
    {
        $data = [];
        $data[] = ['SA', 'test', [SecurityServiceInterface::ROLE_SUPER_ADMIN]];
        $data[] = ['rootadmin', 'test', [SecurityServiceInterface::ROLE_ROOT_ADMIN]];
        $data[] = ['admin', 'test', [SecurityServiceInterface::ROLE_ADMIN]];

        foreach ($data as $array) {

            [$username, $password, $roles] = $array;

            $user = new AdminEntity();
            $user->setUsername($username);
            $user->setPassword($this->hasherService->hash($user, $password));
            $user->setRoles($roles);

            $manager->persist($user);
        }

        $manager->flush();
    }
}

