<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Model\PaginatedInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface PaginatorServiceInterface
{
    public function paginate($target, int $page = 1, int $limit = null): PaginatedInterface;
}
