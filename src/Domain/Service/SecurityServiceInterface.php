<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface SecurityServiceInterface
{
    public const ROLE_SUPER_ADMIN      = 'ROLE_SUPER_ADMIN';
    public const ROLE_ROOT_ADMIN       = 'ROLE_ROOT_ADMIN';
    public const ROLE_ADMIN            = 'ROLE_ADMIN';
    public const ROLE_SUPER_USER       = 'ROLE_SUPER_USER';
    public const ROLE_SUPER_USER_RU    = 'ROLE_SUPER_USER_RU';
    public const ROLE_MUMBLE_USER      = 'ROLE_MUMBLE_USER';

    public function isGranted($attributes, $subject = null): bool;

    public function isGrantedRootAdmin($subject = null): bool;
    public function isGrantedAdmin($subject = null): bool;
    public function isGrantedSuperUser($subject = null): bool;
    public function isGrantedSuperUserRu($subject = null): bool;
    public function isGrantedMumbleUser($subject = null): bool;
    public function isRoleSuperUserRuOrLess($subject = null): bool;

    public function canAccessToAdministration($subject = null): bool;
    public function canAccessToDashboard($subject = null): bool;
    public function canAccessToRegistration($subject = null): bool;
    public function canEditMurmurSettingTypeAdministration($subject = null): bool;
    public function canEditRole($subject = null): bool;
    public function canEditServer($subject = null): bool;
    public function canModifyRegistrationPassword($subject = null): bool;
    public function canModifyRegistrationUsername($subject = null): bool;
    public function canModifyMumbleUsernameOnChannelTree($subject = null): bool;
}
