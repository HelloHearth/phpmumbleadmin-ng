<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service\Role;

use App\Domain\Model\AdminInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface RoleNameServiceInterface
{
    public const SUPER_ADMIN      = 'SuperAdmin';
    public const ROOT_ADMIN       = 'RootAdmin';
    public const ADMIN            = 'Admin';
    public const SUPER_USER       = 'SuperUser';
    public const SUPER_USER_RU    = 'SuperUser_ru';
    public const MUMBLE_USER      = 'Mumble user';

    public const INVALID          = 'Not valid role';

    public function find(AdminInterface $user): string;
}
