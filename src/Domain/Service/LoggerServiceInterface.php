<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface LoggerServiceInterface
{
    public const LEVEL_EMERGENCY = 0;
    public const LEVEL_ALERT = 1;
    public const LEVEL_CRITICAL = 2;
    public const LEVEL_ERROR = 3;
    public const LEVEL_WARNING = 4;
    public const LEVEL_NOTICE = 5;
    public const LEVEL_INFORMATIONAL = 6;
    public const LEVEL_DEBUG = 7;

    public const FACILITY_USER = 1;

    public function emergency(int $facility, string $message, array $context = []): void;
    public function alert(int $facility, string $message, array $context = []): void;
    public function critical(int $facility, string $message, array $context = []): void;
    public function error(int $facility, string $message, array $context = []): void;
    public function warning(int $facility, string $message, array $context = []): void;
    public function notice(int $facility, string $message, array $context = []): void;
    public function info(int $facility, string $message, array $context = []): void;
    public function debug(int $facility, string $message, array $context = []): void;
    public function log(int $facility, int $level, string $message, array $context = []): void;
}
