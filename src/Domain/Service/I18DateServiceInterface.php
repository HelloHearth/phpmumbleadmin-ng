<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface I18DateServiceInterface
{
    /**
     * Format date for a Log
     */
    public function logDate(int $timestamp): string;

    /**
     * Format time for a Log
     */
    public function logTime(int $timestamp): string;
}
