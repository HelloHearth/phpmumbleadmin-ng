<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\HandlerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface BusServiceInterface
{
    public function __construct(HandlerInterface $handler);
    public function addMiddleWare(BusMiddlewareInterface $middleware): void;
    public function handle($action): BusResponse;
}
