<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Common\StopServer;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class StopServerHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return StopServerCommand::class;
    }

    public function handle(StopServerCommand $command): BusResponse
    {
        $prx = $command->prx;

        if (! $prx->isRunning()) {
            return new BusResponse([new ServerAlreadyStopped($command->serverId)]);
        }

        if (! empty($prx->getUsers())) {
            return new BusResponse([new ConfirmationRequired($command->serverId)]);
        }

        $prx->stop();
        $prx->setConf('boot', 'false');

        return new BusResponse([new ServerStopped($command->serverId)]);
    }
}
