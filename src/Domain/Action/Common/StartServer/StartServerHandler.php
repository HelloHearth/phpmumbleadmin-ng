<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Common\StartServer;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class StartServerHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return StartServerCommand::class;
    }

    public function handle(StartServerCommand $command): BusResponse
    {
        $prx = $command->prx;

        if (!$prx->isRunning()) {
            $prx->start();
            $prx->setConf('boot', '');
            return new BusResponse([new ServerStarted($prx->getSid())]);
        }

        return new BusResponse();
    }
}
