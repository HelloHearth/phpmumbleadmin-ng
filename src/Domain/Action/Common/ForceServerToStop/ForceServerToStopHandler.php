<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Common\ForceServerToStop;

use App\Domain\Action\Server\ServerIsNotBooted;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Helper\HtmlHelper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ForceServerToStopHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return ForceServerToStopCommand::class;
    }

    public function handle(ForceServerToStopCommand $command): BusResponse
    {
        $prx = $command->prx;
        $message = $command->message;
        $messageHasBeenStripped = false;

        if (! $prx->isRunning()) {
            return new BusResponse([new ServerIsNotBooted($prx->getSid())]);
        }

        $totalOfUsers = count($prx->getUsers());

        if (strlen($message) > 0) {
            if (
                !$command->adminHasFullAccess
                && $prx->getParameter('allowhtml') !== 'true'
            ) {
                $message = strip_tags($message);
                $messageHasBeenStripped = true;
            }
            $message = HtmlHelper::URLtoHTML($message);
            $prx->sendMessageChannel(0, true, $message);
        }

        if ($command->kickUsers) {
            $prx->kickAllUsers();
        }

        $prx->stop();
        $prx->setConf('boot', 'false');

        $event = new ServerForcedToStop($prx->getSid(), $command->reason, $totalOfUsers, $messageHasBeenStripped);
        return new BusResponse([$event]);
    }
}
