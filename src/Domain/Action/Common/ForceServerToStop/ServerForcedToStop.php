<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Common\ForceServerToStop;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ServerForcedToStop extends AbstractEvent
{
    public const KEY = 'server_stopped_by_force';

    public int $serverId;
    public string $reason;
    public int $totalOfUsers;
    public bool $messageHasBeenStripped;

    public function __construct(
        int $serverId,
        string $reason,
        int $totalOfUsers,
        bool $messageHasBeenStripped
    ) {
        $this->serverId = $serverId;
        $this->reason = $reason;
        $this->totalOfUsers = $totalOfUsers;
        $this->messageHasBeenStripped = $messageHasBeenStripped;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
