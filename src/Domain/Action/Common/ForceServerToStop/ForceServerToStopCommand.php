<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Common\ForceServerToStop;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ForceServerToStopCommand
{
    use ServerActionTrait;

    public bool $kickUsers;
    public string $message;
    public string $reason;
    public bool $adminHasFullAccess;

    public function __construct(
        int $serverId,
        bool $kickUsers,
        string $message,
        string $reason,
        bool $adminHasFullAccess
    ) {
        $this->serverId = $serverId;
        $this->kickUsers = $kickUsers;
        $this->message = $message;
        $this->reason = $reason;
        $this->adminHasFullAccess = $adminHasFullAccess;
    }
}
