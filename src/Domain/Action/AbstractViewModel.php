<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractViewModel
{
    private $hasError;
    private $errorMessage;
    private $translationDomain;
    /** @var mixed */
    private $debug;

    public function __construct()
    {
        $this->hasError = false;
        $this->debug = '';
    }

    public function getHasError(): bool
    {
        return $this->hasError;
    }

    public function getErrorMessage(): ?array
    {
        return ['key' => $this->errorMessage, 'translationDomain' => $this->translationDomain];
    }

    public function setError(string $errorMessage, string $translationDomain = null): void
    {
        $this->hasError = true;
        $this->errorMessage = $errorMessage;
        $this->translationDomain = $translationDomain;
    }

    /** @return mixed */
    public function getDebug()
    {
        return $this->debug;
    }

    public function setDebug($debug): void
    {
        $this->debug = $debug;
    }
}
