<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\DeleteServer;

use App\Domain\Bus\EventHandler;
use App\Domain\Service\LoggerServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LogOnServerDeleted implements EventHandler
{
    private LoggerServiceInterface $logger;

    public function __construct(LoggerServiceInterface $logger)
    {
        $this->logger = $logger;
    }

    public function listenTo(): string
    {
        return ServerDeleted::class;
    }

    public function handle(ServerDeleted $event): void
    {
        $this->logger->info(LoggerServiceInterface::FACILITY_USER, $event->getKey(), ['%id%' => $event->serverId]);
    }
}
