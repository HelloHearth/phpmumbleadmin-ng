<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\DeleteServer;

use App\Domain\Bus\EventHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RemoveAdminAccessOnServerDeleted implements EventHandler
{
    public function listenTo(): string
    {
        return DeleteServerCommand::class;
    }

    /**
     * In the futur
     * Remove admin access to deleted server on ServerDeleted
     */
    public function handle(DeleteServerCommand $command)
    {
//        $PMA->admins = new PMA_datas_admins();
//        $PMA->admins->deleteServerIdsAccess($pid, $sid);
    }
}
