<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\DeleteServer;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteServerHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return DeleteServerCommand::class;
    }

    public function handle(DeleteServerCommand $command): BusResponse
    {
        $prx = $command->prx;

        // You can't delete a running server, so stop it first.
        if ($prx->isRunning()) {
            $prx->kickAllUsers();
            $prx->stop();
        }
        $prx->delete();

        return new BusResponse([new ServerDeleted($prx->getSid())]);
    }
}
