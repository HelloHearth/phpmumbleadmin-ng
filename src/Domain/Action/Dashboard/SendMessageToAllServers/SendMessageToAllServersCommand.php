<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\SendMessageToAllServers;

use App\Domain\Murmur\Model\ConnectionActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SendMessageToAllServersCommand
{
    use ConnectionActionTrait;

    public string $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }
}
