<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\SendMessageToAllServers;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Helper\HtmlHelper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SendMessageToAllServersHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return SendMessageToAllServersCommand::class;
    }

    public function handle(SendMessageToAllServersCommand $command): BusResponse
    {
        $Murmur = $command->Murmur;
        $message = HtmlHelper::URLtoHTML($command->message);
        $stripped = HtmlHelper::URLtoHTML(strip_tags($command->message));

        foreach ($Murmur->getBootedServers() as $prx) {
            // Check admin access.
//            if ($PMA->user->is(PMA_USER_ADMIN)) {
//                if (! $PMA->user->checkServerAccess($prx->getSid())) {
//                    continue;
//                }
//            }
          // Use stripped HTML messages.
//            if ($PMA->user->is(PMA_USERS_ADMINS)) {
//                $prx->sendMessageChannel(0, true, $stripped);
//            }

            $prx->sendMessageChannel(0, true, $message);
        }

        return new BusResponse([new MessageSent()]);
    }
}
