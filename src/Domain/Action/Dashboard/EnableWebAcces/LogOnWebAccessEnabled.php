<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\EnableWebAcces;

use App\Domain\Bus\EventHandler;
use App\Domain\Service\LoggerServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LogOnWebAccessEnabled implements EventHandler
{
    private LoggerServiceInterface $logger;

    public function __construct(LoggerServiceInterface $logger)
    {
        $this->logger = $logger;
    }

    public function listenTo(): string
    {
        return WebAccessEnabled::class;
    }

    public function handle(WebAccessEnabled $event): void
    {
        $this->logger->info(LoggerServiceInterface::FACILITY_USER, $event->getKey(), ['%id%' => $event->serverId]);
    }
}
