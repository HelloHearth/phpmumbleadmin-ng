<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\DisableWebAccess;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class WebAccessDisabled extends AbstractEvent
{
    public const KEY = 'web_access_disabled';

    public int $serverId;

    public function __construct(int $serverId)
    {
        $this->serverId = $serverId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
