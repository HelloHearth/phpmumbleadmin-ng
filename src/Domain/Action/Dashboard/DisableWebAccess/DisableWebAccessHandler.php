<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\DisableWebAccess;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DisableWebAccessHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return DisableWebAccessCommand::class;
    }

    public function handle(DisableWebAccessCommand $command): BusResponse
    {
        $prx = $command->prx;
        $prx->setConf('PMA_permitConnection', '');
        return new BusResponse([new WebAccessDisabled($prx->getSid())]);
    }
}
