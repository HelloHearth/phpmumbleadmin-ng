<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\Dashboard;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Helper\IpHelper;
use App\Domain\Helper\PaginatorHelper;
use App\Domain\Murmur\Service\MurmurConnectionUriServiceInterface;
use App\Domain\Service\ClockInterface;
use App\Domain\Service\PaginatorServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DashboardHandler implements QueryHandler
{
    // Constant for the max page parameter of the paginator.
    public const LIMIT_PER_PAGE = 10;

    private PaginatorServiceInterface $paginator;
    private MurmurConnectionUriServiceInterface $connectionUriService;
    private ClockInterface $clock;

    public function __construct(
        PaginatorServiceInterface $paginator,
        MurmurConnectionUriServiceInterface $connectionUriService,
        ClockInterface $clock
    ) {
        $this->paginator = $paginator;
        $this->connectionUriService = $connectionUriService;
        $this->clock = $clock;
    }

    public function listenTo(): string
    {
        return DashboardQuery::class;
    }

    public function handle(DashboardQuery $query): BusResponse
    {
        $Murmur = $query->Murmur;

        $view = new DashboardView();
        $timestamp = $this->clock->timestamp();

        $view->murmur_version = $Murmur->getMurmurVersion()['str'];
        $view->murmurStartedAt = $timestamp - $Murmur->getUptime();

        $servers = $Murmur->getAllServers();
        $bootedServers = $Murmur->getBootedServers();
        $serversData = [];

        // Add only key and isBooted values for sorting with the pagination.
        foreach ($servers as $key => $prx) {

            ++$view->total_of_servers;

            $data = new ServerData();

            $data->key = $key;
            $data->isBooted = \in_array($prx, $bootedServers);

            if ($data->isBooted) {
                ++$view->total_of_booted_servers;
                $data->users = \count($prx->getUsers());
                $view->total_of_connected_users += $data->users;
            }

            $serversData[] = $data;
        }

        $page = PaginatorHelper::getValidPage($view->total_of_servers, $query->page, self::LIMIT_PER_PAGE);

        $view->servers = $this->paginator->paginate($serversData, $page, self::LIMIT_PER_PAGE);

        /**
         * Get the rest of data after pagination, to limit access to Server proxies
         *
         * @var ServerData $data
         */
        foreach ($view->servers->getItems() as $data) {

            $prx = $servers[$data->key];

            $data->id = $prx->getSid();
            $data->serverName  = $prx->getParameter('registername');

            $data->host  = $prx->getParameter('host');
            if (IpHelper::isIPv6($data->host)) {
                $data->host = '['.$data->host.']';
            }
            $data->port = $prx->getParameter('port');

            if ($data->isBooted) {
                $data->startedAt = $timestamp - $prx->getUptime();
                $data->usersOnline = $data->users.' / '.$prx->getParameter('users');

                $data->url  = $this->connectionUriService->constructUri(
                    $query->userLogin,
                    $prx->getParameter('password'),
                    $data->host,
                    $data->port,
                    $Murmur->getMurmurVersion()['str']
                );
            }

            $data->webAccessEnabled = ($prx->getParameter('PMA_permitConnection') === 'true');
        }

        return (new BusResponse())->setData($view);
    }
}
