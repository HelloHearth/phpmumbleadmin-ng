<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\Dashboard;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerData
{
    public int $id;
    public int $key;
    public bool $isBooted;
    public string $serverName;
    public string $host;
    public string $port;
    public bool $webAccessEnabled;
    public int $startedAt;
    public string $url;
    public int $users;
    public string $usersOnline;
}
