<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\Dashboard;

use App\Domain\Murmur\Model\ConnectionActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DashboardQuery
{
    use ConnectionActionTrait;

    public string $userLogin;
    public int $page;

    public function __construct(string $userLogin, int $page)
    {
        $this->userLogin = $userLogin;
        $this->page = $page;
    }
}
