<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\CreateServer;

use App\Domain\Bus\EventHandler;
use App\Domain\Murmur\Service\ServersListServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class InvalidateServersListCacheOnServerCreated implements EventHandler
{
    private ServersListServiceInterface $serversList;

    public function __construct(ServersListServiceInterface $serversList)
    {
        $this->serversList = $serversList;
    }

    public function listenTo(): string
    {
        return ServerCreated::class;
    }

    public function handle(): void
    {
        $this->serversList->invalidateCache();
    }
}
