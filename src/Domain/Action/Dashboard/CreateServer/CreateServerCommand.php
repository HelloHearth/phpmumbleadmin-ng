<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\CreateServer;

use App\Domain\Murmur\Model\ConnectionActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateServerCommand
{
    use ConnectionActionTrait;

    public ?string $registername;
    public ?string $password;
    public ?int $users;

    public function __construct(?string $registername, ?string $password, ?int $users)
    {
        $this->registername = $registername;
        $this->password = $password;
        $this->users = $users;
    }
}
