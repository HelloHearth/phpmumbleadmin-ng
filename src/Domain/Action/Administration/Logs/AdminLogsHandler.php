<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Logs;

use App\Domain\Model\Log;
use App\Domain\Murmur\Helper\LogsHelper;
use App\Domain\Service\I18DateServiceInterface;
use App\Domain\Service\TranslatorServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminLogsHandler
{
    private I18DateServiceInterface $i18Date;
    private TranslatorServiceInterface $translator;

    public function __construct(I18DateServiceInterface $i18Date, TranslatorServiceInterface $translator)
    {
        $this->i18Date = $i18Date;
        $this->translator = $translator;
    }

    public function handle(AdminLogsQuery $query): AdminLogsView
    {
        $logsBag = [];
        foreach ($query->logs as $key => $adminLog) {

            $message = $this->translator->trans($adminLog->getMessage(), $adminLog->getContext(), 'Log');

            $log = new Log($key, $adminLog->getTimestamp(), $message);
            $log->setTime($this->i18Date->logTime($adminLog->getTimestamp()));

            // Set the day if it's the first log, or if the day has changed
            if (! isset($lastLog) || LogsHelper::dayHasChanged($lastLog->timestamp, $log->timestamp)) {
                $log->setDate($this->i18Date->logDate($log->timestamp));
            }

            $logsBag[] = $lastLog = $log;
        }

        $data = new AdminLogsView();
        $data->setJsonLogs(\json_encode(['logs' => $logsBag]));

        return $data;
    }
}
