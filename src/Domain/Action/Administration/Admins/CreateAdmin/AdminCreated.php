<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\CreateAdmin;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminCreated extends AbstractEvent
{
    public const KEY = 'admin_created';

    public int $userId;
    public string $username;
    public string $role;

    public function __construct(int $userId, string $username, string $role)
    {
        $this->userId = $userId;
        $this->username = $username;
        $this->role = $role;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
