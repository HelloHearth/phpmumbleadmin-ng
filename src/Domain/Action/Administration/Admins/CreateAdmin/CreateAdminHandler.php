<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\CreateAdmin;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Exception\NoPermissionToModifyUserException;
use App\Domain\Manager\AdminManagerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateAdminHandler implements CommandHandler
{
    private AdminManagerInterface $adminManager;

    public function __construct(AdminManagerInterface $adminManager)
    {
        $this->adminManager = $adminManager;
    }

    public function listenTo(): string
    {
        return CreateAdminCommand::class;
    }

    /**
     * @throws NoPermissionToModifyUserException
     */
    public function handle(CreateAdminCommand $command): BusResponse
    {
        $admin = $this->adminManager->add($command);

        return new BusResponse([
            new AdminCreated($admin->getId(), $command->login, $command->role)
        ]);
    }
}
