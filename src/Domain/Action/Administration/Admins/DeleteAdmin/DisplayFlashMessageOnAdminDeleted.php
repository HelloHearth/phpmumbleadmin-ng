<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\DeleteAdmin;

use App\Domain\Bus\EventHandler;
use App\Domain\Service\FlashMessageServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DisplayFlashMessageOnAdminDeleted implements EventHandler
{
    private FlashMessageServiceInterface $flashMessage;

    public function __construct(FlashMessageServiceInterface $flashMessage)
    {
        $this->flashMessage = $flashMessage;
    }

    public function listenTo(): string
    {
        return AdminDeleted::class;
    }

    public function handle(AdminDeleted $event): void
    {
        $this->flashMessage->success($event->getKey(), ['username' => $event->username, 'role' => $event->role]);
    }
}
