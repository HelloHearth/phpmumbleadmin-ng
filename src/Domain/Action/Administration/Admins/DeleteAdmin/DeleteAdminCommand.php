<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\DeleteAdmin;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteAdminCommand
{
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
