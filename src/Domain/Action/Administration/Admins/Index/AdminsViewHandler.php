<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\Index;

use App\Domain\Service\Role\RoleNameServiceInterface;
use App\Domain\Manager\AdminManagerInterface;
use App\Domain\Action\Administration\Admins\Admin;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminsViewHandler
{
    private AdminManagerInterface $adminManager;
    private RoleNameServiceInterface $roleName;

    public function __construct(AdminManagerInterface $adminManager, RoleNameServiceInterface $roleName)
    {
        $this->adminManager = $adminManager;
        $this->roleName = $roleName;
    }

    public function createView(): AdminsViewModel
    {
        $admins = $this->adminManager->findAll();
        $view = new AdminsViewModel();

        foreach ($admins as $admin) {
            $roleName = $this->roleName->find($admin);
            $view->addAdmin(new Admin($admin->getId(), $roleName, $admin->getUsername()));
        }

        return $view;
    }
}
