<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\Index;

use App\Domain\Action\AbstractViewModel;
use App\Domain\Action\Administration\Admins\Admin;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminsViewModel extends AbstractViewModel
{
    /** @var Admin[] */
    private array $admins;

    public function __construct()
    {
        parent::__construct();
        $this->admins = [];
    }

    /** @return Admin[] */
    public function getAdmins(): array
    {
        return $this->admins;
    }

    public function addAdmin(Admin $admins): void
    {
        $this->admins[] = $admins;
    }
}
