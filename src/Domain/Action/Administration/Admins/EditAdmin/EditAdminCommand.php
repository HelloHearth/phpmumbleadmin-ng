<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\EditAdmin;

use App\Domain\Model\AdminInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditAdminCommand
{
    public AdminInterface $admin;
    public string $login;
    public ?string $email;
    public ?string $role;
    public ?string $password;

    public function __construct(AdminInterface $admin, string $login, ?string $email, string $role, ?string $password)
    {
        $this->admin = $admin;
        $this->login = $login;
        $this->email = $email;
        $this->role = $role;
        $this->password = $password;
    }
}
