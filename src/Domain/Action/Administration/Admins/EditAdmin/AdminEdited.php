<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\EditAdmin;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminEdited extends AbstractEvent
{
    public const KEY = 'admin_edited';

    public int $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
