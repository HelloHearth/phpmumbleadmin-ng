<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UnknownError extends AbstractEvent
{
    public const KEY = 'unknown_error';

    public \Exception $exception;

    public function __construct(\Exception $exception)
    {
        $this->exception = $exception;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
