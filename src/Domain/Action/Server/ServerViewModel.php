<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server;

use App\Domain\Model\InfoPanelInterface;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Action\Common\MurmurConnectionViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerViewModel extends MurmurConnectionViewModel
{
    private $serverId;
    private $serverName;
    private $isRunning;
    private $requireIsRunning;
    private $connectionUri;
    private $infoPanel;

    public function __construct(int $serverId)
    {
        $this->serverId = $serverId;

        parent::__construct();
        $this->isRunning = false;
        $this->requireIsRunning = true;
        $this->infoPanel = [];
    }

    public function setServerData(ServerInterface $server): void
    {
        $this->serverName = $server->getServerName();
        $this->isRunning = $server->isRunning();
        $this->setHasConnection();
    }

    public function getServerId(): int
    {
        return $this->serverId;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function isRunning(): bool
    {
        return $this->isRunning;
    }

    public function requireIsRunning(): bool
    {
        return $this->requireIsRunning;
    }

    public function setRequireIsRunning(bool $requireIsRunning): void
    {
        $this->requireIsRunning = $requireIsRunning;
    }

    public function getConnectionUri(): ?string
    {
        return $this->connectionUri;
    }

    public function setConnectionUri(string $connectionUri): void
    {
        $this->connectionUri = $connectionUri;
    }

    public function getInfoPanel(): array
    {
        return $this->infoPanel;
    }

    public function addInfoPanel(InfoPanelInterface $infoPanel): void
    {
        $this->infoPanel[] = $infoPanel;
    }
}
