<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\DeleteChannel;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteChannelHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return DeleteChannelCommand::class;
    }

    public function handle(DeleteChannelCommand $command): BusResponse
    {
        if (0 === $command->channelId) {
            return new BusResponse([new DeleteRootChannelIsForbidden()]);
        }

        $prx = $command->prx;
        $prx->removeChannel($command->channelId);

        // Remove defaultChannel parameter if we had deleted the default channel.
        if ($prx->getParameter('defaultchannel') === (string) $command->channelId) {
            $prx->setConf('defaultchannel' , '');
        }

        return new BusResponse([new ChannelDeleted()]);
    }
}
