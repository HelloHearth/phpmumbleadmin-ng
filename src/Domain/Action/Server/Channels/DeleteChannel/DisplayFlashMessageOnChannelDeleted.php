<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\DeleteChannel;

use App\Domain\Bus\EventHandler;
use App\Domain\Service\FlashMessageServiceInterface;
use App\Infrastructure\Service\RequestService;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DisplayFlashMessageOnChannelDeleted implements EventHandler
{
    private FlashMessageServiceInterface $flashMessage;
    private RequestService $requestService;

    public function __construct(FlashMessageServiceInterface $flashMessage, RequestService $requestService)
    {
        $this->flashMessage = $flashMessage;
        $this->requestService = $requestService;
    }

    public function listenTo(): string
    {
        return ChannelDeleted::class;
    }

    public function handle(ChannelDeleted $event): void
    {
        if ($this->requestService->isHttpRequest()) {
            $this->flashMessage->success($event->getKey());
        }
    }
}
