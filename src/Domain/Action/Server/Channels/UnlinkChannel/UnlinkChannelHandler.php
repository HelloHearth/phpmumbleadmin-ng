<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\UnlinkChannel;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UnlinkChannelHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return UnlinkChannelCommand::class;
    }

    public function handle(UnlinkChannelCommand $command): BusResponse
    {
        if ($command->channelId === $command->unlinkId) {
            $event = new CannotUnlinkSelf($command->serverId, $command->channelId, $command->unlinkId);
            return new BusResponse([$event]);
        }

        $prx = $command->prx;

        $channelState = $prx->getChannelState($command->channelId);

        if (! \in_array($command->unlinkId, $channelState->links, true)) {
            $event = new ChannelIsNotLinked($command->serverId, $command->channelId, $command->unlinkId);
            return new BusResponse([$event]);
        }

        foreach ($channelState->links as $key => $channelId) {
            if ($channelId === $command->unlinkId) {
                unset($channelState->links[$key]);
                $prx->setChannelState($channelState);
                break;
            }
        }

        $event = new ChannelUnlinked($command->serverId, $command->channelId, $command->unlinkId);
        return new BusResponse([$event]);
    }
}
