<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelNameIsInvalid extends AbstractEvent
{
    public const KEY = 'invalid_character_for_channel_name';

    public int $serverId;
    public ?int $channelId;
    public string $name;

    public function __construct(int $serverId, ?int $channelId, string $name)
    {
        $this->serverId = $serverId;
        $this->channelId = $channelId;
        $this->name = $name;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
