<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\SendMessageToChannel;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MessageSentToChannel extends AbstractEvent
{
    public const KEY = 'message_sent_to_channel';

    public int $serverId;
    public int $channelId;
    public bool $toSubChannel;
    public bool $stripped;

    public function __construct(int $serverId, int $channelId, bool $toSubChannel, bool $stripped)
    {
        $this->serverId = $serverId;
        $this->channelId = $channelId;
        $this->toSubChannel = $toSubChannel;
        $this->stripped = $stripped;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    public function getTranslationDomain(): string
    {
        return 'sendMessageToChannel';
    }

    public function getTranslationParameters(): array
    {
        return ['%count%' => $this->toSubChannel ? 1 : 0];
    }
}
