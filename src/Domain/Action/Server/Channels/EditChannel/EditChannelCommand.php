<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\EditChannel;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditChannelCommand
{
    use ServerActionTrait;

    public int $channelId;

    public bool $default;
    public string $name;
    public string $description;
    public string $position;
    public string $password;

    public function __construct(
        int $serverId,
        int $channelId,
        bool $default,
        string $name,
        string $description,
        string $position,
        string $password
    ) {
        $this->serverId = $serverId;
        $this->channelId = $channelId;
        $this->default = $default;
        $this->name = $name;
        $this->description = $description;
        $this->position = $position;
        $this->password = $password;
    }
}
