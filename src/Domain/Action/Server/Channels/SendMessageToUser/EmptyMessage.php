<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EmptyMessage extends AbstractEvent
{
    public const KEY = 'empty_message';

    public int $serverId;
    public int $userSessionId;

    public function __construct(int $serverId, int $userSessionId)
    {
        $this->serverId = $serverId;
        $this->userSessionId = $userSessionId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
