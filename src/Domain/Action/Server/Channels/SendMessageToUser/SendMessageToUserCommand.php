<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SendMessageToUserCommand
{
    use ServerActionTrait;

    public int $userSessionId;
    public string $message;

    public function __construct(int $serverId, int $userSessionId, string $message)
    {
        $this->serverId = $serverId;
        $this->userSessionId = $userSessionId;
        $this->message = $message;
    }
}
