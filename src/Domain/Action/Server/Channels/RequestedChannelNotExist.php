<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class RequestedChannelNotExist extends AbstractEvent
{
    public const KEY = 'requested_channel_not_exist';

    public int $serverId;
    public int $channelId;

    public function __construct(int $serverId, int $channelId)
    {
        $this->serverId = $serverId;
        $this->channelId = $channelId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
