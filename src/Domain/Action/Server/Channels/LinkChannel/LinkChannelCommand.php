<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\LinkChannel;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LinkChannelCommand
{
    use ServerActionTrait;

    public int $channelId;
    public int $withChannelId;

    public function __construct(int $serverId)
    {
        $this->serverId = $serverId;
    }
}
