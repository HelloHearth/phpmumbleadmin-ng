<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\LinkChannel;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CannotLinkSelf extends AbstractEvent
{
    public const KEY = 'cannot_link_self';

    public int $serverId;
    public int $channelId;
    public int $linkId;

    public function __construct(int $serverId, int $channelId, int $linkId)
    {
        $this->serverId = $serverId;
        $this->channelId = $channelId;
        $this->linkId = $linkId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
