<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\MoveUser;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MoveUserHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return MoveUserCommand::class;
    }

    public function handle(MoveUserCommand $command): BusResponse
    {
        $prx = $command->prx;

        $userState = $prx->getState($command->sessionId);

        if ($userState->channel === $command->toChannelId) {
            return new BusResponse([new UserAlreadyInChannel()]);
        }

        $userState->channel = $command->toChannelId;
        $prx->setState($userState);

        return new BusResponse([new UserMoved()]);
    }
}
