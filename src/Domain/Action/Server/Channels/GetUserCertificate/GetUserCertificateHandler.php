<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\GetUserCertificate;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Helper\CommonHelper;
use App\Domain\Manager\CertificateManager;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class GetUserCertificateHandler implements QueryHandler
{
    public function listenTo(): string
    {
        return GetUserCertificateQuery::class;
    }

    public function handle(GetUserCertificateQuery $query): BusResponse
    {
        $prx = $query->prx;

        $userCertificateList = $prx->getCertificateList($query->sessionId);

        if (empty($userCertificateList)) {
            return new BusResponse();
        }

        $certificate = CommonHelper::decimalArrayToChars($userCertificateList[0]);
        $certificate = CommonHelper::der2pem($certificate);

        $certificateManager = new CertificateManager($certificate);
        $parsed = $certificateManager->parse();

        return (new BusResponse())->setData($parsed);
    }
}
