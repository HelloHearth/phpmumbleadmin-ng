<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\ActivatePrioritySpeaker;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ActivatePrioritySpeakerHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return ActivatePrioritySpeakerCommand::class;
    }

    public function handle(ActivatePrioritySpeakerCommand $command): BusResponse
    {
        $prx = $command->prx;

        $userState = $prx->getState($command->userSession);

        if ($userState->prioritySpeaker) {
            $event = new PrioritySpeakerAlreadyActive($command->serverId, $command->userSession);
            return new BusResponse([$event]);
        }

        $userState->prioritySpeaker = true;
        $prx->setState($userState);

        $event = new PrioritySpeakerActivated($command->serverId, $command->userSession);
        return new BusResponse([$event]);
    }
}
