<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\DeactivatePrioritySpeaker;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeactivatePrioritySpeakerHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return DeactivatePrioritySpeakerCommand::class;
    }

    public function handle(DeactivatePrioritySpeakerCommand $command): BusResponse
    {
        $prx = $command->prx;

        $userState = $prx->getState($command->userSession);

        if (! $userState->prioritySpeaker) {
            $event = new PrioritySpeakerNotActive($command->serverId, $command->userSession);
            return new BusResponse([$event]);
        }

        $userState->prioritySpeaker = false;
        $prx->setState($userState);

        $event = new PrioritySpeakerDeactivated($command->serverId, $command->userSession);
        return new BusResponse([$event]);
    }
}
