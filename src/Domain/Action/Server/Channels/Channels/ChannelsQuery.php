<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\Channels;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelsQuery
{
    use ServerActionTrait;

    public ChannelsViewModel $viewModel;

    public function __construct(ChannelsViewModel $viewModel, int $serverId)
    {
        $this->viewModel = $viewModel;
        $this->serverId = $serverId;
    }
}
