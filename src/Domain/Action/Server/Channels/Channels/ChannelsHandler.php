<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\Channels;

use App\Domain\Action\Server\ServerIsNotBooted;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Murmur\Middleware\ConnectionError;
use App\Domain\Murmur\Viewer\ViewerBuilder;
use App\Domain\Service\SecurityServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelsHandler implements QueryHandler
{
    private SecurityServiceInterface $security;

    public function __construct(SecurityServiceInterface $security)
    {
        $this->security = $security;
    }

    public function listenTo(): string
    {
        return ChannelsQuery::class;
    }

    public function handle(ChannelsQuery $query): BusResponse
    {
        $query->viewModel->setServerData($query->prx);

        if (! $query->viewModel->isRunning()) {
            return new BusResponse([new ServerIsNotBooted($query->prx->getSid())]);
        }

        $viewerBuilder = new ViewerBuilder($query->prx);

        try {
            $jsonViewer = $viewerBuilder->getJson();
        } catch (\Ice_MemoryLimitException | \Ice\MemoryLimitException $e) {
            return new BusResponse([new ConnectionError(\get_class($e))]);
        }

        $query->viewModel->setJsonViewer($jsonViewer);
        $isGranted = $this->security->canModifyMumbleUsernameOnChannelTree($query->Murmur);
        $query->viewModel->setIsGrantedToModifyUserName($isGranted);

        return new BusResponse([new ChannelsPageRequested()]);
    }
}
