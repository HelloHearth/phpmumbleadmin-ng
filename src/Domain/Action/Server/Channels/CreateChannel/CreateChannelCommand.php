<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\CreateChannel;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateChannelCommand
{
    use ServerActionTrait;

    public string $name;
    public int $parentId;

    public function __construct(int $serverId, string $name, int $parentId)
    {
        $this->serverId = $serverId;
        $this->name = $name;
        $this->parentId = $parentId;
    }
}
