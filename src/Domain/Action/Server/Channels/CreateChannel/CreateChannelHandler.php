<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\CreateChannel;

use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Domain\Action\Server\Channels\NestingLimitReached;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Murmur\Exception\Server\InvalidChannelException;
use App\Domain\Murmur\Exception\Server\NestingLimitException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateChannelHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return CreateChannelCommand::class;
    }

    public function handle(CreateChannelCommand $command): BusResponse
    {
        $prx = $command->prx;

        try {
            $parent = $prx->getChannelState($command->parentId);
        } catch (InvalidChannelException $e) {
            return new BusResponse([new ParentChannelIsInvalid()]);
        }

        // Don't add a sub channel if parent is a temporary channel.
        // This is not how the Mumble client works, so do so.
        if ($parent->temporary) {
            return new BusResponse([new ParentChannelIsTemporary()]);
        }

        if (! $prx->validateChannelChars($command->name)) {
            return new BusResponse([new ChannelNameIsInvalid($command->serverId, null, $command->name)]);
        }

        try {
            $createdId = $prx->addChannel($command->name, $command->parentId);
        } catch (NestingLimitException $e) {
            return new BusResponse([new NestingLimitReached()]);
        }

        return new BusResponse([new ChannelCreated($createdId)]);
    }
}
