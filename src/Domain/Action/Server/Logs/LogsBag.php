<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Logs;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogsBag
{
    public int $serverId;
    public array $logs;

    public function __construct(int $serverId, array $logs)
    {
        $this->serverId = $serverId;
        $this->logs = $logs;
    }
}
