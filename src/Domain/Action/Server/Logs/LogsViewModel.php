<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Logs;

use App\Domain\Action\Server\ServerViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogsViewModel extends ServerViewModel
{
    private string $logsBag;
    private int $totalOfLogs;

    public function __construct(int $serverId)
    {
        parent::__construct($serverId);

        $this->logsBag = '';
    }

    public function getLogsBag(): string
    {
        return $this->logsBag;
    }

    public function setLogsBag(string $logsBag): void
    {
        $this->logsBag = $logsBag;
    }

    public function getTotalOfLogs(): int
    {
        return $this->totalOfLogs;
    }

    public function setTotalOfLogs(int $totalOfLogs): void
    {
        $this->totalOfLogs = $totalOfLogs;
    }
}
