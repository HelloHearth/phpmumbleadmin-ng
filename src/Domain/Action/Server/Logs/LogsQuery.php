<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Logs;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LogsQuery
{
    use ServerActionTrait;

    public LogsViewModel $viewModel;
    public int $lastLogLen;

    public function __construct(LogsViewModel $viewModel, int $serverId, int $lastLogLen)
    {
        $this->viewModel = $viewModel;
        $this->serverId = $serverId;
        $this->lastLogLen = $lastLogLen;
    }
}
