<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Logs;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Service\ClockInterface;
use App\Domain\Service\I18DateServiceInterface;
use App\Domain\Model\Log;
use App\Domain\Murmur\Exception\Ice\MemoryLimitException;
use App\Domain\Murmur\Exception\Server\InvalidSecretException;
use App\Domain\Murmur\Helper\LogsHelper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LogsHandler implements QueryHandler
{
    private I18DateServiceInterface $i18Date;
    private ClockInterface $clock;

    public function __construct(I18DateServiceInterface $i18Date, ClockInterface $clock)
    {
        $this->i18Date = $i18Date;
        $this->clock = $clock;
    }

    public function listenTo(): string
    {
        return LogsQuery::class;
    }

    public function handle(LogsQuery $query): BusResponse
    {
        $server = $query->prx;

        $query->viewModel->setRequireIsRunning(false);
        $query->viewModel->setServerData($server);

        /*
         * Get murmur log length.
         *
         * The method have a bug with murmur 1.2.3,
         * it's require iceSecretWrite parameter instead of the iceSecretRead.
         * It's fixed with murmur 1.2.4.
         * Catch the InvalidSecretException here to set $logLen with a negative value.
         */
        try {
            $logLen = $server->getLogLen();
            $lastLogEntry = LogsHelper::getLastLogEntry($logLen, $query->lastLogLen);
        } catch (InvalidSecretException $e) {
            $logLen = $lastLogEntry = -1;
        }

        try {
            $logEntries = $server->getLog(0, $lastLogEntry);
        } catch (MemoryLimitException $e) {
            // 100 logs should be enough to avoid the MemoryLimit Exception
            $logEntries = $server->getLog(0, 100);
        }

        $total = \count($logEntries);
        $query->viewModel->addInfoPanel(new TotalLogsInfoPanel($total));

        // Apply log timestamp workaround
        $diff = LogsHelper::timestampDiffWorkaround($this->clock->getTimezoneDiff());

        $logs = [];
        foreach ($logEntries as $key => $logEntry) {

            $log = new Log($key, $logEntry->timestamp + $diff, $logEntry->txt);
            $log->setTime($this->i18Date->logTime($log->timestamp));

            // Set the day if it's the first log, or if the day has changed
            if (! isset($lastLog) || LogsHelper::dayHasChanged($lastLog->timestamp, $log->timestamp)) {
                $log->setDate($this->i18Date->logDate($log->timestamp));
            }

            $logs[] = $lastLog = $log;
        }

        $logsBag = new LogsBag($query->serverId, $logs);
        $logsBag = \json_encode($logsBag);

        $query->viewModel->setTotalOfLogs($logLen);
        $query->viewModel->setLogsBag($logsBag);

        return new BusResponse([new LogsRequested()]);
    }
}
