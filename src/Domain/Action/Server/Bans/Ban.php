<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Ban
{
    public int $id;
    public string $ip;
    public ?int $mask;
    public string $name;
    public string $hash;
    public string $reason;
    public int $start;
    public int $duration;
    public int $end;

    public function __construct(int $id, string $ip, ?int $mask, string $name, string $hash, string $reason, int $start, int $duration)
    {
        $this->id = $id;
        $this->ip = $ip;
        $this->mask = $mask;
        $this->name = $name;
        $this->hash = $hash;
        $this->reason = $reason;
        $this->start = $start;
        $this->duration = $duration;
        $this->end = ($duration !== 0) ? $start + $duration : 0;
    }
}
