<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans;

use App\Domain\Model\InfoPanelInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TotalBansInfoPanel implements InfoPanelInterface
{
    private int $total;

    public function __construct(int $total)
    {
        $this->total = $total;
    }

    public function getArguments(): array
    {
        return ['%total%' => $this->total];
    }

    public function getText(): string
    {
        return 'bans_total';
    }
}
