<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\DeleteBan;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteBanHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return DeleteBanCommand::class;
    }

    public function handle(DeleteBanCommand $command): BusResponse
    {
        $prx = $command->prx;

        $bansList = $prx->getBans();
        unset($bansList[$command->banId]);
        $prx->setBans($bansList);

        $event = new BanDeleted($command->serverId,$command->banId);
        return new BusResponse([$event]);
    }
}
