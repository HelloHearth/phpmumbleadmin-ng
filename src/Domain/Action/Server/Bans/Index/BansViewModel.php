<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\Index;

use App\Domain\Action\Server\Bans\Ban;
use App\Domain\Action\Server\ServerViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BansViewModel extends ServerViewModel
{
    /** @var Ban[] */
    private array $bans;

    public function __construct(int $serverId)
    {
        parent::__construct($serverId);
        $this->bans = [];
    }

    public function getBans(): array
    {
        return $this->bans;
    }

    public function setBans(array $bans): void
    {
        foreach ($bans as $ban) {
            if (! $ban instanceof Ban) {
                throw new \InvalidArgumentException();
            }
        }
        $this->bans = $bans;
    }
}
