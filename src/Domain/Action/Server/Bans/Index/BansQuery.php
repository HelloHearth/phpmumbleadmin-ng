<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\Index;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BansQuery
{
    use ServerActionTrait;

    public BansViewModel $viewModel;

    public function __construct(int $serverId, BansViewModel $viewModel)
    {
        $this->serverId = $serverId;
        $this->viewModel = $viewModel;
    }
}
