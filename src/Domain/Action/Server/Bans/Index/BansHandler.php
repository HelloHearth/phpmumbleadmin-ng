<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\Index;

use App\Domain\Action\Server\ServerIsNotBooted;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Helper\IpHelper;
use App\Domain\Action\Server\Bans\Ban;
use App\Domain\Action\Server\Bans\TotalBansInfoPanel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BansHandler implements QueryHandler
{
    public function listenTo(): string
    {
        return BansQuery::class;
    }

    public function handle(BansQuery $query): BusResponse
    {
        $prx = $query->prx;

        $query->viewModel->setRequireIsRunning(true);
        $query->viewModel->setServerData($prx);

        if (! $prx->isRunning()) {
            return new BusResponse([new ServerIsNotBooted($prx->getSid())]);
        }

        $murmurBans = $prx->getBans();

        $total = \count($murmurBans);
        $query->viewModel->addInfoPanel(new TotalBansInfoPanel($total));

        $bans = [];
        foreach ($murmurBans as $key => $murmurBan) {

            $id = (int) $key;
            $address = IpHelper::decimalTostring($murmurBan->address);
            $ip = $address['ip'];

            // A 128 bits mask mean ip only, no need to show the mask
            $mask = null;
            if ($murmurBan->bits !== 128) {
                if ('ipv4' === $address['type']) {
                    $mask = IpHelper::mask6To4($murmurBan->bits);
                } else {
                    $mask = $murmurBan->bits;
                }
            }

            $bans[] = new Ban($id, $ip, $mask, $murmurBan->name, $murmurBan->hash, $murmurBan->reason, $murmurBan->start, $murmurBan->duration);
        }

        $query->viewModel->setBans($bans);

        return new BusResponse([]);
    }
}
