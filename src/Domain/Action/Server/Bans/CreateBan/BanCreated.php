<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\CreateBan;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BanCreated extends AbstractEvent
{
    public const KEY = 'ban_created';

    public int $serverId;
    public string $login;
    public string $reason;

    public function __construct(int $serverId, string $login, string $reason)
    {
        $this->serverId = $serverId;
        $this->login = $login;
        $this->reason = $reason;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
