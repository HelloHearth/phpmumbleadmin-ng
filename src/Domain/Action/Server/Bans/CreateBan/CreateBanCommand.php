<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\CreateBan;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateBanCommand
{
    use ServerActionTrait;

    public string $address;
    public string $name = '';
    public string $hash = '';
    public string $reason = '';
    public bool $permanent = true;
    public ?\DateTimeImmutable $endDate = null;


    public function address(string $address): self
    {
        $this->address = $address;
        return $this;
    }

    public function name(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function hash(string $hash): self
    {
        $this->hash = $hash;
        return $this;
    }

    public function reason(string $reason): self
    {
        $this->reason = $reason;
        return $this;
    }

    public function permanent(bool $permanent): self
    {
        $this->permanent = $permanent;
        return $this;
    }

    public function endDate(?\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;
        return $this;
    }
}
