<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\CreateBan;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Helper\IpHelper;
use App\Domain\Model\IpAddress;
use App\Domain\Murmur\Factory\MurmurObjectFactory;
use App\Domain\Service\ClockInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateBanHandler implements CommandHandler
{
    private ClockInterface $clock;

    public function __construct(ClockInterface $clock)
    {
        $this->clock = $clock;
    }

    public function listenTo(): string
    {
        return CreateBanCommand::class;
    }

    public function handle(CreateBanCommand $command): BusResponse
    {
        $prx = $command->prx;

        $ban = MurmurObjectFactory::createBan();

        $ipAddress = IpAddress::fromString($command->address);

        if ($ipAddress->isIpv4()) {
            $ban->address = IpHelper::stringToDecimalIPv4($ipAddress->address);
            $ban->bits = IpHelper::mask4To6($ipAddress->bitmask);
        } else {
            $ban->address = IpHelper::stringToDecimalIPv6($ipAddress->address);
            $ban->bits = $ipAddress->bitmask;
        }

        $ban->name = $command->name;
        $ban->hash = $command->hash;
        $ban->reason = $command->reason;
        $ban->start = $this->clock->timestamp();

        if ($command->permanent || \is_null($command->endDate)) {
            // Permanent ban
            $ban->duration = 0;
        } else {
            $ban->duration = $command->endDate->getTimestamp() - $this->clock->timestamp();
        }

        $bansList = $prx->getBans();
        $bansList[] = $ban;
        $prx->setBans($bansList);

        return new BusResponse([new BanCreated($command->serverId, $command->name, $command->reason)]);
    }
}
