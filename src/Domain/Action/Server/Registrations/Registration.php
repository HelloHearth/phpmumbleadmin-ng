<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Registration
{
    public $id;
    public $username;
    public $email;
    public $description;
    public $certificate;
    public $lastActivity;
    public $status;
    public $plainPassword;

    public function __construct(int $id, array $registration)
    {
        $this->id = $id;
        $this->username = $registration[0] ?? '';
        $this->email = $registration[1] ?? '';
        $this->description = $registration[2] ?? '';
        $this->certificate = $registration[3] ?? '';
        $this->lastActivity = $registration[5] ?? '';
        $this->status = false;
    }
}
