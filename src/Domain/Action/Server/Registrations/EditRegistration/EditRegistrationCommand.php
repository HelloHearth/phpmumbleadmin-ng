<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditRegistrationCommand
{
    use ServerActionTrait;

    public int $registrationId;
    public array $originalData;
    public ?string $username;
    public ?string $email;
    public ?string $description;
    public ?string $plainPassword;

    public function __construct(int $serverId, int $registrationId, array $originalData, ?string $username, ?string $email, ?string $description, ?string $plainPassword)
    {
        $this->serverId = $serverId;
        $this->registrationId = $registrationId;
        $this->originalData = $originalData;
        $this->username = $username ?? '';
        $this->email = $email ?? '';
        $this->description = $description ?? '';
        $this->plainPassword = $plainPassword ?? '';
    }
}
