<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UsernameAlreadyExist extends AbstractEvent
{
    public const KEY = 'username_already_exist';

    public int $serverId;
    public string $username;

    public function __construct(int $serverId, string $username)
    {
        $this->serverId = $serverId;
        $this->username = $username;
    }

    public function getTranslationDomain(): string
    {
        return 'validators';
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
