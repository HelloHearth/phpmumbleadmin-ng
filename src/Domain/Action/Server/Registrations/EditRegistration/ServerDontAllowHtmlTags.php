<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ServerDontAllowHtmlTags extends AbstractEvent
{
    public const KEY = 'server_dont_allow_html_tags';

    public int $serverId;
    public int $registrationId;
    public string $description;

    public function __construct(int $serverId, int $registrationId, string $description)
    {
        $this->serverId = $serverId;
        $this->registrationId = $registrationId;
        $this->description = $description;
    }

    public function getTranslationDomain(): string
    {
        return 'validators';
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
