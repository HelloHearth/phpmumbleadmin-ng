<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations;

/**
 * Tranform a Registration into a UserInfo array
 * @see http://mumble.sourceforge.net/slice/1.2.7/Murmur/UserInfo.html
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationToUserInfoTransformer
{
    public static function tranform(Registration $registration): array
    {
        return [
            0 => $registration->username,
            1 => $registration->email,
            2 => $registration->description
        ];
    }
}
