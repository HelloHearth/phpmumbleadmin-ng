<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\DeleteRegistration;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteRegistrationHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return DeleteRegistrationCommand::class;
    }

    public function handle(DeleteRegistrationCommand $command): BusResponse
    {
        $prx = $command->prx;

        if (0 === $command->registrationId) {
            $event = new SuperUserCannotBeDeleted($command->serverId,$command->registrationId);
            return new BusResponse([$event]);
        }

        $prx->unregisterUser($command->registrationId);

        $event = new RegistrationDeleted($command->serverId,$command->registrationId);
        return new BusResponse([$event]);
    }
}
