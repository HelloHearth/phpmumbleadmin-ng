<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\Registration;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class RegistrationQuery
{
    use ServerActionTrait;

    public $registrationId;
    public RegistrationViewModel $viewModel;

    public function __construct(int $serverId, int $registrationId, RegistrationViewModel $viewModel)
    {
        $this->serverId = $serverId;
        $this->registrationId = $registrationId;
        $this->viewModel = $viewModel;
    }
}
