<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\Registration;

use App\Domain\Action\Server\ServerIsNotBooted;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Murmur\Exception\Server\InvalidUserException;
use App\Domain\Murmur\Helper\RegistrationsHelper;
use App\Domain\Murmur\Helper\TextureHelper;
use App\Domain\Action\Server\Registrations\Registration;
use App\Domain\Action\Server\Registrations\TotalRegistrationsInfoPanel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class RegistrationHandler implements QueryHandler
{
    private const TRANSLATION_DOMAIN = 'registration';

    private SecurityServiceInterface $security;

    public function __construct(SecurityServiceInterface $security)
    {
        $this->security  = $security;
    }

    public function listenTo(): string
    {
        return RegistrationQuery::class;
    }

    public function handle(RegistrationQuery $query): BusResponse
    {
        $prx = $query->prx;

        $query->viewModel->setRequireIsRunning(true);
        $query->viewModel->setServerData($prx);

        if (! $prx->isRunning()) {
            return new BusResponse([new ServerIsNotBooted($prx->getSid())]);
        }

        $registrationsList = $prx->getRegisteredUsers('');
        $total = \count($registrationsList);
        $query->viewModel->addInfoPanel(new TotalRegistrationsInfoPanel($total));

        if (! $this->security->canAccessToRegistration($query->registrationId)) {
            $event = new AccessToRegistrationIsNotAllowed();
            $query->viewModel->setError($event->getKey(), self::TRANSLATION_DOMAIN);
            return new BusResponse([$event]);
        }

        try {
            $registration = $prx->getRegistration($query->registrationId);
            $texture = $prx->getTexture($query->registrationId);
        } catch (InvalidUserException $e) {
            $event = new RegistrationDoNotExist($query->serverId, $query->registrationId);
            $query->viewModel->setError($event->getKey(), self::TRANSLATION_DOMAIN);
            return new BusResponse([$event]);
        }

        $registration = new Registration($query->registrationId, $registration);
        $onlineUsers = $prx->getUsers();
        $registration->status = RegistrationsHelper::isOnline($query->registrationId, $onlineUsers);

        $query->viewModel->setRegistration($registration);

        if (! empty($texture)) {
            $query->viewModel->setAvatar(TextureHelper::textureToImage($texture));
        }

        return new BusResponse([]);
    }
}
