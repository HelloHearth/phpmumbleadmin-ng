<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\Registration;

use App\Domain\Action\Server\Registrations\Registration;
use App\Domain\Action\Server\ServerViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationViewModel extends ServerViewModel
{
    private $registration;
    private $avatar;

    public function __construct(int $serverId)
    {
        parent::__construct($serverId);
    }

    public function getRegistration(): ?Registration
    {
        return $this->registration;
    }

    public function setRegistration(Registration $registration): void
    {
        $this->registration = $registration;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): void
    {
        $this->avatar = $avatar;
    }
}
