<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\Index;

use App\Domain\Action\Server\ServerIsNotBooted;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Murmur\Helper\RegistrationsHelper;
use App\Domain\Action\Server\Registrations\Registration;
use App\Domain\Action\Server\Registrations\TotalRegistrationsInfoPanel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class RegistrationsHandler implements QueryHandler
{
    private SecurityServiceInterface $security;

    public function __construct(SecurityServiceInterface $security)
    {
        $this->security  = $security;
    }

    public function listenTo(): string
    {
        return RegistrationsQuery::class;
    }

    public function handle(RegistrationsQuery $query): BusResponse
    {
        $prx = $query->prx;

        $query->viewModel->setRequireIsRunning(true);
        $query->viewModel->setServerData($prx);

        if (! $prx->isRunning()) {
            return new BusResponse([new ServerIsNotBooted($prx->getSid())]);
        }

        $registrationsList = $prx->getRegisteredUsers('');
        $onlineUsers = $prx->getUsers();

        if (! $this->security->canAccessToRegistration(0)) {
            unset($registrationsList[0]);
        }

        $total = \count($registrationsList);
        $query->viewModel->addInfoPanel(new TotalRegistrationsInfoPanel($total));

        $registrations = [];
        foreach ($registrationsList as $uid => $username) {

            $registration = new Registration($uid, $prx->getRegistration($uid));
            $registration->status = RegistrationsHelper::isOnline($uid, $onlineUsers);
            if (0 === $uid && 'superuser' !== strtolower($username)) {
                $registration->username .= ' (SuperUser)';
            }
            $registrations[] = $registration;
        }

        $query->viewModel->setRegistrations($registrations);

        return new BusResponse([]);
    }
}
