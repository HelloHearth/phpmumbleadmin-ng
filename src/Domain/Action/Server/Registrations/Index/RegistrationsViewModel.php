<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\Index;

use App\Domain\Action\Server\Registrations\Registration;
use App\Domain\Action\Server\ServerViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsViewModel extends ServerViewModel
{
    /** @var Registration[] */
    private array $registrations;

    public function __construct(int $serverId)
    {
        parent::__construct($serverId);
        $this->registrations = [];
    }

    public function getRegistrations(): array
    {
        return $this->registrations;
    }

    public function setRegistrations(array $registrations): void
    {
        foreach ($registrations as $registration) {
            if (! $registration instanceof Registration) {
                throw new \InvalidArgumentException();
            }
        }
        $this->registrations = $registrations;
    }
}
