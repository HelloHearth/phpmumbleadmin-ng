<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Settings;

use App\Domain\Action\Server\ServerViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SettingsViewModel extends ServerViewModel
{
    private $certificate;

    public function __construct(int $serverId)
    {
        parent::__construct($serverId);
        $this->certificate = [];
    }

    public function getCertificate(): array
    {
        return $this->certificate;
    }

    public function setCertificate(array $certificate): void
    {
        $this->certificate = $certificate;
    }
}
