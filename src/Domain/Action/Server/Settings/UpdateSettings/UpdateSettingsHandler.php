<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Settings\UpdateSettings;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UpdateSettingsHandler implements CommandHandler
{
    private UpdateSettingsService $settingsManager;

    public function __construct(UpdateSettingsService $settingsService)
    {
        $this->settingsManager = $settingsService;
    }

    public function listenTo(): string
    {
        return UpdateSettingsCommand::class;
    }

    public function handle(UpdateSettingsCommand $command): BusResponse
    {
        $this->settingsManager->setDefaultSettingValues($command->Murmur->getDefaultConf());
        $this->settingsManager->setServer($command->prx);
        $this->settingsManager->saveSettingsOfServer($command->settings);

        return new BusResponse([new SettingsUpdated()]);
    }
}
