<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Settings\ResetCertificate;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ResetCertificateHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return ResetCertificateCommand::class;
    }

    public function handle(ResetCertificateCommand $command): BusResponse
    {
        $server = $command->prx;

        $server->setConf('key', '');
        $server->setConf('certificate', '');

        return new BusResponse([new CertificateReset()]);
    }
}
