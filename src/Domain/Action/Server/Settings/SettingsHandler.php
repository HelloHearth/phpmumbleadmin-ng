<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Settings;

use App\Domain\Action\Server\Settings\UpdateSettings\UpdateSettingsService;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Manager\CertificateManager;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SettingsHandler implements QueryHandler
{
    private UpdateSettingsService $fieldManager;

    public function __construct(UpdateSettingsService $settingsService)
    {
        $this->fieldManager = $settingsService;
    }

    public function listenTo(): string
    {
        return SettingsQuery::class;
    }

    public function handle(SettingsQuery $query): BusResponse
    {
        $prx = $query->prx;

        $query->viewModel->setRequireIsRunning(false);
        $query->viewModel->setServerData($prx);

        // Get default configuration, apply Murmur default port workaround
        $defaultConfig = $query->Murmur->getDefaultConf();
        $defaultConfig['port'] += $prx->getSid() - 1;

        $this->fieldManager->setServer($prx);
        $this->fieldManager->setDefaultSettingValues($defaultConfig);

        $settings = $this->fieldManager->getSettings();
        $pem = $settings['key']->getValue() . $settings['certificate']->getValue();
        unset($settings['key'], $settings['certificate']);

        $certificate = new CertificateManager($pem);
        $query->viewModel->setCertificate($certificate->parse());

        $query->serverSettings = $settings;
        $query->channelNamePattern = $prx->getParameter('channelname');

        return new BusResponse([]);
    }
}
