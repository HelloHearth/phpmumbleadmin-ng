<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Manager;

use App\Domain\Exception\NoPermissionToModifyUserException;
use App\Domain\Model\AdminInterface;
use App\Domain\Action\Administration\Admins\CreateAdmin\CreateAdminCommand;
use App\Domain\Action\Administration\Admins\EditAdmin\EditAdminCommand;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface AdminManagerInterface
{
    /**
     * @return AdminInterface[]
     */
    public function findAll(): array;

    /**
     * @throws NoPermissionToModifyUserException
     */
    public function find(int $id): ?AdminInterface;

    /**
     * @throws NoPermissionToModifyUserException
     */
    public function add(CreateAdminCommand $command): AdminInterface;

    /**
     * @throws NoPermissionToModifyUserException
     */
    public function edit(EditAdminCommand $command): AdminInterface;

    /**
     * @throws NoPermissionToModifyUserException
     */
    public function delete(AdminInterface $admin): void;
}
