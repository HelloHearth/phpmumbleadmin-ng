<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Manager;

/**
 * @final do not override the constructor
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CertificateManager
{
    public const MESSAGE_OPENSSL_MODULE_IS_MISSING = 'openssl_module_is_missing';

    private string $pem;
    private string $privateKey;
    private string $certificate;

    public function __construct(string $pem)
    {
        // Real immutable object
        if (isset($this->pem)) {
            throw new \BadMethodCallException('Constructor called twice.');
        }

        $this->pem = $this->addLastEol($pem);
    }

    private function error(string $message): array
    {
        return ['error' => $message];
    }

    public function parse(): array
    {
        if (! \extension_loaded('openssl')) {
            return $this->error(self::MESSAGE_OPENSSL_MODULE_IS_MISSING);
        }

        if ('' === $this->pem) {
            return [];
        }

        $parse = \openssl_x509_parse($this->pem, false);

        if (! \is_array($parse)) {
            return $this->error('invalid certificate');
        }

        // remove duplicate/useless entries.
        unset($parse['purposes'], $parse['validFrom'], $parse['validTo']);

        // Move orphans values into the array key "OTHER".
        foreach ($parse as $key => $value) {
            if (! \is_array($value)) {
                $parse['others'][$key] = $value;
                unset($parse[$key]);
            }
        }

        $certificate = [];

        // Add titles and values.
        foreach ($parse as $title => $array) {
            \ksort($array);
            foreach ($array as $parameter => $value) {
                if ($parameter === 'validFrom_time_t') {
                    $parameter = 'Valid from';
                    $value = (new \DateTime())->setTimestamp($value);
                } elseif ($parameter === 'validTo_time_t') {
                    $parameter = 'Valid to';
                    $value = (new \DateTime())->setTimestamp($value);
                }
                $certificate[$title][$parameter] = $value;
            }
        }

        return $certificate;
    }

    public function getPrivateKey(): string
    {
        if (isset($this->privateKey)) {
            return $this->privateKey;
        }

        $this->privateKey = \str_replace($this->getCertificate(), '', $this->pem);

        /*
         * Check that the private key is a valid private key
         *
         * openssl_pkey_export() :
         * openssl-php need a valid "openssl.cnf" installed for this function
         * to operate correctly.
         */
        if (! @\openssl_pkey_export($this->privateKey, $output)) {
            return $this->privateKey = '';
        }

        return $this->privateKey;
    }

    public function getCertificate(): string
    {
        if (isset($this->certificate)) {
            return $this->certificate;
        }

        @\openssl_x509_export($this->pem, $certificate);

        return $this->certificate = $certificate ?? '';
    }

    public function verifyCertificateWithPrivateKey(): bool
    {
        return \openssl_x509_check_private_key($this->getCertificate(), $this->getPrivateKey());
    }

    private function addLastEol(string $pem): string
    {
        if (empty($pem)) {
            return $pem;
        }

        if ($pem[-1] !== PHP_EOL) {
            return $pem.PHP_EOL;
        }

        return $pem;
    }
}
