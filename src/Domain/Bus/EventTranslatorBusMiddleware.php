<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Bus;

use App\Domain\Service\BusMiddlewareInterface;
use App\Domain\Service\TranslatorServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EventTranslatorBusMiddleware implements BusMiddlewareInterface
{
    private TranslatorServiceInterface $translator;

    public function __construct(TranslatorServiceInterface $translator)
    {
        $this->translator = $translator;
    }

    public function execute($action, callable $next): BusResponse
    {
        $response = $next($action);

        foreach ($response->getEvents() as $event) {
            $this->translateEvent($event);
        }

        return $response;
    }

    private function translateEvent(Event $event): void
    {
        $message = $this->translator->trans($event->getKey(), $event->getTranslationParameters(), $event->getTranslationDomain());
        $event->setMessage($message);
    }
}
