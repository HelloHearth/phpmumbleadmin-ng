<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Bus;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EventBus
{
    /** @var EventHandler[] */
    private array $handlers = [];

    public function __construct(iterable $handlers)
    {
        foreach ($handlers as $handler) {
            $this->handlers[] = $handler;
        }
    }

    public function dispatch(Event $event): void
    {
        $eventClass = \get_class($event);

        $matchingHandlers = \array_filter(
            $this->handlers,
            function($handler) use ($eventClass) {
                return $handler->listenTo() === $eventClass;
            });

        foreach ($matchingHandlers as $handler) {
            $handler->handle($event);
        }
    }
}
