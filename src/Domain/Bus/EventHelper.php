<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Bus;

use App\Domain\Action\UnknownError;
use App\Domain\Action\Server\ServerNotFound;
use App\Domain\Murmur\Middleware\ConnectionError;
use App\Domain\Murmur\Middleware\ServerActionError;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EventHelper
{
    public static function getStatusCode(BusResponse $busResponse, array $customEvents = []): int
    {
        // Priority to custom events
        foreach ($customEvents as $className => $statusCode) {
            if ($busResponse->hasEvent($className)) {
                return $statusCode;
            }
        }

        // Common events
        switch (true) {
            case $busResponse->hasEvent(UnknownError::class);
                return 500;
            case $busResponse->hasEvent(ConnectionError::class);
                return 503;
            case $busResponse->hasEvent(ServerActionError::class);
                return 400;
            case $busResponse->hasEvent(ServerNotFound::class);
                return 404;
            default:
                return 200;
        }
    }
}
