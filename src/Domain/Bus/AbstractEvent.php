<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Bus;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractEvent implements Event
{
    private string $message;

    public function getTranslationDomain(): string
    {
        return 'event';
    }

    public function getTranslationParameters(): array
    {
        return [];
    }

    public function getMessage(): string
    {
        return $this->message ?? $this->getKey();
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }
}
