<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Bus;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BusResponse
{
    private array $events;
    private $data;

    public function __construct(array $events = [])
    {
        foreach ($events as $event) {
            if (! $event instanceof Event) {
                throw new \TypeError('Event class is required');
            }
        }

        $this->events = $events;
    }

    public function hasEvents(): bool
    {
        return (! empty($this->events));
    }

    public function hasEvent(string $className): bool
    {
        foreach ($this->events as $event) {
            if ($className === \get_class($event)) {
                return true;
            }
        }
        return false;
    }

    /** @return Event[] */
    public function getEvents(): array
    {
        return $this->events;
    }

    public function getEvent(string $className): ?Event
    {
        foreach ($this->events as $event) {
            if ($className === \get_class($event)) {
                return $event;
            }
        }
        return null;
    }

    /** @return mixed */
    public function getData()
    {
        return $this->data;
    }

    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }
}
