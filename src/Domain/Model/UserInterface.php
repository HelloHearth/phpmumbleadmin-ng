<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface UserInterface
{
    public function getUsername(): string;
    public function setUsername(string $username): void;

    public function getEmail(): ?string;
    public function setEmail(?string $email): void;

    public function getRoles(): array;
    public function setRoles(array $roles): void;
}
