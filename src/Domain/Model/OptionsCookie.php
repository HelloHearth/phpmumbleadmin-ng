<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Model;

/**
 * Parameters object of the user configuration cookie
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class OptionsCookie implements \Serializable
{
    private $locale;

    public function serialize(): string
    {
        return \json_encode([$this->locale]);
    }

    public function unserialize($data): void
    {
        [$this->locale] = \json_decode($data);
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }
}
