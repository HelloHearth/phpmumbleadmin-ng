<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Viewer;

use App\Domain\Helper\CommonHelper;
use App\Domain\Helper\DateHelper;
use App\Domain\Helper\IpHelper;
use App\Domain\Manager\CertificateManager;
use App\Domain\Murmur\Manager\AclManager;
use App\Domain\Murmur\Model\ServerInterface;

/**
 * Build a json on the base of the mumble tree
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ViewerBuilder
{
    use CompareTrait;

    /**
     * Tree and status ids
     */
    private const SPACE = 'space';

    private const ICON_ROOT_CHANNEL = 'icon_root_channel';
    private const ICON_CHANNEL = 'icon_channel';
    private const ICON_CHANNEL_WITH_LINK = 'icon_channel_with_link';
    private const ICON_CHANNEL_WITH_DIRECT_LINK = 'icon_channel_with_direct_link';
    private const ICON_CHANNEL_WITH_INDIRECT_LINK = 'icon_channel_with_indirect_link';
    private const ICON_USER = 'icon_user';

    private const TREE_LINE = 'tree_line';
    private const TREE_MID = 'tree_mid';
    private const TREE_LAST = 'tree_last';

    private const STATUS_HAS_COMMENT = 'status_has_comment';

    private const STATUS_CHANNEL_IS_DEFAULT = 'status_chanel_is_default';
    private const STATUS_CHANNEL_IS_TEMP = 'status_chan_temp';
    private const STATUS_CHANNEL_HAS_PASSWORD = 'status_chan_has_password';

    private const STATUS_USER_IS_AUTH = 'status_user_auth';
    private const STATUS_USER_RECORDING = 'status_user_recording';
    private const STATUS_USER_WITH_PRIORITY_SPEAKER = 'status_user_with_priority_speaker';
    private const STATUS_USER_SUPPRESSED = 'status_user_suppressed';
    private const STATUS_USER_MUTED = 'status_user_muted';
    private const STATUS_USER_DEAFENED = 'status_user_deafened';
    private const STATUS_USER_SELF_MUTED = 'status_user_selfmute';
    private const STATUS_USER_SELF_DEAFENED = 'status_user_selfdeaf';

    /*
     * Deep menu bag
     */
    private array $deepMenu = [];

    /*
     * Tree to array bag
     */
    private array $treeArray = [];

    /*
     * Bag of direct links.
     */
    private array $directLinks = [];

    /*
     * Bag of indirect links.
     */
    private array $indirectLinks = [];

    /*
     * Server proxy instance
     */
    private ServerInterface $prx;

    /*
     * Server name
     */
    private ?string $serverName;

    /*
     * Server default channel id
     */
    private int $defaultChannelId;


    public function __construct(ServerInterface $prx)
    {
        $this->prx = $prx;
    }

    /*
     * Core of the class.
     * Transform a Murmur\Tree object into Viewer object.
     */
    public function createViewer(): Viewer
    {
        $this->serverName = $this->prx->getParameter('registername');
        $this->defaultChannelId = (int) $this->prx->getParameter('defaultchannel');

        $this->channel($this->prx->getTree(), 0, true);

        return new Viewer($this->prx->getSid(), $this->treeArray);
    }

    public function getJson(): string
    {
        return \json_encode($this->createViewer());
    }

    /*
    * Channels to array...
    */
    protected function channel($MurmurTree, $deep, $isLastChannel): void
    {
        $Node = get_object_vars($MurmurTree->c);
        $Node['class'] = 'channel';
        $Node['nodeid'] = 'c-'.$MurmurTree->c->id;
        $Node['isDefaultChannel'] = false;
        $Node['token'] = '';
        $Node['deepBag'] = [];
        $Node['icon'] = '';
        $Node['statusBag'] = [];
        unset($Node['children'], $Node['users']);

        /**
         * Deep
         */
        if ($MurmurTree->c->id > 0) {
            $Node['deepBag'] = $this->getDeepBag($deep, $isLastChannel);
            $Node['deepBag'][] = $isLastChannel ? self::TREE_LAST : self::TREE_MID;
        }

        /**
         * Channel icon
         */
        if (in_array($MurmurTree->c->id, $this->directLinks, true)) {
            $Node['icon'] = self::ICON_CHANNEL_WITH_DIRECT_LINK;
        } elseif (in_array($MurmurTree->c->id, $this->indirectLinks, true)) {
            $Node['icon'] = self::ICON_CHANNEL_WITH_INDIRECT_LINK;
        } elseif ($MurmurTree->c->id === 0) {
            $Node['icon'] = self::ICON_ROOT_CHANNEL;
        } elseif (! empty($MurmurTree->c->links)) {
            $Node['icon'] = self::ICON_CHANNEL_WITH_LINK;
        } else {
            $Node['icon'] = self::ICON_CHANNEL;
        }

        // Add indirectLink to channel
        $Node['indirectLinks'] = $this->getIndirectLinks($this->prx->getChannels(), $MurmurTree->c->links);

        /**
         * Root channel name
         */
        if ($MurmurTree->c->id === 0 && $this->serverName !== '') {
            $Node['name'] = $this->serverName;
        }

        /**
         * Channel status
         */
        $this->prx->getACL($MurmurTree->c->id, $aclList, $groupList, $inherit);

        $aclManager = new AclManager([], [], false);

        foreach ($aclList as $acl) {
            if ($aclManager->isToken($acl)) {
                $Node['statusBag'][] = self::STATUS_CHANNEL_HAS_PASSWORD;
                $Node['token'] = substr($acl->group, 1);
                break;
            }
        }

        if ($MurmurTree->c->id === $this->defaultChannelId) {
            $Node['statusBag'][] = self::STATUS_CHANNEL_IS_DEFAULT;
            $Node['isDefaultChannel'] = true;
        }
        if ($MurmurTree->c->description !== '') {
            $Node['statusBag'][] = self::STATUS_HAS_COMMENT;
        }
        if ($MurmurTree->c->temporary) {
            $Node['statusBag'][] = self::STATUS_CHANNEL_IS_TEMP;
        }

        $this->treeArray[] = $Node;


        /**
         * Go next channel (or user)
         */

        $countUsers = count($MurmurTree->users);
        $countChans = count($MurmurTree->children);

        if ($countUsers > 0) {

            usort($MurmurTree->users, 'self::usersCmp');
            $lastSessionId = $MurmurTree->users[$countUsers - 1]->session;
            // Check if a sub channel exists after the last user.
            $subChannelExist = ($countChans > 0);
            foreach ($MurmurTree->users as $user) {
                $isLastUser = ($lastSessionId === $user->session);
                $this->user($user, $deep +1, $isLastUser, $subChannelExist);
            }
        }

        if ($countChans > 0) {

            usort($MurmurTree->children, 'self::channelsCmp');
            $lastChannelId = $MurmurTree->children[$countChans -1]->c->id;
            foreach ($MurmurTree->children as $children) {
                $isLastChannel = ($lastChannelId === $children->c->id);
                $this->channel($children, $deep +1, $isLastChannel);
            }
        }
    }

    protected function user($user, $deep, $isLastUser, $subChannelExist): void
    {
        $Node = get_object_vars($user);
        $Node['class'] = 'user';
        $Node['nodeid'] = 'u-'.$user->session;
        $Node['deepBag'] = [];
        $Node['icon'] = '';
        $Node['statusBag'] = [];

        /*
         * Nicer user informations...
         */
        $Node['onlinesecs'] = DateHelper::uptime ($Node['onlinesecs']);
        $Node['idlesecs'] = DateHelper::uptime ($Node['idlesecs']);
        $Node['tcpPing'] = round($Node['tcpPing'], 2);
        $Node['udpPing'] = round($Node['udpPing'], 2);
        $Node['ip'] = IpHelper::decimalTostring($Node['address'])['ip'];

        $certificatesList = $this->prx->getCertificateList($user->session);
        if (! empty($certificatesList)) {
            $Node['hash'] = sha1(CommonHelper::decimalArrayToChars($certificatesList[0]));
        }

        /*
         * User deep tree
         */
        $Node['deepBag'] = $this->getDeepBag($deep, $isLastUser);
        $Node['deepBag'][] =
            ($isLastUser && !$subChannelExist) ? self::TREE_LAST : self::TREE_MID;
        $Node['icon'] = self::ICON_USER;

        /*
         * User name
         */

        if ($user->userid === 0 && strToLower($user->name) !== 'superuser') {
            $Node['name'] .= ' <i>(SuperUser)</i>';
        }

        /*
         * User status
         */
        if ($user->userid >= 0) {
            $Node['statusBag'][] = self::STATUS_USER_IS_AUTH;
        }
        if (isset($user->recording) && $user->recording) {
            $Node['statusBag'][] = self::STATUS_USER_RECORDING;
        }
        if (isset($user->prioritySpeaker) && $user->prioritySpeaker) {
            $Node['statusBag'][] = self::STATUS_USER_WITH_PRIORITY_SPEAKER;
        }
        if ($user->comment !== '') {
            $Node['statusBag'][] = self::STATUS_HAS_COMMENT;
        }
        if ($user->suppress) {
            $Node['statusBag'][] = self::STATUS_USER_SUPPRESSED;
        }
        if ($user->mute) {
            $Node['statusBag'][] = self::STATUS_USER_SELF_MUTED;
        }
        if ($user->deaf) {
            $Node['statusBag'][] = self::STATUS_USER_DEAFENED;
        }
        if ($user->selfMute) {
            $Node['statusBag'][] = self::STATUS_USER_SELF_MUTED;
        }
        if ($user->selfDeaf) {
            $Node['statusBag'][] = self::STATUS_USER_SELF_DEAFENED;
        }

        $this->treeArray[] = $Node;
    }

    /*
     * This code come from the work of mumbleviewer v0.91 ( GPL 2).
     * Website: http://sourceforge.net/projects/mumbleviewer/
     *
     * It's permit to create channels and users deep.
     *
     * @return array
     */
    protected function getDeepBag(int $deep, bool $isLastNode): array
    {
        $this->deepMenu[$deep] = $isLastNode ? 0 : 1;

        $count = 1;
        $array = array();
        while ($count < $deep) {
            $array[] =
                ($this->deepMenu[$count] === 0) ? self::SPACE : self::TREE_LINE;
            ++$count;
        }
        return $array;
    }

    /*
     * Setup direct and indirect channel links bags
     */
    protected function getSelectedChannelLinks($selectedChannelId, array $channelsList): void
    {
        if (!isset($channelsList[$selectedChannelId])) {
            return;
        }

        $chan = $channelsList[$selectedChannelId];

        /**
         * Memo: check for non empty directLinks to avoid to alway show current
         * channel as linked.
         */
        $this->directLinks = [];

        if (!empty($chan->links)) {
            foreach ($chan->links as $id) {
                $this->directLinks[] = $id;
            }
            // Add current channel as link
            $this->directLinks[] = $chan->id;
        }

        $this->getIndirectLinks($channelsList, $this->directLinks);
    }

    /**
     * Recursive function
     *
     * Search for all indirect links of an array of ids
     */
    protected function getIndirectLinks(array $channelsList, array $ids, array $indirectLinks = []): array
    {
        foreach ($ids as $id) {

            if (! isset($channelsList[$id])) {
                continue;
            }

            foreach ($channelsList[$id]->links as $linkId) {
                if (! \in_array($linkId, $ids, true) && ! \in_array($linkId, $indirectLinks, true)) {
                    $indirectLinks[] = $linkId;
                    $indirectLinks = $this->getIndirectLinks($channelsList, [$linkId], $indirectLinks);
                }
            }
        }

        return $indirectLinks;
    }
}
