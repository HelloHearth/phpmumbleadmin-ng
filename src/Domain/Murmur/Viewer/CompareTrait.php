<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Viewer;

/**
 * Compare methods for the icePHP tree object (channels and users).
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait CompareTrait
{
    protected function channelsCmp($a, $b)
    {
        if ($a->c->parent === $b->c->parent && $a->c->position !== $b->c->position) {
            return $a->c->position - $b->c->position;
        } else {
            return $this->qstringCompare($a->c->name, $b->c->name);
        }
    }

    protected function usersCmp($a, $b)
    {
        return $this->qstringCompare($a->name, $b->name);
    }

    /*
     * Ordering like the mumble client do.
     *
     * Mumble use QT QString::localeAwareCompare to sort channels and users.
     * With the php function strCaseCmp, a channel with same name but not
     * same case ("test", "TEST") can be inverted from time to time.
     *
     * Channel::lessThan:
     * https://github.com/mumble-voip/mumble/blob/master/src/Channel.cpp
     * User::lessThan:
     * https://github.com/mumble-voip/mumble/blob/master/src/User.cpp
     *
     * QT QString::localeAwareCompare:
     * https://doc.qt.io/archives/qt-4.8/qstring.html#localeAwareCompare
     */
    protected function qstringCompare($a, $b)
    {
        // QString sort in case insensitive ("strCaseCmp").
        $strCaseCmp = strCaseCmp($a, $b);

        // But on equal comparaison, switch to case sensitive...
        if (0 === $strCaseCmp) {
            // inverse the case order, to feet with QString
            // ("test" sorted before "TEST")
            return strCmp($b, $a);
        } else {
            return $strCaseCmp;
        }
    }
}
