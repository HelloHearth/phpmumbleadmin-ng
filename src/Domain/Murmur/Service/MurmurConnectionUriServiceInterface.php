<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Service;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface MurmurConnectionUriServiceInterface
{
    /**
     * Construct the mumble connection URI
     */
    public function constructUri(string $login, ?string $password, string $host, $port, ?string $version = null): string;
}
