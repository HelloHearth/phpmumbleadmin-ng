<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Middleware;

use App\Domain\Action\AbstractViewModel;
use App\Domain\Action\UnknownError;
use App\Domain\Action\Server\ServerNotFound;
use App\Domain\Bus\BusResponse;
use App\Domain\Murmur\Exception\Server\ServerException;
use App\Domain\Service\BusMiddlewareInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerBusMiddleware implements BusMiddlewareInterface
{
    public function execute($action, callable $next): BusResponse
    {
        try {
            $server = $action->Murmur->getServer($action->serverId);
            if (\is_null($server)) {
                $this->setViewModelError($action, 'server_not_found');
                return new BusResponse([new ServerNotFound($action->serverId)]);
            }
        } catch (\Exception $e) {
            $this->setViewModelError($action, 'unknown_error', 'Messages');
            $this->setViewModelDebug($action, $e);
            return new BusResponse([new UnknownError($e)]);
        }

        $action->prx = $server;

        try {
            return $next($action);
        } catch (ServerException $e) {
            return new BusResponse([new ServerActionError(get_class($e))], \get_class($e));
        }
    }

    private function setViewModelError(object $action, string $error, string $transDomain = 'server'): void
    {
        if (isset($action->viewModel) && $action->viewModel instanceof AbstractViewModel) {
            $action->viewModel->setError($error, $transDomain);
        }
    }

    private function setViewModelDebug(object $action, \Exception $e): void
    {
        if (isset($action->viewModel) && $action->viewModel instanceof AbstractViewModel) {
            $message = \get_class($e).' : '.$e->getMessage().PHP_EOL.PHP_EOL.' Trace: '.PHP_EOL.$e->getTraceAsString();
            $action->viewModel->setDebug($message);
        }
    }
}
