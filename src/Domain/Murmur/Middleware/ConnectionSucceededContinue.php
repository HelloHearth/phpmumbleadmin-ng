<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Middleware;

use App\Domain\Bus\AbstractEvent;

/**
 * Event to tell to the ServerMiddleware the connection has succeeded, and it
 * can continue.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ConnectionSucceededContinue extends AbstractEvent
{
    public const KEY = 'connection_succeeded_continue';

    public function getKey(): string
    {
        return self::KEY;
    }
}
