<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Connection;

use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\MetaInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface ConnectionInterface
{
    /**
     * Set connection parameters
     */
    public function setParameters(string $host, int $port, int $timeout, string $secret): void;

    /**
     * @throws ConnectionException
     */
    public function connection(): ?MetaInterface;
}
