<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Connection;

use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\MetaInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ConnectionNull implements ConnectionInterface
{
    private string $error;

    public function setError(string $error)
    {
        $this->error = $error;
    }

    /**
     * No op
     *
     * @inheritDoc
     */
    public function setParameters(string $host, int $port, int $timeout, string $secret): void { }

    /**
     * @inheritDoc
     */
    public function connection(): ?MetaInterface
    {
        throw new ConnectionException($this->error);
    }
}
