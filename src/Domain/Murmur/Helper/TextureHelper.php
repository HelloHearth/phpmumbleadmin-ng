<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Helper;

use App\Domain\Helper\CommonHelper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TextureHelper
{
    /*
     * Transform a Murmur texture into an image
     */
    public static function textureToImage(array $texture): string
    {
        $avatar = CommonHelper::decimalArrayToChars($texture);
        return 'data:image/png;base64, '.\base64_encode($avatar);
    }
}
