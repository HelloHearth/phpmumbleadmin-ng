<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerHelper
{
    /**
     * Return the server id from a proxy object.
     * Useful to avoid too many queries to the Murmur_server proxy with $proxy->id(),
     * and save web server resources.
     *
     * example:
     * $proxy::__toString return "s/1 -t:tcp -h 127.0.0.1 -p 6502"
     */
    public static function proxyToId(string $proxy): ?int
    {
        if (\substr($proxy, 0, 2) !== 's/') {
            return null;
        }

        [$serverId] = \explode(' ', $proxy);
        return (int) \substr($serverId, 2);
    }
}
