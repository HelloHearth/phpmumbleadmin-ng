<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogsHelper
{
    /*
     * Workaround for the timestamp bug with Murmur_Server::getLog() method:
     * getLog() return a modified timestamp if the OS time is not UTC (GTM+0).
     *
     * examples of getLog() with different timezones:
     * UTC+01 return a timestamp with -3600 seconds.
     * UTC+02 return a timestamp with -7200 seconds.
     * UTC-03 return a timestamp with +10800 seconds.
     * etc...
     *
     * This workaround have been tested for linux.
     * strftime('%z') return the time difference (+0100, +0200, -0300 etc...).
     *
     * On windows, it's definitely not working.
     * See http://php.net/manual/en/function.strftime.php
     */
    public static function timestampDiffWorkaround(string $diff): int
    {
        if (! \is_numeric($diff)) {
            return 0;
        }

        if (\substr($diff, -2) === '00') {
            $diff = \substr($diff, 0, -2);
        }

        return $diff * 3600;
    }

    /*
     * Check if the day has changed from the last timestamp
     */
    public static function dayHasChanged(int $lastTimestamp, int $timestamp): bool
    {
        [$lastDay, $lastMonth, $lastYear] = \explode('-', \date('d-m-Y', $lastTimestamp));
        [$day, $month, $year] = \explode('-', \date('d-m-Y', $timestamp));

        return (
            $day !== $lastDay
            OR $month !== $lastMonth
            OR $year !== $lastYear
        );
    }

    public static function getLastLogEntry(int $logLen, int $lastLogLen): int
    {
        if ($logLen < 0 || $lastLogLen < 0) {
            return -1;
        }

        return $logLen - $lastLogLen;
    }
}
