<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsHelper
{
    /*
     * Check if a registred user is online
     */
    public static function isOnline(int $id, array $onlineUsers): bool
    {
        foreach ($onlineUsers as $user) {
            if ($id === $user->userid) {
                return true;
            }
        }
        return false;
    }
}
