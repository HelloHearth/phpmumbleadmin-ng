<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Manager;

use App\Domain\Murmur\Factory\MurmurAclFactory;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AclManager
{
    private array $aclList;
    private array $groupList;
    private bool $inherit;

    public function __construct(array $aclList, array $groupList, bool $inherit)
    {
        $this->aclList = $this->removeInherited($aclList);
        $this->groupList = $this->removeInherited($groupList);
        $this->inherit = $inherit;
    }

    public function getAclList(): array
    {
        return $this->aclList;
    }

    public function getGroupList(): array
    {
        return $this->groupList;
    }

    public function getInherit(): bool
    {
        return $this->inherit;
    }

    /**
     * Remove inherited Acl and groups
     *
     * Do not add inherited Acl and groups as new item with
     * Murmur_server::setACL() method.
     */
    private function removeInherited(array $list): array
    {
        foreach ($list as $key => $acl) {
            if ($acl->inherited) {
                unset($list[$key]);
            }
        }

        return $list;
    }

    /**
     * Check if a Murmur ACL rule is a token
     */
    public function isToken($acl): bool
    {
        return (
            ! $acl->inherited
            && $acl->userid === -1
            && substr($acl->group, 0, 1) === '#'
        );
    }

    /**
     * Check if a token has been set
     */
    public function hasToken(): bool
    {
        foreach ($this->aclList as $acl) {
            if ($this->isToken($acl)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if a Murmur ACL rule is a "deny all" added with a token
     */
    public function isDenyAllToken($acl): bool
    {
        return (
            $acl->group === 'all'
            && $acl->userid === -1
            && ! $acl->inherited
            && $acl->applyHere
            && $acl->applySubs
            && $acl->allow === 0
            && $acl->deny === 908
        );
    }

    public function setPassword(string $password): void
    {
        if ($this->hasToken()) {
            // edit the password
            foreach ($this->aclList as $key => $acl) {
                if ($this->isToken($acl)) {
                    $this->aclList[$key]->group = '#'.$password;
                    return;
                }
            }
        } else {
            // Add a new password
            $this->aclList[] = MurmurAclFactory::createDenyAll();
            $this->aclList[] = MurmurAclFactory::createPassword($password);
        }
    }

    public function removePassword(): void
    {
        if (! $this->hasToken()) {
            return;
        }

        foreach ($this->aclList as $key => $acl) {
            if ($this->isDenyAllToken($acl)) {
                unset($this->aclList[$key]);
                break;
            }
        }

        foreach ($this->aclList as $key => $acl) {
            if ($this->isToken($acl)) {
                unset($this->aclList[$key]);
                break;
            }
        }
    }
}
