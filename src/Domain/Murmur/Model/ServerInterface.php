<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

use App\Domain\Murmur\Exception\Ice\IceException;
use App\Domain\Murmur\Exception\Server as ServerException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface ServerInterface
{
    public function __toString(): string;

    public function getSid(): int;

    /**
     * Kick all connected users
     *
     * @throws ServerException\ServerException|IceException|\Exception
     */
    public function kickAllUsers($message = ''): void;

    public function validateUserChars(string $string): bool;

    public function validateChannelChars(string $string): bool;

    public function isAllowHtml(): bool;

    public function getServerName(): string;

    /**
     * Remove HTML tags if the server do not allow HTML
     *
     * @return string $string - The string (stripped or not).
     * @return boolean $stripped - boolean, if the message has been stripped or not.
     */
    public function checkIsAllowHtmlOrStripTags(string $string, &$stripped): string;

    /**
     * Return the specific parameter for a server (custom or default).
     */
    public function getParameter(string $key): string;

    /// ////////////////////////////////////////////
    ///
    /// Murmur_Server methods
    ///
    /// ///////////////////////////////////////////

    /**
     * Add a new channel.
     *
     * Note: The method doesn't validate the channel name characters.
     *
     * @return int - The ID of newly created channel.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function addChannel(string $name, int $parent): int;

    /**
     * Delete server and all it's configuration.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function delete(): void;

    /**
     * Retrieve ACLs and Groups on a channel.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getACL(int $channelId, &$aclList, &$groupList, &$inherit): void;

    /**
     * Retrieve all configuration items.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getAllConf(): array;
    /**
     * Alias for Server::getAllConf()
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getCustomConf(): array;

    /**
     * Fetch all current IP bans on the server.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getBans(): array;

    /**
     * Fetch certificate of user. This returns the complete certificate chain of
     * a user.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getCertificateList(int $sessionId): array;

    /**
     * Get state of single channel.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getChannelState(int $channelId): object;

    /**
     * Fetch all channels. This returns all defined channels on the server.
     * The root channel is always channel 0.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getChannels(): array;

    /**
     * Retrieve configuration item.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getConf(string $key): string;

    /**
     * Fetch log entries.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getLog(int $first, int $last): array;

    /**
     * Fetch length of log.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getLogLen(): int;

    /**
     * Fetch a group of registered users.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getRegisteredUsers(string $filter): array;

    /**
     * Fetch registration for a single user.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getRegistration(int $userId): array;

    /**
     * Get state of a single connected user.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getState(int $sessionId): object;

    /**
     * Fetch user texture. Textures are stored as zlib compress()ed
     * 600x60 32-bit BGRA data.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getTexture(int $userId): array;

    /**
     * Fetch all channels and connected users as a tree. This retrieves an
     * easy-to-use representation of the server as a tree. This is primarily
     * used for viewing the state of the server on a webpage.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getTree(): object;

    /**
     * Get virtual server uptime.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getUptime(): int;

    /**
     * Fetch all users. This returns all currently connected users on the server.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function getUsers(): array;

    /**
     * Check if user is permitted to perform action.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function hasPermission(int $sessionId, int $channelId, int $permission): bool;

    /**
     * Fetch the server id.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function id(): int;

    /**
     * Shows if the server currently running (accepting users).
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function isRunning(): bool;

    /**
     * Kick a user. The user is not banned, and is free to rejoin the server.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function kickUser(int $sessionId, string $reason): void;

    /**
     * Register a new user.
     *
     * @return int - the ID of the user.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function registerUser(array $userInfoMap): int;

    /**
     * Remove a channel and all its sub-channels.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function removeChannel(int $channelId): void;

    /**
     * Send text message to a single user.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function sendMessage(int $sessionId, string $text): void;

    /**
     * Send text message to channel or a tree of channels.
     *
     * @param $tree - If true, the message will be sent to the channel and all its sub-channels
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function sendMessageChannel(int $channelId, bool $tree, string $text): void;

    /**
     * Set ACLs and Groups on a channel.
     *
     * Note that this will replace all
     * existing ACLs and groups on the channel.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function setACL(int $channelId, array $aclList, array $aclGroup, bool $inherit): void;

    /**
     * Set all current IP bans on the server. This will replace any bans
     * already present, so if you want to add a ban, be sure to call getBans
     * and then append to the returned list before calling this method.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function setBans(array $banList): void;

    /**
     * Set channel state.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function setChannelState(object $channel): void;

    /**
     * Set a configuration item.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function setConf(string $key, string $value): void;

    /**
     * Set user state.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function setState(object $user): void;

    /**
     * Set a user texture (now called avatar).
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function setTexture(int $userId, array $texture): void;

    /**
     * Start server.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function start(): void;

    /**
     * Stop server.
     *
     * Note: Server will be restarted on Murmur restart unless explicitly
     * disabled with setConf("boot", false)
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function stop(): void;

    /**
     * Remove a user registration.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function unregisterUser(int $userId): void;

    /**
     * Update the registration for a user.
     *
     * You can use this to set the email or password of a user, and can also use
     * it to change the user's name.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function updateRegistration(int $userId, array  $userInfoMap): void;

    /**
     * Verify the password of a user. You can use this to verify a user's credentials.
     *
     * @see http://mumble.sourceforge.net/slice/1.2.7/Murmur/Server.html#verifyPassword
     *
     * @return int - User ID of registered user (See RegisteredUser::userid),
     *               -1 for failed authentication or
     *               -2 for unknown usernames.
     *
     * @throws ServerException\ServerException|\Exception
     */
    public function verifyPassword(string $name, string $password): int;

    /*
     * Murmur_Server methods not declared:
     * -------------------------------------
     *
     * addUserToGroup
     * effectivePermissions
     * getUserIds
     * getUserNames
     * redirectWhisperGroup
     * removeUserFromGroup
     * setAuthenticator
     * setSuperuserPassword
     *
     * --------------------------------------
     * Callbacks can't be implemented because Ice-php do not support server-side
     * activities.
     * See: https://forums.zeroc.com/discussion/comment/32805#Comment_32805
     * --------------------------------------
     *
     * addCallback
     * addContextCallback
     * removeCallback
     * removeContextCallback
     */
}
