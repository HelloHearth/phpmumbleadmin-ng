<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

use App\Domain\Murmur\Middleware\ConnectionBusMiddleware;
use App\Domain\Service\SecurityServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerSettingsManager
{
    private SecurityServiceInterface $securityService;

    public function __construct(SecurityServiceInterface $securityService)
    {
        $this->securityService = $securityService;
    }

    /**
     * Settings which only works with the ini file, and not included here:
     * sendversion, suggestversion
     *
     * @return ServerSetting[]
     */
    public function getAll(): array
    {
        $parameters = [];

        $parameter = new ServerSetting('registername');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('host');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('port');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('password');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('timeout');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('bandwidth');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('users');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('usersperchannel');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('rememberchannel');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('defaultchannel');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('registerpassword');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('registerhostname');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('registerurl');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('username');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('channelname');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('textmessagelength');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('imagemessagelength');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('allowhtml');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('bonjour');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameters[] = $parameter;

        $parameter = new ServerSetting('certrequired');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('opusthreshold');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameter->setMurmurVersion('1.2.4');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('channelnestinglimit');
        $parameter->setRole(ServerSetting::ROLE_ADMINISTRATION);
        $parameter->setMurmurVersion('1.2.4');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('suggestpositional');
        $parameter->setMurmurVersion('1.2.4');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('suggestpushtotalk');
        $parameter->setMurmurVersion('1.2.4');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('welcometext');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('certificate');
        $parameters[] = $parameter;

        $parameter = new ServerSetting('key');
        $parameters[] = $parameter;

        return $this->sanity($parameters);
    }

    /**
     * @return ServerSetting[]
     */
    private function sanity(array $parameters): array
    {
        $version = $_SERVER[ConnectionBusMiddleware::MURMUR_VERSION_STRING];
        $sanity = [];

        foreach ($parameters as $parameter) {

            if (! $parameter instanceof ServerSetting) {
                throw new \InvalidArgumentException(ServerSetting::class.' is required');
            }

            // Remove parameters with higher required murmur version
            if (\version_compare($parameter->getMurmurVersion(), $version, '>')) {
                continue;
            }

            // Remove parameters which require SuperAdmin rights if the user is not granted
            if (
                ServerSetting::ROLE_ADMINISTRATION === $parameter->getRole() &&
                ! $this->securityService->canEditMurmurSettingTypeAdministration()
            ) {
                continue;
            }

            $sanity[$parameter->getKey()] = $parameter;
        }

        return $sanity;
    }
}
