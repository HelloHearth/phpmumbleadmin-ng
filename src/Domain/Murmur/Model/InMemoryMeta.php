<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class InMemoryMeta extends AbstractMeta
{
    private array $defaultConf = [];
    private array $version = [
        'major' => '42',
        'minor' => '42',
        'patch' => '42',
        'text' => 'fake version',
    ];

    public function getAllServers(): array
    {
        // TODO: Implement getAllServers() method.
    }

    public function getBootedServers(): array
    {
        // TODO: Implement getBootedServers() method.
    }

    public function newServer(): ?ServerInterface
    {
        // TODO: Implement newServer() method.
    }

    public function getServer(int $id): ?ServerInterface
    {
        // TODO: Implement getServer() method.
    }

    public function getUptime(): int
    {
        // TODO: Implement getUptime() method.
    }

    public function getDefaultConf(): array
    {
        return $this->defaultConf;
    }

    public function with_default_conf(array $data): self
    {
        $this->defaultConf = $data;
        return $this;
    }

    public function getVersion(&$major, &$minor, &$patch, &$text): void
    {
        $major = $this->version['major'];
        $minor = $this->version['minor'];
        $patch = $this->version['patch'];
        $text = $this->version['text'];
    }

    public function with_version(string $major, string $minor, string $patch, string $text = ''): self
    {
        $this->version = [
            'major' => $major,
            'minor' => $minor,
            'patch' => $patch,
            'text' => $text,
        ];
        return $this;
    }

    public function getSliceChecksums(): array
    {
        // TODO: Implement getSliceChecksums() method.
    }
}
