<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractMeta implements MetaInterface
{
    private array $MurmurVersion;

    public function getMurmurVersion(): array
    {
        if (! isset($this->MurmurVersion)) {

            $this->getVersion($major, $minor, $patch, $text);

            $this->MurmurVersion['full'] = $major.'.'.$minor.'.'.$patch;
            $this->MurmurVersion['str'] = $this->MurmurVersion['full'];

            if ($text !== '' && $text !== $this->MurmurVersion['str']) {
                $this->MurmurVersion['full'] .= ' - '.$text;
            }
        }

        return $this->MurmurVersion;
    }
}
