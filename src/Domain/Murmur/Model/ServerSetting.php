<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerSetting
{
    public const ROLE_ADMINISTRATION = 'administration';
    public const ROLE_ALL = 'all';

    private string $key;
    private string $role = self::ROLE_ALL;
    private string $murmurVersion  = '1.2.3';

    /**
     * @var mixed
     */
    private $defaultValue;

    /**
     * @var mixed
     */
    private $customValue;

    public function __construct(string $key)
    {
        $this->key = \strtolower($key);
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    public function getMurmurVersion(): string
    {
        return $this->murmurVersion;
    }

    public function setMurmurVersion(string $murmurVersion): void
    {
        $this->murmurVersion = $murmurVersion;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    public function setDefaultValue($defaultValue): void
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return mixed
     */
    public function getCustomValue()
    {
        return $this->customValue;
    }

    public function setCustomValue($customValue): void
    {
        $this->customValue = $customValue;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->isModified() ? $this->getCustomValue() : $this->getDefaultValue();
    }

    public function isModified(): bool
    {
        return isset($this->customValue);
    }
}
