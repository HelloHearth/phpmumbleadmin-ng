<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model\Mock;

/**
 * Fake class to mock ::Murmur::Channel object
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UserMock
{
    public int $session;
    public int $userid;
    public bool $mute;
    public bool $deaf;
    public bool $suppress;
    public bool $prioritySpeaker;
    public bool $selfMute;
    public bool $selfDeaf;
    public bool $recording;
    public int $channel;
    public string $name;
    public int $onlinesecs;
    public int $bytespersec;
    public int $version;
    public string $release;
    public string $os;
    public string $osversion;
    public string $identity;
    public string $context;
    public string $comment;
    public ?array $address;
    public bool $tcponly;
    public int $idlesecs;
    public float $udpPing;
    public float $tcpPing;

    public function __construct($session=0, $userid=0, $mute=false, $deaf=false, $suppress=false, $prioritySpeaker=false, $selfMute=false, $selfDeaf=false, $recording=false, $channel=0, $name='', $onlinesecs=0, $bytespersec=0, $version=0, $release='', $os='', $osversion='', $identity='', $context='', $comment='', $address=null, $tcponly=false, $idlesecs=0, $udpPing=0.0, $tcpPing=0.0)
    {
        $this->session = $session;
        $this->userid = $userid;
        $this->mute = $mute;
        $this->deaf = $deaf;
        $this->suppress = $suppress;
        $this->prioritySpeaker = $prioritySpeaker;
        $this->selfMute = $selfMute;
        $this->selfDeaf = $selfDeaf;
        $this->recording = $recording;
        $this->channel = $channel;
        $this->name = $name;
        $this->onlinesecs = $onlinesecs;
        $this->bytespersec = $bytespersec;
        $this->version = $version;
        $this->release = $release;
        $this->os = $os;
        $this->osversion = $osversion;
        $this->identity = $identity;
        $this->context = $context;
        $this->comment = $comment;
        $this->address = $address;
        $this->tcponly = $tcponly;
        $this->idlesecs = $idlesecs;
        $this->udpPing = $udpPing;
        $this->tcpPing = $tcpPing;
    }

    public function __toString(): string
    {
        return '__MOCK::Murmur::Channel';
    }
}
