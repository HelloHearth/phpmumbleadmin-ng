<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model\Mock;

/**
 * Fake class to mock ::Murmur::Ban object
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BanMock
{
    public $address;
    public int $bits;
    public string $name;
    public string $hash;
    public string $reason;
    public int $start;
    public int $duration;

    public function __construct($address=null, $bits=0, $name='', $hash='', $reason='', $start=0, $duration=0)
    {
        $this->address = $address;
        $this->bits = $bits;
        $this->name = $name;
        $this->hash = $hash;
        $this->reason = $reason;
        $this->start = $start;
        $this->duration = $duration;
    }

    public function __toString(): string
    {
        return '__MOCK::Murmur::Ban';
    }
}
