<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model\Mock;

/**
 * Fake class to mock ::Murmur::ACL object
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class GroupMock
{
    public string $name;
    public bool $inherited;
    public bool $inherit;
    public bool $inheritable;
    public ?array $add;
    public ?array $remove;
    public ?array $members;

    public function __construct($name='', $inherited=false, $inherit=false, $inheritable=false, $add=null, $remove=null, $members=null)
    {
        $this->name = $name;
        $this->inherited = $inherited;
        $this->inherit = $inherit;
        $this->inheritable = $inheritable;
        $this->add = $add;
        $this->remove = $remove;
        $this->members = $members;
    }

    public function __toString(): string
    {
        return '__MOCK::Murmur::Group';
    }
}
