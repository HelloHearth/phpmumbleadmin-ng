<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model\Mock;

/**
 * Fake class to mock ::Murmur::Log object
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogEntryMock
{
    public int $timestamp;
    public string $txt;

    public function __construct($timestamp=0, $txt='')
    {
        $this->timestamp = $timestamp;
        $this->txt = $txt;
    }

    public function __toString(): string
    {
        return '__MOCK::Murmur::Ban';
    }
}
