<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model\Mock;

/**
 * Fake class to mock ::Murmur::ACL object
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AclMock
{
    public bool $applyHere;
    public bool $applySubs;
    public bool $inherited;
    public int $userid;
    public string $group;
    public int $allow;
    public int $deny;

    public function __construct($applyHere = false, $applySubs = false, $inherited = false, $userid = 0, $group = '', $allow = 0, $deny = 0)
    {
        $this->applyHere = $applyHere;
        $this->applySubs = $applySubs;
        $this->inherited = $inherited;
        $this->userid = $userid;
        $this->group = $group;
        $this->allow = $allow;
        $this->deny = $deny;
    }

    public function __toString(): string
    {
        return '__MOCK::Murmur::ACL';
    }
}
