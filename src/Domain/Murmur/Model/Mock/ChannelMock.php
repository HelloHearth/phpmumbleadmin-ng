<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model\Mock;

/**
 * Fake class to mock ::Murmur::Channel object
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelMock
{
    public int $id;
    public string $name;
    public int $parent;
    public ?array $links;
    public string $description;
    public bool $temporary;
    public int $position;

    public function __construct($id = 0, $name = '', $parent = 0, $links = null, $description = '', $temporary = false, $position = 0)
    {
        $this->id = $id;
        $this->name = $name;
        $this->parent = $parent;
        $this->links = $links;
        $this->description = $description;
        $this->temporary = $temporary;
        $this->position = $position;
    }

    public function __toString(): string
    {
        return '__MOCK::Murmur::Channel';
    }
}
