<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

use App\Domain\Helper\DateHelper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServersListCache
{
    private const EXPIRES_AFTER = 3600;

    private array $servers;
    private int $expiresAt;
    private bool $isValid;

    public function __construct(array $servers)
    {
        $this->servers = $servers;

        $this->expiresAt = \time() + self::EXPIRES_AFTER;
        $this->isValid = true;
    }

    public function getServers(): array
    {
        return $this->servers;
    }

    public function getExpiresAt(): int
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(int $expiresAt): void
    {
        $this->expiresAt = \time() + $expiresAt;
    }

    public function isExpired(): bool
    {
        return ($this->expiresAt < \time());
    }

    public function isValid(): bool
    {
        return $this->isValid && ! $this->isExpired();
    }

    public function invalidate(): void
    {
        $this->isValid = false;
    }

    public function uptime(): string
    {
        $timestamp = $this->expiresAt - self::EXPIRES_AFTER - \time();
        return DateHelper::uptime($timestamp);
    }
}
