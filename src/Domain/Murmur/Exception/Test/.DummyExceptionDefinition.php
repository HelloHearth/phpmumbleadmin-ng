<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

/**
 * Define missing Exception class from Murmur for tests in different version of
 * Zeroc-Ice
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */

// Exception with Zeroc-Ice =< 3.6
namespace {
    if (! class_exists('Ice_MemoryLimitException')) {
        class Ice_MemoryLimitException extends Exception { }
    }
    if (! class_exists('Murmur_ServerBootedException')) {
        class Murmur_ServerBootedException extends Exception { }
        class Murmur_ServerFailureException extends Exception { }
        class Murmur_InvalidServerException extends Exception { }
        class Murmur_InvalidSecretException extends Exception { }
        class Murmur_InvalidChannelException extends Exception { }
        class Murmur_InvalidUserException extends Exception { }
        class Murmur_InvalidSessionException extends Exception { }
        class Murmur_NestingLimitException extends Exception { }
        class Murmur_InvalidTextureException extends Exception { }
        class Murmur_InvalidCallbackException extends Exception { }
    }
}

// Exception with Zeroc-Ice >= 3.7
namespace Ice {
    if (! class_exists('Ice\MemoryLimitException')) {
        class MemoryLimitException extends \Exception { }
    }
}

namespace Murmur {
    if (! class_exists('Murmur\ServerBootedException')) {
        class ServerBootedException extends \Exception { }
        class ServerFailureException extends \Exception { }
        class InvalidServerException extends \Exception { }
        class InvalidSecretException extends \Exception { }
        class InvalidChannelException extends \Exception { }
        class InvalidUserException extends \Exception { }
        class InvalidSessionException extends \Exception { }
        class NestingLimitException extends \Exception { }
        class InvalidTextureException extends \Exception { }
        class InvalidCallbackException extends \Exception { }
    }
}
