<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Factory;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MurmurAclFactory
{
    public static function createDenyAll()
    {
        $acl = MurmurObjectFactory::createAcl();

        $acl->group = 'all';
        $acl->userid = -1;
        $acl->inherited = false;
        $acl->applyHere = true;
        $acl->applySubs = true;
        $acl->allow = 0;
        $acl->deny = 908;

        return $acl;
    }

    public static function createPassword(string $password)
    {
        $acl = MurmurObjectFactory::createAcl();

        $acl->group = '#'.$password;
        $acl->userid = -1;
        $acl->inherited = false;
        $acl->applyHere = true;
        $acl->applySubs = true;
        $acl->allow = 908;
        $acl->deny = 0;

        return $acl;
    }
}
