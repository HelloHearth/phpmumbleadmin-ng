<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Factory;

use App\Domain\Murmur\Model\Mock\AclMock;
use App\Domain\Murmur\Model\Mock\BanMock;

/**
 * Get Murmur object
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MurmurObjectFactory
{
    public static function createAcl()
    {
        if (\class_exists('Murmur_acl')) {
            return new Murmur_acl();
        } elseif (\class_exists('\Murmur\Acl')) {
            return new \Murmur\Acl();
        } else {
            return new AclMock();
        }
    }

    public static function createBan(): object
    {
        if (\class_exists('Murmur_ban')) {
            return new Murmur_ban();
        } elseif (\class_exists('\Murmur\Ban')) {
            return new \Murmur\Ban();
        } else {
            return new BanMock();
        }
    }

    /**
     * @return BanMock[]
     */
    public static function transformBansToBanMocks(array $bans): array
    {
        $banMocks = [];

        foreach ($bans as $ban) {

            if ($ban instanceof BanMock) {
                $banMocks[] = $ban;
                continue;
            }

            if (! \is_object($ban)) {
                throw new \InvalidArgumentException('invalid ban : is not an object');
            }

            if (! \in_array(\get_class($ban), ['Murmur\Ban', 'Murmur_ban'])) {
                throw new \InvalidArgumentException('invalid ban class object :'.\get_class($ban));
            }

            $banMock = new BanMock();

            foreach(\get_object_vars($ban) as $key => $value) {
                $banMock->$key = $value;
            }

            $banMocks[] = $banMock;
        }

        return $banMocks;
    }
}
