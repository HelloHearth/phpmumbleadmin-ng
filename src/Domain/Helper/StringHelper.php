<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class StringHelper
{
    public static function cutLongString(?string $string, int $maxlen, string $cutString = '...'): string
    {
        if (\is_null($string)) {
            return  '';
        }

        $strlen = \strlen($string);

        if ($strlen > $maxlen) {
            $sub = $maxlen-$strlen;
            $string = \substr($string, 0, $sub);
            $string = \trim($string).$cutString;
        }

        return $string;
    }
}
