<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SandboxHelper
{
    /**
     * Create the sandbox file with the HTML code
     *
     * @param $path  - full path to sandboxes cache directory
     * @param $datas - custom datas to write into the sandbox file
     */
    public static function createFile(string $path, string $datas): void
    {
        // Create the sandbox directory recursively
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        /*
         * fopen w mode: Open for writing only.
         * Place the file pointer at the beginning of the file and truncate the
         * file to zero length. If the file does not exist, attempt to create it.
         */
        $fp = @fopen($path.self::filename(), 'wb');

        if (is_resource($fp)) {
            fwrite($fp, self::htmlCode($datas));
            fclose($fp);
        }
    }

    /**
     * Get the sandbox file name
     */
    public static function filename(): string
    {
        return 'sandbox_'.session_id().'.html';
    }

    /**
     * Return the HTML5 code with the data of the sandbox
     */
    public static function htmlCode(string $datas): string
    {
        return
'<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>SANDBOX</title>
    </head>
    <body>
        '.$datas.'
    </body>
 </html>
';
    }
}
