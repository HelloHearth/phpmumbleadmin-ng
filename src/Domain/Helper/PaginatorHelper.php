<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PaginatorHelper
{
    /**
     * Return the current page, and if the page is higher than the last page
     * return the last page.
     */
    static function getValidPage(int $totalOfItems, int $currentPage, int $limitPerPage): int
    {
        // One day, I will find a way to divid by zero !
        if ($limitPerPage < 1 ) {
            $limitPerPage = 1;
        }

        // Total of pages
        $delta = (int) ceil($totalOfItems / $limitPerPage);
        if ($delta < 1 ) {
            $delta = 1;
        }

        // Current page can't be superior than the total of pages
        if ($currentPage > $delta) {
            $currentPage = $delta;
        }

        // Current page can't be null or negative
        if ($currentPage < 1 ) {
            $currentPage = 1;
        }

        return $currentPage;
    }
}
