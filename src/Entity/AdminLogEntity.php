<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Entity;

use App\Domain\Model\AdminLog;
use App\Repository\AdminLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdminLogRepository::class)
 * @ORM\Table(name="admin_log")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminLogEntity extends AdminLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected int $timestamp;

    /**
     * @ORM\Column
     */
    protected string $message;

    /**
     * @ORM\Column(type="json")
     */
    protected array $context;

    /**
     * @ORM\Column(type="integer")
     */
    protected int $level;

    /**
     * @ORM\Column(type="integer")
     */
    protected int $facility;

    /**
     * @ORM\Column
     */
    protected string $address;


    public function getId(): int
    {
        return $this->id;
    }
}
