<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Forgotten password tracker
 *
 * @ORM\Entity()
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPassword
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToOne(targetEntity="AdminEntity")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private AdminEntity $admin;

    /**
     * @ORM\Column(unique=true)
     */
    private string $hash;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $validUntil;


    public function getId(): int
    {
        return $this->id;
    }

    public function getAdmin(): AdminEntity
    {
        return $this->admin;
    }

    public function setAdmin(AdminEntity $admin): void
    {
        $this->admin = $admin;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getValidUntil(): \DateTimeImmutable
    {
        return $this->validUntil;
    }

    public function setValidUntil(\DateTimeImmutable $validUntil): void
    {
        $this->validUntil = $validUntil;
    }

    public function generateHash(): void
    {
        $this->hash = \rtrim(\strtr(\base64_encode(\random_bytes(128)), '+/', '-_'), '=');
    }

    public function isExpired(): bool
    {
        if (! $this->validUntil instanceof \DateTimeImmutable) {
            throw new \RuntimeException();
        }

        return $this->validUntil->getTimestamp() < \time();
    }
}
