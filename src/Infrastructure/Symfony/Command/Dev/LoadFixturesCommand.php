<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Command\Dev;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LoadFixturesCommand extends Command implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        parent::configure();
        $this
            ->setName('app:load-fixtures')
            ->setDescription(
                'Run a fast tables truncate process, with no interaction option by default.'.PHP_EOL.
                'Prod environment is not allowed.'
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ('prod' === $this->container->getParameter('kernel.environment')) {
            $output->writeln('<error># Prod environment is not allowed to run this command</error>');
            return Command::INVALID;
        }

        $this->fastTruncateAllTables($output);

        $application = $this->getApplication();
        $application->setAutoExit(false);

        $arguments = [
            'command' => 'doctrine:fixtures:load',
            '--no-interaction' => true
        ];

        $application->run(new ArrayInput($arguments), $output);

        return Command::SUCCESS;
    }

    /*
     * Speed up the truncate action which can be very slow on huge tables.
     * The action do a DELETE all for all tables of entities, and reset the
     * AUTO_INCREMENT like TRUNCATE will do.
     * The problem of slow TRUNCATE maybe come from the InnoDB engine, for more
     * detail, see https://bugs.mysql.com/bug.php?id=68184
     */
    private function fastTruncateAllTables(OutputInterface $output): void
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $connection = $entityManager->getConnection();
        $entitiesMeta = $entityManager->getMetadataFactory()->getAllMetadata();

        $output->writeln('<question> # </question><info>Fast truncate workaround</info>');

        $statement = '';
        foreach ($entitiesMeta as $meta) {

            $tableName = $meta->getTableName();
            $output->writeln('<question> # </question><info>Truncate table '.$tableName.'</info>');

            $statement .= 'DELETE FROM '.$tableName.'; ALTER TABLE '.$tableName.' AUTO_INCREMENT = 0;';
        }

        $connection->executeStatement($statement);
    }
}
