<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractUser implements UserInterface, EquatableInterface
{
    protected string $username;
    protected ?string $email;
    protected array $roles = [];

    abstract public function getAvailableRoles(): array;

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->username;
    }

    /**
     * Todo: to remove after migrating to Symfony 6
     *
     * @inheritDoc
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * {@inheritDoc}
     */
    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        return \array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        foreach ($roles as $key => $role) {
            if (! \in_array($role, $this->getAvailableRoles())) {
                unset($roles[$key]);
            }
        }
        $this->roles = $roles;
    }

    // Do not log out user on change
    public function isEqualTo(BaseUserInterface $user): bool
    {
        return true;
    }
}
