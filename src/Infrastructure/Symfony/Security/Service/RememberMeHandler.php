<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\RememberMe\AbstractRememberMeHandler;
use Symfony\Component\Security\Http\RememberMe\RememberMeDetails;

/**
 * Override the Symfony service TokenBasedRememberMeServices for mumble users
 * To find a mumble user, the server id is also required
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RememberMeHandler extends AbstractRememberMeHandler
{
    public const SERVER_SEPARATOR = '///server-id///';

    public function __construct(
        ParameterBagInterface $parameterBag,
        UserProviderInterface $userProvider,
        RequestStack $requestStack,
        array $options,
        LoggerInterface $logger = null
    ) {
        $rememberMeOptions = [
            'name' => $parameterBag->get('app.name').'_REMEMBERME',
            'remember_me_parameter' => 'app_login[remember_me]',
            'lifetime' => 31536000, // 1 year in seconds
            'path' => '/',
        ];

        parent::__construct($userProvider, $requestStack, $rememberMeOptions, $logger);
    }

    /**
     * {@inheritdoc}
     */
    public function createRememberMeCookie(UserInterface $user): void
    {
        $expires = \time() + $this->options['lifetime'];
        $userIdentifier = $user->getUserIdentifier();

        // For MumbleUserProvider
        if (\method_exists($user, 'getServerId')) {
            $userIdentifier .= self::SERVER_SEPARATOR.$user->getServerId();
        }

        $details = new RememberMeDetails(\get_class($user), $userIdentifier, $expires, $user->getPassword() ?? '');
        $this->createCookie($details);
    }

    /**
     * {@inheritdoc}
     */
    public function processRememberMe(RememberMeDetails $rememberMeDetails, UserInterface $user): void
    {
        $this->createRememberMeCookie($user);
    }
}
