<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Infrastructure\Symfony\Security\Service;

use App\Infrastructure\Symfony\Security\MumbleUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DefaultRedirectionService
{
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function getHomepage(UserInterface $user): RedirectResponse
    {
        if ($user instanceof MumbleUser) {
            return new RedirectResponse($this->urlGenerator->generate('server_channels', ['serverId' => $user->getServerId()]));
        }

        return new RedirectResponse($this->urlGenerator->generate('dashboard'));
    }
}
