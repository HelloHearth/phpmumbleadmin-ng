<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Infrastructure\Symfony\Security;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface UserInterface extends BaseUserInterface
{
    /**
     * Return the field (email, username...) used to log in the user
     */
    public function __toString(): string;
}
