<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security\Voter;

use App\Domain\Murmur\Model\MetaInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CanModifyMumbleUserNameOnChannelTreeVoter extends Voter
{
    public const KEY = 'APP_CAN_MODIFY_MUMBLE_USERNAME_ON_CHANNEL_TREE';

    protected function supports($attribute, $subject): bool
    {
        return $attribute === self::KEY && $subject instanceof MetaInterface;
    }

    /**
     * @var MetaInterface $subject
     *
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        // Modify user session name is possible since murmur 1.2.4
        $version = $subject->getMurmurVersion();
        return \version_compare($version['str'], '1.2.4', '>=');
    }
}
