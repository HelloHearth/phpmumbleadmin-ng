<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security\Voter;

use App\Infrastructure\Service\Role\RoleBitService;
use App\Infrastructure\Symfony\Security\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Checker for CRUD an admin
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CanEditRoleVoter extends Voter
{
    public const KEY = 'APP_CAN_EDIT_ROLE';

    private RoleBitService $roleBit;

    public function __construct(RoleBitService $roleBit)
    {
        $this->roleBit = $roleBit;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        return $attribute === self::KEY && (\is_string($subject) || $subject instanceof UserInterface);
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (empty($token->getUser())) {
            return false;
        }

        $userBit = $this->roleBit->findUserBit($token->getUser());

        // SuperAdmin always can modify any user
        if ($userBit === RoleBitService::SUPER_ADMIN) {
            return true;
        }

        if ($subject instanceof UserInterface) {
            $subjectBit = $this->roleBit->findUserBit($subject);
        } else {
            $subjectBit = $this->roleBit->getFromString($subject);
        }

        if ($userBit < $subjectBit) {
            return true;
        }

        return false;
    }
}
