<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Infrastructure\Symfony\Security;

use App\Domain\Service\Role\RoleNameServiceInterface;
use App\Domain\Service\SecurityServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MumbleUser extends AbstractUser
{
    public const AVAILABLE_ROLES = [
        RoleNameServiceInterface::SUPER_USER => SecurityServiceInterface::ROLE_SUPER_USER,
        RoleNameServiceInterface::SUPER_USER_RU => SecurityServiceInterface::ROLE_SUPER_USER_RU,
        RoleNameServiceInterface::MUMBLE_USER => SecurityServiceInterface::ROLE_MUMBLE_USER
    ];

    protected string $username;
    protected array $roles = [];

    private int $serverId;
    private int $registrationId;

    public function getServerId(): int
    {
        return $this->serverId;
    }

    public function setServerId(int $serverId): void
    {
        $this->serverId = $serverId;
    }

    public function getRegistrationId(): int
    {
        return $this->registrationId;
    }

    public function setRegistrationId(int $registrationId): void
    {
        $this->registrationId = $registrationId;
    }

    /**
     * No op
     *
     * @inheritDoc
     */
    public function getPassword(): void { }

    /**
     * No op
     *
     * @inheritDoc
     */
    public function getSalt(): void { }

    /**
     * No op
     *
     * @inheritDoc
     */
    public function eraseCredentials(): void { }

    public function getAvailableRoles(): array
    {
        return self::AVAILABLE_ROLES;
    }
}
