<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security\UserProvider;

use App\Infrastructure\Murmur\Service\MumbleUserService;
use App\Infrastructure\Symfony\Security\MumbleUser;
use App\Infrastructure\Symfony\Security\Service\RememberMeHandler;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MumbleUserProvider implements UserProviderInterface
{
    private MumbleUserService $mumbleUserService;

    public function __construct(MumbleUserService $mumbleUserService)
    {
        $this->mumbleUserService = $mumbleUserService;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByIdentifier(string $identifier): MumbleUser
    {
        if (! \str_contains($identifier, RememberMeHandler::SERVER_SEPARATOR)) {
            throw new \RuntimeException();
        }

        [$username, $serverId] = \explode(RememberMeHandler::SERVER_SEPARATOR, $identifier);

        $mumbleUser = $this->mumbleUserService->find((int) $serverId, $username);

        if (! $mumbleUser instanceof MumbleUser) {
            throw new UserNotFoundException();
        }

        return $mumbleUser;
    }

    /**
     * @inheritDoc
     */
    public function loadUserByUsername($username): MumbleUser
    {
        return $this->loadUserByIdentifier($username);
    }


    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user): MumbleUser
    {
        if (! $user instanceof MumbleUser) {
            throw new UnsupportedUserException(\sprintf('Invalid user class "%s".', get_class($user)));
        }

        $mumbleUser = $this->mumbleUserService->find($user->getServerId(), $user->getUserIdentifier());

        if (! $mumbleUser instanceof MumbleUser) {
            throw new UserNotFoundException();
        }

        return $mumbleUser;
    }

    /**
     * @inheritDoc
     */
    public function supportsClass($class): bool
    {
        return MumbleUser::class === $class || \is_subclass_of($class, MumbleUser::class);
    }
}
