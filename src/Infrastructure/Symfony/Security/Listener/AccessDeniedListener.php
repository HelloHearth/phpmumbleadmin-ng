<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security\Listener;

use App\Infrastructure\Symfony\Security\LoginFormAuthenticator;
use App\Infrastructure\Symfony\Security\Service\DefaultRedirectionService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

/**
 * On AccessDeniedException after a redirection to the target_path, redirect user
 * to the default "homepage"
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AccessDeniedListener implements EventSubscriberInterface
{
    private DefaultRedirectionService $defaultRedirection;
    private Security $security;

    public function __construct(DefaultRedirectionService $defaultRedirection, Security $security)
    {
        $this->defaultRedirection = $defaultRedirection;
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            // the priority must be greater than the Security HTTP
            // ExceptionListener, to make sure it's called before
            // the default exception listener
            KernelEvents::EXCEPTION => ['onKernelException', 2],
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if (! $exception instanceof AccessDeniedException) {
            return;
        }

        $request = $event->getRequest();

        if ($request->query->has(LoginFormAuthenticator::TARGET_PATH_REDIRECTION_KEY)) {

            $homepage = $this->defaultRedirection->getHomepage($this->security->getUser());
            $event->setResponse($homepage);
            $event->stopPropagation();
        }
    }
}
