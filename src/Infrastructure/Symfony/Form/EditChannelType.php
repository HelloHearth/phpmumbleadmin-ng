<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\Type\ModalJqueryAjaxFormType;
use App\Infrastructure\Symfony\Form\Type\VueJs\CheckboxForVuejsType;
use App\Infrastructure\Symfony\Form\Type\VueJs\InputForVuejsType;
use App\Infrastructure\Symfony\Form\Type\VueJs\IntegerForVuejsType;
use App\Infrastructure\Symfony\Form\Type\VueJs\Model\ChannelIdVueJsType;
use App\Infrastructure\Symfony\Form\Type\VueJs\TextareaForVuejsType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditChannelType extends ModalJqueryAjaxFormType
{
    public const BLOCK_PREFIX = 'edit_channel';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('parentId', ChannelIdVueJsType::class)
            ->add('name', InputForVuejsType::class, [
                'label' => 'channel_name',
                'variable_name' => 'selectedNode.name',
                'vuejs_disabled' => 'selectedNode.id === 0',
            ])
            ->add('defaultChannel', CheckboxForVuejsType::class, [
                'label' => 'set_as_default_channel',
                'translation_domain' => 'form-edit-channel',
                'variable_name' => 'selectedNode.isDefaultChannel',
                'required' => false,
            ])
            ->add('position', IntegerForVuejsType::class, [
                'label' => 'channel_position',
                'translation_domain' => 'form-edit-channel',
                'variable_name' => 'selectedNode.position',
                'required' => false,
            ])
            ->add('description', TextareaForVuejsType::class, [
                'label' => 'channel_description',
                'translation_domain' => 'form-edit-channel',
                'variable_name' => 'selectedNode.description',
                'required' => false,
            ])
            ->add('password', InputForVuejsType::class, [
                'label' => 'channel_password',
                'translation_domain' => 'form-edit-channel',
                'variable_name' => 'selectedNode.token',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'edit',
                'translation_domain' => 'Form',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'channels',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ModalJqueryAjaxFormType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
