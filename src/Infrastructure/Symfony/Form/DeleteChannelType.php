<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\Type\ModalJqueryAjaxFormType;
use App\Infrastructure\Symfony\Form\Type\VueJs\Model\ChannelIdVueJsType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteChannelType extends ModalJqueryAjaxFormType
{
    public const BLOCK_PREFIX = 'delete_channel';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('channelId', ChannelIdVueJsType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'delete',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ModalJqueryAjaxFormType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
