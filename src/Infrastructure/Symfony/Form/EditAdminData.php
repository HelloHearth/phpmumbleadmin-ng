<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Entity\AdminEntity;
use App\Infrastructure\Symfony\Validator\Constraints as Constraints;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Form data with a DTO instead of the entity
 * @see https://stovepipe.systems/post/avoiding-entities-in-forms
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditAdminData
{
    public int $id;

    /**
     * @Assert\NotBlank()
     * @Constraints\UniqueLogin()
     */
    public ?string $login;

    /**
     * @Assert\Email()
     * @Constraints\UniqueEmail()
     */
    public ?string $email = null;

    /** @Assert\NotBlank() */
    public string $role;

    public ?string $password = null;

    public static function fromAdmin(AdminEntity $admin): self
    {
        $self = new self();

        $self->id = $admin->getId();
        $self->login = $admin->getUserIdentifier();
        $self->email = $admin->getEmail();
        $self->role = $admin->getRoles()[0];

        return $self;
    }
}
