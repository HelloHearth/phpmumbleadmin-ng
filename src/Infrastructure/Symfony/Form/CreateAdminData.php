<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Domain\Action\Administration\Admins\CreateAdmin\CreateAdminCommand;
use App\Infrastructure\Symfony\Validator\Constraints as Constraints;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Form data with a DTO instead of the entity
 * @see https://stovepipe.systems/post/avoiding-entities-in-forms
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateAdminData
{
    /**
     * @Assert\NotBlank()
     * @Constraints\UniqueLogin()
     */
    public string $login;

    /**
     * @Assert\Email()
     * @Constraints\UniqueEmail()
     */
    public ?string $email = null;

    /** @Assert\NotBlank() */
    public string $role;

    public string $password;

    public function getCommand(): CreateAdminCommand
    {
        return new CreateAdminCommand($this->login, $this->email, $this->role, $this->password);
    }
}
