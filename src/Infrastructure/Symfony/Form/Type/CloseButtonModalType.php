<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ButtonType;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CloseButtonModalType extends ButtonType
{
    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ButtonType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'app_close_modal_button';
    }
}
