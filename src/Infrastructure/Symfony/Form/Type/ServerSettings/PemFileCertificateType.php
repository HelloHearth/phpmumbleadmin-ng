<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form\Type\ServerSettings;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PemFileCertificateType extends PemTextareaCertificateType
{
    public const BLOCK_PREFIX = 'app_setting_pem_file_certificate';

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            // Remove rows attribute from parent
            'attr' => [],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return FileType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
