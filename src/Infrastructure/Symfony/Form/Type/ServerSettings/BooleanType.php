<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form\Type\ServerSettings;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BooleanType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choice_translation_domain' => 'settings',
            'choices' => [
                '---' => null,
                'activate' => 'true',
                'deactivate' => 'false'
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'app_setting_boolean';
    }
}
