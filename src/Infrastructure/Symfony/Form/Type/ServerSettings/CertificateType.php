<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form\Type\ServerSettings;

use App\Domain\Manager\CertificateManager;
use App\Domain\Service\SystemCheckerServiceInterface;
use App\Infrastructure\Symfony\Form\DataTransformer\UploadedFileTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CertificateType extends AbstractType
{
    private SystemCheckerServiceInterface $systemChecker;
    private UploadedFileTransformer $uploadedFileTransformer;

    public function __construct(SystemCheckerServiceInterface $systemChecker, UploadedFileTransformer $uploadedFileTransformer)
    {
        $this->systemChecker = $systemChecker;
        $this->uploadedFileTransformer = $uploadedFileTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($this->systemChecker->upload_file_is_allowed() && $this->systemChecker->extension_ssl_is_loaded()) {

            $builder->add('pem', PemFileCertificateType::class);
            $builder->get('pem')->addModelTransformer($this->uploadedFileTransformer);

        } elseif (! $this->systemChecker->upload_file_is_allowed() && $this->systemChecker->extension_ssl_is_loaded()) {

            $builder->add('pem', PemTextareaCertificateType::class);

        } elseif ($this->systemChecker->upload_file_is_allowed() && ! $this->systemChecker->extension_ssl_is_loaded()) {

            $builder
                ->add('_certificate', FileType::class, [
                    'label' => 'Certificate',
                ])
                ->add('_key', FileType::class, [
                    'label' => 'Private key',
                    'help' => 'add_new_certificate_help',
                    'translation_domain' => 'module-certificate',
                ])
            ;

            $builder->get('_certificate')->addModelTransformer($this->uploadedFileTransformer);
            $builder->get('_key')->addModelTransformer($this->uploadedFileTransformer);

        } else {

            $builder
                ->add('_certificate', TextareaType::class, [
                    'label' => 'Certificate',
                    'attr' => [
                        'rows' => 6
                    ],
                ])
                ->add('_key', TextareaType::class, [
                    'label' => 'Private key',
                    'help' => 'add_new_certificate_help',
                    'translation_domain' => 'module-certificate',
                    'attr' => [
                        'rows' => 6
                    ],
                ])
            ;
        }

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {

            $data = $event->getData();

            if (isset($data['pem'])) {

                $certificateManager = new CertificateManager($data['pem']);

                // Do not continue, the form will raise a FormError on the certificate
                if (! $certificateManager->verifyCertificateWithPrivateKey()) {
                    return;
                }

                $key = $certificateManager->getPrivateKey();
                $certificate = $certificateManager->getCertificate();

            } else {
                $key = $data['_key'];
                $certificate = $data['_certificate'];
            }

            // Add hidden key and certificate fields to root form
            $rootForm = $event->getForm()->getRoot();

            $rootForm->add('key', HiddenType::class, [
                'empty_data' => $key,
                'trim' => false,
            ]);
            $rootForm->add('certificate', HiddenType::class, [
                'empty_data' => $certificate,
                'trim' => false,
            ]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'app_setting_certificate';
    }
}
