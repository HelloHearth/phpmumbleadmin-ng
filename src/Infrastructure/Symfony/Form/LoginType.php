<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LoginType extends AbstractType
{
    public const BLOCK_PREFIX = 'app_login';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', null, [
                'label' => 'username',
                'data' => $options['last_username'],
                'attr' => $this->addAutofocusForUsername($options),
                'translation_domain' => 'common'
            ])
            ->add('password', PasswordType::class, [
                'label' => 'password',
                'attr' => $this->addAutofocusForPassword($options),
                'translation_domain' => 'common'
            ])
            ->add('server', null, [
                'label' => 'server',
                'required' => false,
                'translation_domain' => 'common'
            ])
            ->add('remember_me', CheckboxType::class, [
                'label' => 'remember_me',
                'required' => false,
                'attr' => [
                    'checked' => 'checked'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'log_in',
                'block_prefix' => self::BLOCK_PREFIX.'_submit'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'login',
            'last_username' => ''
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }

    private function addAutofocusForUsername(array $options): array
    {
        if ('' === $options['last_username']) {
            return ['autofocus' => 'autofocus'];
        }

        return [];
    }

    private function addAutofocusForPassword(array $options): array
    {
        if ('' !== $options['last_username']) {
            return ['autofocus' => 'autofocus'];
        }

        return [];
    }
}
