<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UploadedFileTransformer implements DataTransformerInterface
{
    /**
     * Noop
     *
     * {@inheritDoc}
     */
    public function transform($value): void { }

    /**
     * {@inheritDoc}
     */
    public function reverseTransform($value): string
    {
        if (! $value instanceof UploadedFile) {
            return '';
        }

        $data = \file_get_contents($value->getPathname());

        if (! \is_string($data)) {
            return '';
        }

        return $data;
    }
}
