<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForceServerToStopType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('serverId', HiddenType::class, [
                'data' => $options['serverId']
            ])
            ->add('kickUsers', CheckboxType::class, [
                'label'    => 'kick_all_users',
                'required' => false,
            ])
            ->add('reason', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'the_reason'
                ]
            ])
            ->add('message', TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'message_to_connected_users',
                    'rows' => 5
                ]
            ])
            ->add('cancel', SubmitType::class, [
                'label' => 'cancel',
                'attr' => [
                    'class' => 'btn btn-secondary me-2'
                ]
            ])
            ->add('comfirm_to_stop_server', SubmitType::class, [
                'label' => 'force_to_stop_btn'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'Form',
            'serverId' => null
        ]);
    }
}
