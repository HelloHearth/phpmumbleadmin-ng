<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Domain\Murmur\Model\ServerSetting;
use App\Domain\Service\TranslatorServiceInterface;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\BooleanType;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\CertificateType;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\IntegerNotNegativeType;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\PortType;
use App\Infrastructure\Symfony\Form\Type\ServerSettings\WelcomeTextType;
use App\Infrastructure\Symfony\Validator\Constraints\Characters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerUpdateSettingsType extends AbstractType
{
    private TranslatorServiceInterface $translator;

    public function __construct(TranslatorServiceInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ServerSetting[] $settings */
        $settings = $options['settings'];

        $builder
            ->add('registername', null, [
                'label' => 'Register name',
                'constraints' => [
                    new Characters(['pattern' => $options['registername_pattern']]),
                ],
            ])
            ->add('password', null, [
                'label' => 'Password',
            ])
            ->add('usersperchannel', IntegerNotNegativeType::class, [
                'label' => 'Users per channel',
            ])
            ->add('rememberchannel', BooleanType::class, [
                'label' => 'Remember channel',
            ])
            ->add('defaultchannel', IntegerNotNegativeType::class, [
                'label' => 'Default channel',
            ])
            ->add('registerhostname', null, [
                'label' => 'Register hostname',
            ])
            ->add('registerpassword', null, [
                'label' => 'Register password',
            ])
            ->add('registerurl', null, [
                'label' => 'Register URL',
            ])
            ->add('certrequired', BooleanType::class, [
                'label' => 'Certificate required',
            ])
            ->add('welcometext', WelcomeTextType::class, [
                'label' => 'welcome_text',
            ])
            ->add('new_certificate', CertificateType::class, [
                'label' => 'Add certificate'
            ])
        ;

        if (isset($settings['suggestpositional'])) {
            $builder->add('suggestpositional', BooleanType::class, [
                'label' => 'Suggest positional',
            ]);
        }

        if (isset($settings['suggestpushtotalk'])) {
            $builder->add('suggestpushtotalk', BooleanType::class, [
                'label' => 'Suggest push-to-talk',
            ]);
        }

        if (isset($settings['host'])) {
            $builder
                ->add('host', null,[
                    'label' => 'Host',
                ])
                ->add('port', PortType::class, [
                    'label' => 'Port',
                ])
                ->add('timeout', IntegerNotNegativeType::class, [
                    'label' => 'Timeout',
                ])
                ->add('bandwidth', IntegerNotNegativeType::class, [
                    'label' => 'Bandwidth',
                ])
                ->add('users', IntegerNotNegativeType::class, [
                    'label' => 'Users',
                ])
                ->add('username', null, [
                    'label' => 'User name',
                ])
                ->add('channelname', null, [
                    'label' => 'Channel name',
                ])
                ->add('textmessagelength', IntegerNotNegativeType::class, [
                    'label' => 'Text message length',
                ])
                ->add('imagemessagelength', IntegerNotNegativeType::class, [
                    'label' => 'Image message length',
                ])
                ->add('allowhtml', BooleanType::class, [
                    'label' => 'Allow HTML',
                ])
                ->add('bonjour', BooleanType::class, [
                    'label' => 'Bonjour',
                ])
            ;

            if (isset($settings['opusthreshold'])) {
                $builder->add('opusthreshold', IntegerNotNegativeType::class, [
                    'label' => 'Opus threshold',
                ]);
            }

            if (isset($settings['channelnestinglimit'])) {
                $builder->add('channelnestinglimit', IntegerNotNegativeType::class, [
                    'label' => 'Channel nesting limit',
                ]);
            }
        }

        // Add server settings data to forms
        foreach ($settings as $setting) {
            $key = $setting->getKey();
            $builder->get($key)->setData($setting->getCustomValue());
        }

        $builder->add('submit', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        // Add custom variables on the FormView
        /** @var ServerSetting $parameter */
        foreach ($options['settings'] as $parameter) {

            $children = $view->offsetGet($parameter->getKey());

            $children->vars['output'] = $parameter->getValue();
            $children->vars['modified'] = $parameter->isModified();
            $children->vars['role'] = $parameter->getRole();

            // Setup boolean settings
            if (isset($children->vars['choices'])) {
                if ('true' === $parameter->getValue()) {
                    $children->vars['output'] = $this->translator->trans('enabled', [], 'settings');
                } elseif ('false' === $parameter->getValue()) {
                    $children->vars['output'] = $this->translator->trans('disabled', [], 'settings');
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'settings' => [],
            'translation_domain' => false,
            'required' => false,
            'registername_pattern' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'app_settings_page';
    }
}
