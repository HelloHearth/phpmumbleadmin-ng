<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\Type\AdminRoleChoiceType;
use App\Infrastructure\Symfony\Form\Type\RepeatedPasswordType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditAdminType extends AbstractType
{
    public const BLOCK_PREFIX = 'edit_admin';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('login')
            ->add('email')
            ->add('role', AdminRoleChoiceType::class)
            ->add('password', RepeatedPasswordType::class, [
                'label' => 'change_password',
                'required' => false,
                'block_prefix' => 'app_edit_password',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'edit',
                'translation_domain' => 'Form'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EditAdminData::class,
            'translation_domain' => 'admins'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
