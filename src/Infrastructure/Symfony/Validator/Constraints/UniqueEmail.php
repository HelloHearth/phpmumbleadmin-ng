<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Check if AdminEntity email is unique.
 *
 * Prefer this validator over UniqueEntity, because on entity editing, I
 * encountered some problems, like the email is already exists.
 *
 * @Annotation
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UniqueEmail extends Constraint
{
    public string $message = 'validator_email_exists';
}
