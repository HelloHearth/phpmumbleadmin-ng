<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Constraints;

use App\Domain\Manager\CertificateManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\RuntimeException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class CertificateValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (! $constraint instanceof Certificate) {
            throw new UnexpectedTypeException($constraint, Certificate::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (! \is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        if (! \extension_loaded('openssl')) {
            throw new RuntimeException(CertificateManager::MESSAGE_OPENSSL_MODULE_IS_MISSING);
        }

        $certificateManager = new CertificateManager($value);

        if (! $certificateManager->verifyCertificateWithPrivateKey()) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
