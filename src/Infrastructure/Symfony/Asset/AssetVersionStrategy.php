<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Asset;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Asset version based on the md5 of the file
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AssetVersionStrategy implements VersionStrategyInterface
{
    private ParameterBagInterface $parameters;

    public function __construct(ParameterBagInterface $parameters)
    {
        $this->parameters = $parameters;
    }

    public function getVersion($path): string
    {
        $path = $this->getRealpath($path);

        if (\is_file($path)) {
            return \md5_file($path);
        }

        return '';
    }

    public function applyVersion($path): string
    {
        return $path.'?'.$this->getVersion($path);
    }

    /*
     * Get the real asset path in any situation
     */
    private function getRealpath(string $path): string
    {
        $root = $this->parameters->get('kernel.project_dir');
        $realpath = \realpath($root.'/public/'.$path);

        if (\is_string($realpath)) {
            return $realpath;
        }

        return '';
    }
}
