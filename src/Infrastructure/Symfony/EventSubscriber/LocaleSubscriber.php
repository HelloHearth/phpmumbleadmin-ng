<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Infrastructure\Symfony\EventSubscriber;

use App\Infrastructure\Manager\OptionsCookieManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Setup the custom locale of user stored in the OptionsCookie
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LocaleSubscriber implements EventSubscriberInterface
{
    private OptionsCookieManager $optionsCookieManager;

    public function __construct(OptionsCookieManager $optionsCookieManager)
    {
        $this->optionsCookieManager = $optionsCookieManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 20]]
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $cookie = $this->optionsCookieManager->getCookie();

        if (\is_null($cookie->getLocale())) {
            return;
        }

        $request = $event->getRequest();
        $request->setLocale($cookie->getLocale());
    }
}
