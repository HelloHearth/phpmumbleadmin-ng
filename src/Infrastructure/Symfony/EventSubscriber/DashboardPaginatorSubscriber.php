<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\EventSubscriber;

use App\Domain\Action\Dashboard\Dashboard\ServerDataSort;
use App\Domain\Action\Dashboard\Dashboard\ServerData;
use App\Infrastructure\Service\RequestService;
use Knp\Component\Pager\Event\ItemsEvent;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DashboardPaginatorSubscriber implements EventSubscriberInterface
{
    private RequestService $requestService;

    public function __construct(RequestService $requestService)
    {
        $this->requestService = $requestService;
    }

    public function items(ItemsEvent $event): void
    {
        // Check if the result has already been sorted by another sort subscriber
        $customPaginationParameters = $event->getCustomPaginationParameters();
        if (! empty($customPaginationParameters['sorted'])) {
            return;
        }

        // We are looking for ServerData only
        if (! \is_array($event->target) || empty($event->target) || ! $event->target[0] instanceof ServerData) {
            return;
        }

        $request = $this->requestService->getMainRequest();

        // We are looking for isBooted field only
        $sortField = $event->options[PaginatorInterface::SORT_FIELD_PARAMETER_NAME];
        if ($request->query->get($sortField) !== 'isBooted') {
            return;
        }

        $sortDir = $event->options[PaginatorInterface::SORT_DIRECTION_PARAMETER_NAME];
        $direction = $request->query->get($sortDir, 'asc');

        $sort = new ServerDataSort($direction);
        $sort->sortByIsBooted($event->target);

        $event->count = \count($event->target);
        $event->items = \array_slice($event->target, $event->getOffset(), $event->getLimit());
        $event->stopPropagation();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            // increased priority to override any internal
            'knp_pager.items' => ['items', 10],
        ];
    }
}
