<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Manager;

use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Exception\NoPermissionToModifyUserException;
use App\Domain\Manager\AdminManagerInterface;
use App\Domain\Model\AdminInterface;
use App\Domain\Action\Administration\Admins\CreateAdmin\CreateAdminCommand;
use App\Domain\Action\Administration\Admins\EditAdmin\EditAdminCommand;
use App\Entity\AdminEntity;
use App\Infrastructure\Service\AdminPasswordHasherService;
use App\Infrastructure\Service\Role\RoleCheckerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminManager implements AdminManagerInterface
{
    private EntityManagerInterface $em;
    private SecurityServiceInterface $securityService;
    private RoleCheckerService $roleChecker;
    private AdminPasswordHasherService $passwordHasherService;

    private ObjectRepository $repository;

    public function __construct(
        EntityManagerInterface $em,
        SecurityServiceInterface $securityService,
        RoleCheckerService $roleChecker,
        AdminPasswordHasherService $passwordHasherService
    ) {
        $this->em = $em;
        $this->securityService = $securityService;
        $this->roleChecker = $roleChecker;
        $this->passwordHasherService = $passwordHasherService;

        $this->repository = $em->getRepository(AdminEntity::class);
    }

    /**
     * {@inheritDoc}
     */
    public function findAll(): array
    {
        $admins = [];
        $result = $this->repository->findAll();

        foreach ($result as $admin) {
            if (! $this->securityService->canEditRole($admin)) {
                continue;
            }
            if (! $this->roleChecker->isGranted($admin, SecurityServiceInterface::ROLE_ADMIN)) {
                continue;
            }
            $admins[] = $admin;
        }

        return $admins;
    }

    /**
     * {@inheritDoc}
     */
    public function find(int $id): ?AdminInterface
    {
        $admin = $this->repository->find($id);

        if (\is_null($admin)) {
            return null;
        }

        if (! $this->securityService->canEditRole($admin)) {
            throw new NoPermissionToModifyUserException();
        }

        return $admin;
    }

    /**
     * {@inheritDoc}
     */
    public function add(CreateAdminCommand $command): AdminInterface
    {
        $admin = new AdminEntity();

        $hashedPassword = $this->passwordHasherService->hash($admin, $command->password);

        $admin->setUsername($command->login);
        $admin->setEmail($command->email);
        $admin->setRoles([$command->role]);
        $admin->setPassword($hashedPassword);

        if (! $this->securityService->canEditRole($admin)) {
            throw new NoPermissionToModifyUserException();
        }

        $this->em->persist($admin);
        $this->em->flush();

        return $admin;
    }

    /**
     * {@inheritDoc}
     */
    public function edit(EditAdminCommand $command): AdminInterface
    {
        $admin = $command->admin;

        if (! $this->securityService->canEditRole($admin)) {
            throw new NoPermissionToModifyUserException();
        }

        $admin->setUsername($command->login);
        $admin->setEmail($command->email);
        $admin->setRoles([$command->role]);

        if (! is_null($command->password)) {
            $hashedPassword = $this->passwordHasherService->hash($admin, $command->password);
            $admin->setPassword($hashedPassword);
        }

        if (! $this->securityService->canEditRole($admin)) {
            throw new NoPermissionToModifyUserException();
        }

        $this->em->flush();

        return $admin;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(AdminInterface $admin): void
    {
        if (! $this->securityService->canEditRole($admin)) {
            throw new NoPermissionToModifyUserException();
        }

        $this->em->remove($admin);
        $this->em->flush();
    }
}
