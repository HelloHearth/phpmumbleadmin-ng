<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\ClockInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ClockService implements ClockInterface
{
    public function timestamp(): int
    {
        return \time();
    }

    public function getTimezoneDiff(): string
    {
        return \strftime('%z');
    }
}
