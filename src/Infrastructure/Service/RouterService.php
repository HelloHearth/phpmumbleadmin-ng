<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RouterService
{
    private RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /*
     * On serversList selection, we want to be redirected to an equivalence of
     * the current route.
     *
     * Return the current route if the user is in any route of the Server page,
     * otherwise, return the default route.
     */
    public function getNameOnServersListSelection(Request $request): string
    {
        $defaultRoute = 'server_channels';
        $availableRoutes = [$defaultRoute, 'server_settings', 'server_registrations', 'server_bans', 'server_logs'];
        $routesToRedirect = ['server_registration' => 'server_registrations', 'server_ban' => 'server_bans'];

        $routePath = \str_replace($request->getUriForPath(''), '', $request->headers->get('referer'));

        try {
            $currentRoute = $this->router->match($routePath)['_route'];
        } catch (\Exception $e) {
            return $defaultRoute;
        }

        foreach ($availableRoutes as $availableRoute) {
            if ($availableRoute === $currentRoute) {
                return $currentRoute;
            }
        }

        foreach ($routesToRedirect as $key => $routeToRedirect) {
            if (false !== \stripos($currentRoute, $key)) {
                return $routeToRedirect;
            }
        }

        return $defaultRoute;
    }
}
