<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\UserServiceInterface;
use App\Entity\AdminEntity;
use App\Infrastructure\Symfony\Security\MumbleUser;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UserService implements UserServiceInterface
{
    /**
     * @var AdminEntity|MumbleUser|null
     */
    private ?UserInterface $user;

    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    public function getAdminId(): int
    {
        return $this->user->getId();
    }

    public function getUsername(): string
    {
        return $this->user->getUserIdentifier();
    }
}
