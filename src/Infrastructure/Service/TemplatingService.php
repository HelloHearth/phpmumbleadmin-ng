<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\TemplatingServiceInterface;
use Twig\Environment;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TemplatingService implements TemplatingServiceInterface
{
    private Environment $templating;

    public function __construct(Environment $templating)
    {
        $this->templating = $templating;
    }

    public function renderView(string $template, array $parameters = []): string
    {
        return $this->templating->render($template, $parameters);
    }
}
