<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RequestService
{
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function isHttpRequest(): bool
    {
        $request = $this->getMainRequest();
        return ($request instanceof Request && ! $request->isXmlHttpRequest());
    }

    public function isXmlHttpRequest(): bool
    {
        $request = $this->getMainRequest();
        return ($request instanceof Request && $request->isXmlHttpRequest());
    }

    public function getMainRequest(): ?Request
    {
        return $this->requestStack->getMainRequest();
    }

    public function getSession(): ?SessionInterface
    {
        $request = $this->requestStack->getMainRequest();

        if ($request instanceof Request) {
            return $request->getSession();
        }

        return null;
    }
}
