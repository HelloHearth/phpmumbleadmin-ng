<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\FlashMessageServiceInterface;
use App\Domain\Service\TranslatorServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class FlashMessageService implements FlashMessageServiceInterface
{
    private RequestService $requestService;
    private TranslatorServiceInterface $translator;

    public function __construct(RequestService $requestService, TranslatorServiceInterface $translator)
    {
        $this->requestService = $requestService;
        $this->translator = $translator;
    }

    private function send($level, $message, array $transParameters = []): void
    {
        if (\is_null($session = $this->requestService->getSession())) {
            return;
        }

        $message = $this->translator->trans($message, $transParameters, 'Messages');
        $session->getFlashBag()->add($level, $message);
    }

    public function basedOnHttpCode(int $code, $message, array $transParameters = []): void
    {
        switch ($code) {
            case 200:
                $this->success($message, $transParameters);
                break;
            default:
                $this->error($message, $transParameters);
        }
    }

    public function success($message, array $transParameters = []): void
    {
        $this->send(self::SUCCESS, $message, $transParameters);
    }

    public function error($message, array $transParameters = []): void
    {
        $this->send(self::ERROR, $message, $transParameters);
    }

    public function warning($message, array $transParameters = []): void
    {
        $this->send(self::WARNING, $message, $transParameters);
    }
}
