<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Model\PaginatedInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Paginated implements PaginatedInterface
{
    /** @var PaginationInterface $pagination */
    private object $pagination;

    public function setPagination(object $pagination): void
    {
        $this->pagination = $pagination;
    }

    public function getPagination(): object
    {
        return $this->pagination;
    }

    public function getItems(): iterable
    {
        return $this->pagination->getItems();
    }
}
