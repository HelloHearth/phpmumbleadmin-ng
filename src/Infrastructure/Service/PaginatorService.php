<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Model\PaginatedInterface;
use App\Domain\Service\PaginatorServiceInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PaginatorService implements PaginatorServiceInterface
{
    private PaginatorInterface $paginator;

    public function __construct(PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
    }

    public function paginate($target, int $page = 1, int $limit = null): PaginatedInterface
    {
        $pagination = $this->paginator->paginate($target, $page, $limit);

        $paginated = new Paginated();
        $paginated->setPagination($pagination);

        return $paginated;
    }
}
