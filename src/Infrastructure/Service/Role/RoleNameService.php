<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service\Role;

use App\Domain\Service\Role\RoleNameServiceInterface;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Model\AdminInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RoleNameService implements RoleNameServiceInterface
{
    private RoleCheckerService $roleChecker;

    public function __construct(RoleCheckerService $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }

    public function find(AdminInterface $user): string
    {
        if ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_SUPER_ADMIN)) {
            $roleName = self::SUPER_ADMIN;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_ROOT_ADMIN)) {
            $roleName =  self::ROOT_ADMIN;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_ADMIN)) {
            $roleName = self::ADMIN;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_SUPER_USER)) {
            $roleName = self::SUPER_USER;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_SUPER_USER_RU)) {
            $roleName = self::SUPER_USER_RU;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_MUMBLE_USER)) {
            $roleName = self::MUMBLE_USER;
        } else {
            $roleName = self::INVALID;
        }

        return $roleName;
    }
}
