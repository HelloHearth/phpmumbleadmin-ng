<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\I18DateServiceInterface;
use App\Domain\Service\LocaleServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class I18DateService implements I18DateServiceInterface
{
    private string $locale;

    public function __construct(LocaleServiceInterface $localeService)
    {
        $this->locale = $localeService->getLocale();
    }

    /**
     * {@inheritDoc}
     */
    public function logDate(int $timestamp): string
    {
        $intlFormatter = new \IntlDateFormatter($this->locale, \IntlDateFormatter::LONG, \IntlDateFormatter::NONE);
        return $intlFormatter->format($timestamp);
    }

    /**
     * {@inheritDoc}
     */
    public function logTime(int $timestamp): string
    {
        $intlFormatter = new \IntlDateFormatter($this->locale, \IntlDateFormatter::NONE, \IntlDateFormatter::MEDIUM);
        return $intlFormatter->format($timestamp);
    }
}
