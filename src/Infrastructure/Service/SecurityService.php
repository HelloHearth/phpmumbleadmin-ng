<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Symfony\Security\Voter;
use Symfony\Component\Security\Core\Security;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SecurityService implements SecurityServiceInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function isGranted($attributes, $subject = null): bool
    {
        return $this->security->isGranted($attributes, $subject);
    }

    public function isGrantedRootAdmin($subject = null): bool
    {
        return $this->security->isGranted(self::ROLE_ROOT_ADMIN, $subject);
    }

    public function isGrantedAdmin($subject = null): bool
    {
        return $this->security->isGranted(self::ROLE_ADMIN, $subject);
    }

    public function isGrantedSuperUser($subject = null): bool
    {
        return $this->security->isGranted(self::ROLE_SUPER_USER, $subject);
    }

    public function isGrantedSuperUserRu($subject = null): bool
    {
        return $this->security->isGranted(self::ROLE_SUPER_USER_RU, $subject);
    }

    public function isGrantedMumbleUser($subject = null): bool
    {
        return $this->security->isGranted(self::ROLE_MUMBLE_USER, $subject);
    }

    public function isRoleSuperUserRuOrLess($subject = null): bool
    {
        $user = $this->security->getUser();
        return (
            \is_null($user) ||
            \in_array(self::ROLE_SUPER_USER_RU, $user->getRoles()) ||
            ! $this->security->isGranted(self::ROLE_SUPER_USER_RU, $subject)
        );
    }

    public function canAccessToAdministration($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanAccessToAdministrationVoter::KEY, $subject);
    }

    public function canAccessToDashboard($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanAccessToDashboardVoter::KEY, $subject);
    }

    public function canAccessToRegistration($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanAccessToRegistrationVoter::KEY, $subject);
    }

    public function canEditMurmurSettingTypeAdministration($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanEditMurmurSettingTypeAdministrationVoter::KEY, $subject);
    }

    public function canEditRole($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanEditRoleVoter::KEY, $subject);
    }

    public function canEditServer($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanEditServerVoter::KEY, $subject);
    }

    public function canModifyRegistrationPassword($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanModifyRegistrationPasswordVoter::KEY, $subject);
    }

    public function canModifyRegistrationUsername($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanModifyRegistrationUsernameVoter::KEY, $subject);
    }

    public function canModifyMumbleUsernameOnChannelTree($subject = null): bool
    {
        return $this->security->isGranted(Voter\CanModifyMumbleUserNameOnChannelTreeVoter::KEY, $subject);
    }
}
