<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\LocaleServiceInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Get the App locale from the Request or fallback with the locale parameter
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LocaleService implements LocaleServiceInterface
{
    private string $locale;

    public function __construct(RequestService $requestService, ParameterBagInterface $parameterBag)
    {
        $request = $requestService->getMainRequest();

        if ($request instanceof Request) {
            $this->setLocale($request->getLocale());
            return;
        }

        if ($parameterBag->has('kernel.default_locale')) {
            $this->setLocale($parameterBag->get('kernel.default_locale'));
            return;
        }

        $this->setLocale('en');
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    private function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }
}
