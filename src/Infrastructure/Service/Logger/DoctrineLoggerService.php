<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service\Logger;

use App\Domain\Service\ClockInterface;
use App\Domain\Service\LoggerServiceInterface;
use App\Entity\AdminLogEntity;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DoctrineLoggerService extends AbstractLoggerService
{
    private EntityManagerInterface $entityManager;

    public function __construct(ClockInterface $clock, EntityManagerInterface $entityManager)
    {
        parent::__construct($clock);
        $this->entityManager = $entityManager;
    }

    public function emergency(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_EMERGENCY, $message, $context);
    }

    public function alert(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_ALERT, $message, $context);
    }

    public function critical(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_CRITICAL, $message, $context);
    }

    public function error(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_ERROR, $message, $context);
    }

    public function warning(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_WARNING, $message, $context);
    }

    public function notice(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_NOTICE, $message, $context);
    }

    public function info(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_INFORMATIONAL, $message, $context);
    }

    public function debug(int $facility, string $message, array $context = []): void
    {
        $this->log($facility, LoggerServiceInterface::LEVEL_DEBUG, $message, $context);
    }

    public function log(int $facility, int $level, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, $level, $message, $context);

        $entity = new AdminLogEntity();

        $entity->setFacility($log->getFacility());
        $entity->setLevel($log->getLevel());
        $entity->setMessage($log->getMessage());
        $entity->setContext($log->getContext());
        $entity->setAddress($log->getAddress());
        $entity->setTimestamp($log->getTimestamp());

        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);
    }
}
