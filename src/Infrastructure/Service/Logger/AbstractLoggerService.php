<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service\Logger;

use App\Domain\Model\AdminLog;
use App\Domain\Service\ClockInterface;
use App\Domain\Service\LoggerServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractLoggerService implements LoggerServiceInterface
{
    private ClockInterface $clock;

    public function __construct(ClockInterface $clock)
    {
        $this->clock = $clock;
    }

    protected function createAdminLog(int $facility, int $level, string $message, array $context): AdminLog
    {
        $log = new AdminLog();

        $log->setFacility($facility);
        $log->setLevel($level);
        $log->setMessage($message);
        $log->setContext($context);
        $log->setAddress($_SERVER['REMOTE_ADDR'] ?? '');
        $log->setTimestamp($this->clock->timestamp());

        return $log;
    }
}
