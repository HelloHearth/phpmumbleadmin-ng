<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Service\SystemCheckerServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SystemCheckerService implements SystemCheckerServiceInterface
{
    public function extension_ssl_is_loaded(): bool
    {
        return \extension_loaded('openssl');
    }

    public function upload_file_is_allowed(): bool
    {
        return '1' === \ini_get('file_uploads');
    }
}
