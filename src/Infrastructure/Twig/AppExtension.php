<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Twig;

use App\Domain\Service\LocaleServiceInterface;
use App\Domain\Helper\HtmlHelper;
use App\Infrastructure\Menu\LocalesMenuBuilder;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AppExtension extends AbstractExtension implements GlobalsInterface
{
    private LocaleServiceInterface $localeService;
    private LocalesMenuBuilder $localesMenuBuilder;

    public function __construct(LocaleServiceInterface $localeService, LocalesMenuBuilder $localesMenuBuilder)
    {
        $this->localeService = $localeService;
        $this->localesMenuBuilder = $localesMenuBuilder;
    }

    /**
     * {@inheritDoc}
     *
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('raw_without_javascript', [$this, 'RawWithoutJavascript'], ['is_safe' => ['html']])
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @return TwigTest[]
     */
    public function getTests(): array
    {
        return [
            new TwigTest('date', [$this, 'testIsDate'])
        ];
    }

    public function getGlobals(): array
    {
        return [
            'lang' => $this->localeService->getLocale(),
            'localesMenu' => $this->localesMenuBuilder->build()
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_servers_list', [ServersList::class, 'getServersList']),
            new TwigFunction('get_servers_list_cache_uptime', [ServersList::class, 'getCacheUptime']),
        ];
    }

    public function RawWithoutJavascript(?string $html): string
    {
        return HtmlHelper::RawWithoutJavascript($html);
    }

    public function testIsDate($argument): bool
    {
        return ($argument instanceof \DateTime);
    }
}
