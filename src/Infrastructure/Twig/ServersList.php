<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Twig;

use App\Domain\Murmur\Service\ServersListServiceInterface;
use Twig\Extension\RuntimeExtensionInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServersList implements RuntimeExtensionInterface
{
    private ServersListServiceInterface $serversListService;

    public function __construct(ServersListServiceInterface $serversListService)
    {
        $this->serversListService = $serversListService;
    }

    public function getServersList(): array
    {
        return $this->serversListService->getList();
    }

    public function getCacheUptime(): string
    {
        return $this->serversListService->getCacheUptime();
    }
}
