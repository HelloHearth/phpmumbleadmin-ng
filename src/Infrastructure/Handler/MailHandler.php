<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Handler;

use App\Domain\Service\TranslatorServiceInterface;
use App\Entity\ForgottenPassword;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MailHandler
{
    private MailerInterface $mailer;
    private ParameterBagInterface $parameterBag;
    private TranslatorServiceInterface $translator;

    public function __construct(MailerInterface $mailer, ParameterBagInterface $parameterBag, TranslatorServiceInterface $translator)
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->translator = $translator;
    }

    public function sendForgottenPassword(ForgottenPassword $forgottenPassword): void
    {
        $admin = $forgottenPassword->getAdmin();

        $email = $this->parameterBag->get('app.from_email_automated_sent');
        $name = 'No reply - '.$this->parameterBag->get('app.name');
        $from = new Address($email, $name);

        $email = new TemplatedEmail();
        $email->to($admin->getEmail());
        $email->from($from);
        $email->subject($this->translator->trans('email_title', [], 'forgotten_password'));
        $email->htmlTemplate('Mail/forgotten_password.html.twig');
        $email->context([
            'hash' => $forgottenPassword->getHash()
        ]);

        $this->mailer->send($email);
    }
}
