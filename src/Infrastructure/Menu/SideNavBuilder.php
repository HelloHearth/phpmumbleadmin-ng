<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Menu;

use App\Domain\Service\SecurityServiceInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SideNavBuilder extends AbstractNavBuilder
{
    private FactoryInterface $factory;
    private RouterInterface $router;
    private SecurityServiceInterface $security;

    public function __construct(
        FactoryInterface $factory,
        RouterInterface $router,
        SecurityServiceInterface $securityService,
        RequestStack $requestStack
    ) {
        $this->factory = $factory;
        $this->router = $router;
        $this->security = $securityService;

        parent::__construct($requestStack);
    }

    public function build(): ItemInterface
    {
        $currentRoute = $this->getCurrentRoute();

        $menu = $this->factory->createItem('sidenav');
        $menu->setExtra('translation_domain', 'side-nav');

        if ($this->security->canAccessToDashboard()) {

            $menu->addChild('dashboard', [
                'uri' => $this->router->generate('dashboard'),
                'current' => ($currentRoute === 'dashboard'),
                'extras' => ['icon' => 'fas fa-tachometer-alt']
            ]);
        }

        if ($this->security->canAccessToAdministration()) {

            $administration = $menu->addChild('administration', [
                'uri' => 'collapse',
                'extras' => ['icon' => 'fas fa-sliders-h']
            ]);

            $administration->addChild('admins', [
                'uri' => $this->router->generate('administration_admins'),
                'current' => (false !== \stripos($currentRoute, 'administration_admin')),
            ]);

            $administration->addChild('logs', [
                'uri' => $this->router->generate('administration_logs'),
                'current' => ($currentRoute === 'administration_logs')
            ]);
        }

        return $menu;
    }
}
