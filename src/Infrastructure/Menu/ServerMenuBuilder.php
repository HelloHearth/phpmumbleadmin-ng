<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Menu;

use App\Domain\Service\SecurityServiceInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ServerMenuBuilder extends AbstractNavBuilder
{
    private FactoryInterface $factory;
    private RouterInterface $router;
    private SecurityServiceInterface $security;

    public function __construct(
        RequestStack $requestStack,
        FactoryInterface $factory,
        RouterInterface $router,
        SecurityServiceInterface $securityService
    ) {
        $this->factory = $factory;
        $this->router = $router;
        $this->security = $securityService;

        parent::__construct($requestStack);
    }

    public function build(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('serverMenu');
        $menu->setExtra('translation_domain', 'server-menu');

        $currentRoute = $this->getCurrentRoute();

        if (! \is_null($options['connectionUri'])) {
            $menu->addChild('connection_button', [
                'attributes' => ['class' => 'connection-button'],
                'extras' => [
                    'block' => [
                        'name' => 'connection_button',
                        'connectionUri' => $options['connectionUri']
                    ]
                ]
            ]);
        }

        $menu->addChild('status_button', [
            'extras' => [
                'block' => [
                    'name' => 'server_status_button',
                    'serverId' => $options['serverId'],
                    'isRunning' => $options['isRunning']
                ]
            ]
        ]);

        if ($this->security->canEditServer()) {

            $pages = $menu->addChild('pages', [
                'label' => false
            ]);

            $pages->addChild('channels', [
                'uri' => $this->router->generate('server_channels', ['serverId' => $options['serverId']]),
                'current' => ($currentRoute === 'server_channels')
            ]);

            $pages->addChild('settings', [
                'uri' => $this->router->generate('server_settings', ['serverId' => $options['serverId']]),
                'current' => ($currentRoute === 'server_settings')
            ]);

            $pages->addChild('registrations', [
                'uri' => $this->router->generate('server_registrations', ['serverId' => $options['serverId']]),
                'current' => (false !== \strpos($currentRoute, 'server_registration'))
            ]);

            $pages->addChild('bans', [
                'uri' => $this->router->generate('server_bans', ['serverId' => $options['serverId']]),
                'current' => ($currentRoute === 'server_bans')
            ]);

            $pages->addChild('logs', [
                'uri' => $this->router->generate('server_logs', ['serverId' => $options['serverId']]),
                'current' => ($currentRoute === 'server_logs')
            ]);
        }

        return $menu;
    }
}
