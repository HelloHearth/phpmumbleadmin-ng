<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Menu;

use App\Domain\Service\LocaleServiceInterface;
use App\Domain\Model\Flag;

/**
 * The menu return an array of locales, where the first entry is the selected locale
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LocalesMenuBuilder
{
    private LocaleServiceInterface $localeService;

    public function __construct(LocaleServiceInterface $localeService)
    {
        $this->localeService = $localeService;
    }

    /**
     * @return Flag[]
     */
    public function build(): array
    {
        $currentLocale = $this->localeService->getLocale();
        return $this->getFlags($currentLocale);
    }

    /**
     * @return Flag[]
     */
    private function getFlags(string $selected): array
    {
        $flags = [];
        $flags[] = new Flag('en', 'English', 'assets/img/flags/gb.png');
        $flags[] = new Flag('fr', 'French', 'assets/img/flags/fr.png');

        // Find and mark the selected flag, and remove it from the flags bag
        foreach ($flags as $key => $flag) {
            if ($flag->id === $selected) {
                $flag->selected = true;
                unset($flags[$key]);
                break;
            }
        }

        // Add to the top of the flags bag the selected flag
        \array_unshift($flags, $flag);

        return $flags;
    }
}
