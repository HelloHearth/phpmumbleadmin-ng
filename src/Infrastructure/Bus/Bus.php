<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Bus;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\HandlerInterface;
use App\Domain\Service\BusMiddlewareInterface;
use App\Domain\Service\BusServiceInterface;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Middleware;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Bus implements BusServiceInterface
{
    private CommandHandlerMiddleware $handlerMiddleware;

    /** @var Middleware[] */
    private array $middlewares = [];

    public function __construct(HandlerInterface $handler)
    {
        $locator = new InMemoryLocator();
        $locator->addHandler($handler, $handler->listenTo());

        $this->handlerMiddleware = new CommandHandlerMiddleware(
            new ClassNameExtractor(), $locator, new HandleInflector()
        );
    }

    public function addMiddleWare(BusMiddlewareInterface $middleware): void
    {
        $this->middlewares[] = new BusMiddleware($middleware);
    }

    public function handle($action): BusResponse
    {
        $this->middlewares[] = $this->handlerMiddleware;
        $bus = new CommandBus($this->middlewares);

        return $bus->handle($action);
    }
}
