<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Bus;

use App\Domain\Bus\CommandHandler;
use App\Domain\Bus\EventDispatcherBusMiddleware;
use App\Domain\Bus\EventTranslatorBusMiddleware;
use App\Domain\Bus\QueryHandler;
use App\Domain\Service\BusServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BusFactory
{
    private EventDispatcherBusMiddleware $eventDispatcher;
    private EventTranslatorBusMiddleware $eventTranslator;

    public function __construct(EventDispatcherBusMiddleware $eventDispatcher, EventTranslatorBusMiddleware $eventTranslator)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->eventTranslator = $eventTranslator;
    }

    public function buildForCommand(CommandHandler $handler): BusServiceInterface
    {
        $bus = new Bus($handler);
        $bus->addMiddleWare($this->eventTranslator);
        $bus->addMiddleWare($this->eventDispatcher);
        return $bus;
    }

    public function buildForQuery(QueryHandler $handler): BusServiceInterface
    {
        $bus = new Bus($handler);
        $bus->addMiddleWare($this->eventDispatcher);
        return $bus;
    }
}
