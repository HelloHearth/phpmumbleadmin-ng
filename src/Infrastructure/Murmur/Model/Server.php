<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Model;

use App\Domain\Murmur\Exception\Server as ServerException;
use App\Domain\Murmur\Helper\ServerHelper;
use App\Domain\Murmur\Model\AbstractServer;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Server extends AbstractServer
{
    // Murmur_Server proxy interface.
    private object $proxy;

    public function __construct(object $proxy, array $defaultConf)
    {
        $this->proxy = $proxy;
        $this->defaultConf = $defaultConf;
    }

    public function __toString(): string
    {
        return $this->proxy->__toString();
    }

    public function getSid(): int
    {
        return ServerHelper::proxyToId($this->__toString());
    }

    /// ////////////////////////////////////////////
    ///
    /// Murmur_Server methods implementation
    ///
    /// ///////////////////////////////////////////

    /**
     * {@inheritDoc}
     */
    public function addChannel(string $name, int $parent): int
    {
        try {
            return $this->proxy->addChannel($name, $parent);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 2')) {
                throw new ServerException\InvalidChannelException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function delete(): void
    {
        try {
            $this->proxy->delete();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getACL(int $channelId, &$aclList, &$groupList, &$inherit): void
    {
        try {
            $this->proxy->getACL($channelId, $aclList, $groupList, $inherit);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAllConf(): array
    {
        try {
            return $this->proxy->getAllConf();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getCustomConf(): array
    {
        return $this->getAllConf();
    }

    /**
     * {@inheritDoc}
     */
    public function getBans(): array
    {
        try {
            return $this->proxy->getBans();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getCertificateList(int $sessionId): array
    {
        try {
            return $this->proxy->getCertificateList($sessionId);
        } catch (\InvalidArgumentException $e) {
            throw new ServerException\InvalidSessionException();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getChannelState(int $channelId): object
    {
        try {
            return $this->proxy->getChannelState($channelId);
        } catch (\InvalidArgumentException $e) {
            throw new ServerException\InvalidChannelException();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getChannels(): array
    {
        try {
            return $this->proxy->getChannels();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getConf(string $key): string
    {
        try {
            return $this->proxy->getConf($key);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getLog(int $first, int $last): array
    {
        try {
            return $this->proxy->getLog($first, $last);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getLogLen(): int
    {
        try {
            return $this->proxy->getLogLen();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getRegisteredUsers(string $filter): array
    {
        try {
            return $this->proxy->getRegisteredUsers($filter);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getRegistration(int $userId): array
    {
        try {
            return $this->proxy->getRegistration($userId);
        } catch (\InvalidArgumentException $e) {
            throw new ServerException\InvalidUserException();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getState(int $sessionId): object
    {
        try {
            return $this->proxy->getState($sessionId);
        } catch (\InvalidArgumentException $e) {
            throw new ServerException\InvalidSessionException();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getTexture(int $userId): array
    {
        try {
            return $this->proxy->getTexture($userId);
        } catch (\InvalidArgumentException $e) {
            throw new ServerException\InvalidUserException();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getTree(): object
    {
        try {
            return $this->proxy->getTree();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getUptime(): int
    {
        try {
            return $this->proxy->getUptime();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getUsers(): array
    {
        try {
            return $this->proxy->getUsers();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function hasPermission(int $sessionId, int $channelId, int $permission): bool
    {
        try {
            return $this->proxy->hasPermission($sessionId, $channelId, $permission);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 1')) {
                throw new ServerException\InvalidSessionException();
            }
            if (false !== strpos($e->getMessage(), 'argument 2')) {
                throw new ServerException\InvalidChannelException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function id(): int
    {
        try {
            return $this->proxy->id();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function isRunning(): bool
    {
        try {
            return $this->proxy->isRunning();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function kickUser(int $sessionId, string $reason): void
    {
        try {
            $this->proxy->kickUser($sessionId, $reason);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function registerUser(array $userInfoMap): int
    {
        try {
            return $this->proxy->registerUser($userInfoMap);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function removeChannel(int $channelId): void
    {
        try {
            $this->proxy->removeChannel($channelId);
        } catch (\InvalidArgumentException $e) {
            throw new ServerException\InvalidChannelException();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function sendMessage(int $sessionId, string $text): void
    {
        try {
            $this->proxy->sendMessage($sessionId, $text);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 1')) {
                throw new ServerException\InvalidSessionException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function sendMessageChannel(int $channelId, bool $tree, string $text): void
    {
        try {
            $this->proxy->sendMessageChannel($channelId, $tree, $text);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 1')) {
                throw new ServerException\InvalidChannelException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setACL(int $channelId, array $aclList, array $aclGroup, bool $inherit): void
    {
        try {
            $this->proxy->setACL($channelId, $aclList, $aclGroup, $inherit);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 1')) {
                throw new ServerException\InvalidChannelException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setBans(array $banList): void
    {
        try {
            $this->proxy->setBans($banList);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setChannelState(object $channel): void
    {
        try {
            $this->proxy->setChannelState($channel);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setConf(string $key, string $value): void
    {
        try {
            $this->proxy->setConf($key, $value);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setState(object $user): void
    {
        try {
            $this->proxy->setState($user);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setTexture(int $userId, array $texture): void
    {
        try {
            $this->proxy->setTexture($userId, $texture);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 1')) {
                throw new ServerException\InvalidUserException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function start(): void
    {
        try {
            $this->proxy->start();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function stop(): void
    {
        try {
            $this->proxy->stop();
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function unregisterUser(int $userId): void
    {
        try {
            $this->proxy->unregisterUser($userId);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 1')) {
                throw new ServerException\InvalidUserException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function updateRegistration(int $userId, array  $userInfoMap): void
    {
        try {
            $this->proxy->updateRegistration($userId, $userInfoMap);
        } catch (\InvalidArgumentException $e) {
            if (false !== strpos($e->getMessage(), 'argument 1')) {
                throw new ServerException\InvalidUserException();
            }
            throw $e;
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function verifyPassword(string $name, string $password): int
    {
        try {
            return $this->proxy->verifyPassword($name, $password);
        } catch (\Exception $e) {
            throw $this->exceptionFactory($e);
        }
    }
}
