<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Model;

use App\Domain\Murmur\Model\AbstractMeta;
use App\Domain\Murmur\Model\ServerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Meta extends AbstractMeta
{
    // \Murmur\Meta proxy interface.
    private object $meta;

    // Secret context to dialogue with the daemon
    private array $secret;

    // Murmur daemon default configuration
    private array $defaultConf;

    public function __construct(object $meta, string $secret)
    {
        $this->meta = $meta;
        $this->secret = ['secret' => $secret];
    }

    /**
     * Adding secret context to a \Murmur\Server proxy object, and return
     * a Server object.
     */
    private function createServer(object $proxy): ServerInterface
    {
        $proxy = $proxy->ice_context($this->secret);
        return new Server($proxy, $this->defaultConf);
    }

    /**
     * {@inheritDoc}
     */
    public function getAllServers(): array
    {
        $servers = $this->meta->getAllServers();

        foreach ($servers as &$proxy) {
            $proxy = $this->createServer($proxy);
        }

        return $servers;
    }

    /**
     * {@inheritDoc}
     */
    public function getBootedServers(): array
    {
        $booted = $this->meta->getBootedServers();

        foreach ($booted as &$proxy) {
            $proxy = $this->createServer($proxy);
        }

        return $booted;
    }

    public function newServer(): ?ServerInterface
    {
        $proxy = $this->meta->newServer();

        if (\is_null($proxy)) {
            return null;
        }

        return $this->createServer($proxy);
    }

    public function getServer(int $id): ?ServerInterface
    {
        $proxy = $this->meta->getServer($id);

        if (\is_null($proxy)) {
            return null;
        }

        return $this->createServer($proxy);
    }

    public function getUptime(): int
    {
        return $this->meta->getUptime();
    }

    public function getDefaultConf(): array
    {
        if (! isset($this->defaultConf)) {
            $this->defaultConf = $this->meta->getDefaultConf();
        }
        return $this->defaultConf;
    }

    public function getVersion(&$major, &$minor, &$patch, &$text): void
    {
        $this->meta->getVersion($major, $minor, $patch, $text);
    }

    public function getSliceChecksums(): array
    {
        return $this->meta->getSliceChecksums();
    }
}
