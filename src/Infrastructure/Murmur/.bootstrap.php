<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

use App\Infrastructure\Murmur\Helper\IceHelper;

/*
 * Load Ice.php and Murmur slice definitions in the global scope.
 *
 * It's always a problem to load Ice.php inside a class because of Ice
 * global variables.
 * This permit to solve a lot of problems, in unit tests also.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
if (\extension_loaded('ice')) {

    /*
     * Workaround:
     *
     * See:
     * https://github.com/zeroc-ice/ice/issues/412
     * https://github.com/zeroc-ice/ice/commit/e93a84339e2e7fd6b62c65adfad075319f60028b
     * Probably fixed in zeroc-ice 3.7.3 (I said probably, because I didn't
     * tested with the version).
     * Tested with zeroc-ice 3.7.5, the bug have been fixed
     *
     * Loading the Ice.php (on tests) can produce this error:
     * "IcePHP_defineSequence() expects parameter 2 to be object, null given in /usr/share/php/Ice/BuiltinSequences.php on line 113"
     * Fix the error with a workaround, by adding in the file "/usr/share/php/Ice.php" on line 118 :
     */
    $iceVersion = IceHelper::getIceVersion();

    if (\version_compare($iceVersion, '3.7', '>=') && \version_compare($iceVersion, '3.7.3', '<')) {
        global $Ice__t_Value;
        global $Ice__t_ObjectSeq;
        global $Ice__t_LocalObject;
        global $Ice__t_ObjectPrx;
        global $Ice__t_ObjectProxySeq;
    }
    // End of the workaround

    /*
     * Load Ice.php and Murmur slice definitions
     */
    if (false !== $icePath = \stream_resolve_include_path('Ice.php')) {

        require_once $icePath;

        $sliceVersion = $_SERVER['MURMUR_SLICE_VERSION'] ?? '';
        $sliceFile = IceHelper::getSlicePath($sliceVersion);

        if (\is_string($sliceFile)) {
            require_once $sliceFile;
        }
    }
}
