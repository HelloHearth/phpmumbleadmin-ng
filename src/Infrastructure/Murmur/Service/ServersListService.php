<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Service;

use App\Domain\Helper\HtmlHelper;
use App\Domain\Helper\StringHelper;
use App\Domain\Murmur\Connection\ConnectionHandlerInterface;
use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\MetaInterface;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Murmur\Model\ServersListCache;
use App\Domain\Murmur\Service\ServersListServiceInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ServersListService implements ServersListServiceInterface
{
    public const CACHE_KEY = 'servers';

    private ConnectionHandlerInterface $connection;
    private CacheItemPoolInterface $cacheItemPool;

    public function __construct(ConnectionHandlerInterface $connection, CacheItemPoolInterface $cacheItemPool)
    {
        $this->connection = $connection;
        $this->cacheItemPool = $cacheItemPool;
    }

    /** @param ServerInterface[] $servers */
    private function createServersList(array $servers): array
    {
        $list = [];

        foreach ($servers as $server) {

            $registerName = $server->getParameter('registername');
            $name = StringHelper::cutLongString($registerName, 40);
            $name = \htmlEntities($name, ENT_QUOTES, 'UTF-8');
            $name = HtmlHelper::spaceToEntity($name);

            $list[] = [
                'id' => $server->getSid(),
                'name' => $name
            ];
        }

        return $list;
    }

    public function getList(): array
    {
        $item = $this->cacheItemPool->getItem(self::CACHE_KEY);

        $serverList = $item->get();

        // On invalid data, refresh the list
        if (! $serverList instanceof ServersListCache || ! $serverList->isValid()) {

            try {
                $meta = $this->connection->connect();
            } catch (ConnectionException $e) {
                $meta = null;
            }

            if ($meta instanceof MetaInterface) {
                $servers = $this->createServersList($meta->getAllServers());
            } else {
                // On connection error, return the old cache of servers lists, or an empty array
                $servers = ($serverList instanceof ServersListCache) ? $serverList->getServers() : [];
            }

            // Create (or recreate) the cache object
            $serverList = new ServersListCache($servers);

            $item->set($serverList);
            $this->cacheItemPool->save($item);
        }

        return $serverList->getServers();
    }

    public function invalidateCache(): void
    {
        $item = $this->cacheItemPool->getItem(self::CACHE_KEY);
        $serverList = $item->get();

        if (! $serverList instanceof ServersListCache) {
            return;
        }

        $serverList->invalidate();
        $item->set($serverList);

        $this->cacheItemPool->save($item);
    }

    public function getCacheUptime(): string
    {
        $serverList = $this->cacheItemPool->getItem(self::CACHE_KEY)->get();

        if (! $serverList instanceof ServersListCache) {
            return '';
        }

        return $serverList->uptime();
    }
}
