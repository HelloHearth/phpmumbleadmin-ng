<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Connection;

use App\Domain\Murmur\Connection\ConnectionHandlerInterface;
use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\MetaInterface;
use App\Infrastructure\Murmur\Helper\IceHelper;

/**
 * Murmur connection
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ConnectionHandler implements ConnectionHandlerInterface
{
    private ?MetaInterface $murmur;

    private string $host;
    private int $port;
    private int $timeout;
    private string $secret;

    public function __construct(string $host, $port, $timeout, string $secret)
    {
        $this->host = $host;
        $this->port = (int) $port;
        $this->timeout = (int) $timeout;
        $this->secret = $secret;
    }

    /**
     * @throws ConnectionException
     */
    public function connect(): ?MetaInterface
    {
        if (! isset($this->murmur)) {
            $interface = IceHelper::connectionFactory();
            $interface->setParameters($this->host, $this->port, $this->timeout, $this->secret);

            $this->murmur = $interface->connection();
        }

        return $this->murmur;
    }
}
