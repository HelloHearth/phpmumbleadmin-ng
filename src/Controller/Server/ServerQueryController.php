<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server;

use App\Domain\Murmur\Model\MetaInterface;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Murmur\Service\MurmurConnectionUriServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class ServerQueryController extends ServerController
{
    protected MurmurConnectionUriServiceInterface $connectionUriService;

    /**
     * @required
     */
    public function setMurmurConnectionUriService(MurmurConnectionUriServiceInterface $service): void
    {
        $this->connectionUriService = $service;
    }

    public function getConnectionUri(MetaInterface $Murmur, ServerInterface $prx): string
    {
        return $this->connectionUriService->constructUri(
            $this->getUser()->getUsername(),
            $prx->getParameter('password'),
            $prx->getParameter('host'),
            $prx->getParameter('port'),
            $Murmur->getMurmurVersion()['str']
        );
    }
}
