<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Settings;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Settings\ResetCertificate\ResetCertificateCommand;
use App\Domain\Action\Server\Settings\ResetCertificate\ResetCertificateHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ResetCertificateController extends ServerQueryController
{
    private ResetCertificateHandler $handler;

    public function __construct(ResetCertificateHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/settings/cmd/reset_certificate", name="cmd_server_settings_reset_certificate", methods={"GET"})
     */
    public function resetCertificateAction(int $serverId): RedirectResponse
    {
        $command = new ResetCertificateCommand($serverId);

        $commandBus = $this->busFactory->buildForCommand($this->handler);
        $this->addServerMiddlewares($commandBus);

        $commandBus->handle($command);

        return $this->redirectToRoute('server_settings', [
            'serverId' => $serverId,
            '_fragment' => 'tab-certificate',
        ]);
    }
}
