<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Settings;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Settings\SettingsHandler;
use App\Domain\Action\Server\Settings\SettingsQuery;
use App\Domain\Action\Server\Settings\SettingsViewModel;
use App\Domain\Action\Server\Settings\UpdateSettings\UpdateSettingsCommand;
use App\Domain\Action\Server\Settings\UpdateSettings\UpdateSettingsHandler;
use App\Infrastructure\Symfony\Form\ServerUpdateSettingsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SettingsController extends ServerQueryController
{
    private SettingsHandler $querySettingsHandler;
    private UpdateSettingsHandler $updateSettingsHandler;

    public function __construct(
        SettingsHandler $querySettingsHandler,
        UpdateSettingsHandler $updateSettingsHandler
    ) {
        $this->querySettingsHandler = $querySettingsHandler;
        $this->updateSettingsHandler = $updateSettingsHandler;
    }

    /**
     * @Route("/server/{serverId}/settings", name="server_settings", methods={"GET", "POST"})
     */
    public function settingsQueryAction(Request $request, int $serverId): Response
    {
        $viewModel = new SettingsViewModel($serverId);
        $query = new SettingsQuery($serverId, $viewModel);

        $queryBus = $this->busFactory->buildForQuery($this->querySettingsHandler);
        $this->addServerMiddlewares($queryBus);

        $busResponse = $queryBus->handle($query);

        $form = $this->createForm(ServerUpdateSettingsType::class, null, [
            'settings' => $query->serverSettings,
            'registername_pattern' => $query->channelNamePattern,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $command = new UpdateSettingsCommand($serverId, $form->getData());

            $commandBus = $this->busFactory->buildForCommand($this->updateSettingsHandler);
            $this->addServerMiddlewares($commandBus);

            $commandBus->handle($command);

            return $this->redirectToReferer($request);
        }

        if ($viewModel->isRunning()) {
            $viewModel->setConnectionUri($this->getConnectionUri($query->Murmur, $query->prx));
        }

        $response = $this->render('Page/Server/Settings/settings.html.twig', [
            'viewModel' => $viewModel,
            'form' => $form->createView(),
        ]);
        return $response->setStatusCode($this->getStatusCode($busResponse));
    }
}
