<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Bans;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Bans\Index\BansHandler;
use App\Domain\Action\Server\Bans\Index\BansQuery;
use App\Domain\Action\Server\Bans\Index\BansViewModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BansController extends ServerQueryController
{
    private BansHandler $handler;

    public function __construct(BansHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/bans", name="server_bans", methods={"GET"})
     */
    public function bansQueryAction(int $serverId): Response
    {
        $viewModel = new BansViewModel($serverId);
        $query = new BansQuery($serverId, $viewModel);

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $this->addServerMiddlewares($queryBus);

        $busResponse = $queryBus->handle($query);

        if ($viewModel->isRunning()) {
            $viewModel->setConnectionUri($this->getConnectionUri($query->Murmur, $query->prx));
        }

        $response = $this->render('Page/Server/Bans/index.html.twig', ['viewModel' => $viewModel]);
        return $response->setStatusCode($this->getStatusCode($busResponse));
    }
}
