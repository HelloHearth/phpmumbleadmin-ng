<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Channels\Channels\ChannelsHandler;
use App\Domain\Action\Server\Channels\Channels\ChannelsQuery;
use App\Domain\Action\Server\Channels\Channels\ChannelsViewModel;
use App\Infrastructure\Symfony\Form\ActivatePrioritySpeakerType;
use App\Infrastructure\Symfony\Form\BanUserType;
use App\Infrastructure\Symfony\Form\CreateChannelType;
use App\Infrastructure\Symfony\Form\DeactivatePrioritySpeakerType;
use App\Infrastructure\Symfony\Form\DeleteChannelType;
use App\Infrastructure\Symfony\Form\EditChannelType;
use App\Infrastructure\Symfony\Form\KickUserType;
use App\Infrastructure\Symfony\Form\LinkChannelType;
use App\Infrastructure\Symfony\Form\ModifyUserNameType;
use App\Infrastructure\Symfony\Form\MoveChannelType;
use App\Infrastructure\Symfony\Form\SendMessageToChannelType;
use App\Infrastructure\Symfony\Form\SendMessageToUserType;
use App\Infrastructure\Symfony\Form\UnlinkChannelType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/server/{serverId}", name="server_channels", methods={"GET"})
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ChannelsController extends ServerQueryController
{
    private ChannelsHandler $viewerJsonHandler;

    public function __construct(ChannelsHandler $viewerJsonHandler)
    {
        $this->viewerJsonHandler = $viewerJsonHandler;
    }

    /**
     * @Route("", name="", methods={"GET"})
     */
    public function channelsAction(int $serverId, Request $request): Response
    {
        $viewModel = new ChannelsViewModel($serverId);
        $query = new ChannelsQuery($viewModel, $serverId);

        $queryBus = $this->busFactory->buildForQuery($this->viewerJsonHandler);
        $this->addServerMiddlewares($queryBus);

        $busResponse = $queryBus->handle($query);
        $statusCode = $this->getStatusCode($busResponse);

        if ($request->isXmlHttpRequest()) {
            return new Response($viewModel->getJsonViewer(), $statusCode);
        }

        if ($viewModel->isRunning()) {
            $viewModel->setConnectionUri($this->getConnectionUri($query->Murmur, $query->prx));
        }

        $response = $this->render('Page/Server/Channels/channels.html.twig', [
            'viewModel' => $viewModel,
            'forms' => $this->createChannelsForms($serverId),
        ]);
        return $response->setStatusCode($statusCode);
    }

    /**
     * @Route("/tree", name="_tree", methods={"GET"}, options={"expose"=true})
     */
    public function treeAction(int $serverId): Response
    {
        return $this->forward(__CLASS__.'::channelsAction', ['serverId' => $serverId]);
    }

    private function createChannelsForms(int $serverId): array
    {
        return [
            'createChannel' => $this->createForm(CreateChannelType::class, null, [
                'action' => $this->generateUrl('cmd_create_channel', ['serverId' => $serverId])
            ])->createView(),
            'editChannel' => $this->createForm(EditChannelType::class, null, [
                'action' => $this->generateUrl('cmd_edit_channel', ['serverId' => $serverId])
            ])->createView(),
            'deleteChannel' => $this->createForm(DeleteChannelType::class, null, [
                'action' => $this->generateUrl('cmd_delete_channel', ['serverId' => $serverId])
            ])->createView(),
            'kickUser' => $this->createForm(KickUserType::class, null, [
                'action' => $this->generateUrl('cmd_kick_user', ['serverId' => $serverId])
            ])->createView(),
            'banUser' => $this->createForm(BanUserType::class, null, [
                'action' => $this->generateUrl('cmd_server_ban_user', ['serverId' => $serverId])
            ])->createView(),
            'sendMessageToChannel' => $this->createForm(SendMessageToChannelType::class, null, [
                'action' => $this->generateUrl('cmd_send_message_to_channel', ['serverId' => $serverId])
            ])->createView(),
            'sendMessageToUser' => $this->createForm(SendMessageToUserType::class, null, [
                'action' => $this->generateUrl('cmd_send_message_to_user', ['serverId' => $serverId])
            ])->createView(),
            'modifyUserName' => $this->createForm(ModifyUserNameType::class, null, [
                'action' => $this->generateUrl('cmd_modify_user_name', ['serverId' => $serverId])
            ])->createView(),
            'activatePrioritySpeaker' => $this->createForm(ActivatePrioritySpeakerType::class, null, [
                'action' => $this->generateUrl('cmd_activate_priority_speaker', ['serverId' => $serverId])
            ])->createView(),
            'deactivatePrioritySpeaker' => $this->createForm(DeactivatePrioritySpeakerType::class, null, [
                'action' => $this->generateUrl('cmd_deactivate_priority_speaker', ['serverId' => $serverId])
            ])->createView(),
            'linkChannel' => $this->createForm(LinkChannelType::class, null, [
                'action' => $this->generateUrl('cmd_link_channel', ['serverId' => $serverId])
            ])->createView(),
            'unlinkChannel' => $this->createForm(UnlinkChannelType::class, null, [
                'action' => $this->generateUrl('cmd_unlink_channel', ['serverId' => $serverId])
            ])->createView(),
            'moveChannel' => $this->createForm(MoveChannelType::class, null, [
                'action' => $this->generateUrl('cmd_move_channel', ['serverId' => $serverId])
            ])->createView(),
        ];
    }
}
