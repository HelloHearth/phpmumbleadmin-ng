<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Bans\CreateBan\BanCreated;
use App\Domain\Action\Server\Bans\CreateBan\CreateBanCommand;
use App\Domain\Action\Server\Bans\CreateBan\CreateBanHandler;
use App\Domain\Action\Server\Channels\KickUser\KickUserCommand;
use App\Domain\Action\Server\Channels\KickUser\KickUserHandler;
use App\Infrastructure\Symfony\Form\BanUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BanUserController extends ServerController
{
    private CreateBanHandler $handler;

    public function __construct(CreateBanHandler $banUserHandler)
    {
        $this->handler = $banUserHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/ban_user", name="cmd_server_ban_user", methods={"POST"})
     */
    public function banUserAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(BanUserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $command = (new CreateBanCommand())->server($serverId)
                ->address($data['ip'])
                ->name($data['name'] ?? '')
                ->hash($data['hash'] ?? '')
                ->reason($data['reason'] ?? '')
                ->permanent($data['endDate']['permanent'])
                ->endDate($data['endDate']['date']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);

            if ($busResponse->hasEvent(BanCreated::class)) {

                // Kick the recently banned user
                $kickCommand = new KickUserCommand($serverId, $data['userSession'], $data['reason'] ?? '');
                $kickCommand->prx = $command->prx;

                $kickHandler = new KickUserHandler();
                $kickHandler->handle($kickCommand);
            }
        }

        return $this->createAjaxFormResponse(BanCreated::class, $form, $busResponse ?? null);
    }
}
