<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\UnlinkChannel\CannotUnlinkSelf;
use App\Domain\Action\Server\Channels\UnlinkChannel\ChannelIsNotLinked;
use App\Domain\Action\Server\Channels\UnlinkChannel\ChannelUnlinked;
use App\Domain\Action\Server\Channels\UnlinkChannel\UnlinkChannelCommand;
use App\Domain\Action\Server\Channels\UnlinkChannel\UnlinkChannelHandler;
use App\Domain\Model\HttpCode;
use App\Infrastructure\Symfony\Form\UnlinkChannelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UnlinkChannelController extends ServerController
{
    private UnlinkChannelHandler $handler;

    public function __construct(UnlinkChannelHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/cmd/unlink_channel", name="cmd_unlink_channel", methods={"POST"})
     */
    public function unlinkAction(int $serverId, Request $request): JsonResponse
    {
        $command = new UnlinkChannelCommand($serverId);

        $form = $this->createForm(UnlinkChannelType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);

            $statusCode = $this->getStatusCode($busResponse, [
                CannotUnlinkSelf::class => 403,
                ChannelIsNotLinked::class => 403,
            ]);
        }

        return $this->createXmlHttpResponse(ChannelUnlinked::class, $busResponse ?? null, $statusCode ?? HttpCode::PRECONDITION_FAILED);
    }
}
