<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\ChannelDoNotExist;
use App\Domain\Action\Server\Channels\SendMessageToChannel\EmptyMessage;
use App\Domain\Action\Server\Channels\SendMessageToChannel\MessageSentToChannel;
use App\Domain\Action\Server\Channels\SendMessageToChannel\SendMessageToChannelCommand;
use App\Domain\Action\Server\Channels\SendMessageToChannel\SendMessageToChannelHandler;
use App\Infrastructure\Symfony\Form\SendMessageToChannelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SendMessageToChannelController extends ServerController
{
    private SendMessageToChannelHandler $handler;

    public function __construct(SendMessageToChannelHandler $sendMessageHandler)
    {
        $this->handler = $sendMessageHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/send_message_to_channel", name="cmd_send_message_to_channel", methods={"POST"})
     */
    public function sendMessageAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(SendMessageToChannelType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $command = new SendMessageToChannelCommand($serverId, (int) $data['channelId'], $data['message'], $data['toSubChannels']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);

            $statusCode = $this->getStatusCode($busResponse, [
                EmptyMessage::class => 412,
                ChannelDoNotExist::class => 400,
            ]);
        }

        return $this->createAjaxFormResponse(MessageSentToChannel::class, $form, $busResponse ?? null, [], $statusCode ?? null);
    }
}
