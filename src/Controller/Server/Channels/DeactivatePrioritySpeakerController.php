<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\DeactivatePrioritySpeaker\DeactivatePrioritySpeakerCommand;
use App\Domain\Action\Server\Channels\DeactivatePrioritySpeaker\DeactivatePrioritySpeakerHandler;
use App\Domain\Action\Server\Channels\DeactivatePrioritySpeaker\PrioritySpeakerDeactivated;
use App\Domain\Action\Server\Channels\DeactivatePrioritySpeaker\PrioritySpeakerNotActive;
use App\Domain\Model\HttpCode;
use App\Infrastructure\Symfony\Form\DeactivatePrioritySpeakerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeactivatePrioritySpeakerController extends ServerController
{
    private DeactivatePrioritySpeakerHandler $handler;

    public function __construct(DeactivatePrioritySpeakerHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route(
     *     "/server/{serverId}/cmd/deactivate_priority_speaker",
     *     name="cmd_deactivate_priority_speaker",
     *     methods={"POST"}
     *     )
     */
    public function deactivatePriorityAction(int $serverId, Request  $request): JsonResponse
    {
        $command = new DeactivatePrioritySpeakerCommand($serverId);

        $form = $this->createForm(DeactivatePrioritySpeakerType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);

            $busResponse = $commandBus->handle($command);
            $statusCode = $this->getStatusCode($busResponse, [PrioritySpeakerNotActive::class => 403]);
        }

        return $this->createXmlHttpResponse(PrioritySpeakerDeactivated::class, $busResponse ?? null, $statusCode ?? HttpCode::PRECONDITION_FAILED);
    }
}
