<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\DeleteChannel\ChannelDeleted;
use App\Domain\Action\Server\Channels\DeleteChannel\DeleteChannelCommand;
use App\Domain\Action\Server\Channels\DeleteChannel\DeleteChannelHandler;
use App\Domain\Action\Server\Channels\DeleteChannel\DeleteRootChannelIsForbidden;
use App\Infrastructure\Symfony\Form\DeleteChannelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteChannelController extends ServerController
{
    private DeleteChannelHandler $handler;

    public function __construct(DeleteChannelHandler $deleteChannelHandler)
    {
        $this->handler = $deleteChannelHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/delete_channel", name="cmd_delete_channel", methods={"POST"})
     */
    public function deleteChannelAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(DeleteChannelType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $command = new DeleteChannelCommand($serverId, (int) $data['channelId']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);

            if ($busResponse->hasEvent(DeleteRootChannelIsForbidden::class)) {
                $httpStatus = 403;
            }
        }

        return $this->createAjaxFormResponse(ChannelDeleted::class, $form, $busResponse ?? null, [], $httpStatus ?? null);
    }
}
