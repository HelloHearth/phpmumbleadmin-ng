<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\LinkChannel\CannotLinkSelf;
use App\Domain\Action\Server\Channels\LinkChannel\ChannelLinked;
use App\Domain\Action\Server\Channels\LinkChannel\LinkChannelCommand;
use App\Domain\Action\Server\Channels\LinkChannel\LinkChannelHandler;
use App\Domain\Model\HttpCode;
use App\Infrastructure\Symfony\Form\LinkChannelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LinkChannelController extends ServerController
{
    private LinkChannelHandler $handler;

    public function __construct(LinkChannelHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/cmd/link_channel", name="cmd_link_channel", methods={"POST"})
     */
    public function linkAction(int $serverId, Request $request): JsonResponse
    {
        $command = new LinkChannelCommand($serverId);

        $form = $this->createForm(LinkChannelType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);

            $busResponse = $commandBus->handle($command);
            $statusCode = $this->getStatusCode($busResponse, [CannotLinkSelf::class => 403]);
        }

        return $this->createXmlHttpResponse(ChannelLinked::class, $busResponse ?? null, $statusCode ?? HttpCode::PRECONDITION_FAILED);
    }
}
