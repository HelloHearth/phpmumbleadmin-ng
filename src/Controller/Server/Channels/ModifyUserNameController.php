<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\ModifyUserName\CantModifyUserName;
use App\Domain\Action\Server\Channels\ModifyUserName\ModifyUserNameCommand;
use App\Domain\Action\Server\Channels\ModifyUserName\ModifyUserNameHandler;
use App\Domain\Action\Server\Channels\ModifyUserName\UserNameModified;
use App\Domain\Action\Server\Channels\ModifyUserName\UserNotConnected;
use App\Infrastructure\Symfony\Form\ModifyUserNameType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ModifyUserNameController extends ServerController
{
    private ModifyUserNameHandler $handler;

    public function __construct(ModifyUserNameHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/cmd/modify_user_name", name="cmd_modify_user_name", methods={"POST"})
     */
    public function modifyAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(ModifyUserNameType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $command = new ModifyUserNameCommand($serverId, (int) $data['userSession'], $data['newName']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);

            $statusCode = $this->getStatusCode($busResponse, [
                CantModifyUserName::class => 403,
                UserNotConnected::class => 404,
            ]);
        }

        return $this->createAjaxFormResponse(UserNameModified::class, $form, $busResponse ?? null, [], $statusCode ?? null);
    }
}
