<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\MoveUser\MoveUserCommand;
use App\Domain\Action\Server\Channels\MoveUser\MoveUserHandler;
use App\Domain\Action\Server\Channels\MoveUser\UserAlreadyInChannel;
use App\Domain\Action\Server\Channels\MoveUser\UserMoved;
use App\Domain\Model\HttpCode;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MoveUserController extends ServerController
{
    private MoveUserHandler $handler;

    public function __construct(MoveUserHandler $moveUserHandler)
    {
        $this->handler = $moveUserHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/move_user", name="cmd_move_user", methods={"POST"}, options={"expose"=true})
     */
    public function moveUserAction(int $serverId, Request $request): JsonResponse
    {
        $form = $request->get('form');

        if (isset($form['userSession'], $form['toChannelId'])) {
            $command = new MoveUserCommand($serverId, (int) $form['userSession'],(int)  $form['toChannelId']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);

            $busResponse = $commandBus->handle($command);
            $statusCode = $this->getStatusCode($busResponse, [
                UserAlreadyInChannel::class => 403,
            ]);
        }

        return $this->createXmlHttpResponse(UserMoved::class, $busResponse ?? null, $statusCode ?? HttpCode::PRECONDITION_FAILED);
    }
}
