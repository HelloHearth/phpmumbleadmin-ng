<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\KickUser\KickUserCommand;
use App\Domain\Action\Server\Channels\KickUser\KickUserHandler;
use App\Domain\Action\Server\Channels\KickUser\UserKicked;
use App\Infrastructure\Symfony\Form\KickUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class KickUserController extends ServerController
{
    private KickUserHandler $handler;

    public function __construct(KickUserHandler $kickUserHandler)
    {
        $this->handler = $kickUserHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/kick_user", name="cmd_kick_user", methods={"POST"})
     */
    public function kickUserAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(KickUserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $command = new KickUserCommand($serverId, (int) $data['userSession'], $data['reason']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);
        }

        return $this->createAjaxFormResponse(UserKicked::class, $form, $busResponse ?? null);
    }
}
