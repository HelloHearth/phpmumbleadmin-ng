<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Registrations;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Registrations\EditRegistration\EditRegistrationCommand;
use App\Domain\Action\Server\Registrations\EditRegistration\EditRegistrationHandler;
use App\Domain\Action\Server\Registrations\EditRegistration\InvalidCharacterForUsername;
use App\Domain\Action\Server\Registrations\EditRegistration\RegistrationEdited;
use App\Domain\Action\Server\Registrations\EditRegistration\ServerDontAllowHtmlTags;
use App\Domain\Action\Server\Registrations\EditRegistration\UsernameAlreadyExist;
use App\Domain\Action\Server\Registrations\Registration\RegistrationHandler;
use App\Domain\Action\Server\Registrations\Registration\RegistrationQuery;
use App\Domain\Action\Server\Registrations\Registration\RegistrationViewModel;
use App\Domain\Action\Server\Registrations\RegistrationToUserInfoTransformer;
use App\Domain\Service\FlashMessageServiceInterface;
use App\Infrastructure\Symfony\Form\RegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditRegistrationController extends ServerQueryController
{
    private RegistrationHandler $registrationHandler;
    private EditRegistrationHandler $handler;
    private FlashMessageServiceInterface $flashMessageService;

    public function __construct(
        RegistrationHandler $registrationHandler,
        EditRegistrationHandler $handler,
        FlashMessageServiceInterface $flashMessageService
    ) {
        $this->registrationHandler = $registrationHandler;
        $this->handler = $handler;
        $this->flashMessageService = $flashMessageService;
    }

    /**
     * @Route("/server/{serverId}/registration/edit/{id}", name="server_registration_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, int $serverId, int $id): Response
    {
        $viewModel = new RegistrationViewModel($serverId);
        $query = new RegistrationQuery($serverId, $id, $viewModel);

        $queryBus = $this->busFactory->buildForQuery($this->registrationHandler);
        $this->addServerMiddlewares($queryBus);

        $queryBusResponse = $queryBus->handle($query);

        if ($queryBusResponse->hasEvents()) {
            return $this->redirectToRoute('server_registration', ['serverId' => $serverId, 'id' => $id], 307);
        }

        $registration = $viewModel->getRegistration();
        $originalData = RegistrationToUserInfoTransformer::tranform($registration);

        $form = $this->createForm(RegistrationType::class, $registration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $command = new EditRegistrationCommand($serverId, $id, $originalData, $registration->username, $registration->email, $registration->description, $registration->plainPassword);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $command->prx = $query->prx;

            $busResponse = $commandBus->handle($command);

            if ($busResponse->hasEvent(RegistrationEdited::class)) {
                $this->flashMessageService->success(RegistrationEdited::KEY);
                return $this->redirectToReferer($request);
            }

            $this->handleBusResponseEventsWithForm($form, $busResponse, [
                InvalidCharacterForUsername::class => 'username',
                UsernameAlreadyExist::class => 'username',
                ServerDontAllowHtmlTags::class => 'description',
            ]);
        }

        if ($viewModel->isRunning()) {
            $viewModel->setConnectionUri($this->getConnectionUri($query->Murmur, $query->prx));
        }

        return $this->render('Page/Server/Registrations/form.html.twig', [
            'viewModel' => $viewModel,
            'form' => $form->createView()
        ]);
    }
}
