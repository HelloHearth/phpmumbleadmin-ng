<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Registrations;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Registrations\DeleteRegistration\DeleteRegistrationCommand;
use App\Domain\Action\Server\Registrations\DeleteRegistration\DeleteRegistrationHandler;
use App\Domain\Action\Server\Registrations\DeleteRegistration\SuperUserCannotBeDeleted;
use App\Domain\Service\FlashMessageServiceInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteRegistrationController extends ServerController
{
    private DeleteRegistrationHandler $handler;
    private FlashMessageServiceInterface $flashMessageService;

    public function __construct(
        DeleteRegistrationHandler $handler,
        FlashMessageServiceInterface $flashMessageService
    ) {
        $this->handler = $handler;
        $this->flashMessageService = $flashMessageService;
    }

    /**
     * @Route(
     *     "/server/{serverId}/cmd/registration/delete/{id}",
     *     name="cmd_server_registration_delete",
     *     methods={"GET"}
     *     )
     */
    public function deleteAction(int $serverId, int $id): RedirectResponse
    {
        $command = new DeleteRegistrationCommand($serverId, $id);

        $commandBus = $this->busFactory->buildForCommand($this->handler);
        $this->addServerMiddlewares($commandBus);

        $busResponse = $commandBus->handle($command);
        $statusCode = $this->getStatusCode($busResponse, [
            SuperUserCannotBeDeleted::class => 403,
        ]);

        $this->flashMessageService->basedOnHttpCode($statusCode, $busResponse->getEvents()[0]->getMessage());

        return $this->redirectToRoute('server_registrations', ['serverId' => $serverId]);
    }
}
