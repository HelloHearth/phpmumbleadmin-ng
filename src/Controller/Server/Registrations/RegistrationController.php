<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Registrations;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Registrations\Registration\AccessToRegistrationIsNotAllowed;
use App\Domain\Action\Server\Registrations\Registration\RegistrationDoNotExist;
use App\Domain\Action\Server\Registrations\Registration\RegistrationHandler;
use App\Domain\Action\Server\Registrations\Registration\RegistrationQuery;
use App\Domain\Action\Server\Registrations\Registration\RegistrationViewModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class RegistrationController extends ServerQueryController
{
    private RegistrationHandler $handler;

    public function __construct(RegistrationHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/registration/{id}", name="server_registration", methods={"GET"})
     */
    public function registrationQueryAction(int $serverId, int $id): Response
    {
        $viewModel = new RegistrationViewModel($serverId);
        $query = new RegistrationQuery($serverId, $id, $viewModel);

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $this->addServerMiddlewares($queryBus);

        $busResponse = $queryBus->handle($query);
        $statusCode = $this->getStatusCode($busResponse, [
            AccessToRegistrationIsNotAllowed::class => 403,
            RegistrationDoNotExist::class => 404,
        ]);

        if ($viewModel->isRunning()) {
            $viewModel->setConnectionUri($this->getConnectionUri($query->Murmur, $query->prx));
        }

        $response = $this->render('Page/Server/Registrations/registration.html.twig', ['viewModel' => $viewModel]);
        return $response->setStatusCode($statusCode);
    }
}
