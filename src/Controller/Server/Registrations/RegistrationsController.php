<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Registrations;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Registrations\Index\RegistrationsHandler;
use App\Domain\Action\Server\Registrations\Index\RegistrationsQuery;
use App\Domain\Action\Server\Registrations\Index\RegistrationsViewModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class RegistrationsController extends ServerQueryController
{
    private RegistrationsHandler $handler;

    public function __construct(RegistrationsHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/registrations", name="server_registrations", methods={"GET"})
     */
    public function registrationsQueryAction(int $serverId): Response
    {
        $viewModel = new RegistrationsViewModel($serverId);
        $query = new RegistrationsQuery($serverId, $viewModel);

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $this->addServerMiddlewares($queryBus);

        $busResponse = $queryBus->handle($query);

        if ($viewModel->isRunning()) {
            $viewModel->setConnectionUri($this->getConnectionUri($query->Murmur, $query->prx));
        }

        $response = $this->render('Page/Server/Registrations/index.html.twig', ['viewModel' => $viewModel]);
        return $response->setStatusCode($this->getStatusCode($busResponse));
    }
}
