<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Dashboard;

use App\Controller\BaseController;
use App\Controller\ConnectionBusMiddlewareTrait;
use App\Domain\Action\Dashboard\CreateServer\CreateServerCommand;
use App\Domain\Action\Dashboard\CreateServer\CreateServerHandler;
use App\Domain\Action\Dashboard\CreateServer\ServerCreated;
use App\Infrastructure\Symfony\Form\CreateServerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_DASHBOARD")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateServerController extends BaseController
{
    use ConnectionBusMiddlewareTrait;

    private CreateServerHandler $handler;

    public function __construct(CreateServerHandler $createServerHandler)
    {
        $this->handler = $createServerHandler;
    }

    /**
     * @Route("/cmd/create_server", name="cmd_create_server", methods={"POST"})
     */
    public function createServerAction(Request $request): Response
    {
        $form = $this->createForm(CreateServerType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $command = new CreateServerCommand($data['registername'], $data['password'], $data['users']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $commandBus->addMiddleWare($this->connectionMiddleware);
            $busResponse = $commandBus->handle($command);
        }

        return $this->createAjaxFormResponse(ServerCreated::class, $form, $busResponse ?? null);
    }
}
