<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Dashboard;

use App\Controller\BaseController;
use App\Controller\ConnectionBusMiddlewareTrait;
use App\Domain\Action\Dashboard\SendMessageToAllServers\MessageSent;
use App\Domain\Action\Dashboard\SendMessageToAllServers\SendMessageToAllServersCommand;
use App\Domain\Action\Dashboard\SendMessageToAllServers\SendMessageToAllServersHandler;
use App\Infrastructure\Symfony\Form\SendMessageToAllServersType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_DASHBOARD")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SendMessageToAllServersController extends BaseController
{
    use ConnectionBusMiddlewareTrait;

    private SendMessageToAllServersHandler $handler;

    public function __construct(SendMessageToAllServersHandler $sendMessageToAllServersHandler)
    {
        $this->handler = $sendMessageToAllServersHandler;
    }

    /**
     * @Route("/cmd/send_message_to_all_servers", name="cmd_send_message_to_all_servers", methods={"POST"})
     */
    public function SendMessageToAllServersAction(Request $request): Response
    {
        $form = $this->createForm(SendMessageToAllServersType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $command = new SendMessageToAllServersCommand($data['message']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $commandBus->addMiddleWare($this->connectionMiddleware);
            $busResponse = $commandBus->handle($command);
        }

        return $this->createAjaxFormResponse(MessageSent::class, $form, $busResponse ?? null);
    }
}
