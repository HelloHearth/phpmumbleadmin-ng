<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Dashboard;

use App\Controller\BaseController;
use App\Controller\ServerBusTrait;
use App\Domain\Action\Dashboard\DisableWebAccess\DisableWebAccessCommand;
use App\Domain\Action\Dashboard\DisableWebAccess\DisableWebAccessHandler;
use App\Domain\Action\Dashboard\EnableWebAcces\EnableWebAccessCommand;
use App\Domain\Action\Dashboard\EnableWebAcces\EnableWebAccessHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_DASHBOARD")
 * @Route("/cmd", name="cmd")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class WebAccessController extends BaseController
{
    use ServerBusTrait;

    private EnableWebAccessHandler $enableHandler;
    private DisableWebAccessHandler $disableHandler;

    public function __construct(
        EnableWebAccessHandler  $enableWebAccessHandler,
        DisableWebAccessHandler $disableWebAccessHandler
    ) {
        $this->enableHandler = $enableWebAccessHandler;
        $this->disableHandler = $disableWebAccessHandler;
    }

    /**
     * @Route("/web_access_allow/{serverId}", name="_allow_web_access", methods={"GET"})
     */
    public function EnableWebAccessAction(int $serverId): RedirectResponse
    {
        /*
         * Control also that admin has acces to the vserver
         */
//        if ($PMA->user->is(PMA_USER_ADMIN) && ! $PMA->user->checkServerAccess($sid)) {
//            throw new PMA_cmdException('illegal_operation');
//        }


        $command = new EnableWebAccessCommand($serverId);

        $commandBus = $this->busFactory->buildForCommand($this->enableHandler);
        $this->addServerMiddlewares($commandBus);
        $commandBus->handle($command);

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/web_access_remove/{serverId}", name="_remove_web_access", methods={"GET"})
     */
    public function DisableWebAccessAction(int $serverId): RedirectResponse
    {
        /*
         * Control also that admin has acces to the vserver
         */
//        if ($PMA->user->is(PMA_USER_ADMIN) && ! $PMA->user->checkServerAccess($sid)) {
//            throw new PMA_cmdException('illegal_operation');
//        }


        $command = new DisableWebAccessCommand($serverId);

        $commandBus = $this->busFactory->buildForCommand($this->disableHandler);
        $this->addServerMiddlewares($commandBus);

        $commandBus->handle($command);

        return $this->redirectToRoute('dashboard');
    }
}
