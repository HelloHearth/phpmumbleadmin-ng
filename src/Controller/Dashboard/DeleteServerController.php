<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Dashboard;

use App\Controller\BaseController;
use App\Controller\ServerBusTrait;
use App\Domain\Action\Dashboard\DeleteServer\DeleteServerCommand;
use App\Domain\Action\Dashboard\DeleteServer\DeleteServerHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_DASHBOARD")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteServerController extends BaseController
{
    use ServerBusTrait;

    private DeleteServerHandler $handler;

    public function __construct(DeleteServerHandler $deleteServerHandler)
    {
        $this->handler = $deleteServerHandler;
    }

    /**
     * @Route("/cmd/delete_server/{serverId}", name="cmd_delete_server", methods={"GET"})
     */
    public function deleteServerAction(int $serverId): RedirectResponse
    {
        $command = new DeleteServerCommand($serverId);

        $commandBus = $this->busFactory->buildForCommand($this->handler);
        $this->addServerMiddlewares($commandBus);
        $commandBus->handle($command);

        return $this->redirectToRoute('dashboard');
    }
}
