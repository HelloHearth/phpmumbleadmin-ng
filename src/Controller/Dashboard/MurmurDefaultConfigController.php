<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Dashboard;

use App\Controller\BaseController;
use App\Controller\ConnectionBusMiddlewareTrait;
use App\Domain\Action\Dashboard\MurmurDefaultConfig\MurmurDefaultConfigQuery;
use App\Domain\Action\Dashboard\MurmurDefaultConfig\MurmurDefaultConfigHandler;
use App\Domain\Action\Dashboard\MurmurDefaultConfig\MurmurDefaultConfigView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_ADMINISTRATION")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MurmurDefaultConfigController extends BaseController
{
    use ConnectionBusMiddlewareTrait;

    private MurmurDefaultConfigHandler $handler;

    public function __construct(MurmurDefaultConfigHandler $murmurDefaultConfigHandler)
    {
        $this->handler = $murmurDefaultConfigHandler;
    }

    /**
     * @Route("/dashboard/murmurDefaultParameters", name="dashboard_murmurDefaultParameters", methods={"GET"})
     */
    public function displayMurmurDefaultParametersAction(): Response
    {
        $query = new MurmurDefaultConfigQuery();
        $query->view = new MurmurDefaultConfigView();

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $queryBus->addMiddleWare($this->connectionMiddleware);

        $queryBus->handle($query);

        return $this->render('Page/Dashboard/murmur_default_config.html.twig', [
            'viewModel' => $query->view,
        ]);
    }
}
