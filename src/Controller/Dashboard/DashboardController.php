<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Dashboard;

use App\Controller\BaseController;
use App\Controller\ConnectionBusMiddlewareTrait;
use App\Domain\Action\Dashboard\Dashboard\DashboardHandler;
use App\Domain\Action\Dashboard\Dashboard\DashboardQuery;
use App\Domain\Action\Dashboard\Dashboard\DashboardView;
use App\Domain\Service\UserServiceInterface;
use App\Infrastructure\Symfony\Form\CreateServerType;
use App\Infrastructure\Symfony\Form\SendMessageToAllServersType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_DASHBOARD")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DashboardController extends BaseController
{
    use ConnectionBusMiddlewareTrait;

    /**
     * Constant for the session variable which store the page number of the
     * paginator.
     */
    public const SESSION_PAGE_NAME = 'dashboard_page';

    private DashboardHandler $handler;
    private UserServiceInterface $userService;

    public function __construct(
        DashboardHandler $dashboardHandler,
        UserServiceInterface $userService
    ) {
        $this->handler = $dashboardHandler;
        $this->userService = $userService;
    }

    /**
     * @Route("/", name="dashboard", methods={"GET"})
     */
    public function displayDashboardAction(Request $request): Response
    {
        $username = $this->userService->getUsername();
        $session = $request->getSession();

        if ($request->query->has('page') && \ctype_digit($request->query->get('page'))) {
            $session->set(self::SESSION_PAGE_NAME, $request->query->getInt('page'));
        }

        $query = new DashboardQuery($username, $session->get(self::SESSION_PAGE_NAME, 1));

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $queryBus->addMiddleWare($this->connectionMiddleware);

        $busResponse = $queryBus->handle($query);
        /** @var $view DashboardView */
        $view = $busResponse->getData();

        return $this->render('Page/Dashboard/dashboard.html.twig', [
            'view' => $view,
            'forms' => [
                'createServer' => $this->createForm(CreateServerType::class, null, ['action' => $this->generateUrl('cmd_create_server')])->createView(),
                'sendMessage' => $this->createForm(SendMessageToAllServersType::class, null, ['action' => $this->generateUrl('cmd_send_message_to_all_servers')])->createView(),
            ],
        ]);
    }
}
