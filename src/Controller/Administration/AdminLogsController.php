<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Administration;

use App\Controller\BaseController;
use App\Domain\Action\Administration\Logs\AdminLogsHandler;
use App\Domain\Action\Administration\Logs\AdminLogsQuery;
use App\Entity\AdminLogEntity;
use App\Infrastructure\Mapping\AdminLogMapping;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_ADMINISTRATION")
 * @Route("/administration/logs", name="administration_logs", methods={"GET"})
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminLogsController extends BaseController
{
    private EntityManagerInterface $entityManager;
    private AdminLogsHandler $handler;

    public function __construct(EntityManagerInterface $entityManager, AdminLogsHandler $handler)
    {
        $this->entityManager = $entityManager;
        $this->handler = $handler;
    }

    /**
     * @Route("", name="")
     */
    public function displayLogsAction(): Response
    {
        $entities = $this->entityManager->getRepository(AdminLogEntity::class)->findBy([], ['id' => 'DESC']);

        $mapper = new AdminLogMapping();
        $adminLogs = $mapper->fromEntities($entities);

        $query = new AdminLogsQuery($adminLogs);
        $data = $this->handler->handle($query);

        return $this->render('Page/Administration/Logs/logs.html.twig', [
            'data' => $data,
        ]);
    }

    /**
     * @Route("/latest/{lastLogLen}", name="_latest", options={"expose"=true})
     */
    public function latestLogsAction(int $lastLogLen): Response
    {
        $entities = $this->entityManager->getRepository(AdminLogEntity::class)->getLatest($lastLogLen);

        $mapper = new AdminLogMapping();
        $adminLogs = $mapper->fromEntities($entities);

        $query = new AdminLogsQuery($adminLogs);
        $data = $this->handler->handle($query);

        return new Response($data->getJsonLogs(), 200);
    }
}
