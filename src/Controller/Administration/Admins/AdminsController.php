<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Administration\Admins;

use App\Controller\BaseController;
use App\Domain\Action\Administration\Admins\Index\AdminsViewHandler;
use App\Infrastructure\Symfony\Form\CreateAdminType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_ADMINISTRATION")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminsController extends BaseController
{
    private AdminsViewHandler $handler;

    public function __construct(AdminsViewHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/administration/admins", name="administration_admins", methods={"GET"})
     */
    public function indexAction(): Response
    {
        $view = $this->handler->createView();

        return $this->render('Page/Administration/Admins/admins.html.twig', [
            'view' => $view,
            'createAdminForm' => $this->createForm(CreateAdminType::class, null, ['action' => $this->generateUrl('cmd_create_admin')])->createView()
        ]);
    }
}
