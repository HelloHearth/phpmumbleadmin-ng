<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Controller\Security;

use App\Controller\BaseController;
use App\Infrastructure\Symfony\Form\ForgottenPasswordType;
use App\Infrastructure\Symfony\Security\Service\ForgottenPasswordService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ForgottenPasswordController extends BaseController
{
    private ForgottenPasswordService $service;

    public function __construct(ForgottenPasswordService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/forgotten-password", name="app_forgotten_password", methods={"GET", "POST"})
     */
    public function forgottenPasswordAction(Request $request): Response
    {
        $form = $this->createForm(ForgottenPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $username = $form->get('username')->getData();
            $this->service->handle($username);
            return $this->render('Page/Security/forgotten_password_processing.html.twig');
        }

        return $this->render('Page/Security/forgotten_password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
