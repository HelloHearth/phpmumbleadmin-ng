<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Controller\Security;

use App\Controller\BaseController;
use App\Domain\Service\FlashMessageServiceInterface;
use App\Entity\ForgottenPassword;
use App\Infrastructure\Symfony\Form\ForgottenPasswordResetType;
use App\Infrastructure\Symfony\Security\Service\ForgottenPasswordService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/forgotten-password", name="app_forgotten_password")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ForgottenPasswordResetController extends BaseController
{
    private ForgottenPasswordService $service;

    public function __construct(ForgottenPasswordService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/reset/{hash}", name="_reset", methods={"GET", "POST"})
     */
    public function resetAction(Request $request, ForgottenPassword $forgottenPassword): Response
    {
        if ($forgottenPassword->isExpired()) {
            $this->service->remove($forgottenPassword);
            return $this->redirectToRoute('app_forgotten_password_reset_hash_expired');
        }

        $form = $this->createForm(ForgottenPasswordResetType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->service->updatePassword($forgottenPassword, $form->get('password')->getData());
            $this->addFlash(FlashMessageServiceInterface::SUCCESS, 'password_has_been_reset');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('Page/Security/password_reset.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/expired", name="_reset_hash_expired", methods={"GET"})
     */
    public function resetHashHasExpiredAction(): Response
    {
        return $this->render('Page/Security/password_reset_hash_has_expired.html.twig');
    }
}
