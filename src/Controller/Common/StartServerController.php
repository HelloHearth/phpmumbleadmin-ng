<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\BaseController;
use App\Controller\ServerBusTrait;
use App\Domain\Action\Common\StartServer\StartServerCommand;
use App\Domain\Action\Common\StartServer\StartServerHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_EDIT_SERVER_STATUS")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class StartServerController extends BaseController
{
    use ServerBusTrait;

    private StartServerHandler $handler;

    public function __construct(StartServerHandler $startServerHandler)
    {
        $this->handler = $startServerHandler;
    }

    /**
     * @Route("/cmd/startServer/{serverId}", name="cmd_startServer", methods={"GET"})
     */
    public function StartServerAction(int $serverId, Request $request): RedirectResponse
    {
        $command = new StartServerCommand($serverId);

        $commandBus = $this->busFactory->buildForCommand($this->handler);
        $this->addServerMiddlewares($commandBus);
        $commandBus->handle($command);

        return $this->redirectToReferer($request);
    }
}
