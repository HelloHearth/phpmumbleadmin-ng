<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UserController extends BaseController
{
    /**
     * @Route("/settings", name="settings", methods={"GET"})
     */
    public function settingsAction(): Response
    {
        return $this->render('Page/Common/user_settings.html.twig');
    }

    /**
     * @Route("/profile", name="profile", methods={"GET"})
     */
    public function profileAction(): Response
    {
        return $this->render('Page/Common/user_profile.html.twig');
    }
}
