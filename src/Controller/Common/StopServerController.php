<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\BaseController;
use App\Controller\ServerBusTrait;
use App\Domain\Action\Common\StopServer\ConfirmationRequired;
use App\Domain\Action\Common\StopServer\StopServerCommand;
use App\Domain\Action\Common\StopServer\StopServerHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_EDIT_SERVER_STATUS")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class StopServerController extends BaseController
{
    use ServerBusTrait;

    public const REFERER_CONFIRM_KEY = 'referer_for_confirm_server_to_stop';

    private StopServerHandler $handler;

    public function __construct(StopServerHandler $stopServerHandler)
    {
        $this->handler = $stopServerHandler;
    }

    /**
     * @Route("/cmd/stopServer/{serverId}", name="cmd_stopServer", methods={"GET"})
     */
    public function StopServerAction(int $serverId, Request  $request): RedirectResponse
    {
        $command = new StopServerCommand($serverId);

        $commandBus = $this->busFactory->buildForCommand($this->handler);
        $this->addServerMiddlewares($commandBus);
        $busResponse = $commandBus->handle($command);

        if ($busResponse->hasEvent(ConfirmationRequired::class)) {
            $session = $request->getSession();
            $session->set(self::REFERER_CONFIRM_KEY, $request->headers->get('referer'));
            return $this->redirectToRoute('confirm_server_to_stop', ['serverId' => $serverId]);
        }

        return $this->redirectToReferer($request);
    }
}
