<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\BaseController;
use App\Infrastructure\Manager\OptionsCookieManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LocaleController extends BaseController
{
    private OptionsCookieManager $cookieManager;

    public function __construct(OptionsCookieManager $cookieManager)
    {
        $this->cookieManager = $cookieManager;
    }

    /**
     * @Route("/cmd/options/locale/{locale}", name="cmd_options_locale", methods={"GET"})
     */
    public function setLocaleAction(string $locale, Request $request): RedirectResponse
    {
        $this->cookieManager->getCookie()->setLocale($locale);
        $this->cookieManager->updateCookie();

        return $this->redirectToReferer($request);
    }
}
