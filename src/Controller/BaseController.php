<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\EventHelper;
use App\Domain\Model\HttpCode;
use App\Infrastructure\Bus\BusFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class BaseController extends AbstractController
{
    protected BusFactory $busFactory;

    /**
     * @required
     */
    public function setBusFactory(BusFactory $busFactory): void
    {
        $this->busFactory = $busFactory;
    }

    protected function redirectToReferer(Request $request): RedirectResponse
    {
        return $this->redirect($request->headers->get('referer'));
    }

    protected function getStatusCode(BusResponse $busResponse, array $customEvents = []): int
    {
        return EventHelper::getStatusCode($busResponse, $customEvents);
    }

    protected function createXmlHttpResponse(string $expectedEvent, ?BusResponse $busResponse, ?int $httpStatus = null): JsonResponse
    {
        if ($busResponse instanceof BusResponse) {
            // On success
            if ($busResponse->hasEvent($expectedEvent)) {
                return new JsonResponse($busResponse->getEvent($expectedEvent)->getMessage());
            }
            // On other events, return the first event message
            if ($busResponse->hasEvents() > 0) {
                return new JsonResponse($busResponse->getEvents()[0]->getMessage(), $httpStatus);
            }
        }

        return new JsonResponse('', $httpStatus);
    }

    protected function createAjaxFormResponse(string $expectedEvent, FormInterface $form, ?BusResponse $busResponse, array $fieldParameters = [], ?int $httpStatus = null): Response
    {
        if ($busResponse instanceof BusResponse) {

            // On success, return the event message
            if ($busResponse->hasEvent($expectedEvent)) {
                return new Response($busResponse->getEvent($expectedEvent)->getMessage());
            }

            // If a BusResponse is submitted, handle events with the form
            $this->handleBusResponseEventsWithForm($form, $busResponse, $fieldParameters);
        }

        // Create AjaxForm errors Response
        $view = $this->renderView('Partial/Modal/ajax_form_errors.html.twig', [
            'form' => $form->createView(),
        ]);

        // On form error, the default is PRECONDITION_FAILED. A status code of 200 is not allowed
        if (\is_null($httpStatus) || $httpStatus === 200) {
            $httpStatus = HttpCode::PRECONDITION_FAILED;
        }

        return new Response($view, $httpStatus);
    }

    // Add form errors based on BusResponse events
    public function handleBusResponseEventsWithForm(FormInterface $form, BusResponse $busResponse, array $fieldParameters = []): void
    {
        foreach ($busResponse->getEvents() as $event) {

            $eventClass = \get_class($event);

            if (isset($fieldParameters[$eventClass])) {
                $form->get($fieldParameters[$eventClass])->addError(new FormError($event->getMessage()));
            } else {
                $form->addError(new FormError($event->getMessage()));
            }
        }
    }
}
