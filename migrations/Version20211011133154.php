<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211011133154 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('RENAME TABLE user TO admin');
        $this->addSql('DROP INDEX uniq_8d93d649f85e0677 ON admin');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_880E0D76F85E0677 ON admin (username)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('RENAME TABLE admin TO user');
        $this->addSql('DROP INDEX uniq_880e0d76f85e0677 ON admin');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON admin (username)');
    }
}
