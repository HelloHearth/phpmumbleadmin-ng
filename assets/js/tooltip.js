(function($) {

    "use strict";

    if (typeof $({}).tooltip === 'function') {

        $('[data-bs-toggle="tooltip"],[data-bs-tooltip="tooltip"]').tooltip({
            trigger : 'hover',
        });

        // Form error tooltip
        $('[data-bs-tooltip="form-error"]').tooltip({
            trigger: 'manual',
            customClass: 'tooltip-form-error tooltip-small',
        });
        $(document).ready(function () {
            $('[data-bs-tooltip="form-error"]').tooltip('show');
            $('.tooltip-form-error').on('click', function () {
                $(this).hide();
            })
        });
    }


})(jQuery);
