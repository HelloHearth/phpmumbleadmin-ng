
(function($) {

    "use strict";

    if (0 === $('#MumbleSettingsTabs').length) {
        return;
    }

    // store the currently selected tab in the hash value
    $('ul.nav-tabs > li > button').on('shown.bs.tab', function(e) {
        window.location.hash = $(e.target).data('bs-target').substr(1);
    });

    // on load of the page: switch to the currently selected tab
    let hash = window.location.hash;
    let currentTab = new bootstrap.Tab($('#MumbleSettingsTabs button[data-bs-target="'+ hash +'"]'));
    currentTab.show();

})(jQuery);
