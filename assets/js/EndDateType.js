(function($) {

    "use strict";

    let permanentElement = $('#end_date_permanent');

    function togglePickDate() {
        $('#end_date_pick').attr('disabled', permanentElement.prop('checked'));
    }

    // Toggle the disabled attribute of pick date element with the checked property of the permanent element
    permanentElement.on('change', function() {
        togglePickDate();
        $('#end_date_pick').val('');
    });

    // Disable the pick date element when reopen the modal of the permanent element
    permanentElement.parents('.modal').on('show.bs.modal', function() {
        togglePickDate();
    });

})(jQuery);
