
function innerLoadingAnimation(element)
{
    element.append('<div class="loading-animation"><div class="spinner-grow text-primary" role="status"><span class="sr-only">Loading...</span></div>');
}

function removeLoadingAnimation(element)
{
    element.children('.loading-animation').remove();
}
