(function($) {

    "use strict";

    const APP_NAME = 'phpMumbleAdmin-ng';

    /**
     * Start the sidebar closed for small devices
     */
    if ($(window).width() < 768) {
        $("body").addClass("sidebar-toggled");
        $(".sidebar").addClass("toggled");
    }

    /**
     * Jquery-confirm plugin default settings
     *
     * Doc: https://craftpip.github.io/jquery-confirm/
     */
    if(typeof jconfirm !== 'undefined') {
        jconfirm.defaults = {
            type: 'dark',
            title: APP_NAME,
            theme: 'modern',
            columnClass: 'medium',
        };
    }

    // Submit the form when an option of the server list has been selected
    $('form#select-server').on('change', function () {
        $(this).submit();
    });

})(jQuery);

function getBaseUrl()
{
    let url = window.location;
    return url.protocol + "//" + url.host + "/" + url.pathname.split('/')[1];
}

function getLanguage()
{
    return $('html').attr('lang');
}
