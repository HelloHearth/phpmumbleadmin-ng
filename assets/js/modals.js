(function($) {

    "use strict";

    // delete modal: confirm before do the delete action
    $('#deleteModal').on('show.bs.modal', function(e) {

        let text = $(this).find('.modal-body-hidden').text();

        let path = $(e.relatedTarget).data('action');
        let name = $(e.relatedTarget).data('name');

        $(this).find('.modal-body').text(text.replace('%name%', name));

        $(this).find('#confirm-delete-button').on('click', function() {
            document.location.href=path;
        });
    });

    // Autofocus fields on modal shown
    $('.modal').on('shown.bs.modal', function () {
        $(this).find('[autofocus="autofocus"]').focus();
    });

    // Ajax form modal
    const AJAX_INNER = '.ajax-inner';
    const INVALID_FEEDBACK = '.invalid-feedback';

    $('.modal').on('hidden.bs.modal', function () {
        if ($(this).find(AJAX_INNER).length > 0) {
            // Remove inserted classes if a 500 HTTP error occurred previously
            $(this).find('.modal-dialog').removeClass('modal-lg-http-500-error');
            $(this).find('.modal-footer button[type="submit"]').css('display', '');
            removeAjaxFormErrors($(this));
            // Reset the form
            $(this).find('form').trigger('reset');
            // Reset form values from Vue js
            if (typeof MumbleViewer !== 'undefined') {
                MumbleViewer.$forceUpdate();
            }
        }
    });

    // Submit forms with AJAX, and reload the HTML of the form on errors.
    $('.modal-form-jquery-ajax').on('submit', function (e) {

        e.preventDefault();
        let form = $(this);
        let modal = $(form.closest('.modal'));
        let modalInner = modal.find(AJAX_INNER);

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function () {
                innerLoadingAnimation(modalInner);
            },
            success: function(response) {
                if (modal.hasClass('reload-on-success')) {
                    return window.location.reload(true);
                }
                if (! jQuery.isEmptyObject(response)) {
                    addFlashMessage('success', response);
                }
                modal.modal('hide');
                form.trigger('ajax-form-action-succeeded');
            }
        }).fail(function(jqXHR) {
            if ('""' !== jqXHR.responseText) {
                if (jqXHR.status >= 500) {
                    modal.find('.modal-dialog').addClass('modal-lg-http-500-error');
                    modalInner.append('<div class="http-500-error-inner">'+jqXHR.responseText+'</div>');
                    // modal.find('.modal-footer button[type="submit"]').css('display', 'none');
                } else {
                    injectAjaxFormErrors(modalInner, jqXHR.responseText);
                }
            }
        }).always(function () {
            removeLoadingAnimation(modalInner);
        });
    });

    // Fetch and inject data into a modal when it's opens
    $('.modal[data-fetch-and-inject]').on('show.bs.modal', function () {

        let modalInner = $(this).find(AJAX_INNER);

        $.get({
            url: $(this).data('fetch-and-inject'),
            beforeSend: function () {
                innerLoadingAnimation(modalInner);
            },
            success: function(response) {
                modalInner.html(response);
            }
        }).always(function () {
            removeLoadingAnimation(modalInner);
        });
    });

    /**
     * Injecting form errors from Ajax request
     *
     * Root errors are injected on top of the innerElement, and field errors
     * are inserted after the field
     */
    function injectAjaxFormErrors(innerElement, errors)
    {
        // Remove previous errors
        removeAjaxFormErrors(innerElement);

        let errorItems = $(errors).filter('div[data-form-error]');

        errorItems.each(function () {

            let field = $(this).data('form-error');
            let formError = $(this).children().get(0);

            if (field === 'root') {
                innerElement.prepend(formError);
            } else if (innerElement.find('#'+field)[0]) {

                // Insert form error after the form (input, textarea, etc..) field id
                $(formError).insertAfter(innerElement.find('#'+field)[0]);
            } else {
                innerElement.prepend(formError);
            }
        });
    }

    function removeAjaxFormErrors(element)
    {
        element.find('.alert').remove();
        element.find(INVALID_FEEDBACK).remove();
    }

})(jQuery);
