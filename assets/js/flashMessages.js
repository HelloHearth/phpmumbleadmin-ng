(function($) {

    'use strict';

    let containerId = '#sticky-flash-messages';
    let appFlash = $(containerId+' .app-flash');

    // Close the flash message on click
    appFlash.on('click', function(){
        $(this).alert('close');
    });

    $(document).ready(function() {
        autoClose($('.alert-success'));
    });

})(jQuery);

// Close elements with fadeout animation
function autoClose(elements) {
    setTimeout(function() {
        elements.fadeTo(1000, 0).slideUp(1000, function() {
            $(this).remove();
        });
    }, 5000);
}

function addFlashMessage(label, text) {
    let alert = $(
        '<div class="alert alert-'+label+' alert-dismissible fade show app-flash app-flash-js" role="alert">'+
            text+
            '<button type="button" class="btn-close small" data-bs-dismiss="alert" aria-label="Close"></button>'+
        '</div>'
    ).appendTo('#sticky-flash-messages');

    // Enable timer to close new alerts
    autoClose(alert);
}
