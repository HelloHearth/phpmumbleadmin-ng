
(function() {

    "use strict";

    if (typeof Quill === 'undefined') {
        return;
    }

    let quill = new Quill('.simple-html-editor', {
        theme: 'snow'
    });

    let targetId = $('.simple-html-editor').data('target-id');

    // Insert the content on the hidden input
    quill.on('text-change', function() {
        document.getElementById(targetId).value = quill.root.innerHTML;
    });

})(jQuery);
