
/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

const Logs = {
    data: {
        logs: null,
    },
    created() {
        this.setUpdateLogsTimer();
    },
    methods: {
        getUrl() {
            return null;
        },
        getLogs() {
            return this.logs;
        },
        updateLogs() {
            if (! document.hasFocus()) {
                return;
            }
            axios
                .get(this.getUrl())
                .then((response) => {
                    this.mergeLatestLogs(response.data.logs);
                })
                .catch((error) => {
                    this.updateDatasError(error);
                });
        },
        mergeLatestLogs(logs) {
            if (logs.length === 0) {
                return;
            }
            this.removeDuplicateDays(logs);
            this.logs = [...logs, ...this.logs];
            this.updateInfoPanelTotal(this.logs.length);
        },
        removeDuplicateDays(logs) {

            let newDays = [];

            // Mark new logs for 10 seconds
            logs.forEach(function(log) {
                log.isNewLog = true;
                setTimeout(() => log.isNewLog = false, 10000)
            });

            // Find for new date on latest logs
            logs.forEach(function(log) {
                if (log.date !== '') {
                    newDays.push(log.date);
                }
            });

            // Remove duplicated date on old logs
            this.logs.forEach(function (log) {
                if (log.date !== '' && newDays.includes(log.date)) {
                    log.date = '';
                }
            });
        },
        updateInfoPanelTotal(total) {
            $('#info-panel .card').text(function(index, text) {
                return text.replace(/\d+/, total);
            });
        },
        setUpdateLogsTimer() {
            clearInterval(this.timer);
            this.timer = setInterval(this.updateLogs, UPDATE_LOGS_IN_MS);
        }
    },
    beforeDestroy() {
        clearInterval(this.timer);
    },
};
