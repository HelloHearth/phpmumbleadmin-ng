
/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const Fetcher = {
    data: {
        timer: '',
        hasUpdateDatasError: false,
        countFetchDataErrors: 0
    },
    methods: {
        updateDatasError() {

            ++this.countFetchDataErrors;

            // Stop the auto-refresh after 3 errors on update
            if (this.countFetchDataErrors > 2) {
                clearInterval(this.timer);
                this.hasUpdateDatasError = true;
            }
        },
        clearDatasError() {
            this.countFetchDataErrors = 0;
            this.hasUpdateDatasError = false;
        }
    }
};
