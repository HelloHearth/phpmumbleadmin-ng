
/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

const STORAGE_SELECTED_NODE = 'phpMumbleAdmin_viewer_selectedNodeId';

const MumbleViewer = new Vue({
    mixins: [Base, Fetcher],
    el: '#MumbleViewerContainer',
    data: {
        MumbleTree: MumbleTree,
        selectedNode: [],
        action: {
            enable: false,
            mode: null,
            nodeId: null,
        }
    },
    created() {
        // Select the stored channel at start, and by default, the Root channel
        let id = this.getStoredNodeId();
        if (! id) {
            id = this.MumbleTree.nodes[0].nodeid;
        }
        this.selectNodeId(id);
        this.setUpdateTreeTimer();
    },
    beforeDestroy() {
        clearInterval(this.timer);
    },
    methods: {
        // Store NodeId with the current server id
        storeNodeId(nodeId) {
            const server = 's-'+ this.getServerId() +'-';
            sessionStorage.setItem(STORAGE_SELECTED_NODE, server + nodeId);
        },
        // Retrieve stored NodeId
        getStoredNodeId() {
            const stored = sessionStorage.getItem(STORAGE_SELECTED_NODE);
            if (! stored) {
                return null;
            }

            const server = 's-'+ this.getServerId() +'-';

            // Remove stored nodeId if the serverId has changed
            if (! stored.match(server)) {
                sessionStorage.removeItem(STORAGE_SELECTED_NODE);
                return null;
            }

            // Return the NodeId without the serverId
            return stored.replace(server, '');
        },
        // Return the id of a nodeId
        getIdOfNode(nodeId) {
            for(let i = 0; i < this.MumbleTree.nodes.length; ++i) {
                if (this.MumbleTree.nodes[i].nodeid === nodeId) {
                    return this.MumbleTree.nodes[i].id;
                }
            }
        },
        getServerId() {
            return MumbleTree.serverId;
        },
        async updateTree() {
            if (! document.hasFocus()) {
                return;
            }
            // Do not update data if a .stop-update-mumble-tree is open, or the inputs
            // of the form will be refreshed each time (because of value binding)
            if ($('.stop-update-mumble-tree').is(':visible')) {
                return;
            }
            await this.fetchTree()
        },
        async fetchTree() {
            await axios
                .get(Routing.generate('server_channels_tree', { serverId: this.getServerId() }))
                .then((response) => {
                    this.MumbleTree = response.data;
                    this.selectNodeId(this.selectedNode.nodeid);
                    this.clearDatasError();
                })
                .catch((error) => {
                    this.updateDatasError(error);
                })
                .finally(() => {
                    if (! this.hasUpdateDatasError && ! this.actionModeOn()) {
                        // Update draggable and droppable elements
                        MumbleTreeDrag();
                    }
                })
            ;
        },
        setUpdateTreeTimer() {
            clearInterval(this.timer);
            this.timer = setInterval(this.updateTree, 5000);
        },
        // Update the Tree and reset the timer
        async updateTreeManualy() {
            await this.updateTree();
            this.setUpdateTreeTimer();
        },
        // Force to update the Tree and reset the timer
        async forceToUpdateTreeManualy() {
            await this.fetchTree();
            this.setUpdateTreeTimer();
        },
        selectedNodeAction(NodeId) {
            if (this.actionModeOn()) {
                this.executeActionForNode(NodeId);
            } else {
                this.selectNodeId(NodeId);
            }
        },
        selectNodeId(id) {
            for(let i = 0; i < this.MumbleTree.nodes.length; i++) {
                if (this.MumbleTree.nodes[i].nodeid === id) {
                    this.selectedNode = this.MumbleTree.nodes[i];
                    this.storeNodeId(id);
                    this.markLinkedChannels();
                    return;
                }
            }
            // If the id doesn't exist, fallback to the Root channel
            this.selectNodeId(this.MumbleTree.nodes[0].nodeid);
        },
        getSelectedNodeComment() {
            const node = this.selectedNode;
            return node.class === 'channel' ? node.description : node.comment;
        },
        markLinkedChannels() {

            let directLinks = [];
            let indirectLinks = [];

            // Only channels have links
            if ('channel' === this.selectedNode.class) {

                directLinks = this.selectedNode.links;
                indirectLinks = this.selectedNode.indirectLinks;

                // Include selected node as direct link
                if (0 !== directLinks.length) {
                    directLinks.push(this.selectedNode.id);
                }
            }

            for(let i = 0; i < this.MumbleTree.nodes.length; ++i) {
                this.MumbleTree.nodes[i].isDirectLink = (-1 !== directLinks.indexOf(this.MumbleTree.nodes[i].id));
                this.MumbleTree.nodes[i].isIndirectLink = (-1 !== indirectLinks.indexOf(this.MumbleTree.nodes[i].id));
            }
        },
        // Toggle the priority speaker status of an user
        prioritySpeaker(action) {
            if (-1 === ['activate', 'deactivate'].indexOf(action)) {
                return;
            }
            this.postForm(action+'_priority_speaker');
        },
        // Check if the action mode is enabled
        actionModeOn() {
            return (this.action.enable !== false);
        },
        activateAction(mode) {
            this.action.enable = true;
            this.action.mode = mode;
            // Disable the draggable element if it's enable
            if (typeof $(DRAGGABLE_ELEMENTS).draggable('instance') !== 'undefined') {
                disableMumbleTreeDrag();
            }
        },
        terminateAction() {
            this.action.enable = false;
            this.action.mode = null;
            MumbleTreeDrag();
        },
        // Process the action for a nodeId
        executeActionForNode(nodeId) {
            const id = this.getIdOfNode(nodeId);
            if (typeof id === 'undefined') {
                return;
            }
            this.action.nodeId = id;
            // Add a small timer to wait for vueJs to update the DOM
            setTimeout(()=> {
                this.postForm(this.action.mode).then(() => {
                    this.action.nodeId = null;
                    if (this.action.mode === 'unlink_channel' && ! this.selectedNodeHasLinks()) {
                        this.terminateAction();
                    }
                });
            }, 10);
        },
        selectedNodeHasLinks() {
            return (this.selectedNode.links.length > 0);
        },
        getUserCertificateUrl() {
            return Routing.generate('server_user_certificate', {
                serverId: this.getServerId(),
                sessionId: this.selectedNode.session,
            });
        },
        // Action to post a form.
        async postForm(form) {
            // If the form is a string, clone it from form name
            if (typeof form === 'string') {
                form = $('form[name="'+form+'"]').clone();
            }
            await axios
                .post(form.attr('action'), form.serialize())
                .catch((error) => {
                    const request = error.response.request;
                    if (request.responseText === '""') {
                        return;
                    }
                    $.alert({
                        title: 'Error',
                        content: request.responseText,
                        type: 'red',
                        typeAnimated: true
                    });
                })
                .finally(() => {
                    return this.updateTreeManualy();
                })
            ;
        },
    },
});

// Update the tree when submitting modal forms in the viewer page
$('.modal-form-jquery-ajax').on('ajax-form-action-succeeded', function () {
    MumbleViewer.forceToUpdateTreeManualy();
});

const DRAGGABLE_ELEMENTS = '.MumbleViewer .tree-draggable-item';
const DROPPABLE_ELEMENTS = '.MumbleViewer .channel .tree-draggable-item';

function MumbleTreeDrag() {

    "use strict";

    // Check for plugins jquery-ui & jconfirm
    if(typeof $.ui === 'undefined') {
        return console.error('plugin jquery-ui not found');
    }

    if(typeof jconfirm === 'undefined') {
        return console.error('plugin jquery-confirm not found');
    }

    // Enabling draggable elements
    // Doc: http://api.jqueryui.com/draggable/
    $(DRAGGABLE_ELEMENTS).draggable({
        revert: true,
        revertDuration: 1,
        // Distance is deprecated with jquery-ui 1.12, and should be replaced with v1.13
        distance: 10,
    }) ;

    // Enabling droppable elements
    // Doc: http://api.jqueryui.com/droppable/
    $(DROPPABLE_ELEMENTS).droppable({
        classes: {
            'ui-droppable-hover': 'drop-hover'
        },
        greedy: true,
        // On drop
        drop: function(e, ui) {

            let entityId = ui.helper.attr('id');
            let toChannelId = $(this).attr('id');

            const isChannel = (entityId.substr(0, 2) === 'c-');

            entityId = entityId.substr(2);
            toChannelId = toChannelId.substr(2);

            if (isChannel) {
                moveChannelAction(entityId, toChannelId);
            } else {
                moveUserAction(entityId, toChannelId);
            }
        }
    });

    function moveChannelAction(entityId, toChannelId) {
        $.confirm({
            content: 'Do you confirm to move channel '+entityId+' to '+toChannelId+' ?',
            closeIcon: true,
            closeIconClass: 'fas fa-times',
            buttons: {
                confirm: {
                    btnClass: 'jconfirm-submit-action btn-blue',
                    action: function () {
                        const form = $('form[name="move_channel"]').clone();
                        form.find('input[name="move_channel[channelId]"]').val(entityId);
                        form.find('input[name="move_channel[toChannelId]"]').val(toChannelId);
                        MumbleViewer.postForm(form);
                    }
                },
                cancel: function () {
                }
            }
        });
    }

    function moveUserAction(entityId, toChannelId) {
        $.post(
            Routing.generate('cmd_move_user', { serverId: MumbleViewer.getServerId() }),
            {
                form: {
                    userSession: entityId,
                    toChannelId: toChannelId
                }
            },
            // Success callback
            function() {
                MumbleViewer.updateTreeManualy();
            }
        );
    }
}

function disableMumbleTreeDrag() {
    "use strict";
    $(DRAGGABLE_ELEMENTS).draggable('destroy') ;
    $(DROPPABLE_ELEMENTS).droppable('destroy') ;
}

MumbleTreeDrag();
