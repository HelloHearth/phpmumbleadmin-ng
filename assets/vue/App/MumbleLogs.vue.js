
/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

const MumbleLogs = new Vue({
    mixins: [Base, Fetcher, Logs],
    el: '#MumbleLogsContainer',
    data: {
        logs: logsBag.logs,
    },
    methods: {
        getUrl() {
            return Routing.generate('server_logs_latest', { serverId: logsBag.serverId, 'lastLogLen': this.logs.length });
        },
    }
});
