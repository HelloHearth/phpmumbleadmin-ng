
/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

const AdminLogs = new Vue({
    mixins: [Base, Fetcher, Logs],
    el: '#AdminLogsContainer',
    data: {
        logs: logs.logs,
    },
    methods: {
        getUrl() {
            return Routing.generate('administration_logs_latest', { 'lastLogLen': this.logs.length });
        },
    }
});
